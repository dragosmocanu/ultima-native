//VOLUNTAS SUPREMUM EST

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

//COM
#include <Windows.h>

#ifdef UNICODE
typedef LPCWSTR LPCTSTR;
#else
typedef LPCSTR LPCTSTR;
#endif

//C++
#include <map>
#include <memory>
#include <mmsystem.h>
#include <stdlib.h>
#include <string>
#include <vector>

#if DIRECTX
//D3D
#include <D3D11.h>

//DXMath
#include <DirectXMath.h>
#include <DirectXCollision.h>
using namespace DirectX;

//DSound
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")
#include <dsound.h>

//D3DX
#include <D3DX11.h>

#endif