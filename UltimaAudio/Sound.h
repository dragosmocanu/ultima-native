#pragma once

#include "Macros.h"

#include "AudioManager.h"

namespace Ultima
{
	/*!
	Defines a sound.
	*/
	class Sound
	{
	public:

		/*!
		Creates a new instance of Sound.
		*/
		ULTIMAAUDIO_API Sound(const std::string& filePath)
			ULTIMA_NOEXCEPT;

		/*
		Copies an instance of Sound.
		*/
		ULTIMAAUDIO_API Sound(const Sound& other)
			ULTIMA_NOEXCEPT;

		ULTIMAAUDIO_API Sound& operator=(const Sound& other)
			ULTIMA_NOEXCEPT;

		ULTIMAAUDIO_API virtual ~Sound()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the path of the sound file.
		*/
		ULTIMAAUDIO_API virtual const std::string& GetFilePath()
			const ULTIMA_NOEXCEPT;

		/*!
		Plays the sound with a specified volume.
		Values for volume should be in the [0, 1] range.
		*/
		ULTIMAAUDIO_API virtual void Play(float volume = 1.0f)
			const ULTIMA_NOEXCEPT;

	protected:
		std::string filePath;
#if DIRECTX
		IDirectSoundBuffer8* soundBuffer;
#endif
	};
}