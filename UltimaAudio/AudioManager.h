#pragma once

#include "Macros.h"

#include "Camera3D.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	[THREADSAFE] Singleton manager class for all audio components.
	Must be initialized with Initialize() before using any audio components.
	*/
	class AudioManager
	{
	public:
		ULTIMAAUDIO_API virtual ~AudioManager()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the single instance of AudioManager.
		*/
		ULTIMAAUDIO_API static AudioManager& GetInstance()
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Initializes the AudioManager with the required data.
		*/
		ULTIMAAUDIO_API virtual void Initialize(const HWND& hwnd)
			ULTIMA_NOEXCEPT;

		/*!
		Gets a const pointer to an IDirectSound8 object.
		*/
		ULTIMAAUDIO_API virtual IDirectSound8* const GetDirectSound()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the position and orientation of the 3D sound listener object.
		*/
		ULTIMAAUDIO_API virtual void SetListener(
			const Vector3& position,
			const Vector3& forwardVector,
			const Vector3& upVector = Vector3::Up)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the position and orientation of the 3D sound listener object.
		*/
		ULTIMAAUDIO_API virtual void SetListener(const Camera3D& camera)
			ULTIMA_NOEXCEPT;

#endif

	protected:
		ULTIMAAUDIO_API AudioManager()
			ULTIMA_NOEXCEPT;

#if DIRECTX
		IDirectSound8* directSound;
		IDirectSoundBuffer* primaryBuffer;
		IDirectSound3DListener* soundListener;
#endif
	};
}