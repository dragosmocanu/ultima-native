#include "stdafx.h"

#include "AudioManager.h"

#include "Utilities.h"

namespace Ultima
{
	AudioManager::AudioManager() 
		ULTIMA_NOEXCEPT
	{ }

	AudioManager::~AudioManager()
		ULTIMA_NOEXCEPT
	{
		Utilities::Release(soundListener);
		Utilities::Release(primaryBuffer);
		Utilities::Release(directSound);
	}

	AudioManager& AudioManager::GetInstance()
		ULTIMA_NOEXCEPT
	{
		static AudioManager instance;

		return instance;
	}

	void AudioManager::Initialize(const HWND& hwnd)
		ULTIMA_NOEXCEPT
	{
		Utilities::Assert(DirectSoundCreate8(nullptr, &directSound, nullptr));

		Utilities::Assert(directSound->SetCooperativeLevel(hwnd, DSSCL_PRIORITY));

		DSBUFFERDESC bufferDesc;
		SecureZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.dwSize = sizeof(DSBUFFERDESC);
		bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME 
			| DSBCAPS_CTRL3D;
		bufferDesc.lpwfxFormat = nullptr;
		bufferDesc.guid3DAlgorithm = GUID_NULL;

		Utilities::Assert(directSound->CreateSoundBuffer(
			&bufferDesc, &primaryBuffer, nullptr));

		WAVEFORMATEX format;
		SecureZeroMemory(&format, sizeof(format));
		format.wFormatTag = WAVE_FORMAT_PCM;
		format.nSamplesPerSec = 44100;
		format.wBitsPerSample = 16;
		format.nChannels = 2;
		format.nBlockAlign = (format.wBitsPerSample / 8) * format.nChannels;
		format.nAvgBytesPerSec = format.nSamplesPerSec * format.nBlockAlign;

		Utilities::Assert(primaryBuffer->SetFormat(&format));

		Utilities::Assert(primaryBuffer->QueryInterface(IID_IDirectSound3DListener,
			(LPVOID*)&soundListener));

		soundListener->SetPosition(0, 0, 0, DS3D_IMMEDIATE);
	}

	IDirectSound8* const AudioManager::GetDirectSound()
		const ULTIMA_NOEXCEPT
	{
		return directSound;
	}

	void AudioManager::SetListener(
		const Vector3& position,
		const Vector3& forwardVector,
		const Vector3& upVector)
		ULTIMA_NOEXCEPT
	{
		Utilities::Assert(
			soundListener->SetPosition(
			position.GetX(),
			position.GetY(),
			position.GetZ(),
			DS3D_IMMEDIATE));

		Utilities::Assert(
			soundListener->SetOrientation(
			-forwardVector.GetX(), //for some reason, these are reversed
			forwardVector.GetY(),
			forwardVector.GetZ(),
			upVector.GetX(),
			upVector.GetY(),
			upVector.GetZ(),
			DS3D_IMMEDIATE));
	}

	void AudioManager::SetListener(
		const Camera3D& camera)
		ULTIMA_NOEXCEPT
	{
		SetListener(
			camera.GetPosition(),
			Vector3(
				camera.GetView().GetM20(),
				camera.GetView().GetM21(),
				camera.GetView().GetM22()),
			Vector3(
				camera.GetView().GetM10(),
				camera.GetView().GetM11(), 
				camera.GetView().GetM12()));
	}

}