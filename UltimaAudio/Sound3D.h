#pragma once

#include "Macros.h"

#include "Sound.h"

#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a 3D sound.
	*/
	class Sound3D
	{
	public:
		/*!
		Creates a new instance of Sound3D.
		*/
		ULTIMAAUDIO_API Sound3D(
			const std::string& filePath,
			const Vector3& position = Vector3::Zero)
			ULTIMA_NOEXCEPT;

		/*
		Copies an instance of Sound3D.
		*/
		ULTIMAAUDIO_API Sound3D(const Sound3D& other)
			ULTIMA_NOEXCEPT;

		ULTIMAAUDIO_API Sound3D& operator=(const Sound3D& other)
			ULTIMA_NOEXCEPT;

		ULTIMAAUDIO_API virtual ~Sound3D()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the path of the sound file.
		*/
		ULTIMAAUDIO_API virtual const std::string& GetFilePath()
			const ULTIMA_NOEXCEPT;

		/*!
		Plays the 3D sound with a specified volume.
		Values for volume should be in the [0, 1] range.
		*/
		ULTIMAAUDIO_API virtual void Play(float volume = 1.0f)
			const ULTIMA_NOEXCEPT;

	protected:
		std::string filePath;
		Vector3 position;
#if DIRECTX
		IDirectSoundBuffer8* soundBuffer;
		IDirectSound3DBuffer* soundBuffer3D;
#endif
	};
}