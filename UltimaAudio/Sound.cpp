#include "stdafx.h"

#include "Sound.h"

#include "AudioManager.h"
#include "ContentManager.h"
#include "MathExtensions.h"
#include "Utilities.h"

namespace Ultima
{

	Sound::Sound(const std::string& filePath)
		ULTIMA_NOEXCEPT
	{
		this->filePath = filePath;

		soundBuffer = ContentManager::GetInstance().LoadSoundWAV(
			AudioManager::GetInstance().GetDirectSound(), filePath);
	}

	Sound::Sound(const Sound& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Sound& Sound::operator=(const Sound& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			this->filePath = other.GetFilePath();

			ContentManager::GetInstance().LoadSoundWAV(
				AudioManager::GetInstance().GetDirectSound(), this->filePath);
		}

		return *this;
	}

	Sound::~Sound()
		ULTIMA_NOEXCEPT
	{
		ContentManager::GetInstance().Unregister(soundBuffer);
	}

	const std::string& Sound::GetFilePath()
		const ULTIMA_NOEXCEPT
	{
		return filePath;
	}

	void Sound::Play(float volume)
		const ULTIMA_NOEXCEPT
	{
		volume = MathExtensions::Clamp(volume, 0.0f, 1.0f);

		Utilities::Assert(soundBuffer->SetCurrentPosition(0));

		long longVolume = static_cast<long>(MathExtensions::MapInterval(volume,
			0.0f, 1.0f, 
			DSBVOLUME_MIN, DSBVOLUME_MAX));

		Utilities::Assert(soundBuffer->SetVolume(longVolume));

		Utilities::Assert(soundBuffer->Play(0, 0, 0));
	}

}