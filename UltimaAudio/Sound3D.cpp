#include "stdafx.h"

#include "Sound3D.h"

#include "ContentManager.h"
#include "MathExtensions.h"
#include "Utilities.h"

namespace Ultima
{
	Sound3D::Sound3D(const std::string& filePath,
		const Vector3& position)
		ULTIMA_NOEXCEPT
		: position(position)
	{
		ContentManager::GetInstance().LoadSound3DWAV(
			AudioManager::GetInstance().GetDirectSound(),
			filePath,
			soundBuffer,
			soundBuffer3D);
	}

	Sound3D::Sound3D(const Sound3D& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Sound3D& Sound3D::operator=(const Sound3D& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			this->filePath = other.GetFilePath();

			ContentManager::GetInstance().LoadSoundWAV(
				AudioManager::GetInstance().GetDirectSound(), this->filePath);
		}

		return *this;
	}

	Sound3D::~Sound3D()
		ULTIMA_NOEXCEPT
	{
		ContentManager::GetInstance().Unregister(soundBuffer3D);
	}

	const std::string& Sound3D::GetFilePath()
		const ULTIMA_NOEXCEPT
	{
		return filePath;
	}

	void Sound3D::Play(float volume)
		const ULTIMA_NOEXCEPT
	{
		volume = MathExtensions::Clamp(volume, 0.0f, 1.0f);

		Utilities::Assert(soundBuffer->SetCurrentPosition(0));

		long longVolume = static_cast<long>(MathExtensions::MapInterval(volume,
			0.0f, 1.0f,
			DSBVOLUME_MIN, DSBVOLUME_MAX));

		Utilities::Assert(soundBuffer->SetVolume(longVolume));

		Utilities::Assert(soundBuffer3D->SetPosition(
			position.GetX(),
			position.GetY(),
			position.GetZ(),
			DS3D_IMMEDIATE));

		Utilities::Assert(soundBuffer->Play(0, 0, 0));
	}

}