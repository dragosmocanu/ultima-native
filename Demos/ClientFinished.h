#pragma once

#include "IState.h"

using namespace Ultima;

class ClientFinished : public IState
{
public:
	ClientFinished();
	~ClientFinished();

	void Update(float deltaTime, StateManager& stateManager)
		ULTIMA_NOEXCEPT override;
};

