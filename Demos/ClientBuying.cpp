#include "ClientBuying.h"

#include "Client.h"

ClientBuying::ClientBuying()
{ 
	time = 0;
}

ClientBuying::~ClientBuying()
{ }

void ClientBuying::Update(float deltaTime, StateManager& manager)
ULTIMA_NOEXCEPT
{ 
	Client* client = dynamic_cast<Client*>(&manager);

	if (client == nullptr)
		return;

	client->SetTintColor(Color::WindowsBlue);

	Merchant* from = client->GetTarget();

	if (from == nullptr)
		return;

	auto visitedMerchants = client->GetVisitedMerchantList();

	if (time == 0)
	{
		uintptr_t ptr = reinterpret_cast<uintptr_t>(&from);
		bool isNew = true;

#if DEBUG || _DEBUG
		float myMoney = static_cast<float>(client->GetGlobalNumber("Money"));
		float thatMoney = static_cast<float>(from->GetGlobalNumber("Money"));
#endif

		for (unsigned int i = 0; i < visitedMerchants.size(); i++)
		{
			if (ptr == visitedMerchants[i])
			{
				isNew = false;
				break;
			}
		}

		if (!isNew)
		{
			return;
		}

		visitedMerchants.push_back(reinterpret_cast<uintptr_t>(from));
		client->SetVisitedMerchantList(visitedMerchants);

		int myType, thatType;
		thatType = -1;

		for (int i = static_cast<int>(client->GetGlobalNumber("ArraySize")) - 1; i >= 0; i--)
		{
			myType = static_cast<int>(client->GetGlobalNumber("ProduceTypes["
				+ std::to_string(i) + "]"));

			int j;
			for (j = static_cast<int>(from->GetGlobalNumber("ArraySize")) - 1; j >= 0; j--)
			{
				thatType = static_cast<int>(from->GetGlobalNumber("ProduceTypes["
					+ std::to_string(j) + "]"));

				if (myType == thatType)
					break;
			}

			if (myType != thatType)
				continue;

			double q1, q2;
			q1 = client->GetGlobalNumber("Quantities[" + std::to_string(i) + "]");
			q2 = from->GetGlobalNumber("Quantities[" + std::to_string(j) + "]");

			bool result = false;

			client->Parse("TryBuy(" + std::to_string(from->GetGlobalNumber(
				"PriceAverages[" + std::to_string(j) + "]")) + ", "
				+ std::to_string(i) + ")");

			result = client->GetGlobalBool("Result");

			if (result)
			{
				float cost = static_cast<float>(client->GetGlobalNumber("BargainPrice"));
				int quantity = static_cast<int>(client->GetGlobalNumber("Money") / cost);
				int otherQuantity = static_cast<int>(client->GetGlobalNumber("Quantities["
					+ std::to_string(j) + "]"));

				if (quantity > otherQuantity)
					quantity = otherQuantity;

				otherQuantity = static_cast<int>(
					client->GetGlobalNumber("Priorities[" + std::to_string(j) + "]")
					* client->GetGlobalNumber("MoneyOriginal")
					/ cost);

				if (quantity > otherQuantity)
					quantity = otherQuantity;

				cost *= quantity;

				client->Parse("ExchangeMoney("
					+ std::to_string(cost) + ", "
					+ std::to_string(quantity) + ", "
					+ std::to_string(i) + ")");
				from->Parse("ExchangeMoney("
					+ std::to_string(cost) + ", "
					+ std::to_string(quantity) + ", "
					+ std::to_string(j) + ")");
			}
			else
			{
				from->Parse("ReceiveOffer("
					+ std::to_string(client->GetGlobalNumber("BargainPrice")) + ", "
					+ std::to_string(j) + ")");

				result = from->GetGlobalBool("Result");

				if (result)
				{
					float cost = static_cast<float>(from->GetGlobalNumber("BargainPrice"));
					int quantity = static_cast<int>(client->GetGlobalNumber("Money") / cost);
					int otherQuantity = static_cast<int>(from->GetGlobalNumber(
						"Quantities[" + std::to_string(j) + "]"));

					if (quantity > otherQuantity)
						quantity = otherQuantity;

					otherQuantity = static_cast<int>(
						client->GetGlobalNumber("Priorities[" + std::to_string(j) + "]")
						* client->GetGlobalNumber("MoneyOriginal")
						/ cost);

					if (quantity > otherQuantity)
						quantity = otherQuantity;

					cost *= quantity;

					client->Parse("ExchangeMoney("
						+ std::to_string(cost) + ", "
						+ std::to_string(quantity) + ", "
						+ std::to_string(i) + ")");
					from->Parse("ExchangeMoney("
						+ std::to_string(cost) + ", "
						+ std::to_string(quantity) + ", "
						+ std::to_string(j) + ")");
				}
			}
		}
	}
	else if (time > client->GetBargainTime())
	{
		time = 0.0f;

		bool finished = false;

		if (visitedMerchants.size() == client->GetMerchantList()->size())
			finished = true;

		if (!finished)
		{
			double min = DBL_MAX;

			double min2;

			for (int i = static_cast<int>(client->GetGlobalNumber("ArraySize")) - 1; i >= 0; i--)
			{
				min2 = client->GetGlobalNumber("PriceAverages[" + std::to_string(i) + "]");

				if (min > min2)
					min = min2;
			}

			if (client->GetGlobalNumber("Money") < min)
				finished = true;

		}

		if (finished)
			client->ChangeStateTo(static_cast<unsigned int>(ClientState::Finished));
		else
			client->ChangeStateTo(static_cast<unsigned int>(ClientState::SearchingTarget));

		return;
	}

	time += deltaTime;
}