#pragma once

#include "IState.h"

using namespace Ultima;

class ClientBuying : public IState
{
public:
	ClientBuying();
	~ClientBuying();

	void Update(float deltaTime, StateManager& stateManager)
		ULTIMA_NOEXCEPT override;

protected:
	float time;
};