#include "stdafx.h"

#include "Merchant.h"

#include "RandomNumberGenerator.h"

Merchant::Merchant(
	const std::string& scriptFileName,
	const Device& device,
	const std::string& meshPath,
	const std::string& texturePath,
	const Vector3& position,
	const Matrix& rotation,
	float scale,
	unsigned int anisotropicSampleCount)
	: LuaAgent(scriptFileName, device, meshPath, texturePath, position, rotation,
	scale, anisotropicSampleCount),
	tintColor(Color::Transparent)
{ }

Merchant::~Merchant()
{
}

void Merchant::Update(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);
	//TODO!
}

bool Merchant::HasProduce() const
{
	bool ret = false;
	double d;

	for (int i = static_cast<int>(GetGlobalNumber("ArraySize") - 1); i >= 0; i--)
	{
		d = GetGlobalNumber("Quantities[" + std::to_string(i) + "]");

		if (d > 0)
		{
			ret = true;
			break;
		}
	}

	return ret;
}

const Color& Merchant::GetTintColor() const
{
	return tintColor;
}

void Merchant::SetTintColor(const Color& tintColor)
{
	this->tintColor = tintColor;
}