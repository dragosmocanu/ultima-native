#include "stdafx.h"

#include "SsaoDemo.h"

#include "AudioManager.h"
#include "MathExtensions.h"
#include "Utilities.h"

enum class KeyControls
{
	Forward,
	Backward,
	Left,
	Right,
	ToggleSSAO,
	ToggleFXAA,
	RotateX,
	RotateY,
	RotateZ,
	PlaySound2D,
	PlaySound3D,
	BrightnessAdd,
	BrightnessRemove,
	ContrastAdd,
	ContrastRemove,
	ToggleGrayscale,
	ToggleNegative
};


SsaoDemo::SsaoDemo(
	const HINSTANCE& hInstance,
	const HINSTANCE& prevInstance,
	const LPWSTR& cmdLine,
	int cmdShow,
	const std::string& title,
	unsigned int width,
	unsigned int height,
	bool isInVsync,
	bool isShowingFPS,
	unsigned int antiAliasingSampleCount)
	: Application(hInstance, prevInstance, cmdLine, cmdShow, 
	title, width, height, isInVsync, isShowingFPS, 0)
{
	UNREFERENCED_PARAMETER(antiAliasingSampleCount);

	ContentManager::GetInstance().SetSmartContentManagementType(SmartContentManagementType::Manual);

	AudioManager::GetInstance().Initialize(this->hwnd);

	this->isShowingFPS = true;

	buildTextures();

	camera = FreeCamera3D(aspectRatio);

	light.Diffuse = light.Specular = 0.0f;
	light.Color = Color::SpectralRed.ToXMFLOAT4();

	aaFilter.Type = AntiAliasingType::FXAA;

	aoFilter.Type = AmbientOcclusionType::HBAO;
	aoFilter.Intensity = 0.2f;
	aoFilter.HbaoR = 4.0f;
	aoFilter.HbaoNumberOfDirections = 1;
	aoFilter.HbaoNumberOfSteps = 5;
	aoFilter.HbaoAngleBias = 0;
	aoFilter.HbaoMaxRadiusPixels = 0.1f * min(width, height);

	this->title = "SSAO Demo | AO Status: ";

	if (aoFilter.Type == AmbientOcclusionType::None)
		this->title += "OFF";
	else
		this->title += "ON";

	this->title += " | AA Status: ";

	if (aaFilter.Type == AntiAliasingType::None)
		this->title += "OFF";
	else
		this->title += "ON";

	models.push_back(new Model(
		device, "Assets/torus.FBX", "Assets/desert.dds", "", "", Vector3(0, -35, 125)));

	auto mat = models[0]->GetMaterial();
	mat.Base = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
	models[0]->SetMaterial(mat);

	sound = new Sound("sound.wav");
	sound3D = new Sound3D("sound3D.wav", models[0]->GetPosition());

	graphicsFilter.IsActive = true;

	isShowingOnlyAO = false;

	InputManager::GetInstance().AddKeyBinding(Key::W, (int)KeyControls::Forward);
	InputManager::GetInstance().AddKeyBinding(Key::S, (int)KeyControls::Backward);
	InputManager::GetInstance().AddKeyBinding(Key::A, (int)KeyControls::Left);
	InputManager::GetInstance().AddKeyBinding(Key::D, (int)KeyControls::Right);
	InputManager::GetInstance().AddKeyBinding(Key::E, (int)KeyControls::ToggleSSAO);
	InputManager::GetInstance().AddKeyBinding(Key::F, (int)KeyControls::ToggleFXAA);
	InputManager::GetInstance().AddKeyBinding(Key::R, (int)KeyControls::RotateX);
	InputManager::GetInstance().AddKeyBinding(Key::T, (int)KeyControls::RotateY);
	InputManager::GetInstance().AddKeyBinding(Key::Y, (int)KeyControls::RotateZ);
	InputManager::GetInstance().AddKeyBinding(Key::G, (int)KeyControls::PlaySound2D);
	InputManager::GetInstance().AddKeyBinding(Key::H, (int)KeyControls::PlaySound3D);

	InputManager::GetInstance().AddKeyBinding(Key::Equals, (int)KeyControls::BrightnessAdd);
	InputManager::GetInstance().AddKeyBinding(Key::Minus, (int)KeyControls::BrightnessRemove);
	InputManager::GetInstance().AddKeyBinding(Key::RightBracket, (int)KeyControls::ContrastAdd);
	InputManager::GetInstance().AddKeyBinding(Key::LeftBracket, (int)KeyControls::ContrastRemove);
	InputManager::GetInstance().AddKeyBinding(Key::O, (int)KeyControls::ToggleGrayscale);
	InputManager::GetInstance().AddKeyBinding(Key::P, (int)KeyControls::ToggleNegative);
}

void SsaoDemo::OnResize()
ULTIMA_NOEXCEPT
{
	Application::buildTextures();
	
	camera.Reinitialize(aspectRatio);
	
	buildTextures();
}

void SsaoDemo::buildTextures()
ULTIMA_NOEXCEPT
{
	Utilities::Release(renderMapsSRV[1]);
	Utilities::Release(renderMapsSRV[0]);
	Utilities::Release(renderMapsRTV[1]);
	Utilities::Release(renderMapsRTV[0]);
	Utilities::Release(blurSRV);
	Utilities::Release(blurSRV2);
	Utilities::Release(aoSRV);
	Utilities::Release(aaSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(blurRTV2);
	Utilities::Release(aoRTV);
	Utilities::Release(aaRTV);

	Utilities::CreateColorTexture(device.GetDev(), width, height, &aaRTV, &aaSRV);
	Utilities::CreateColorTexture(device.GetDev(), width / 2, height / 2, &blurRTV, &blurSRV);
	Utilities::CreateColorTexture(device.GetDev(), width / 2, height / 2, &blurRTV2, &blurSRV2);

	Utilities::CreateAmbientOcclusionTexture(device.GetDev(), width, height, &aoRTV, &aoSRV);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &renderMapsRTV[0], &renderMapsSRV[0]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &renderMapsRTV[1], &renderMapsSRV[1]);
}

SsaoDemo::~SsaoDemo()
{
	Utilities::Release(renderMapsSRV[1]);
	Utilities::Release(renderMapsSRV[0]);
	Utilities::Release(renderMapsRTV[1]);
	Utilities::Release(renderMapsRTV[0]);
	Utilities::Release(blurSRV);
	Utilities::Release(blurSRV2);
	Utilities::Release(aoSRV);
	Utilities::Release(aaSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(blurRTV2);
	Utilities::Release(aoRTV);
	Utilities::Release(aaRTV);

	Model* temp;
	for (unsigned int i = 0; i < models.size(); i++)
	{
		temp = models[i];
		delete temp;
	}

	delete sound3D;
	delete sound;
}

void SsaoDemo::Update(float deltaTime)
ULTIMA_NOEXCEPT
{
	if (InputManager::GetInstance().IsPressing((int)KeyControls::Forward))
		camera.MoveZ(0.09f * deltaTime);
	else if (InputManager::GetInstance().IsPressing((int)KeyControls::Backward))
		camera.MoveZ(-0.09f * deltaTime);

	if (InputManager::GetInstance().IsPressing((int)KeyControls::Right))
		camera.MoveX(0.09f * deltaTime);
	else if (InputManager::GetInstance().IsPressing((int)KeyControls::Left))
		camera.MoveX(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetMouse()->IsPressing(1))
	{
		camera.RotateY(
			-InputManager::GetInstance().GetMouse()->GetDeltaX() / 170.0f);
		camera.RotateX(
			-InputManager::GetInstance().GetMouse()->GetDeltaY() / 170.0f);
	}

	camera.Update(deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Space))
		isShowingOnlyAO = !isShowingOnlyAO;

	if (InputManager::GetInstance().HasPressed((int)KeyControls::ToggleSSAO))
	{
		aoFilter.Type = static_cast<AmbientOcclusionType>(
			(static_cast<int>(aoFilter.Type) + 1) % 3);

		if (aoFilter.Type == AmbientOcclusionType::SSAO)
			aoFilter.Intensity = 1.0f;
		else if (aoFilter.Type == AmbientOcclusionType::HBAO)
			aoFilter.Intensity = 0.3f;
	}

	if (InputManager::GetInstance().HasPressed((int)KeyControls::ToggleFXAA))
	{
		aaFilter.Type = static_cast<AntiAliasingType>(
			(static_cast<int>(aaFilter.Type) + 1) % 2);
	}

	if (InputManager::GetInstance().IsPressing((int)KeyControls::RotateX))
		models[0]->RotateX(deltaTime / 347.0f);
	if (InputManager::GetInstance().IsPressing((int)KeyControls::RotateY))
		models[0]->RotateY(deltaTime / 347.0f);
	if (InputManager::GetInstance().IsPressing((int)KeyControls::RotateZ))
		models[0]->RotateZ(deltaTime / 347.0f);

	if (InputManager::GetInstance().HasPressed((int)KeyControls::PlaySound2D))
	{
		float volume = (models[0]->GetPosition() - camera.GetPosition()).GetLength();
		volume = static_cast<float>(MathExtensions::MapInterval(
			volume * -1, -700, 0, 0, 1));
		sound->Play(volume);
		//sound->Play(0.9f);
	}

	if (InputManager::GetInstance().HasPressed((int)KeyControls::PlaySound3D))
	{
		AudioManager::GetInstance().SetListener(camera);
		sound3D->Play();
	}


	if (InputManager::GetInstance().IsPressing((int)KeyControls::BrightnessAdd))
		graphicsFilter.Brightness += 0.003f * deltaTime;
	else if (InputManager::GetInstance().IsPressing((int)KeyControls::BrightnessRemove))
		graphicsFilter.Brightness -= 0.003f * deltaTime;

	if (InputManager::GetInstance().IsPressing((int)KeyControls::ContrastAdd))
		graphicsFilter.Contrast += 0.003f * deltaTime;
	else if (InputManager::GetInstance().IsPressing((int)KeyControls::ContrastRemove))
		graphicsFilter.Contrast -= 0.003f * deltaTime;

	graphicsFilter.Brightness = MathExtensions::Clamp(
		graphicsFilter.Brightness, 0.0f, 1.0f);
	graphicsFilter.Contrast = MathExtensions::Clamp(
		graphicsFilter.Contrast, 0.0f, 1.0f);

	if (InputManager::GetInstance().HasPressed((int)KeyControls::ToggleGrayscale))
		graphicsFilter.IsGrayscale = !graphicsFilter.IsGrayscale;

	if (InputManager::GetInstance().HasPressed((int)KeyControls::ToggleNegative))
		graphicsFilter.IsNegative = !graphicsFilter.IsNegative;


	title = "SSAO Demo | AO Status: ";

	if (aoFilter.Type == AmbientOcclusionType::None)
		title += "OFF";
	else if (aoFilter.Type == AmbientOcclusionType::SSAO)
		title += "SSAO";
	else
		title += "HBAO";

	title += " | AA Status: ";

	if (aaFilter.Type == AntiAliasingType::None)
		title += "OFF";
	else if (aaFilter.Type == AntiAliasingType::FXAA)
		title += "FXAA";
	else
		title += "SMAA";
}

void SsaoDemo::Render(float deltaTime)
ULTIMA_NOEXCEPT
{
	UNREFERENCED_PARAMETER(deltaTime);

	Clear(Color::White);

	float f[4];
	f[0] = f[1] = f[2] = f[3] = 0.0f;

	context->ClearRenderTargetView(aoRTV, f);
	context->ClearRenderTargetView(blurRTV, f);
	context->ClearRenderTargetView(blurRTV2, f);
	context->ClearRenderTargetView(renderMapsRTV[0], f);
	context->ClearRenderTargetView(renderMapsRTV[1], f);

	f[0] = f[1] = f[2] = f[3] = 1.0f;
	context->ClearRenderTargetView(aaRTV, f);

	//Rendering positions and normals
	context->OMSetRenderTargets(2, &renderMapsRTV[0], backBuffer.GetDsv());
	SetDepth(true);

	for (auto& x : models)
		x->RenderPositionsAndNormals(context, camera);

	context->ClearDepthStencilView(backBuffer.GetDsv(),
		D3D11_CLEAR_DEPTH, 1.0f, 0);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Q))
	{
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->Copy(context, renderMapsSRV[0], true);
		return;
	}
	//Rendering positions and normals

	//Computing AO map
	SetDepth(false);
	context->OMSetRenderTargets(1, &aoRTV, nullptr);
	quad->ComputeAmbientOcclusion(context, renderMapsSRV[0], renderMapsSRV[1], aoFilter);
	//Computing AO map

	//blur
	context->RSSetViewports(1, &viewportHalved);
	context->OMSetRenderTargets(1, &blurRTV, nullptr);
	quad->Blur(context, aoSRV, GaussianBlurFilter(true));

	context->OMSetRenderTargets(1, &blurRTV2, nullptr);
	quad->Blur(context, blurSRV, GaussianBlurFilter(false));

	context->RSSetViewports(1, &viewport);

	if (isShowingOnlyAO)
	{
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->Copy(context, blurSRV2, true);
		return;
	}
	else
	{
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->Copy(context, blurSRV2, false);
	}
	//blur

	//Applying AO via models' PS, not the quad's!
	context->OMSetRenderTargets(1, &aaRTV, backBuffer.GetDsv());
	SetDepth(true);
	for (auto& x : models)
		x->Render(context, camera, light, Fog(), graphicsFilter, aoSRV);
	//Applying AO

	//AA
	context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
	SetDepth(false);
	quad->Fxaa(context, aaSRV, aaFilter);
	//AA
}