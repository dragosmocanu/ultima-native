#include "ClientSearchingTarget.h"

#include <float.h>

#include "Client.h"

ClientSearchingTarget::ClientSearchingTarget()
{ }

ClientSearchingTarget::~ClientSearchingTarget()
{ }

void ClientSearchingTarget::Update(float deltaTime, StateManager& manager)
	ULTIMA_NOEXCEPT
{
	UNREFERENCED_PARAMETER(deltaTime);

	Client* client = dynamic_cast<Client*>(&manager);

	if (client == nullptr)
		return;

	client->SetTintColor(Color::SpectralRed);

	float distance = FLT_MAX;
	float distance2;
	int targetMerchant = -1;

	std::vector<Merchant*>* merchants = client->GetMerchantList();

	if (merchants == nullptr)
		return;

	for (unsigned int i = 0; i < merchants->size(); i++)
	{
		if (!client->HasVisited(*merchants->at(i)))
		{
			distance2 = client->GetPosition().DistanceTo(merchants->at(i)->GetPosition());

			if (distance > distance2)
			{
				distance = distance2;
				targetMerchant = i;
			}
		}
	}

	if (targetMerchant > -1)
	{
		client->ChangeStateTo(static_cast<unsigned int>(ClientState::Moving));
		client->SetTarget(merchants->at(targetMerchant));
	}
}