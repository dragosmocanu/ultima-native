#pragma once

#include "IState.h"

using namespace Ultima;

class ClientMoving : public IState
{
public:
	ClientMoving();
	~ClientMoving();

	void Update(float deltaTime, StateManager& stateManager)
		ULTIMA_NOEXCEPT override;
};

