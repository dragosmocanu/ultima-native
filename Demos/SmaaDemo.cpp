#include "stdafx.h"

#include "SmaaDemo.h"

#include "Color.h"
#include "ContentManager.h"
#include "MathExtensions.h"
#include "RandomColorGenerator.h"
#include "Utilities.h"

SmaaDemo::SmaaDemo(
	const HINSTANCE& hInstance,
	const HINSTANCE& prevInstance,
	const LPWSTR& cmdLine,
	int cmdShow,
	const std::string& title,
	unsigned int width,
	unsigned int height,
	bool isInVsync,
	bool isShowingFPS,
	unsigned int antiAliasingSampleCount)
	: Application(hInstance, prevInstance, cmdLine, cmdShow,
	title, width, height, isInVsync, isShowingFPS, 0)
{
	UNREFERENCED_PARAMETER(antiAliasingSampleCount);

	ContentManager::GetInstance().SetSmartContentManagementType(SmartContentManagementType::Manual);

	D3D11_TEXTURE2D_DESC texDesc;
	SecureZeroMemory(&texDesc, sizeof(texDesc));
	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.Width = width;
	texDesc.Height = height;

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
	rtvDesc.Texture2D.MipSlice = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	SecureZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

	srvDesc.Format = rtvDesc.Format = texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texDesc.Format = DXGI_FORMAT_R8G8B8A8_TYPELESS;
	rtvDesc.Texture2D.MipSlice = 0;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

	ID3D11Texture2D* smaaTexture1 = nullptr;
	ID3D11Texture2D* smaaTexture2 = nullptr;
	ID3D11Texture2D* imageTexture = nullptr;

	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &smaaTexture1));
	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &smaaTexture2));
	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &imageTexture));

	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(smaaTexture1, &rtvDesc, &smaaRTV1));
	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(smaaTexture2, &rtvDesc, &smaaRTV2));
	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(imageTexture, &rtvDesc, &imageRTV));

	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(smaaTexture1, &srvDesc, &smaaSRV1));
	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(smaaTexture2, &srvDesc, &smaaSRV2));
	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(imageTexture, &srvDesc, &imageSRV));

	Utilities::Release(smaaTexture1);
	Utilities::Release(smaaTexture2);
	Utilities::Release(imageTexture);
	
	float range = 2;

	for (float z = -range; z <= range; z++)
		for (float y = -range; y <= range; y++)
			for (float x = -range; x <= range; x++)
				models.push_back(new Model(device, "Assets/cube.FBX", "Assets/dmlogo.png", "", "", 
					Vector3(x * 100, y * 100, z * 100)));

	aaFilter.Type = AntiAliasingType::None;
}

void SmaaDemo::OnResize()
ULTIMA_NOEXCEPT
{
	Application::OnResize();

	D3D11_TEXTURE2D_DESC texDesc;
	SecureZeroMemory(&texDesc, sizeof(texDesc));
	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texDesc.Width = width;
	texDesc.Height = height;

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
	rtvDesc.Texture2D.MipSlice = 0;

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	SecureZeroMemory(&srvDesc, sizeof(srvDesc));
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
	srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

	srvDesc.Format = rtvDesc.Format = texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	texDesc.Format = DXGI_FORMAT_R8G8B8A8_TYPELESS;
	rtvDesc.Texture2D.MipSlice = 0;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

	ID3D11Texture2D* smaaTexture1 = nullptr;
	ID3D11Texture2D* smaaTexture2 = nullptr;
	ID3D11Texture2D* imageTexture = nullptr;

	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &smaaTexture1));
	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &smaaTexture2));
	Utilities::Assert(device.GetDev()->CreateTexture2D(&texDesc, nullptr, &imageTexture));

	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(smaaTexture1, &rtvDesc, &smaaRTV1));
	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(smaaTexture2, &rtvDesc, &smaaRTV2));
	Utilities::Assert(
		device.GetDev()->CreateRenderTargetView(imageTexture, &rtvDesc, &imageRTV));

	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(smaaTexture1, &srvDesc, &smaaSRV1));
	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(smaaTexture2, &srvDesc, &smaaSRV2));
	Utilities::Assert(
		device.GetDev()->CreateShaderResourceView(imageTexture, &srvDesc, &imageSRV));

	Utilities::Release(smaaTexture1);
	Utilities::Release(smaaTexture2);
	Utilities::Release(imageTexture);
}

SmaaDemo::~SmaaDemo()
{
	Utilities::Release(imageSRV);
	Utilities::Release(imageRTV);
	Utilities::Release(smaaSRV2);
	Utilities::Release(smaaRTV2);
	Utilities::Release(smaaSRV1);
	Utilities::Release(smaaRTV1);

	Model* temp = nullptr;

	for (unsigned int i = 0; i < models.size(); i++)
	{
		temp = models[i];
		delete temp;
	}
}

void SmaaDemo::Update(float deltaTime)
ULTIMA_NOEXCEPT
{
	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::W))
		freeCamera.MoveZ(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::S))
		freeCamera.MoveZ(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D))
		freeCamera.MoveX(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::A))
		freeCamera.MoveX(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetMouse()->IsPressing(1))
	{
		freeCamera.RotateY(
			-InputManager::GetInstance().GetMouse()->GetDeltaX() / 170.0f);
		freeCamera.RotateX(
			-InputManager::GetInstance().GetMouse()->GetDeltaY() / 170.0f);
	}

	freeCamera.Update(deltaTime);


	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::T))
	{
		aaFilter.Type = static_cast<AntiAliasingType>(
			(static_cast<unsigned int>(aaFilter.Type) + 1) % 3);
	}

	isShowingFPS = true;

	title = "SMAA Demo";

	title += " | AA: ";

	if (aaFilter.Type == AntiAliasingType::None)
		title += "OFF";
	else if (aaFilter.Type == AntiAliasingType::FXAA)
		title += "FXAA";
	else
		title += "SMAA";
}

void SmaaDemo::Render(float deltaTime)
ULTIMA_NOEXCEPT
{
	UNREFERENCED_PARAMETER(deltaTime);

	Clear(Color::White);

	float f[4];
	f[0] = f[1] = f[2] = f[3] = 1.0f;

	context->ClearRenderTargetView(imageRTV, f);
	context->ClearRenderTargetView(smaaRTV1, f);
	context->ClearRenderTargetView(smaaRTV2, f);
	
	SetDepth(true);

	context->OMSetRenderTargets(1, &imageRTV, backBuffer.GetDsv());

	for (auto& x : models)
		x->Render(context, freeCamera);

	SetDepth(false);

	Vector2 screenSize(
		static_cast<float>(width),
		static_cast<float>(height));

	quad->AntiAlias(context, imageSRV, backBuffer.GetRtv(), aaFilter, smaaSRV1, smaaRTV1,
		smaaSRV2, smaaRTV2, screenSize);

	SetDepth(true);
}