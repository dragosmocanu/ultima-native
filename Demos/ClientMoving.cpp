#include "ClientMoving.h"

#include "Client.h"

ClientMoving::ClientMoving()
{ }

ClientMoving::~ClientMoving()
{ }

void ClientMoving::Update(float deltaTime, StateManager& manager)
	ULTIMA_NOEXCEPT
{
	Client* client = dynamic_cast<Client*>(&manager);

	if (client == nullptr)
		return;

	Merchant* target = client->GetTarget();

	if (target == nullptr) 
		client->ChangeStateTo(static_cast<unsigned int>(ClientState::SearchingTarget));

	Vector3 v = target->GetPosition() - client->GetPosition();

	if (v.GetLength() > 40.0f)
	{
		v.Normalize();
		float speed = 0.11f;
		v *= speed * deltaTime;
		client->Move(v);
	}
	else
	{
		client->ChangeStateTo(static_cast<unsigned int>(ClientState::Buying));
	}
}