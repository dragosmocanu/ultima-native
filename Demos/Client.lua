Money = 120;
ProduceTypes = {};
Quantities = {};
PriceAverages = {};
PriceMaxes = {};
ArraySize = -1;

Priorities = {};
MoneyOriginal = -1;

Result = false;
BargainPrice = -1;

IsChaotic = false;

ExchangeMoney = function(arg1, arg2, index)
	local i = tonumber(index)
	Money = Money - arg1
	Quantities[i] = Quantities[i] + arg2
end

TryBuy = function(otherPrice, index)
	local i = tonumber(index)
	local t1 = tonumber(PriceAverages[i]) - otherPrice
	local t2 = Money - otherPrice

	if (t1 >= 0) and (t2 >= 0) then
		BargainPrice = otherPrice
		Result = true
	else
		rand = tonumber(math.random())
		BargainPrice = tonumber(PriceAverages[i]) * rand + tonumber(PriceMaxes[i]) * (1.0 - rand)
		Result = false
	end
end