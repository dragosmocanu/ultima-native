#include "Application.h"

#include "LuaDemo.h"
#include "SmaaDemo.h"
#include "SsaoDemo.h"

#include "HdrDemo.h"
#include "HouseDemo.h"

#if DIRECTX
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmdLine, int cmdShow)
{
	const bool enableVsync = false;
	const bool showFPS = true;
	const unsigned int antiAliasingCount = 4;

	UNREFERENCED_PARAMETER(antiAliasingCount);

	//LuaDemo demo(hInstance, prevInstance, cmdLine, cmdShow, "", 1280, 720, enableVsync, showFPS, antiAliasingCount);
	//SmaaDemo demo(hInstance, prevInstance, cmdLine, cmdShow, "", 1280, 720, enableVsync, showFPS, 0);
	//SsaoDemo demo(hInstance, prevInstance, cmdLine, cmdShow, "", 1280, 720, enableVsync, showFPS, 0);

	//In construction
	HouseDemo demo(hInstance, prevInstance, cmdLine, cmdShow, "", 1280, 720, enableVsync, showFPS, 0);
	//HdrDemo demo(hInstance, prevInstance, cmdLine, cmdShow, "", 1280, 720, enableVsync, showFPS, 0);
	//In construction

	return demo.Run();
}
#endif