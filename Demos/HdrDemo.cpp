#include "stdafx.h"

#include "HdrDemo.h"

#include "Color.h"
#include "ContentManager.h"
#include "MathExtensions.h"
#include "RandomColorGenerator.h"
#include "Utilities.h"

HdrDemo::HdrDemo(
	const HINSTANCE& hInstance,
	const HINSTANCE& prevInstance,
	const LPWSTR& cmdLine,
	int cmdShow,
	const std::string& title,
	unsigned int width,
	unsigned int height,
	bool isInVsync,
	bool isShowingFPS,
	unsigned int antiAliasingSampleCount)
	: Application(hInstance, prevInstance, cmdLine, cmdShow,
	title, width, height, isInVsync, isShowingFPS, 0)
{
	UNREFERENCED_PARAMETER(antiAliasingSampleCount);

	ContentManager::GetInstance().SetSmartContentManagementType(SmartContentManagementType::Manual);

	buildTextures();

	toneMappingFilter.Type = ToneMappingType::None;

	bloomFilter.IsActive = false;
	bloomFilter.Threshold = 1.0f;

	skybox = new Skybox(device, "Assets/Ennis.dds", 7, 7);

	modelReflective = new ModelReflective(device, "Assets/sphere.FBX", "Assets/desert.dds", 
		//"", "Assets/displacementMap.png",
		"", "",
		Vector3::Zero);

	auto material = modelReflective->GetMaterial();
	material.Metallic = 0.0f;
	material.Roughness = 1.0f;
	modelReflective->SetMaterial(material);

	light.Direction = Vector3::Normalize(Vector3(0, -20, -50)).ToXMFLOAT3();

	isAdaptive = true;

	toneMappingFilter.Type = ToneMappingType::Hejl;
	bloomFilter.IsActive = true;
}

void HdrDemo::OnResize()
ULTIMA_NOEXCEPT
{
	Application::buildTextures();

	buildTextures();
}

void HdrDemo::buildTextures()
ULTIMA_NOEXCEPT
{
	Utilities::Release(blurSRV2);
	Utilities::Release(blurRTV2);
	Utilities::Release(blurSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(bloomSRV);
	Utilities::Release(bloomRTV);
	Utilities::Release(luminanceSRV2);
	Utilities::Release(luminanceRTV2);
	Utilities::Release(luminanceSRV);
	Utilities::Release(luminanceRTV);
	Utilities::Release(hdrSRV);
	Utilities::Release(hdrRTV);



	Utilities::CreateHdrTexture(device.GetDev(), width, height, &hdrRTV, &hdrSRV);
						
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &bloomRTV, &bloomSRV);
						
	Utilities::CreateHdrTexture(device.GetDev(), width / 2, height / 2, &blurRTV, &blurSRV);
	Utilities::CreateHdrTexture(device.GetDev(), width / 2, height / 2, &blurRTV2, &blurSRV2);

	Utilities::CreateLuminanceTexture(device.GetDev(), width, height, &luminanceRTV, &luminanceSRV, mipLevel);
	Utilities::CreateLuminanceTexture(device.GetDev(), width, height, &luminanceRTV2, &luminanceSRV2, mipLevel);

	float f[4];
	f[0] = f[1] = f[2] = f[3] = 10000.0f;
	context->ClearRenderTargetView(luminanceRTV, f);
	context->ClearRenderTargetView(luminanceRTV2, f);
}

HdrDemo::~HdrDemo()
{
	delete skybox;
	delete modelReflective;

	Utilities::Release(blurSRV2);
	Utilities::Release(blurRTV2);
	Utilities::Release(blurSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(bloomSRV);
	Utilities::Release(bloomRTV);
	Utilities::Release(luminanceSRV2);
	Utilities::Release(luminanceRTV2);
	Utilities::Release(luminanceSRV);
	Utilities::Release(luminanceRTV);
	Utilities::Release(hdrSRV);
	Utilities::Release(hdrRTV);
}

void HdrDemo::Update(float deltaTime)
ULTIMA_NOEXCEPT
{
	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::W))
		camera.MoveZ(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::S))
		camera.MoveZ(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D))
		camera.MoveX(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::A))
		camera.MoveX(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetMouse()->IsPressing(1))
	{
		camera.RotateY(
			-InputManager::GetInstance().GetMouse()->GetDeltaX() / 170.0f);
		camera.RotateX(
			-InputManager::GetInstance().GetMouse()->GetDeltaY() / 170.0f);
	}

	float keyboardRotateFactor = 1 / 100.0f;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Up))
		camera.RotateX(0.09f * deltaTime * keyboardRotateFactor);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Down))
		camera.RotateX(-0.09f * deltaTime * keyboardRotateFactor);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Right))
		camera.RotateY(-0.09f * deltaTime * keyboardRotateFactor);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Left))
		camera.RotateY(0.09f * deltaTime * keyboardRotateFactor);

	camera.Update(deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::T))
		toneMappingFilter.Type = static_cast<ToneMappingType>(
			(static_cast<int>(toneMappingFilter.Type) + 1) % 6);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::B))
		bloomFilter.IsActive = !bloomFilter.IsActive;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadPlus))
		bloomFilter.Threshold += 0.1f;
	else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadMinus))
		bloomFilter.Threshold -= 0.1f;

	if (bloomFilter.Threshold < 0)
		bloomFilter.Threshold = 0;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::R))
		isAdaptive = !isAdaptive;

	Material materialPBR = modelReflective->GetMaterial();

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Minus))
		//materialPBR.Metallic -= deltaTime / 500.f;
		materialPBR.Metallic -= 0.1f;
	else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Equals))
		//materialPBR.Metallic += deltaTime / 500.f;
		materialPBR.Metallic += 0.1f;

	materialPBR.Metallic = MathExtensions::Saturate(materialPBR.Metallic);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::LeftBracket))
		//materialPBR.Roughness -= deltaTime / 500.f;
		materialPBR.Roughness -= 0.1f;
	else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::RightBracket))
		//materialPBR.Roughness += deltaTime / 500.f;
		materialPBR.Roughness += 0.1f;

	materialPBR.Roughness = MathExtensions::Saturate(materialPBR.Roughness);

	modelReflective->SetMaterial(materialPBR);

	isShowingFPS = true;
	title = "HDR Demo | Tone: ";

	switch (toneMappingFilter.Type)
	{
	case (ToneMappingType::None) :
	{
		title += "None";
		break;
	}
	case (ToneMappingType::Linear) :
	{
		title += "Linear";
		break;
	}
	case (ToneMappingType::Reinhard) :
	{
		title += "Reinhard";
		break;
	}
	case (ToneMappingType::Reinhard2) :
	{
		title += "Reinhard2";
		break;
	}
	case (ToneMappingType::Hejl) :
	{
		title += "Hejl";
		break;
	}
	case (ToneMappingType::Hable) :
	{
		title += "Hable";
		break;
	}
	default:
	{
		title + "None";
		break;
	}
	}

	title += " | Bloom: ";

	if (bloomFilter.IsActive)
		title += "ON";
	else
		title += "OFF";

	title += " | Threshold: " + std::to_string(bloomFilter.Threshold);
	title += " | Adaptive Luminance: ";

	if (isAdaptive)
		title += "On";
	else
		title += "Off";

	title += " | MM: " + std::to_string(materialPBR.Metallic);
	title += " | MR: " + std::to_string(materialPBR.Roughness);
}

void HdrDemo::Render(float deltaTime)
ULTIMA_NOEXCEPT
{
	Color clearColor = Color::White;
	clearColor *= MathExtensions::Saturate(static_cast<float>(light.Type));
	Clear(Color::White);

	float f[4];
	f[0] = f[1] = f[2] = f[3] = 0.0f;

	context->ClearRenderTargetView(hdrRTV, f);
	context->ClearRenderTargetView(luminanceRTV, f);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Q))
		context->RSSetState(wireframe);

	//scene rendering
	context->OMSetRenderTargets(1, &hdrRTV, nullptr);
	skybox->Render(context, camera);

	SetDepth(false);
	modelReflective->ComputeReflections(context, camera, Color::White, skybox, 
		std::vector<Model*>(), light);
	context->RSSetViewports(1, &viewport);
	context->OMSetRenderTargets(1, &hdrRTV, backBuffer.GetDsv());
	SetDepth(true);

	modelReflective->RenderReflective(context, camera, light);
	//scene rendering

	SetDepth(false);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Q))
	{
		context->RSSetState(cullBack);
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->Copy(context, hdrSRV, false);
		return;
	}

	//luminance
	context->OMSetRenderTargets(1, &luminanceRTV, nullptr);
	quad->ComputeLuminance(context, hdrSRV, luminanceSRV2, deltaTime / 1000.0f, isAdaptive);

	context->GenerateMips(luminanceSRV);
	//luminance

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Space))
	{
		//show luminance
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->ShowLuminance(context, luminanceSRV, mipLevel);
		//show luminance
	}
	else
	{
		//compute bloom
		context->OMSetRenderTargets(1, &bloomRTV, nullptr);
		quad->ComputeBloom(context, hdrSRV, luminanceSRV, mipLevel, bloomFilter);
		//compute bloom

		//blur bloom
		context->RSSetViewports(1, &viewportHalved);
		context->OMSetRenderTargets(1, &blurRTV, nullptr);
		quad->Blur(context, bloomSRV, GaussianBlurFilter(true));

		context->OMSetRenderTargets(1, &blurRTV2, nullptr);
		quad->Blur(context, blurSRV, GaussianBlurFilter(false));
		context->RSSetViewports(1, &viewport);
		//blur bloom

		//tone mapping + apply bloom
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		quad->Tone(context, hdrSRV, luminanceSRV, mipLevel, blurSRV2, toneMappingFilter,
			bloomFilter, GraphicsFilter(false));
		//tone mapping + apply bloom
	}

	//swap luminance ptrs
	std::swap(luminanceSRV, luminanceSRV2);
	std::swap(luminanceRTV, luminanceRTV2);
	//swap luminance ptrs

	SetDepth(true);
}