#include "ClientFinished.h"

#include "Client.h"

ClientFinished::ClientFinished()
{ }

ClientFinished::~ClientFinished()
{ }

void ClientFinished::Update(float deltaTime, StateManager& manager)
ULTIMA_NOEXCEPT
{ 
	UNREFERENCED_PARAMETER(deltaTime);

	Client* client = dynamic_cast<Client*>(&manager);

	if (client == nullptr)
		return;

	client->SetTintColor(Color::WindowsPhoneViolet);
}