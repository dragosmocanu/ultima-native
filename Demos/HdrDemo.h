#pragma once

#include "Application.h"
#include "FreeCamera3D.h"
#include "ModelReflective.h"
#include "Skybox.h"
#include "ToneMappingFilter.h"

using namespace Ultima;

class HdrDemo : public Application
{
public:
	HdrDemo(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title,
		unsigned int width = 1280,
		unsigned int height = 720,
		bool isInVsync = false,
		bool isShowingFps = true,
		unsigned int antiAliasingSampleCount = 4);

	virtual ~HdrDemo();

	void OnResize() 
		ULTIMA_NOEXCEPT override;

	void Update(float deltaTime) 
		ULTIMA_NOEXCEPT override;

	void Render(float deltaTime) 
		ULTIMA_NOEXCEPT override;

protected:

	void buildTextures()
		ULTIMA_NOEXCEPT override;

	FreeCamera3D camera;
	Skybox* skybox;
	ModelReflective* modelReflective;
	Light light;

	ID3D11RenderTargetView* hdrRTV;
	ID3D11ShaderResourceView* hdrSRV;

	ID3D11RenderTargetView* luminanceRTV;
	ID3D11ShaderResourceView* luminanceSRV;

	ID3D11RenderTargetView* luminanceRTV2;
	ID3D11ShaderResourceView* luminanceSRV2;

	ID3D11RenderTargetView* bloomRTV;
	ID3D11ShaderResourceView* bloomSRV;

	ID3D11RenderTargetView* blurRTV;
	ID3D11ShaderResourceView* blurSRV;

	ID3D11RenderTargetView* blurRTV2;
	ID3D11ShaderResourceView* blurSRV2;

	unsigned int mipLevel;
	ToneMappingFilter toneMappingFilter;
	BloomFilter bloomFilter;

	bool isAdaptive;
};