#pragma once

#include <vector>

#include "Color.h"
#include "LuaAgent.h"
#include "Merchant.h"
#include "StateManager.h"

using namespace Ultima;

enum class ClientState
{
	SearchingTarget,
	Moving,
	Buying,
	Finished,
};

class Client : public LuaAgent, public StateManager
{
public:
	Client(
		const std::string& scriptFileName,
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const Vector3& position = Vector3::Zero,
		const Matrix& rotation = Matrix::Identity,
		float scale = 1.0f,
		unsigned int anisotropicSampleCount = 4);

	virtual ~Client();

	void Update(float deltaTime, std::vector<Merchant*>* const merchants);

	bool HasFinishedShopping() const;

	const Color& GetTintColor() const;

	void SetTintColor(const Color& tintColor);

	bool HasVisited(const Merchant& m);

	Merchant* const GetTarget();
	std::vector<Merchant*>* GetMerchantList();
	void SetTarget(Merchant* target);
	std::vector<uintptr_t>& GetVisitedMerchantList();
	void SetVisitedMerchantList(std::vector<uintptr_t>& visitedMerchants);
	float GetBargainTime();
	void SetBargainTime(float bargainTime);

protected:
	std::vector<uintptr_t> visitedMerchants;
	Color tintColor;
	Merchant* target;

	std::vector<Merchant*>* merchants;
	bool hasFinishedShopping;
	float bargainTime;
};

