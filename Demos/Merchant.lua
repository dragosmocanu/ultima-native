Money = 10;
ProduceTypes = {};
Quantities = {};
PriceAverages = {};
PriceMins = {};
ArraySize = -1;

Result = false
BargainPrice = -1

ExchangeMoney = function(arg1, arg2, index)
	local i = tonumber(index)
	Money = Money + arg1
	Quantities[i] = Quantities[i] - arg2
end

ReceiveOffer = function(arg1, index)
	local i = tonumber(index)
	local t1 = arg1 - PriceMins[i]
	local t2 = arg1 - PriceAverages[i]

	if (t1 < 0) then
		Result = false
	elseif (t2 >= 0) then
		BargainPrice = arg1
		Result = true
	else
		diffPercentage = arg1 / PriceAverages[i]

		rand = math.random()

		local t3 = rand - diffPercentage

		if (t3 < 0) then
			BargainPrice = arg1
			Result = true
		else
			Result = false
		end
	end
end