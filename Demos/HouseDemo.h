#pragma once

#include <string>

#include "Application.h"
#include "BloomFilter.h"
#include "Camera3D.h"
#include "Color.h"
#include "Fog.h"
#include "FollowCamera3D.h"
#include "FreeCamera3D.h"
#include "Light.h"
#include "Matrix.h"
#include "Model.h"
#include "ModelReflective.h"
#include "RandomColorGenerator.h"
#include "ShadowFilter.h"

using namespace Ultima;

class HouseDemo : public Application
{
public:
#if DIRECTX
	HouseDemo(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title,
		unsigned int width = 1280,
		unsigned int height = 720,
		bool isInVsync = false,
		bool isShowingFps = true,
		unsigned int antiAliasingSampleCount = 4);
#endif

	~HouseDemo();

	void OnResize() 
		ULTIMA_NOEXCEPT override;

	void Update(float deltaTime) 
		ULTIMA_NOEXCEPT override;

	void Render(float deltaTime) 
		ULTIMA_NOEXCEPT override;

protected:
	void buildTextures()
		ULTIMA_NOEXCEPT override;

	void updateTitle(unsigned int noTriangles)
		ULTIMA_NOEXCEPT;

	FreeCamera3D camera;
	Light light;
	Fog fog;
	GraphicsFilter graphicsFilter;
	ShadowFilter shadowFilter;
	AntiAliasingFilter aaFilter;
	AmbientOcclusionFilter aoFilter;

	bool isWireframe;

	std::vector<Model*> models;
	std::vector<ModelReflective*> modelsReflective;

	Matrix lightViewProjection;

	ID3D11RenderTargetView* mapsRTV[2];
	ID3D11ShaderResourceView* mapsSRV[2];

	ID3D11RenderTargetView* deferredRTV[6];
	ID3D11ShaderResourceView* deferredSRV[6];

	ID3D11RenderTargetView* lightingRTV[3];
	ID3D11ShaderResourceView* lightingSRV[3];

	ID3D11DepthStencilView* pcfDSV;
	ID3D11ShaderResourceView* pcfSRV;

	ID3D11RenderTargetView* vsmRTV;
	ID3D11ShaderResourceView* vsmSRV;
	ID3D11RenderTargetView* vsmRTV2;
	ID3D11ShaderResourceView* vsmSRV2;

	ID3D11RenderTargetView* aoRTV;
	ID3D11ShaderResourceView* aoSRV;

	ID3D11RenderTargetView* blurRTV;
	ID3D11ShaderResourceView* blurSRV;

	ID3D11RenderTargetView* blurRTV2;
	ID3D11ShaderResourceView* blurSRV2;

	ID3D11RenderTargetView* hdrRTV;
	ID3D11ShaderResourceView* hdrSRV;

	ID3D11RenderTargetView* smaaRTV1;
	ID3D11ShaderResourceView* smaaSRV1;
	ID3D11RenderTargetView* smaaRTV2;
	ID3D11ShaderResourceView* smaaSRV2;

	ID3D11RenderTargetView* luminanceRTV;
	ID3D11ShaderResourceView* luminanceSRV;

	ID3D11RenderTargetView* luminanceRTV2;
	ID3D11ShaderResourceView* luminanceSRV2;

	ID3D11RenderTargetView* bloomRTV;
	ID3D11ShaderResourceView* bloomSRV;

	ID3D11RenderTargetView* backBufferRTV2;
	ID3D11ShaderResourceView* backBufferSRV2;

	unsigned int mipLevel;
	ToneMappingFilter toneMappingFilter;
	BloomFilter bloomFilter;
};