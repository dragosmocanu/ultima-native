#include "stdafx.h"

#include "Client.h"

#include <math.h>

#include "ClientMoving.h"
#include "ClientSearchingTarget.h"
#include "ClientBuying.h"
#include "ClientFinished.h"
#include "RandomNumberGenerator.h"

Client::Client(
	const std::string& scriptFileName,
	const Device& device,
	const std::string& meshPath,
	const std::string& texturePath,
	const Vector3& position,
	const Matrix& rotation,
	float scale,
	unsigned int anisotropicSampleCount)
	: LuaAgent(scriptFileName, device, meshPath, texturePath, position, rotation,
	scale, anisotropicSampleCount),
	tintColor(Color::SpectralRed)
{
	AddState(new ClientSearchingTarget());
	AddState(new ClientMoving());
	AddState(new ClientBuying());
	AddState(new ClientFinished());

	stateIndex = static_cast<unsigned int>(ClientState::SearchingTarget);

	hasFinishedShopping = false;
}

Client::~Client()
{ }

Merchant* const Client::GetTarget()
{
	return target;
}

std::vector<Merchant*>* Client::GetMerchantList()
{
	return this->merchants;
}

void Client::SetTarget(Merchant* target)
{
	this->target = target;
}

std::vector<uintptr_t>& Client::GetVisitedMerchantList()
{
	return this->visitedMerchants;
}

void Client::SetVisitedMerchantList(std::vector<uintptr_t>& visitedMerchants)
{
	this->visitedMerchants = visitedMerchants;
}

void Client::Update(
	float deltaTime, 
	std::vector<Merchant*>* const merchants)
{
	sphere.SetPosition(this->position);

	//create copies for the states to use
	this->merchants = merchants;

	StateManager::Update(deltaTime);
}

bool Client::HasFinishedShopping() const
{
	return (stateIndex == static_cast<unsigned int>(ClientState::Finished));
}

const Color& Client::GetTintColor() const
{
	return tintColor;
}

void Client::SetTintColor(const Color& tintColor)
{
	this->tintColor = tintColor;
}

bool Client::HasVisited(const Merchant& m)
{
	uintptr_t ptr = reinterpret_cast<uintptr_t>(&m);
	bool isNew = true;

	for (unsigned int i = 0; i < visitedMerchants.size(); i++)
	{
		if (ptr == visitedMerchants[i])
		{
			isNew = false;
			break;
		}
	}

	return !isNew;
}

float Client::GetBargainTime()
{
	return bargainTime;
}

void Client::SetBargainTime(float bargainTime)
{
	this->bargainTime = bargainTime;
}