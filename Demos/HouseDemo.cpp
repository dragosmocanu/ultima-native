#include "stdafx.h"

#include "HouseDemo.h"

#include "Color.h"
#include "ContentManager.h"
#include "MathExtensions.h"
#include "RandomColorGenerator.h"
#include "Utilities.h"

HouseDemo::HouseDemo(
	const HINSTANCE& hInstance,
	const HINSTANCE& prevInstance,
	const LPWSTR& cmdLine,
	int cmdShow,
	const std::string& title,
	unsigned int width,
	unsigned int height,
	bool isInVsync,
	bool isShowingFPS,
	unsigned int antiAliasingSampleCount)
	: Application(hInstance, prevInstance, cmdLine, cmdShow,
	title, width, height, isInVsync, isShowingFPS, 0),
	camera(aspectRatio)
{
	UNREFERENCED_PARAMETER(antiAliasingSampleCount);

	ContentManager::GetInstance().SetSmartContentManagementType(SmartContentManagementType::Manual);

	buildTextures();
	
	models.push_back(new Model(device, "Assets/table.FBX", "", "", "",
		Vector3(-50.0f, 0.0f, 20.0f)));

	Material m = models[0]->GetMaterial();
	m.Base = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);
	m.Metallic = 0.4f;
	m.Roughness = 0.5f;
	models[0]->SetMaterial(m);

	models.push_back(new Model(device, "Assets/bookcase.FBX", "", "", "",
		Vector3(17.0f, -7.0f, 20.0f)));

	m.Base = XMFLOAT4(0.6f, 0.2f, 0.01f, 1.0f);
	m.Metallic = 0.0f;
	models[1]->SetMaterial(m);

	models.push_back(new Model(device, "Assets/window.FBX", "", "", "",
		Vector3(5.0f, 0.0f, -10.0f), Matrix::CreateRotationX(MathExtensions::Pi / 2.0f)));

	m.Base = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	m.Metallic = 0.5f;
	models[2]->SetMaterial(m);

	models.push_back(new Model(device, "Assets/smallSphere.FBX", "", 
		"", "Assets/displacementMap.png",
		Vector3(-30.0f, 53.0f, 20.0f), Matrix::Identity));

	m.Metallic = 0.0f;
	m.Roughness = 0.0f;
	models[3]->SetMaterial(m);

	auto dmf = models[3]->GetDisplacementMappingFilter();
	dmf.Bias = 0.0f;
	dmf.Factor = 1.0f;
	models[3]->SetDisplacementMappingFilter(dmf);

	modelsReflective.push_back(new ModelReflective(device, "Assets/smallSphere.FBX",
		"", "", "", Vector3(33.0f, 17.0f, 20.0f)));

	m.Base = XMFLOAT4(0.8f, 0.3f, 0.3f, 1.0f);
	m.Metallic = 0.0f;
	m.Roughness = 1.0f;

	modelsReflective[0]->SetMaterial(m);

	camera.SetY(3.0f);
	camera.SetZ(-13.0f);

	graphicsFilter.IsActive = true;

	Vector3 lightPosition = Vector3(20, 80, 80);
	Matrix lightView, lightProjection;

	lightView = Matrix::CreateLookAt(
		lightPosition,
		lightPosition + light.Direction,
		Vector3::Up);

	lightProjection = Matrix::CreateOthographicProjection(
		static_cast<float>(width / 4), static_cast<float>(height / 4), 0.1f, 400.0f);

	lightViewProjection = lightView * lightProjection;

	light.Position = lightPosition.ToXMFLOAT3();
	light.Radius = 0.9f;

	fog.Color = Color::White.ToXMFLOAT4();

	camera.SetPosition(Vector3(-50.0f, 0.0f, -150.0f));
	camera.RotateY(MathExtensions::ToRadians(-20.0f));

	//AO camera
	//freeCamera.SetPosition(Vector3(-36.0f, 22.0f, 25.0f));
	//freeCamera.RotateY(MathExtensions::ToRadians(-30.0f));
	//freeCamera.RotateX(MathExtensions::ToRadians(15.0f));

	aoFilter.Type = AmbientOcclusionType::HBAO;
	aoFilter.Intensity = 0.4f;
	aoFilter.HbaoR = 4.0f;
	aoFilter.HbaoNumberOfDirections = 1;
	aoFilter.HbaoNumberOfSteps = 8;
	aoFilter.HbaoAngleBias = 0;
	aoFilter.HbaoMaxRadiusPixels = 0.2f * min(width, height);

	aaFilter.Type = AntiAliasingType::SMAA;

	shadowFilter.VsmMinVariance = 0.00001f;
	shadowFilter.VsmBleedingReduction = 0.99f;

	bloomFilter.IsActive = true;
	bloomFilter.Threshold = -2.0f;

	toneMappingFilter.Type = ToneMappingType::Hejl;
}

void HouseDemo::OnResize()
ULTIMA_NOEXCEPT
{
	Application::buildTextures();

	camera.Reinitialize(aspectRatio);

	buildTextures();
}

void HouseDemo::buildTextures()
ULTIMA_NOEXCEPT
{
	Utilities::Release(backBufferSRV2);
	Utilities::Release(backBufferRTV2);

	Utilities::Release(bloomSRV);
	Utilities::Release(bloomRTV);
	Utilities::Release(luminanceSRV2);
	Utilities::Release(luminanceRTV2);
	Utilities::Release(luminanceSRV);
	Utilities::Release(luminanceRTV);

	Utilities::Release(deferredSRV[3]);
	Utilities::Release(deferredRTV[3]);
	Utilities::Release(deferredSRV[2]);
	Utilities::Release(deferredRTV[2]);
	Utilities::Release(deferredSRV[1]);
	Utilities::Release(deferredRTV[1]);
	Utilities::Release(deferredSRV[0]);
	Utilities::Release(deferredRTV[0]);

	Utilities::Release(lightingSRV[2]);
	Utilities::Release(lightingRTV[2]);
	Utilities::Release(lightingSRV[1]);
	Utilities::Release(lightingRTV[1]);
	Utilities::Release(lightingSRV[0]);
	Utilities::Release(lightingRTV[0]);

	Utilities::Release(mapsSRV[1]);
	Utilities::Release(mapsRTV[1]);
	Utilities::Release(mapsSRV[0]);
	Utilities::Release(mapsRTV[0]);

	Utilities::Release(smaaSRV2);
	Utilities::Release(smaaRTV2);
	Utilities::Release(smaaSRV1);
	Utilities::Release(smaaRTV1);
	Utilities::Release(hdrSRV);
	Utilities::Release(hdrRTV);
	Utilities::Release(blurSRV2);
	Utilities::Release(blurRTV2);
	Utilities::Release(blurSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(aoSRV);
	Utilities::Release(aoRTV);
	Utilities::Release(vsmSRV2);
	Utilities::Release(vsmRTV2);
	Utilities::Release(vsmSRV);
	Utilities::Release(vsmRTV);
	Utilities::Release(pcfSRV);
	Utilities::Release(pcfDSV);

	Utilities::CreatePcfTexture(device.GetDev(), width / 2, height / 2, &pcfDSV, &pcfSRV);
	Utilities::CreateVsmTexture(device.GetDev(), width / 2, height / 2, &vsmRTV, &vsmSRV);
	Utilities::CreateVsmTexture(device.GetDev(), width / 2, height / 2, &vsmRTV2, &vsmSRV2);

	Utilities::CreateAmbientOcclusionTexture(device.GetDev(), width, height, &aoRTV, &aoSRV);

	Utilities::CreateColorTexture(device.GetDev(), width, height, &smaaRTV1, &smaaSRV1);
	Utilities::CreateColorTexture(device.GetDev(), width, height, &smaaRTV2, &smaaSRV2);
	Utilities::CreateColorTexture(device.GetDev(), width / 2, height / 2, &blurRTV, &blurSRV);
	Utilities::CreateColorTexture(device.GetDev(), width / 2, height / 2, &blurRTV2, &blurSRV2);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &hdrRTV, &hdrSRV);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &mapsRTV[0], &mapsSRV[0]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &mapsRTV[1], &mapsSRV[1]);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &lightingRTV[0], &lightingSRV[0]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &lightingRTV[1], &lightingSRV[1]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &lightingRTV[2], &lightingSRV[2]);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[0], &deferredSRV[0]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[1], &deferredSRV[1]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[2], &deferredSRV[2]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[3], &deferredSRV[3]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[4], &deferredSRV[4]);
	Utilities::CreateHdrTexture(device.GetDev(), width, height, &deferredRTV[5], &deferredSRV[5]);

	Utilities::CreateHdrTexture(device.GetDev(), width, height, &bloomRTV, &bloomSRV);

	Utilities::CreateLuminanceTexture(device.GetDev(), width, height, &luminanceRTV, &luminanceSRV, mipLevel);
	Utilities::CreateLuminanceTexture(device.GetDev(), width, height, &luminanceRTV2, &luminanceSRV2, mipLevel);

	Utilities::CreateColorTexture(device.GetDev(), width, height, &backBufferRTV2, &backBufferSRV2);

	float f[4];
	f[0] = f[1] = f[2] = f[3] = 250.0f;
	context->ClearRenderTargetView(luminanceRTV, f);
	context->ClearRenderTargetView(luminanceRTV2, f);
}

HouseDemo::~HouseDemo()
{
	Utilities::Release(backBufferSRV2);
	Utilities::Release(backBufferRTV2);

	Utilities::Release(bloomSRV);
	Utilities::Release(bloomRTV);
	Utilities::Release(luminanceSRV2);
	Utilities::Release(luminanceRTV2);
	Utilities::Release(luminanceSRV);
	Utilities::Release(luminanceRTV);

	Utilities::Release(deferredSRV[5]);
	Utilities::Release(deferredRTV[5]);
	Utilities::Release(deferredSRV[4]);
	Utilities::Release(deferredRTV[4]);
	Utilities::Release(deferredSRV[3]);
	Utilities::Release(deferredRTV[3]);
	Utilities::Release(deferredSRV[2]);
	Utilities::Release(deferredRTV[2]);
	Utilities::Release(deferredSRV[1]);
	Utilities::Release(deferredRTV[1]);
	Utilities::Release(deferredSRV[0]);
	Utilities::Release(deferredRTV[0]);

	Utilities::Release(lightingSRV[2]);
	Utilities::Release(lightingRTV[2]);
	Utilities::Release(lightingSRV[1]);
	Utilities::Release(lightingRTV[1]);
	Utilities::Release(lightingSRV[0]);
	Utilities::Release(lightingRTV[0]);

	Utilities::Release(mapsSRV[1]);
	Utilities::Release(mapsRTV[1]);
	Utilities::Release(mapsSRV[0]);
	Utilities::Release(mapsRTV[0]);

	Utilities::Release(smaaSRV2);
	Utilities::Release(smaaRTV2);
	Utilities::Release(smaaSRV1);
	Utilities::Release(smaaRTV1);
	Utilities::Release(hdrSRV);
	Utilities::Release(hdrRTV);
	Utilities::Release(blurSRV2);
	Utilities::Release(blurRTV2);
	Utilities::Release(blurSRV);
	Utilities::Release(blurRTV);
	Utilities::Release(aoSRV);
	Utilities::Release(aoRTV);
	Utilities::Release(vsmSRV2);
	Utilities::Release(vsmRTV2);
	Utilities::Release(vsmSRV);
	Utilities::Release(vsmRTV);
	Utilities::Release(pcfSRV);
	Utilities::Release(pcfDSV);

	Model* temp;

	for (unsigned int i = 0; i < models.size(); i++)
	{
		temp = models[i];
		delete temp;
	}

	ModelReflective* tempX;

	for (unsigned int i = 0; i < modelsReflective.size(); i++)
	{
		tempX = modelsReflective[i];
		delete tempX;
	}
}

void HouseDemo::Update(float deltaTime)
ULTIMA_NOEXCEPT
{
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::L))
	{
		light.Type = static_cast<LightType>((static_cast<unsigned int>(light.Type) + 1) % 4);
		modelsReflective[0]->SetPosition(light.Position);
	}

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Q))
		isWireframe = !isWireframe;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::H))
		aoFilter.Type = static_cast<AmbientOcclusionType>(
			(static_cast<int>(aoFilter.Type) + 1) % 3);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::R))
		aaFilter.Type = static_cast<AntiAliasingType>(
			(static_cast<int>(aaFilter.Type) + 1) % 3);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::T))
		toneMappingFilter.Type = static_cast<ToneMappingType>(
		(static_cast<int>(toneMappingFilter.Type) + 1) % 6);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::B))
		bloomFilter.IsActive = !bloomFilter.IsActive;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::LeftBracket))
	{
		Material m = modelsReflective[0]->GetMaterial();
		m.Roughness -= 0.1f;
		m.Roughness = MathExtensions::Saturate(m.Roughness);
		modelsReflective[0]->SetMaterial(m);
	}
	else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::RightBracket))
	{
		Material m = modelsReflective[0]->GetMaterial();
		m.Roughness += 0.1f;
		m.Roughness = MathExtensions::Saturate(m.Roughness);
		modelsReflective[0]->SetMaterial(m);
	}

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Minus))
	{
		Material m = modelsReflective[0]->GetMaterial();
		m.Metallic -= 0.1f;
		m.Metallic = MathExtensions::Saturate(m.Metallic);
		modelsReflective[0]->SetMaterial(m);
	}
	else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Equals))
	{
		Material m = modelsReflective[0]->GetMaterial();
		m.Metallic += 0.1f;
		m.Metallic = MathExtensions::Saturate(m.Metallic);
		modelsReflective[0]->SetMaterial(m);
	}

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad0))
	{
		shadowFilter.Type = static_cast<ShadowType>(
			(static_cast<int>(shadowFilter.Type) + 1) % 5);

		if (shadowFilter.Type == ShadowType::VSM)
		{
			shadowFilter.VsmMinVariance = 0.002f;
			shadowFilter.VsmBleedingReduction = 0.1f;
		}
		else if (shadowFilter.Type == ShadowType::EVSM
			|| shadowFilter.Type == ShadowType::EVSM4)
		{
			shadowFilter.VsmMinVariance = 0.00001f;
			shadowFilter.VsmBleedingReduction = 0.99f;
		}
	}

	int dir = 1;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftShift))
		dir = -1;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad1))
		aoFilter.HbaoFocalLen.x += 1.7f * dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad2))
		aoFilter.HbaoFocalLen.y += 1.7f * dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad3))
		aoFilter.HbaoR += 0.05f * dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad4))
		aoFilter.HbaoNumberOfDirections += dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad5))
		aoFilter.HbaoNumberOfSteps += dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad6))
		aoFilter.HbaoAngleBias += 0.2f * dir;
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad9))
		aoFilter.HbaoMaxRadiusPixels += 3 * dir;
	//y = -0.04f;
	//{x=-0.0500002168 y=0.109999940 z=0.000000000 }
	//{x=-2.16066837e-007 y=0.0599999391 z=0.000000000 }
	//{x=-1.56462193e-007 y=-0.0500000380 z=0.000000000 }
	//{x=0.00710666925 y=-0.0928933322 z=0.000000000 }

	//if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad7))
	//	bloomFilter.Threshold -= 0.25f;
	//else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad8))
	//	bloomFilter.Threshold += 0.25f;

	if (shadowFilter.Type == ShadowType::PCF)
	{
		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
		{
			if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadPlus))
				shadowFilter.PcfBias *= 2.0f;
			else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadMinus))
				shadowFilter.PcfBias /= 2.0f;
			else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::E))
				shadowFilter.PcfBias *= -1;
		}
		else
		{
			if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadPlus))
				shadowFilter.PcfNumberOfSamples += 1;
			else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadMinus))
				shadowFilter.PcfNumberOfSamples -= 1;

			if (shadowFilter.PcfNumberOfSamples < 1)
				shadowFilter.PcfNumberOfSamples = 1;
		}
	}
	else
	{
		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
		{
			if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadPlus))
				shadowFilter.VsmBleedingReduction += 0.01f;
			else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadMinus))
				shadowFilter.VsmBleedingReduction -= 0.01f;
		}
		else
		{
			if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadPlus))
				shadowFilter.VsmMinVariance *= 1.2f;
			else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::NumpadMinus))
				shadowFilter.VsmMinVariance /= 1.2f;
		}

		if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad1))
			shadowFilter.EvsmExponents.x -= 0.1f;
		else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad2))
			shadowFilter.EvsmExponents.x += 0.1f;

		if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad4))
			shadowFilter.EvsmExponents.y -= 0.1f;
		else if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Numpad5))
			shadowFilter.EvsmExponents.y += 0.1f;
	}
	//vsmMinVar 0.00101579865
	//vsmBleed 0.949999750

	//if (models.size() >= 3)
	//{
	//	float speed = 0.2f;

	//	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Up))
	//	{
	//		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
	//			models[2]->MoveY(speed * deltaTime);
	//		else
	//			models[2]->MoveZ(speed * deltaTime);
	//	}
	//	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Down))
	//	{
	//		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
	//			models[2]->MoveY(-speed * deltaTime);
	//		else
	//			models[2]->MoveZ(-speed * deltaTime);
	//	}

	//	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Right))
	//		models[2]->MoveX(speed * deltaTime);
	//	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Left))
	//		models[2]->MoveX(-speed * deltaTime);
	//}

	float speed = 0.2f;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Up))
	{
		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
			modelsReflective[0]->MoveY(speed * deltaTime);
		else
			modelsReflective[0]->MoveZ(speed * deltaTime);
	}
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Down))
	{
		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftAlt))
			modelsReflective[0]->MoveY(-speed * deltaTime);
		else
			modelsReflective[0]->MoveZ(-speed * deltaTime);
	}

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Right))
		modelsReflective[0]->MoveX(speed * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Left))
		modelsReflective[0]->MoveX(-speed * deltaTime);

	//if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Minus))
	//	graphicsFilter.Brightness -= 0.003f * deltaTime;
	//else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Equals))
	//	graphicsFilter.Brightness += 0.003f * deltaTime;

	//if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftBracket))
	//	graphicsFilter.Contrast -= 0.003f * deltaTime;
	//else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::RightBracket))
	//	graphicsFilter.Contrast += 0.003f * deltaTime;

	//graphicsFilter.Brightness = MathExtensions::Saturate(graphicsFilter.Brightness);
	//graphicsFilter.Contrast = MathExtensions::Saturate(graphicsFilter.Contrast);

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::O))
		graphicsFilter.IsGrayscale = !graphicsFilter.IsGrayscale;

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::P))
		graphicsFilter.IsNegative = !graphicsFilter.IsNegative;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::LeftShift))
		speed *= 3.0f;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::W))
		camera.MoveZ(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::S))
		camera.MoveZ(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D))
		camera.MoveX(0.09f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::A))
		camera.MoveX(-0.09f * deltaTime);

	if (InputManager::GetInstance().GetMouse()->IsPressing(1))
	{
		camera.RotateY(
			-InputManager::GetInstance().GetMouse()->GetDeltaX() / 170.0f);
		camera.RotateX(
			-InputManager::GetInstance().GetMouse()->GetDeltaY() / 170.0f);
	}

	camera.Update(deltaTime);

	isShowingFPS = true;
}

void HouseDemo::Render(float deltaTime)
ULTIMA_NOEXCEPT
{
	Color clearColor = Color::White;
	clearColor *= MathExtensions::Saturate(static_cast<float>(light.Type));
	Clear(clearColor);

	Utilities::Clear(context, blurRTV);
	Utilities::Clear(context, blurRTV2);
	Utilities::Clear(context, smaaRTV1);
	Utilities::Clear(context, smaaRTV2);
	Utilities::Clear(context, aoRTV);
	Utilities::Clear(context, mapsRTV[0]);
	Utilities::Clear(context, mapsRTV[1]);
	
	Utilities::Clear(context, lightingRTV[0]);
	Utilities::Clear(context, lightingRTV[1]);
	Utilities::Clear(context, lightingRTV[2]);
	
	Utilities::Clear(context, deferredRTV[1]);
	Utilities::Clear(context, deferredRTV[2]);
	Utilities::Clear(context, deferredRTV[4]);
	Utilities::Clear(context, deferredRTV[5]);

	Utilities::Clear(context, deferredRTV[0], clearColor);
	Utilities::Clear(context, hdrRTV, clearColor);
	Utilities::Clear(context, vsmRTV, clearColor);
	Utilities::Clear(context, vsmRTV2, clearColor);

	Utilities::Clear(context, deferredRTV[3], Color::White);

	Utilities::Clear(context, pcfDSV);

	unsigned int noTriangles = 0;

	////Deferred
	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Tab))
	{
		//Render deferred (colors, materials, positions, normals)
		SetDepth(true);
		context->OMSetRenderTargets(6, &deferredRTV[0], backBuffer.GetDsv());

		for (auto& x : models)
			noTriangles += x->RenderDeferred(context, camera);

		for (auto& x : modelsReflective)
			noTriangles += x->RenderDeferredReflective(context, camera);
		//Render deferred

		//Compute shadow map
		SetRenderingToDepth(true, vsmRTV, pcfDSV);

		for (auto& model : models)
			noTriangles += model->RenderDepth(context, lightViewProjection, shadowFilter);

		for (auto& model : modelsReflective)
			noTriangles += model->RenderDepth(context, lightViewProjection, shadowFilter);

		SetRenderingToDepth(false);
		//Compute shadow map

		//Blur shadow map
		if (shadowFilter.Type != ShadowType::PCF
			&& !InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Space))
		{
			SetDepth(false);

			context->RSSetViewports(1, &viewportHalved);

			GaussianBlurFilter f(true);
			context->OMSetRenderTargets(1, &vsmRTV2, nullptr);
			noTriangles += quad->Blur(context, vsmSRV, f);

			f.IsHorizontal = false;
			context->OMSetRenderTargets(1, &vsmRTV, nullptr);
			noTriangles += quad->Blur(context, vsmSRV2, f);

			SetDepth(true);

			context->RSSetViewports(1, &viewport);
		}
		//Blur shadow map

		ID3D11ShaderResourceView* shadowMap = nullptr;

		if (shadowFilter.Type == ShadowType::PCF)
			shadowMap = pcfSRV;
		else
			shadowMap = vsmSRV;

		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D1))
		{
			context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);

			if (shadowFilter.Type == ShadowType::PCF)
				noTriangles += quad->Copy(context, shadowMap, true);
			else
				noTriangles += quad->Copy(context, shadowMap, false);

			updateTitle(noTriangles);
			return;
		}

		//Reflections
		SetDepth(true);
		for (auto& x : modelsReflective)
			noTriangles += x->ComputeReflections(context, camera, Color::White, models, light,
				fog, graphicsFilter, shadowMap, lightViewProjection, shadowFilter);

		context->RSSetViewports(1, &viewport);
		//Reflections

		//Compute illumination
		SetDepth(false);
		SetBlend(true);

		context->OMSetRenderTargets(3, &lightingRTV[0], nullptr);

		noTriangles += quad->ComputeIllumination(
			context, camera, deferredSRV[1], deferredSRV[2],
			light, shadowMap, lightViewProjection, shadowFilter);

		SetBlend(false);
		//Compute illumination

		//Compute AO map
		SetDepth(false);
		context->OMSetRenderTargets(1, &aoRTV, nullptr);
		noTriangles += quad->ComputeAmbientOcclusion(context, deferredSRV[4], deferredSRV[5],
			aoFilter);
		//Compute AO map

		//Blur AO
		context->RSSetViewports(1, &viewportHalved);
		context->OMSetRenderTargets(1, &blurRTV, nullptr);
		noTriangles += quad->Blur(context, aoSRV, GaussianBlurFilter(true));

		context->OMSetRenderTargets(1, &blurRTV2, nullptr);
		noTriangles += quad->Blur(context, blurSRV, GaussianBlurFilter(false));

		context->RSSetViewports(1, &viewport);
		//Blur AO

		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D2))
		{
			context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
			noTriangles += quad->Copy(context, blurSRV2, true);
			updateTitle(noTriangles);
			return;
		}

		//Illuminate scene
		context->OMSetRenderTargets(1, &hdrRTV, nullptr);
		quad->Illuminate(context, deferredSRV[0], lightingSRV[0], lightingSRV[1], lightingSRV[2],
			deferredSRV[3], blurSRV2, aoFilter, graphicsFilter);
		//Illuminate scene

		//luminance
		context->OMSetRenderTargets(1, &luminanceRTV, nullptr);
		quad->ComputeLuminance(context, hdrSRV, luminanceSRV2, deltaTime / 1000.0f, true);

		context->GenerateMips(luminanceSRV);
		//luminance

		//compute bloom
		context->OMSetRenderTargets(1, &bloomRTV, nullptr);
		quad->ComputeBloom(context, hdrSRV, luminanceSRV, mipLevel, bloomFilter);
		//compute bloom

		//blur bloom
		context->RSSetViewports(1, &viewportHalved);
		context->OMSetRenderTargets(1, &blurRTV, nullptr);
		quad->Blur(context, bloomSRV, GaussianBlurFilter(true));

		context->OMSetRenderTargets(1, &blurRTV2, nullptr);
		quad->Blur(context, blurSRV, GaussianBlurFilter(false));
		context->RSSetViewports(1, &viewport);
		//blur bloom

		//tone mapping + apply bloom
		context->OMSetRenderTargets(1, &backBufferRTV2, nullptr);
		quad->Tone(context, hdrSRV, luminanceSRV, mipLevel, blurSRV2, toneMappingFilter,
			bloomFilter, graphicsFilter);
		//tone mapping + apply bloom

		//swap luminance ptrs
		std::swap(luminanceSRV, luminanceSRV2);
		std::swap(luminanceRTV, luminanceRTV2);
		//swap luminance ptrs

		//AA
		noTriangles += quad->AntiAlias(context, backBufferSRV2, backBuffer.GetRtv(), aaFilter, smaaSRV1, smaaRTV1, smaaSRV2,
			smaaRTV2, Vector2(static_cast<float>(width), static_cast<float>(height)));
		//AA

		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D4))
		{
			context->RSSetState(cullBack);
			context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
			noTriangles += quad->Copy(context, smaaSRV1);
			updateTitle(noTriangles);
			return;
		}

		if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D5))
		{
			context->RSSetState(cullBack);
			context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
			noTriangles += quad->Copy(context, smaaSRV2);
			updateTitle(noTriangles);
			return;
		}

		updateTitle(noTriangles);

		SetDepth(true);
		return;
	}
	////Deferred

	//Compute shadow map
	SetRenderingToDepth(true, vsmRTV, pcfDSV);

	for (auto& model : models)
		noTriangles += model->RenderDepth(context, lightViewProjection, shadowFilter);

	for (auto& model : modelsReflective)
		noTriangles += model->RenderDepth(context, lightViewProjection, shadowFilter);

	SetRenderingToDepth(false);
	//Compute shadow map

	//Blur shadow map
	if (shadowFilter.Type != ShadowType::PCF
		&& !InputManager::GetInstance().GetKeyboard()->IsPressing(Key::Space))
	{
		SetDepth(false);

		context->RSSetViewports(1, &viewportHalved);

		GaussianBlurFilter f(true);
		context->OMSetRenderTargets(1, &vsmRTV2, nullptr);
			noTriangles += quad->Blur(context, vsmSRV, f);

		f.IsHorizontal = false;
		context->OMSetRenderTargets(1, &vsmRTV, nullptr);
			noTriangles += quad->Blur(context, vsmSRV2, f);

		SetDepth(true);

		context->RSSetViewports(1, &viewport);
	}
	//Blur shadow map

	ID3D11ShaderResourceView* shadowMap = nullptr;

	if (shadowFilter.Type == ShadowType::PCF)
		shadowMap = pcfSRV;
	else
		shadowMap = vsmSRV;

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D1))
	{
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);

		if (shadowFilter.Type == ShadowType::PCF)
			noTriangles += quad->Copy(context, shadowMap, true);
		else
			noTriangles += quad->Copy(context, shadowMap, false);

		updateTitle(noTriangles);
		return;
	}

	//Reflections
	for (auto& x : modelsReflective)
		noTriangles += x->ComputeReflections(context, camera, Color::White, models, light, 
			fog, graphicsFilter, shadowMap, lightViewProjection, shadowFilter);

	context->RSSetViewports(1, &viewport);
	//Reflections

	//Render positions & normals
	context->OMSetRenderTargets(2, &mapsRTV[0], backBuffer.GetDsv());
	SetDepth(true);

	for (auto& x : models)
		noTriangles += x->RenderPositionsAndNormals(context, camera);

	for (auto& x : modelsReflective)
		noTriangles += x->RenderPositionsAndNormals(context, camera);

	context->ClearDepthStencilView(backBuffer.GetDsv(),
		D3D11_CLEAR_DEPTH, 1.0f, 0);
	//Render positions & normals

	//Compute AO map
	SetDepth(false);
	context->OMSetRenderTargets(1, &aoRTV, nullptr);
	noTriangles += quad->ComputeAmbientOcclusion(context, mapsSRV[0], mapsSRV[1], aoFilter);
	//Compute AO map

	//Blur AO
	context->RSSetViewports(1, &viewportHalved);
	context->OMSetRenderTargets(1, &blurRTV, nullptr);
	noTriangles += quad->Blur(context, aoSRV, GaussianBlurFilter(true));

	context->OMSetRenderTargets(1, &blurRTV2, nullptr);
	noTriangles += quad->Blur(context, blurSRV, GaussianBlurFilter(false));

	context->RSSetViewports(1, &viewport);
	//Blur AO

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D2))
	{
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		noTriangles += quad->Copy(context, blurSRV2, true);
		updateTitle(noTriangles);
		return;
	}

	//Apply AO
	context->OMSetRenderTargets(1, &hdrRTV, backBuffer.GetDsv());
	SetDepth(true);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D3))
	{
		context->RSSetState(wireframe);
	}

	for (auto& x : models)
		noTriangles += x->Render(context, camera, light, fog, graphicsFilter, 
			blurSRV2, shadowMap, lightViewProjection, shadowFilter);

	for (auto& x : modelsReflective)
		noTriangles += x->RenderReflective(context, camera, light, fog, graphicsFilter, 
			blurSRV2, shadowMap, lightViewProjection, shadowFilter);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D3))
	{
		context->RSSetState(cullBack);
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		noTriangles += quad->Copy(context, hdrSRV);
		updateTitle(noTriangles);
		return;
	}
	//Apply AO

	//luminance
	context->OMSetRenderTargets(1, &luminanceRTV, nullptr);
	quad->ComputeLuminance(context, hdrSRV, luminanceSRV2, deltaTime / 1000.0f, true);

	context->GenerateMips(luminanceSRV);
	//luminance

	//compute bloom
	context->OMSetRenderTargets(1, &bloomRTV, nullptr);
	quad->ComputeBloom(context, hdrSRV, luminanceSRV, mipLevel, bloomFilter);
	//compute bloom

	//blur bloom
	context->RSSetViewports(1, &viewportHalved);
	context->OMSetRenderTargets(1, &blurRTV, nullptr);
	quad->Blur(context, bloomSRV, GaussianBlurFilter(true));

	context->OMSetRenderTargets(1, &blurRTV2, nullptr);
	quad->Blur(context, blurSRV, GaussianBlurFilter(false));
	context->RSSetViewports(1, &viewport);
	//blur bloom

	//tone mapping + apply bloom
	context->OMSetRenderTargets(1, &backBufferRTV2, nullptr);
	quad->Tone(context, hdrSRV, luminanceSRV, mipLevel, blurSRV2, toneMappingFilter,
		bloomFilter, graphicsFilter);
	//tone mapping + apply bloom

	//swap luminance ptrs
	std::swap(luminanceSRV, luminanceSRV2);
	std::swap(luminanceRTV, luminanceRTV2);
	//swap luminance ptrs

	//AA
	noTriangles += quad->AntiAlias(context, backBufferSRV2, backBuffer.GetRtv(), aaFilter, smaaSRV1, smaaRTV1, smaaSRV2,
		smaaRTV2, Vector2(static_cast<float>(width), static_cast<float>(height)));
	//AA

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D4))
	{
		context->RSSetState(cullBack);
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		noTriangles += quad->Copy(context, smaaSRV1);
		updateTitle(noTriangles);
		return;
	}

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D5))
	{
		context->RSSetState(cullBack);
		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), nullptr);
		noTriangles += quad->Copy(context, smaaSRV2);
		updateTitle(noTriangles);
		return;
	}

	updateTitle(noTriangles);
}

void HouseDemo::updateTitle(unsigned int noTriangles)
	ULTIMA_NOEXCEPT
{
	isShowingFPS = true;
	title = "House Demo | Triangles: " + std::to_string(noTriangles)
		+ " | Shadow Type: ";

	switch (shadowFilter.Type)
	{
	case (ShadowType::None) :
	{
		title += "None";
		break;
	}
	case (ShadowType::PCF) :
	{
		title += "PCF";
		break;
	}
	case (ShadowType::VSM) :
	{
		title += "VSM";
		break;
	}
	case (ShadowType::EVSM) :
	{
		title += "EVSM";
		break;
	}
	case (ShadowType::EVSM4) :
	{
		title += "EVSM4";
		break;
	}
	default:
	{
		title + "None";
		break;
	}
	}

	title += " | AO: ";
	if (aoFilter.Type == AmbientOcclusionType::None)
		title += "None";
	else if (aoFilter.Type == AmbientOcclusionType::SSAO)
		title += "SSAO";
	else
		title += "HBAO";

	title += " | AA: ";
	if (aaFilter.Type == AntiAliasingType::None)
		title += "None";
	else if (aaFilter.Type == AntiAliasingType::FXAA)
		title += "FXAA";
	else if (aaFilter.Type == AntiAliasingType::SMAA)
		title += "SMAA";

	title += " | Tone: ";

	switch (toneMappingFilter.Type)
	{
	case (ToneMappingType::None) :
	{
		title += "None";
		break;
	}
	case (ToneMappingType::Linear) :
	{
		title += "Linear";
		break;
	}
	case (ToneMappingType::Reinhard) :
	{
		title += "Reinhard";
		break;
	}
	case (ToneMappingType::Reinhard2) :
	{
		title += "Reinhard2";
		break;
	}
	case (ToneMappingType::Hejl) :
	{
		title += "Hejl";
		break;
	}
	case (ToneMappingType::Hable) :
	{
		title += "Hable";
		break;
	}
	default:
	{
		title + "None";
		break;
	}
	}

	title += " | Bloom: ";

	if (bloomFilter.IsActive)
		title += "ON";
	else
		title += "OFF";
}