#pragma once

#include "Color.h"
#include "LuaAgent.h"

using namespace Ultima;

class Merchant : public LuaAgent
{
public:
	Merchant(
		const std::string& scriptFileName,
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const Vector3& position = Vector3::Zero,
		const Matrix& rotation = Matrix::Identity,
		float scale = 1.0f,
		unsigned int anisotropicSampleCount = 4);

	virtual ~Merchant();

	void Update(float deltaTime) override;

	bool HasProduce() const;

	const Color& GetTintColor() const;

	void SetTintColor(const Color& tintColor);

protected:
	Color tintColor;
};

