#pragma once

#include "Application.h"

#include "FreeCamera3D.h"
#include "Model.h"

using namespace Ultima;

class SmaaDemo : public Application
{
public:
	SmaaDemo(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title,
		unsigned int width = 1280,
		unsigned int height = 720,
		bool isInVsync = false,
		bool isShowingFps = true,
		unsigned int antiAliasingSampleCount = 4);

	virtual ~SmaaDemo();

	void OnResize() 
		ULTIMA_NOEXCEPT override;

	void Update(float deltaTime) 
		ULTIMA_NOEXCEPT override;

	void Render(float deltaTime) 
		ULTIMA_NOEXCEPT override;

protected:
	AntiAliasingFilter aaFilter;

	ID3D11RenderTargetView* smaaRTV1;
	ID3D11ShaderResourceView* smaaSRV1;

	ID3D11RenderTargetView* smaaRTV2;
	ID3D11ShaderResourceView* smaaSRV2;

	ID3D11RenderTargetView* imageRTV;
	ID3D11ShaderResourceView* imageSRV;

	std::vector<Model*> models;
	FreeCamera3D freeCamera;
};