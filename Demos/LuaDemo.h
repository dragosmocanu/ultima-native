#pragma once

#include "Application.h"
#include "Camera3D.h"
#include "Color.h"
#include "Fog.h"
#include "FollowCamera3D.h"
#include "FreeCamera3D.h"
#include "LuaAgent.h"
#include "LuaScript.h"
#include "Matrix.h"
#include "RandomColorGenerator.h"
#include "ShadowFilter.h"
#include "Skybox.h"

using namespace Ultima;

#include "Client.h"
#include "Merchant.h"

class LuaDemo : public Application
{
public:

#if DIRECTX
	LuaDemo(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title,
		unsigned int width = 1280,
		unsigned int height = 720,
		bool isInVsync = false,
		bool isShowingFps = true,
		unsigned int antiAliasingSampleCount = 4);
#endif

	~LuaDemo();

	void OnResize() 
		ULTIMA_NOEXCEPT override;

	void Update(float deltaTime)
		ULTIMA_NOEXCEPT override;

	void Render(float deltaTime) 
		ULTIMA_NOEXCEPT override;

protected:
	LuaScript* environment;
	FreeCamera3D camera;
	std::vector<Client*> clients;
	std::vector<Merchant*> merchants;
	bool isSimulating;
};