#include "stdafx.h"

#include "LuaDemo.h"

LuaDemo::LuaDemo(
	const HINSTANCE& hInstance,
	const HINSTANCE& prevInstance,
	const LPWSTR& cmdLine,
	int cmdShow,
	const std::string& title,
	unsigned int width,
	unsigned int height,
	bool isInVsync,
	bool isShowingFPS,
	unsigned int antiAliasingSampleCount)
	: Application(hInstance, prevInstance, cmdLine, cmdShow,
	title, width, height, isInVsync, isShowingFPS, antiAliasingSampleCount)
{
	ContentManager::GetInstance().SetSmartContentManagementType(SmartContentManagementType::Manual);

	this->title = "Lua Demo";

	isSimulating = false;

	environment = new LuaScript("Environment.lua");

	int noMerchants, noClients, noTypes,
		quantityStart, quantityEnd;
	float maxRangeStart, maxRangeEnd, 
		minRangeStart, minRangeEnd, 
		priceStart, priceEnd,
		moneyStart, moneyEnd, bargainTime;

	noMerchants = static_cast<int>(
		environment->GetGlobalNumber("NumberOfMerchants"));
	noClients = static_cast<int>(
		environment->GetGlobalNumber("NumberOfClients"));
	noTypes = static_cast<int>(
		environment->GetGlobalNumber("NumberOfProduceTypes"));

	maxRangeStart = static_cast<float>(
		environment->GetGlobalNumber("MaxRangeStart"));
	maxRangeEnd = static_cast<float>(
		environment->GetGlobalNumber("MaxRangeEnd"));
	minRangeStart = static_cast<float>(
		environment->GetGlobalNumber("MinRangeStart"));
	minRangeEnd = static_cast<float>(
		environment->GetGlobalNumber("MinRangeEnd"));
	priceStart = static_cast<float>(
		environment->GetGlobalNumber("PriceStart"));
	priceEnd = static_cast<float>(
		environment->GetGlobalNumber("PriceEnd"));
	moneyStart = static_cast<float>(
		environment->GetGlobalNumber("MoneyStart"));
	moneyEnd = static_cast<float>(
		environment->GetGlobalNumber("MoneyEnd"));
	quantityStart = static_cast<int>(
		environment->GetGlobalNumber("QuantityStart"));
	quantityEnd = static_cast<int>(
		environment->GetGlobalNumber("QuantityEnd"));
	bargainTime = static_cast<float>(
		environment->GetGlobalNumber("BargainTime"));

	camera = FreeCamera3D(aspectRatio);

	int i;
	float price, min, max;
	bool wasAdded = false;

	for (i = 0; i < noClients; i++)
	{
		clients.push_back(new Client(
			"Client.lua", 
			device, 
			"Assets/sphere.FBX", 
			"Assets/desert.dds",
			Vector3(
				RandomNumberGenerator::GetInstance().Generate(0, 1000), 
				-320, 
				RandomNumberGenerator::GetInstance().Generate(0, 1000))));

		clients[i]->Parse("Money = MoneyOriginal = " 
			+ std::to_string(
				RandomNumberGenerator::GetInstance().GenerateInclusive(
					moneyStart, moneyEnd)));

		int arraySize = RandomNumberGenerator::GetInstance().GenerateInclusive(
			1, noTypes);

		clients[i]->Parse("ArraySize = " + std::to_string(arraySize));

		clients[i]->SetBargainTime(bargainTime);

		std::vector<int> added;

		double priority, priorityTotal;
		priorityTotal = 0;

		for (int j = arraySize - 1; j >= 0; j--)
		{
			wasAdded = false;

			int randID = RandomNumberGenerator::GetInstance().Generate(0, arraySize);

			for (int r = added.size() - 1; r >= 0; r--)
			{
				if (randID == added[r])
				{
					wasAdded = true;
					break;
				}
			}

			if (wasAdded)
			{
				j++;
				continue;
			}

			added.push_back(randID);

			clients[i]->SetGlobalNumber(
				"ProduceTypes[" + std::to_string(j) + "]",
				randID);

			clients[i]->Parse("Quantities[" + std::to_string(j) + "] = 0");

			price = RandomNumberGenerator::GetInstance().Generate(
				priceStart, priceEnd);

			max = price *
				(1.0f + RandomNumberGenerator::GetInstance().Generate(
				maxRangeStart, maxRangeEnd));

			clients[i]->SetGlobalNumber(
				"PriceAverages[" + std::to_string(j) + "]", price);
			clients[i]->SetGlobalNumber(
				"PriceMaxes[" + std::to_string(j) + "]", max);

			priority = RandomNumberGenerator::GetInstance().Generate();
			priorityTotal += priority;

			clients[i]->SetGlobalNumber(
				"Priorities[" + std::to_string(j) + "]",
				priority);
		}

		std::string str;

		for (int j = arraySize - 1; j >= 0; j--)
		{
			str = "Priorities[" + std::to_string(j) + "]";

			clients[i]->SetGlobalNumber(
				str,
				static_cast<float>(clients[i]->GetGlobalNumber(str) / priorityTotal));
		}
	}

	for (i = 0; i < noMerchants; i++)
	{
		merchants.push_back(new Merchant(
			"Merchant.lua", 
			device, 
			"Assets/sphere.FBX", 
			"Assets/desert.dds",
			Vector3(
			RandomNumberGenerator::GetInstance().Generate(0, 1000),
			-320,
			RandomNumberGenerator::GetInstance().Generate(0, 1000))));

		merchants[i]->SetGlobalNumber(
			"Money",
			RandomNumberGenerator::GetInstance().GenerateInclusive(
			moneyStart, moneyEnd));

		int arraySize = RandomNumberGenerator::GetInstance().GenerateInclusive(
			1, noTypes);

		merchants[i]->Parse("ArraySize = " + std::to_string(arraySize));

		std::vector<int> added;

		for (int j = arraySize - 1; j >= 0; j--)
		{
			wasAdded = false;

			int randID = RandomNumberGenerator::GetInstance().Generate(0, arraySize);

			for (int r = added.size() - 1; r >= 0; r--)
			{
				if (randID == added[r])
				{
					wasAdded = true;
					break;
				}
			}

			if (wasAdded)
			{
				j++;
				continue;
			}

			added.push_back(randID);

			merchants[i]->SetGlobalNumber(
				"ProduceTypes[" + std::to_string(j) + "]",
				randID);

			merchants[i]->Parse("Quantities[" + std::to_string(j) + "] = "
				+ std::to_string(RandomNumberGenerator::GetInstance().
				GenerateInclusive(
					quantityStart, quantityEnd)));

			price = RandomNumberGenerator::GetInstance().Generate(
				priceStart, priceEnd);

			min = price *
				(1.0f - RandomNumberGenerator::GetInstance().Generate(
				minRangeStart, minRangeEnd));

			merchants[i]->SetGlobalNumber(
				"PriceAverages[" + std::to_string(j) + "]", price);
			merchants[i]->SetGlobalNumber(
				"PriceMins[" + std::to_string(j) + "]", min);

			double d = merchants[i]->GetGlobalNumber(
				"Quantities[" + std::to_string(j) + "]");
			d = 0;
		}
	}
}

LuaDemo::~LuaDemo()
{
	for (int i = clients.size() - 1; i >= 0; i--)
	{
		Client* temp = clients[i];
		delete temp;
		clients.erase(clients.begin() + i);
	}

	for (int i = merchants.size() - 1; i >= 0; i--)
	{
		Merchant* temp = merchants[i];
		delete temp;
		merchants.erase(merchants.begin() + i);
	}

	delete environment;
}

void LuaDemo::OnResize()
ULTIMA_NOEXCEPT
{
	camera.Reinitialize(aspectRatio);

	Application::OnResize();
}

void LuaDemo::Update(float deltaTime)
ULTIMA_NOEXCEPT
{
	Timer t1;

	//REGION cam update
	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::W))
		camera.MoveZ(0.17f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::S))
		camera.MoveZ(-0.17f * deltaTime);

	if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::D))
		camera.MoveX(0.17f * deltaTime);
	else if (InputManager::GetInstance().GetKeyboard()->IsPressing(Key::A))
		camera.MoveX(-0.17f * deltaTime);

	if (InputManager::GetInstance().GetMouse()->IsPressing(1))
	{
		camera.RotateY(
			-InputManager::GetInstance().GetMouse()->GetDeltaX() / 170.0f);
		camera.RotateX(
			-InputManager::GetInstance().GetMouse()->GetDeltaY() / 170.0f);
	}

	camera.Update(deltaTime);
	//ENDREGION cam update

	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Space))
	{
		isSimulating = !isSimulating;
	}

	//
	if (InputManager::GetInstance().GetKeyboard()->HasPressed(Key::Escape))
	{
		isRunning = false;
	}
	//

	Vector3 v;
	std::vector<int> targets;

	for (auto& x : clients)
	{
		if (!isSimulating)
			break;
		
		x->Update(deltaTime, &merchants);
	}

	unsigned int count = 0;
	bool isDone = false;;

	for (int i = clients.size() - 1; i >= 0; i--)
	{
		if (clients[i]->HasFinishedShopping())
		{
			count++;
		}
	}

	if (count == clients.size())
		isDone = true;

	count = 0;

	for (int i = merchants.size() - 1; i >= 0; i--)
	{
		if (!merchants[i]->HasProduce())
		{
			merchants[i]->SetTintColor(Color::XboxGreen);
			count++;
		}
	}
	
	if (count == merchants.size())
		isDone = true;

	title = "Lua Demo | Simulation: ";

	if (isSimulating)
		title += "ON";
	else
		title += "OFF";

	title += " | Finished: ";

	if (isDone)
		title += "Y";
	else
		title += "N";

	t1.Update();
	title += " | T1: " + std::to_string(t1.GetDeltaTime());
}

void LuaDemo::Render(float deltaTime)
ULTIMA_NOEXCEPT
{
	UNREFERENCED_PARAMETER(deltaTime);

	Timer t1;
	t1.Restart();

	Clear(Color::AtomicTangerine);

	for (auto& x : clients)
		x->Render(
			context, 
			camera, 
			Light(), 
			Fog(), 
			GraphicsFilter(true, 0.5f, 0.5f, x->GetTintColor()));

	for (auto& x : merchants)
		x->Render(
			context,
			camera,
			Light(),
			Fog(),
			GraphicsFilter(true, 0.5f, 0.5f, x->GetTintColor()));

	t1.Update();
	title += " | T2: " + std::to_string(t1.GetDeltaTime());
}