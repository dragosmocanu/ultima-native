#pragma once

#include "Application.h"
#include "FreeCamera3D.h"
#include "GraphicsFilter.h"
#include "Light.h"
#include "Model.h"
#include "Sound.h"
#include "Sound3D.h"
#include "Quad.h"

using namespace Ultima;

class SsaoDemo : public Application
{
public:
	SsaoDemo(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title,
		unsigned int width = 1280,
		unsigned int height = 720,
		bool isInVsync = false,
		bool isShowingFps = true,
		unsigned int antiAliasingSampleCount = 4);

	~SsaoDemo();

	void OnResize() 
		ULTIMA_NOEXCEPT override;

	void Update(float deltaTime) 
		ULTIMA_NOEXCEPT override;

	void Render(float deltaTime) 
		ULTIMA_NOEXCEPT override;

protected:

	void buildTextures() 
		ULTIMA_NOEXCEPT override;

	std::vector<Model*> models;

	FreeCamera3D camera;
	Light light;
	AntiAliasingFilter aaFilter;
	AmbientOcclusionFilter aoFilter;
	Sound* sound;
	Sound3D* sound3D;
	GraphicsFilter graphicsFilter;

	ID3D11RenderTargetView* aoRTV;
	ID3D11ShaderResourceView* aoSRV;
	ID3D11RenderTargetView* aaRTV;
	ID3D11ShaderResourceView* aaSRV;
	ID3D11RenderTargetView* blurRTV;
	ID3D11ShaderResourceView* blurSRV;
	ID3D11RenderTargetView* blurRTV2;
	ID3D11ShaderResourceView* blurSRV2;

	ID3D11RenderTargetView* renderMapsRTV[2];
	ID3D11ShaderResourceView* renderMapsSRV[2];

	bool isShowingOnlyAO;
};

