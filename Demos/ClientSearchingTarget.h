#pragma once

#include "IState.h"

using namespace Ultima;

class ClientSearchingTarget : public IState
{
public:
	ClientSearchingTarget();
	~ClientSearchingTarget();

	void Update(float deltaTime, StateManager& stateManager)
		ULTIMA_NOEXCEPT override;
};

