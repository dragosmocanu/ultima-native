#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "TextureDx11.h"
namespace Ultima
{
    class Texture : public TextureDx11
    {
    public:
        ULTIMAGRAPHICS_API Texture(const Device* device = nullptr) : TextureDx11(device) { }
    };
}
#endif