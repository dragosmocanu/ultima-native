#pragma once

#include "Macros.h"

namespace Ultima
{
    /*!
    Defines the texture formats that can be used in the engine.
    */
    enum class TextureFormat
    {
        R8,
        R8G8,
        R8G8B8A8,
        R32,
        R32G32,
        R32G32B32A32,
        D32,
        D24S8,
        R11G11B10,
        R10G10B10A2,
        R16,
        R16G16,
        R16G16B16A16,
    };

    /*!
    Defines the view dimensions that can be used in the engine.
    */
    enum class TextureViewDimension
    {
        OneDimensional,
        TwoDimensional,
        ThreeDimensional,
        Cube,
        Array,
    };
}