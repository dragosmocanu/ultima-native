#pragma once

#include "Macros.h"

#include "Vector2.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines the different ambient occlusion algorithms.
	*/
	enum class AmbientOcclusionType
	{
		None, //No ambient occlusion
		SSAO, //Screen Space Ambient Occlusion
		HBAO //Horizon Based Ambient Occlusion
	};

	/*!
	Defines a structure that holds all the AO settings needed for rendering.
	SSAO & HBAO implementations.
	*/
	struct AmbientOcclusionFilter
	{
	public:
		/*!
		Creates a new instance of AmbientOcclusionFilter.
		*/
		ULTIMAGRAPHICS_API AmbientOcclusionFilter(
			AmbientOcclusionType type = AmbientOcclusionType::SSAO,
			float intensity = 1.0f,
			float ssaoSampleRadius = 0.3f,
			float ssaoScale = 2.2f,
			float ssaoBias = 0.001f,
			unsigned int hbaoNumberOfSteps = 5,
			unsigned int hbaoNumberOfDirections = 1,
			float hbaoR = 5.0f,
			float hbaoAngleBias = 0.008f,
			float hbaoMaxRadiusPixels = 170.0f,
			Vector2 hbaoFocalLen = 2.7f)
			ULTIMA_NOEXCEPT;

		AmbientOcclusionType Type;
		float Intensity;
		float SsaoSampleRadius;
		float SsaoScale;

		float SsaoBias;
		unsigned int HbaoNumberOfSteps;
		unsigned int HbaoNumberOfDirections;
		float HbaoR;

		float HbaoAngleBias;
		float HbaoMaxRadiusPixels;
		XMFLOAT2 HbaoFocalLen;
	};
}