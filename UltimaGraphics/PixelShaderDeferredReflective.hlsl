#include "BumpMappingFilter.hlsli"
#include "Fog.hlsli"
#include "GraphicsFilter.hlsli"
#include "Material.hlsli"
#include "ShadowFilter.hlsli"

Texture2D modelTexture : register(t0);
Texture2D normalTexture : register(t1);
TextureCube reflectanceTexture : register(t2);

SamplerState anisotropicSampler : register(s0);

cbuffer cb0 : register(b0)
{
	Material material;
}

cbuffer cb1 : register(b1)
{
	BumpMappingFilter bumpMappingFilter;
}

cbuffer cb2 : register(b2)
{
	float3 camPos;
}

cbuffer cb3 : register(b3)
{
	bool hasTexture;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 NormalW : NORMAL;
	float3 PositionV : POSITIONV;
	float3 NormalV : NORMALV;
	float2 Texture : TEXCOORD0;
	float3 TangentW : TANGENT;
};

struct PixelOut
{
	float4 Color : SV_TARGET0;
	float4 Position : SV_TARGET1;
	float4 Normal : SV_TARGET2;
	float4 Material : SV_TARGET3;
	float4 PositionV : SV_TARGET4;
	float4 NormalV : SV_TARGET5;
};

PixelOut main(VertexOut input)
{
	input.NormalW = normalize(input.NormalW);

	input.NormalW = bumpMappingFilter.Process(
		normalTexture,
		anisotropicSampler,
		input.Texture,
		input.NormalW,
		input.TangentW);

	float4 output = float4(1.0f, 1.0f, 1.0f, 1.0f); 
		
	if (hasTexture)
		output = modelTexture.Sample(anisotropicSampler, input.Texture);

	float3 toEye = camPos - input.PositionW;
	float distanceToEye = length(toEye);
	toEye /= distanceToEye;

	float3 reflectionVector = reflect(-toEye, input.NormalW);
	float4 reflectionColor = reflectanceTexture.Sample(anisotropicSampler,
		reflectionVector);
	float alpha = material.Metallic * (1.0f - material.Roughness);
	alpha = alpha * alpha * alpha * alpha * alpha;
	output = lerp(output, reflectionColor, alpha);

	PixelOut po = (PixelOut)0;
	po.Color = output;
	po.Position.xyz = input.PositionW;
	po.Normal.xyz = input.NormalW;
	po.Material = material.Base;
	po.Position.w = material.Metallic;
	po.Normal.w = material.Roughness;
	po.PositionV.xyz = input.PositionV;
	po.NormalV.xyz = input.NormalV;

	return po;
}