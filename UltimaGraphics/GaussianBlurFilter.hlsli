#ifndef GAUSSIANBLURFILTER_HLSLI
#define GUASSIANBLURFILTER_HLSLI

struct GaussianBlurFilter
{
	//http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling/
	float4 Process(
		float2 textureCoordinates,
		SamplerState bilinearSampler,
		Texture2D tex,
		float2 screenSize)
	{
		float4 result = (float4)0;

			float offset[3] = { 0.0f, 1.3846153846f, 3.2307692308f };

		float weight[3] = {
			0.2270270270f,
			0.3162162162f,
			0.0702702703f };

		result = tex.Sample(bilinearSampler, textureCoordinates) * weight[0];

		float2 texelSize = 1.0f / screenSize;

			[flatten]
		if (IsHorizontal)
		{
			[unroll]
			for (int i = 1; i < 3; i++)
			{
				result += tex.Sample(
					bilinearSampler,
					textureCoordinates + float2(0.0f, offset[i]) * texelSize)
					* weight[i];

				result += tex.Sample(
					bilinearSampler,
					textureCoordinates - float2(0.0f, offset[i]) * texelSize)
					* weight[i];
			}
		}
		else
		{
			[unroll]
			for (int i = 1; i < 3; i++)
			{
				result += tex.Sample(
					bilinearSampler,
					textureCoordinates + float2(offset[i], 0.0f) * texelSize)
					* weight[i];

				result += tex.Sample(
					bilinearSampler,
					textureCoordinates - float2(offset[i], 0.0f) * texelSize)
					* weight[i];
			}
		}

		return result;
	}

	bool IsHorizontal;
};

#endif