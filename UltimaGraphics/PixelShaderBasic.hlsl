struct Vertex
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
};

float4 main(Vertex input) : SV_TARGET
{
	return input.Color;
}