struct VertexOut
{
	float4 Position : SV_POSITION;

	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
};

struct PixelOut
{
	float3 Position: SV_TARGET0;
	float3 Normal : SV_TARGET1;
};

PixelOut main(VertexOut input)
{
	PixelOut output;

	output.Position = input.PositionV;
	output.Normal = normalize(input.NormalV);

	return output;
}