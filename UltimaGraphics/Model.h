#pragma once

#include "Macros.h"

#include "BumpMappingFilter.h"
#include "Camera3D.h"
#include "Device.h"
#include "DisplacementMappingFilter.h"
#include "Fog.h"
#include "GraphicsFilter.h"
#include "IManipulable3D.h"
#include "Light.h"
#include "Material.h"
#include "Matrix.h"
#include "Mesh.h"
#include "ShadowFilter.h"
#include "Texture.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a 3D PBR (Physically Based Rendering) model that can be used in a multimedia application.
	In order to free up memory space, the class does not have a Mesh attribute, it only loads a mesh and sets everything up.
	*/
	class Model : public IManipulable3D
	{
	public:
		/*!
		Creates a new instance of Model.
		*/
		ULTIMAGRAPHICS_API Model(
			const Device& device,
			const std::string& meshPath,
			const std::string& texturePath,
			const std::string& bumpMapPath,
			const std::string& displacementMapPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Model.
		*/
		ULTIMAGRAPHICS_API Model(
			const Device& device,
			const std::string& meshPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*
		Copies an instance of Model.
		*/
		ULTIMAGRAPHICS_API Model(const Model& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API Model& operator=(const Model& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~Model()
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Gets the position of the model.
		*/
		ULTIMAGRAPHICS_API virtual const Vector3& GetPosition()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the model.
		*/
		ULTIMAGRAPHICS_API virtual void SetPosition(const Vector3& position)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the rotation of the model.
		*/
		ULTIMAGRAPHICS_API virtual const Matrix& GetRotation()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the model.
		The vector represents the number of radians the rotation will be along each axis.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotation(const Vector3& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the model.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotation(const Matrix& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the scale of the model.
		*/
		ULTIMAGRAPHICS_API virtual float GetScale()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the scale of the model.
		*/
		ULTIMAGRAPHICS_API virtual void SetScale(float scale)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the model along its X axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveX(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the model along its Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveY(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the model along its Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveZ(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the model by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Move(const Vector3& moveVector)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the model along its X axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateX(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the model along its Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateY(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the model along its Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateZ(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the model by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Rotate(const Vector3& rotateVector)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the material object.
		*/
		ULTIMAGRAPHICS_API virtual const Material& GetMaterial()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the material object.
		*/
		ULTIMAGRAPHICS_API void SetMaterial(const Material& material)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the mesh file path of the model.
		*/
		ULTIMAGRAPHICS_API virtual const std::string& GetMeshPath()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the texture file path of the model.
		*/
		ULTIMAGRAPHICS_API virtual const std::string& GetTexturePath()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the bump map file path of the model.
		*/
		ULTIMAGRAPHICS_API virtual const std::string& GetBumpMapPath()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the displacement map file path of the model.
		*/
		ULTIMAGRAPHICS_API virtual const std::string& GetDisplacementMapPath()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets a pointer to the Direct3D graphics device needed for loading graphics assets.
		*/
		ULTIMAGRAPHICS_API virtual const Device* GetDevice()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the sample count of anisotropic filtering passes.
		*/
		ULTIMAGRAPHICS_API unsigned int GetAnisotropicSampleCount()
			const ULTIMA_NOEXCEPT;
		
		/*!
		Gets the bounding sphere of the model.
		*/
		ULTIMAGRAPHICS_API const Sphere& GetSphere()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the bump mapping filter object.
		*/
		ULTIMAGRAPHICS_API const BumpMappingFilter& GetBumpMappingFilter()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the bump mapping filter object.
		*/
		ULTIMAGRAPHICS_API void SetBumpMappingFilter(const BumpMappingFilter& bumpMappingFilter)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the displacement mapping filter object.
		*/
		ULTIMAGRAPHICS_API const DisplacementMappingFilter& GetDisplacementMappingFilter()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the displacement mapping filter object.
		*/
		ULTIMAGRAPHICS_API void SetDisplacementMappingFilter(
			const DisplacementMappingFilter& displacementMappingFilter)
			ULTIMA_NOEXCEPT;

		/*!
		Updates the model logic.
		*/
		ULTIMAGRAPHICS_API virtual void Update(float deltaTime);

		/*!
		Renders the model.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Render(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			const Light& light = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const ambientOcclusionMap = nullptr,
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the model.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Render(
			ID3D11DeviceContext* const context,
			const Matrix& viewProjection,
			const Vector3& cameraPosition,
			const Frustum& frustum,
			const Light& light = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const ambientOcclusionMap = nullptr,
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the depth of the model to a shadow map.
		Returns the number of rendered triangles.
		A shadow/depth map should be set as a render target before calling this function.
		Should be called before the actual Render() function.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderDepth(
			ID3D11DeviceContext* const context,
			const Matrix& lightViewProjection,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the positions and the normals to 2 RTVs.
		Returns the nubmer of rendered triangles.
		Two RTVs (one for positions, one for normals) should be set as render targets before calling this function.
		Should be called before the actual Render() function.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderPositionsAndNormals(
			ID3D11DeviceContext* const context,
			const Camera3D& camera)
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the positions and the normals to 2 RTVs.
		Returns the nubmer of rendered triangles.
		Two RTVs (one for positions, one for normals) should be set as render targets before calling this function.
		Should be called before the actual Render() function.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderPositionsAndNormals(
			ID3D11DeviceContext* const context,
			const Matrix& view,
			const Matrix& projection,
			const Frustum& frustum)
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the color, position, normal (both in world and in view space) and material to six textures.
		Returns the number of rendered triangles.
		Six different HDR textures should be set as a render targets before calling this function.
		Position and normal will be both in world and in view space.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderDeferred(
			ID3D11DeviceContext* const context,
			const Camera3D& camera)
			const ULTIMA_NOEXCEPT;

#endif

	protected:
		/*!
		Loads all the data needed by the model and builds everything.
		*/
		ULTIMAGRAPHICS_API virtual void buildModel()
			ULTIMA_NOEXCEPT;

		/*!
		Releases the textures and cleans up any pointers. Called by the actual destructor and the copy/move constructors.
		*/
		ULTIMAGRAPHICS_API virtual void destructModel()
			ULTIMA_NOEXCEPT;

		Vector3 position;
		Matrix rotation;
		float scale;

		Sphere sphere;

		Material material;

		BumpMappingFilter bumpMappingFilter;
		DisplacementMappingFilter displacementMappingFilter;

		unsigned int numberOfIndices;

#if DIRECTX
		std::string meshPath;
		std::string texturePath;
		std::string bumpMapPath;
		std::string displacementMapPath;

		unsigned int anisotropicSampleCount;

		const Device* device;

		ID3D11InputLayout* inputLayout;

		ID3D11Buffer* vertexBuffer;
		ID3D11Buffer* indexBuffer;

		VertexShader vertexShader;
		PixelShader pixelShader;

        VertexShader vertexShaderPositionsAndNormals;
        PixelShader pixelShaderPositionsAndNormals;

		ID3D11InputLayout* inputLayoutShadow;
        VertexShader vertexShaderShadow;
        PixelShader pixelShaderShadow;

        VertexShader vertexShaderDeferred;
        PixelShader pixelShaderDeferred;

		HullShader hullShaderStandard;
		DomainShader domainShaderStandard;

		HullShader hullShaderPositionsAndNormals;
		DomainShader domainShaderPositionsAndNormals;

		HullShader hullShaderDeferred;
		DomainShader domainShaderDeferred;

		ID3D11Buffer* worldBuffer;
		ID3D11Buffer* viewBuffer;
		ID3D11Buffer* projectionBuffer;

		ID3D11Buffer* lightBuffer;
		ID3D11Buffer* cameraPositionBuffer;
		ID3D11Buffer* materialBuffer;
		ID3D11Buffer* fogBuffer;
		ID3D11Buffer* graphicsFilterBuffer;
		ID3D11Buffer* shadowFilterBuffer;
		ID3D11Buffer* bumpMappingFilterBuffer;
		ID3D11Buffer* displacementMappingFilterBuffer;

		ID3D11Buffer* lightViewProjectionBuffer;

		ID3D11Buffer* ambientOcclusionBuffer;

		ID3D11Buffer* boolBuffer;

		//ID3D11ShaderResourceView* textureSRV;
		//ID3D11ShaderResourceView* normalSRV;
		//ID3D11ShaderResourceView* displacementSRV;
        Texture colorTex;
        Texture normalTex;
        Texture displacementTex;

		ID3D11SamplerState* anisotropicSampler;
		ID3D11SamplerState* comparisonSampler;
		ID3D11SamplerState* bilinearSampler;
#endif
	};
}