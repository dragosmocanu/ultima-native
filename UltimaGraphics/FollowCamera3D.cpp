#include "stdafx.h"

#include "FollowCamera3D.h"

#include "MathExtensions.h"

namespace Ultima
{

	FollowCamera3D::FollowCamera3D(float aspectRatio, float viewDistance)
		ULTIMA_NOEXCEPT
	{
		position = Vector3::Zero;
		view = Matrix::Identity;
		Reinitialize(aspectRatio, viewDistance);
		pitch = 0.0f;
		yaw = 0.0f;
		distance = 100.0f;
		viewProjection = view * projection;
		frustum = Frustum(projection);
	}

	FollowCamera3D::FollowCamera3D(float aspectRatio, float viewDistance, 
		float fieldOfView)
		ULTIMA_NOEXCEPT
	{
		position = Vector3::Zero;
		view = Matrix::Identity;
		projection = Matrix(fieldOfView, aspectRatio, 0.001f, viewDistance);
		//projection.Transpose();
		pitch = 0.0f;
		yaw = 0.0f;
		distance = 100.0f;
		viewProjection = view * projection;
		frustum = Frustum(projection);
	}

	FollowCamera3D::~FollowCamera3D() 
		ULTIMA_NOEXCEPT 
	{ }

	float FollowCamera3D::GetPitch() 
		const ULTIMA_NOEXCEPT
	{
		return pitch;
	}

	void FollowCamera3D::SetPitch(float pitch)
		ULTIMA_NOEXCEPT
	{
		if (pitch < 0)
			pitch += MathExtensions::TwoPi;
		else if (pitch > MathExtensions::TwoPi)
			pitch -= MathExtensions::TwoPi;

		this->pitch = pitch;
	}

	float FollowCamera3D::GetYaw() 
		const ULTIMA_NOEXCEPT
	{
		return yaw;
	}

	void FollowCamera3D::SetYaw(float yaw)
		ULTIMA_NOEXCEPT
	{
		if (yaw < 0)
			yaw += MathExtensions::TwoPi;
		else if (yaw > MathExtensions::TwoPi)
			yaw -= MathExtensions::TwoPi;

		this->yaw = yaw;
	}

	float FollowCamera3D::GetDistance() 
		const ULTIMA_NOEXCEPT
	{
		return distance;
	}

	void FollowCamera3D::SetDistance(float distance)
		ULTIMA_NOEXCEPT
	{
		this->distance = MathExtensions::Clamp(distance, 0, FLT_MAX);
	}

	void FollowCamera3D::SetPosition(const Vector3& position)
		ULTIMA_NOEXCEPT
	{
		this->position = position;
	}

	void FollowCamera3D::SetRotation(const Vector3& rotation)
		ULTIMA_NOEXCEPT
	{
		this->view = Matrix::CreateFromPitchYawRoll(
			rotation.GetX(),
			rotation.GetY(),
			rotation.GetZ());
	}

	void FollowCamera3D::Move(const Vector3& moveVector)
		ULTIMA_NOEXCEPT
	{
		position += moveVector;
	}

	void FollowCamera3D::Rotate(const Vector3& rotateVector)
		ULTIMA_NOEXCEPT
	{
		this->view *= Matrix::CreateFromPitchYawRoll(
			rotateVector.GetX(),
			rotateVector.GetY(),
			rotateVector.GetZ());
	}

	const Frustum& FollowCamera3D::GetFrustum() 
		const ULTIMA_NOEXCEPT
	{
		return frustum;
	}

	void FollowCamera3D::Update(float deltaTime,
		const Vector3& targetPosition, const Matrix& targetRotation)
		ULTIMA_NOEXCEPT
	{
		UNREFERENCED_PARAMETER(deltaTime);

		Matrix newRotation = targetRotation;
		newRotation.SetM30(0.0f);
		newRotation.SetM31(0.0f);
		newRotation.SetM32(0.0f);
		newRotation = Matrix::CreateFromPitchYawRoll(
			pitch,
			yaw,
			0)
			* newRotation;

		Vector3 newPosition = Vector3::Transform(Vector3(0.0f, 0.0f, distance),
			newRotation);
		newPosition += targetPosition;
		position = newPosition;

		Vector3 rotatedUp = Vector3::Transform(Vector3::Up, newRotation);

		view = Matrix::CreateLookAt(position, targetPosition, rotatedUp);

		viewProjection = view * projection;
		
		frustum.Transform(projection, position, view);
	}

	const Matrix& FollowCamera3D::GetViewProjection() 
		const ULTIMA_NOEXCEPT
	{
		return viewProjection;
	}

}