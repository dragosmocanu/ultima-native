Texture2D colorTexture : register(t0);

SamplerState pointSampler : register(s0);

cbuffer cb0 : register(b0)
{
	unsigned int mipLevel;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float luminance = colorTexture.SampleLevel(pointSampler, input.Texture, mipLevel).r;
	luminance /= 20.0f;
	return float4(luminance, luminance, luminance, 1);
}