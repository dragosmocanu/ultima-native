#include "stdafx.h"

#include "BloomFilter.h"

namespace Ultima
{
	BloomFilter::BloomFilter(
		bool isActive,
		float threshold,
		float strength)
		ULTIMA_NOEXCEPT
		: IsActive(isActive),
		Threshold(threshold),
		Strength(strength)
	{ }
}