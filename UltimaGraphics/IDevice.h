#pragma once

#include "Macros.h"

#include "Texture.h"
#include "ISwapChain.h"
#include "ShaderTypes.h"
#include "Sphere.h"

namespace Ultima
{
    /*!
    Defines the interface for a graphics format.
    */
    class IDevice
    {
    public:
        ULTIMAGRAPHICS_API IDevice() { swapChain = nullptr; };

        /*!
        Loads the texture data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadTexture(
            Texture& outTexture,
            const std::string& filePath,
            TextureViewDimension textureViewDimension = TextureViewDimension::TwoDimensional)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the vertex shader data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadVertexShader(
            VertexShader& outVS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the vertex shader data from a given file. Also returns the data as a new char* to be used in constructing an input assembler.
        */
        ULTIMAGRAPHICS_API virtual void LoadVertexShader(
            VertexShader& outVS,
            const std::string& filePath,
            char*& data,
            unsigned int& size)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the pixel shader data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadPixelShader(
            PixelShader& outPS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the hull shader data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadHullShader(
            HullShader& outHS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the domain shader data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadDomainShader(
            DomainShader& outDS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the compute shader data from a given file.
        */
        ULTIMAGRAPHICS_API virtual void LoadComputeShader(
            ComputeShader& outCS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT = 0;

        /*!
        Loads the mesh data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadMesh(
            const std::string& filePath,
            ID3D11Buffer*& outVertexBuffer,
            ID3D11Buffer*& outIndexBuffer,
            unsigned int& outNumberOfIndices,
            Sphere& outSphere)
            const ULTIMA_NOEXCEPT;

    protected:
        ISwapChain* swapChain;
    };
}