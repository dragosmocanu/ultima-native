#include "GaussianBlurFilter.hlsli"

Texture2D colorTexture : register(t0);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	GaussianBlurFilter blurFilter;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float2 screenSize;
	colorTexture.GetDimensions(screenSize.x, screenSize.y);

	float4 output = blurFilter.Process(
		input.Texture, bilinearSampler, colorTexture, screenSize);

	return output;
}