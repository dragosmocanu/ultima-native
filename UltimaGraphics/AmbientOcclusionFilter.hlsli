#ifndef AMBIENTOCCLUSIONFILTER_HLSLI
#define AMBIENTOCCLUSIONFILTER_HLSLI

#pragma warning (disable : 4000)
#pragma warning (disable : 4121)

#define USE_NORMAL_FREE_HBAO 0
#define SAMPLE_FIRST_STEP 0
#include "HBAO.hlsli"

struct AmbientOcclusionFilter
{
	float Process(
		Texture2D positionTexture,
		Texture2D normalTexture,
		SamplerState bilinearSampler,
		float2 textureCoordinates,
		float2 screenSize)
	{
		float output = 1.0f;

		if (Type == 1) //SSAO - http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/a-simple-and-practical-approach-to-ssao-r2753
		{
			output = 0.0f;

			float2 vec[4] =
			{
				float2(1.0f, 0.0f),
				float2(-1.0f, 0.0f),
				float2(0.0f, 1.0f),
				float2(0.0f, -1.0f)
			};

			float3 position =
				positionTexture.Sample(bilinearSampler, textureCoordinates).xyz;

			float3 normal =
				normalTexture.Sample(bilinearSampler, textureCoordinates).xyz;

			[flatten]
			if (normal.x == 0 && normal.y == 0 && normal.z == 0)
				return 1.0f;

			normal = normalize(normal);

			float radius = SsaoSampleRadius / position.z;

			[unroll]
			for (int i = 0; i < 4; i++)
			{
				float2 coord1 = reflect(vec[i], float2(0.7f, 0.7f)) * radius;

				float2 coord2 = float2(coord1.x * 0.707f - coord1.y * 0.707f,
					coord1.x * 0.707f + coord1.y * 0.707f);

				float3 diff, v;
				float d;

				diff = positionTexture.Sample(bilinearSampler,
					textureCoordinates + coord1 * 0.25f).xyz - position;
				v = normalize(diff);
				d = length(diff) * SsaoScale;
				output += max(0.0f, dot(normal, v) - SsaoBias) * (1.0f / (1.0f + d))
					* Intensity;

				diff = positionTexture.Sample(bilinearSampler,
					textureCoordinates + coord2 * 0.50f).xyz - position;
				v = normalize(diff);
				d = length(diff) * SsaoScale;
				output += max(0.0f, dot(normal, v) - SsaoBias) * (1.0f / (1.0f + d))
					* Intensity;

				diff = positionTexture.Sample(bilinearSampler,
					textureCoordinates + coord1 * 0.75f).xyz - position;
				v = normalize(diff);
				d = length(diff) * SsaoScale;
				output += max(0.0f, dot(normal, v) - SsaoBias) * (1.0f / (1.0f + d))
					* Intensity;

				diff = positionTexture.Sample(bilinearSampler,
					textureCoordinates + coord2).xyz - position;
				v = normalize(diff);
				d = length(diff) * SsaoScale;
				output += max(0.0f, dot(normal, v) - SsaoBias) * (1.0f / (1.0f + d))
					* Intensity;
			}

			output = 1.0f - saturate(output / 16.0f);

			return output;
		}
		else if (Type == 2) //HBAO - http://developer.download.nvidia.com/presentations/2008/SIGGRAPH/HBAO_SIG08b.pdf
		{
			output = 1.0f;

			float3 random = float3(0.00710666925f, -0.0928933322f, 0.0f);

			float2 texelSize = 1.0f / screenSize;

			float3 P = positionTexture.Sample(
				bilinearSampler,
				textureCoordinates).xyz;

			float2 ray_radius_uv = 0.5 * HbaoR * HbaoFocalLen / P.z;
				float ray_radius_pix = ray_radius_uv.x * screenSize.x;

			if (ray_radius_pix < 1)
				return output;

			float R2 = HbaoR * HbaoR;
			float NegativeInverseR2 = -1.0f / R2;
			float TanAngleBias = tan(HbaoAngleBias);

			float noDirections = (float)HbaoNumberOfDirections;
			float noSteps = (float)HbaoNumberOfSteps;

			float numSteps;
			float2 step_size;
			ComputeSteps(step_size, numSteps, ray_radius_pix, random.z, noSteps, HbaoMaxRadiusPixels, texelSize);

			// Nearest neighbor pixels on the tangent plane
			float3 Pr, Pl, Pt, Pb;
			Pr = FetchEyePos(textureCoordinates + float2(texelSize.x, 0), positionTexture, bilinearSampler);
			Pl = FetchEyePos(textureCoordinates + float2(-texelSize.x, 0), positionTexture, bilinearSampler);
			Pt = FetchEyePos(textureCoordinates + float2(0, texelSize.y), positionTexture, bilinearSampler);
			Pb = FetchEyePos(textureCoordinates + float2(0, -texelSize.y), positionTexture, bilinearSampler);

			// Screen-aligned basis for the tangent plane
			float3 dPdu = MinDiff(P, Pr, Pl);
			float3 dPdv = MinDiff(P, Pt, Pb) * (screenSize.y * texelSize.x);

			float ao = 0;
			float d;
			float alpha = 2.0f * 3.14159f / noDirections;

#if USE_NORMAL_FREE_HBAO
			[unroll(1)]
			for (d = 0; d < 3; d++)
			{
				float angle = alpha * d;
				float2 dir = RotateDirections(float2(cos(angle), sin(angle)), random.xy);
				float2 deltaUV = dir * step_size.xy;
				float2 texelDeltaUV = dir * g_InvAOResolution;
				ao += NormalFreeHorizonOcclusion(deltaUV, texelDeltaUV, input.Texture, P, numSteps, random.z);
			}
#else
			[unroll(1)]
			for (d = 0; d < noDirections; d++)
			{
				float angle = alpha * d;
				float2 dir = RotateDirections(float2(cos(angle), sin(angle)), random.xy);
				float2 deltaUV = dir * step_size.xy;
				float2 texelDeltaUV = dir * texelSize;
				ao += horizon_occlusion(deltaUV, texelDeltaUV, textureCoordinates, P, numSteps, random.z, dPdu, dPdv,
					screenSize,
					texelSize,
					TanAngleBias,
					R2,
					NegativeInverseR2,
					positionTexture, 
					bilinearSampler);
			}
#endif

			output = 1.0f - saturate(ao / noDirections * Intensity);
		}

		return output;
	}

	unsigned int Type;
	float Intensity;
	float SsaoSampleRadius;
	float SsaoScale;

	float SsaoBias;
	unsigned int HbaoNumberOfSteps;
	unsigned int HbaoNumberOfDirections;
	float HbaoR;

	float HbaoAngleBias;
	float HbaoMaxRadiusPixels;
	float2 HbaoFocalLen;
};

#endif