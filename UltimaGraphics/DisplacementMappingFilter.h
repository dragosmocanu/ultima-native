#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines a structure that holds all the displacement mapping parameters.
	*/
	struct DisplacementMappingFilter
	{
	public:
		/*!
		Creates a new instance of DisplacementMappingFilter.
		*/
		ULTIMAGRAPHICS_API DisplacementMappingFilter(
			bool isActive = true,
			float bias = 0.0f,
			float factor = 1.0f)
			ULTIMA_NOEXCEPT;

		float IsActive;
		float Bias;
		float Factor;
	};
}