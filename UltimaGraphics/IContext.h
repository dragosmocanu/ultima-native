#pragma once

#include "Macros.h"

#include "Color.h"
#include "IDevice.h"
#include "TextureBase.h"

namespace Ultima
{
    enum ClearDepthStencilFlag
    {
        Depth   = (1 << 0),
        Stencil = (1 << 1),
    };

    /*!
    Defines the interface for a device context used in graphics rendering.
    */
    class IContext
    {
    public:
        /*!
        Clears a color render target.
        */
        ULTIMAGRAPHICS_API virtual void ClearColor(TextureBase& texture, Color& color)
            ULTIMA_NOEXCEPT = 0;
      
        /*!
        Clears a depth/stencil render target.
        */
        ULTIMAGRAPHICS_API virtual void ClearDepthStencil(TextureBase& texture, ClearDepthStencilFlag flag, float depthVal, char stencilVal)
            ULTIMA_NOEXCEPT = 0;

    protected:
        Device& device;
    };
}