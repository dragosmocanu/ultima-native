#ifndef MATERIAL_HLSLI
#define MATERIAL_HLSLI

struct Material
{
	float4 Modulate(
		float4 color,
		float4 diffuse,
		float4 specular,
		float4 ambient)
	{
		color = color * this.Base * (ambient + diffuse) + specular;

		return clamp(color, 0.0f, 15.0f);
	}

	float4 Base;
	float Metallic;
	float Roughness;
};

#endif