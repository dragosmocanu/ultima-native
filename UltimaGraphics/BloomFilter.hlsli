#ifndef BLOOMFILTER_HLSLI
#define BLOOMFILTER_HLSLI

#include "HDR.hlsli"

struct BloomFilter
{
	float3 Compute(
		Texture2D colorTexture,
		Texture2D luminanceTexture,
		SamplerState bilinearSampler,
		SamplerState pointSampler,
		float2 textureCoordinates,
		unsigned int mipLevel)
	{
		float3 output = (float3)0;

		if (IsActive)
		{
			output = colorTexture.Sample(bilinearSampler, textureCoordinates).rgb;

			float avgLuminance = luminanceTexture.SampleLevel(
				pointSampler, textureCoordinates, mipLevel).r;

			output = CalculateExposedColor(output, avgLuminance, Threshold);
		}

		return output;
	}

	float3 Process(
		Texture2D colorTexture,
		SamplerState bilinearSampler,
		float2 textureCoordinates)
	{
		float3 output = (float3)0;

		if (IsActive)
			output = colorTexture.Sample(bilinearSampler, textureCoordinates).rgb
				* Strength;

		return output;
	}

	bool IsActive;
	float Threshold;
	float Strength;
};

#endif