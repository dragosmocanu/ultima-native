#include "DisplacementMappingFilter.hlsli"

Texture2D displacementTexture : register(t0);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	row_major matrix view;
}

cbuffer cb1 : register(b1)
{
	row_major matrix projection;
}

cbuffer cb2 : register(b2)
{
	DisplacementMappingFilter dmf;
}

// Output control point
struct HS_CONTROL_POINT_OUTPUT
{
	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
	float2 Texture : TEXCOORD3;
};

struct DS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
};

// Output patch constant data.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]			: SV_TessFactor; // e.g. would be [4] for a quad domain
	float InsideTessFactor : SV_InsideTessFactor; // e.g. would be Inside[2] for a quad domain
	// TODO: change/add other stuff
};

#define NUM_CONTROL_POINTS 3

[domain("tri")]
DS_OUTPUT main(
	HS_CONSTANT_DATA_OUTPUT input,
	float3 domain : SV_DomainLocation,
	const OutputPatch<HS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> patch)
{
	DS_OUTPUT output = (DS_OUTPUT)0;

	output.PositionV = domain.x * patch[0].PositionV
		+ domain.y * patch[1].PositionV
		+ domain.z * patch[2].PositionV;

	output.NormalV = domain.x * patch[0].NormalV
		+ domain.y * patch[1].NormalV
		+ domain.z * patch[2].NormalV;

	float2 texCoord = domain.x * patch[0].Texture
		+ domain.y * patch[1].Texture
		+ domain.z * patch[2].Texture;

	output.PositionV = dmf.Process(displacementTexture, bilinearSampler, texCoord,
		output.PositionV, output.NormalV);

	output.Position = mul(float4(output.PositionV, 1.0f), projection);

	return output;
}
