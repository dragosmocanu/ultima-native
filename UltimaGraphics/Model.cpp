#include "stdafx.h"

#include "Model.h"

#include "ContentManager.h"
#include "Utilities.h"

namespace Ultima
{
	Model::Model(
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const std::string& bumpMapPath,
		const std::string& displacementMapPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount)
		ULTIMA_NOEXCEPT
		: position(position),
		rotation(rotation),
		scale(scale),
		meshPath(meshPath),
		texturePath(texturePath),
		bumpMapPath(bumpMapPath),
		displacementMapPath(displacementMapPath),
		device(&device),
		anisotropicSampleCount(anisotropicSampleCount),
		bumpMappingFilter(false)
	{
		buildModel();
	}

	Model::Model(
		const Device& device,
		const std::string& meshPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount)
		ULTIMA_NOEXCEPT
		: position(position),
		rotation(rotation),
		scale(scale),
		meshPath(meshPath),
		texturePath(""),
		bumpMapPath(""),
		displacementMapPath(""),
		device(&device),
		anisotropicSampleCount(anisotropicSampleCount),
		bumpMappingFilter(false)
	{
		buildModel();
	}

	Model::Model(const Model& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Model& Model::operator=(const Model& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			destructModel();

			position = other.GetPosition();
			rotation = other.GetRotation();
			scale = other.GetScale();
			material = other.GetMaterial();
			meshPath = other.GetMeshPath();
			texturePath = other.GetTexturePath();
			bumpMapPath = other.GetBumpMapPath();
			displacementMapPath = other.GetDisplacementMapPath();
			device = other.GetDevice();
			anisotropicSampleCount = other.GetAnisotropicSampleCount();

			buildModel();
		}

		return *this;
	}

	void Model::buildModel()
		ULTIMA_NOEXCEPT
	{
		//shaders
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{
				"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			},
			{
				"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			},
			{
				"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			},
			{
				"TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			}
		};

		unsigned int totalLayoutElements = ARRAYSIZE(layout);

		unsigned int vsSize;
		char* vsData;

		device->LoadVertexShader(vertexShader, "UltimaShaders/VertexShaderStandard.cso", vsData, vsSize);

		Utilities::Assert(
			device->GetDev()->CreateInputLayout(layout, totalLayoutElements,
			(const void*)vsData, (SIZE_T)vsSize, &inputLayout));

		delete[] vsData;

		device->LoadVertexShader(vertexShaderPositionsAndNormals, "UltimaShaders/VertexShaderPositionsAndNormals.cso", vsData, vsSize);

		delete[] vsData;

		device->LoadPixelShader(pixelShader, "UltimaShaders/PixelShaderStandard.cso");

		device->LoadPixelShader(pixelShaderPositionsAndNormals, "UltimaShaders/PixelShaderPositionsAndNormals.cso");

		D3D11_INPUT_ELEMENT_DESC layoutShadow[] =
		{
			{
				"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			}
		};

		unsigned int totalLayoutElementsShadow = ARRAYSIZE(layoutShadow);

		unsigned int vsSizeShadow;
		char* vsDataShadow;

		device->LoadVertexShader(vertexShaderShadow, "UltimaShaders/VertexShaderDepthMap.cso", vsDataShadow, vsSizeShadow);

		Utilities::Assert(
			device->GetDev()->CreateInputLayout(layoutShadow, totalLayoutElementsShadow,
			(const void*)vsDataShadow, (SIZE_T)vsSizeShadow, &inputLayoutShadow));

		delete[] vsDataShadow;

		device->LoadPixelShader(pixelShaderShadow, "UltimaShaders/PixelShaderDepthMap.cso");

		device->LoadVertexShader(vertexShaderDeferred, "UltimaShaders/VertexShaderDeferred.cso");

		device->LoadPixelShader(pixelShaderDeferred, "UltimaShaders/PixelShaderDeferred.cso");

		device->LoadHullShader(hullShaderStandard, "UltimaShaders/HullShaderStandard.cso");

		device->LoadDomainShader(domainShaderStandard, "UltimaShaders/DomainShaderStandard.cso");

		device->LoadHullShader(hullShaderPositionsAndNormals, "UltimaShaders/HullShaderPositionsAndNormals.cso");

		device->LoadDomainShader(domainShaderPositionsAndNormals, "UltimaShaders/DomainShaderPositionsAndNormals.cso");

		device->LoadHullShader(hullShaderDeferred, "UltimaShaders/HullShaderDeferred.cso");

		device->LoadDomainShader(domainShaderDeferred, "UltimaShaders/DomainShaderDeferred.cso");
		//shaders

		//mesh
		ContentManager::GetInstance().LoadMesh(device->GetDev(), meshPath, vertexBuffer, indexBuffer,
			numberOfIndices, sphere);

		sphere.SetPosition(position);
		sphere.SetRadius(sphere.GetRadius() * this->scale);
		//mesh

		//buffers
		worldBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(XMMATRIX));

		viewBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(XMMATRIX));

		projectionBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(XMMATRIX));

		lightBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(Light));

		cameraPositionBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(XMFLOAT4));

		materialBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(Material));

		fogBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(Fog));

		graphicsFilterBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(GraphicsFilter));

		shadowFilterBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(ShadowFilter));

		bumpMappingFilterBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(BumpMappingFilter));

		displacementMappingFilterBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(DisplacementMappingFilter));

		lightViewProjectionBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(XMMATRIX));

		ambientOcclusionBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(XMFLOAT4));

		boolBuffer = Utilities::CreateBuffer(device->GetDev(),
			sizeof(bool));
		//buffers

		//textures
		D3D11_SAMPLER_DESC samplerDesc;
		SecureZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		samplerDesc.MaxAnisotropy = anisotropicSampleCount;

		Utilities::Assert(
			device->GetDev()->CreateSamplerState(&samplerDesc, &anisotropicSampler));

		SecureZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
		samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;

		Utilities::Assert(
			device->GetDev()->CreateSamplerState(&samplerDesc, &comparisonSampler));

		SecureZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;

		Utilities::Assert(
			device->GetDev()->CreateSamplerState(&samplerDesc, &bilinearSampler));

        if (!texturePath.empty())
        {
            device->LoadTexture(colorTex, texturePath);
            //textureSRV = ContentManager::GetInstance().LoadTexture(device->GetDev(), texturePath);
        }

		bumpMappingFilter.IsActive = false;

		if (!bumpMapPath.empty())
		{
            device->LoadTexture(normalTex, bumpMapPath);
			//normalSRV = ContentManager::GetInstance().LoadTexture(device->GetDev(), bumpMapPath);
            bumpMappingFilter.IsActive = true;
		}

        if (!displacementMapPath.empty())
        {
            device->LoadTexture(displacementTex, displacementMapPath);
            //displacementSRV = ContentManager::GetInstance().LoadTexture(device->GetDev(), displacementMapPath);
        }
		//textures
	}

	void Model::destructModel()
		ULTIMA_NOEXCEPT
	{
		Utilities::Release(bilinearSampler);
		Utilities::Release(comparisonSampler);
		Utilities::Release(anisotropicSampler);

		//ContentManager::GetInstance().Unregister(displacementSRV);
		//ContentManager::GetInstance().Unregister(normalSRV);
		//ContentManager::GetInstance().Unregister(textureSRV);

		Utilities::Release(boolBuffer);
		Utilities::Release(ambientOcclusionBuffer);
		Utilities::Release(lightViewProjectionBuffer);
		Utilities::Release(displacementMappingFilterBuffer);
		Utilities::Release(bumpMappingFilterBuffer);
		Utilities::Release(shadowFilterBuffer);
		Utilities::Release(graphicsFilterBuffer);
		Utilities::Release(fogBuffer);
		Utilities::Release(materialBuffer);
		Utilities::Release(cameraPositionBuffer);
		Utilities::Release(lightBuffer);
		Utilities::Release(projectionBuffer);
		Utilities::Release(viewBuffer);
		Utilities::Release(worldBuffer);

		Utilities::Release(inputLayoutShadow);
		Utilities::Release(inputLayout);

		//Utilities::Release(indexBuffer);
		//Utilities::Release(vertexBuffer);
		ContentManager::GetInstance().Unregister(vertexBuffer);
	}

	Model::~Model()
		ULTIMA_NOEXCEPT
	{
		destructModel();
	}

#if DIRECTX
	const Vector3& Model::GetPosition()
		const ULTIMA_NOEXCEPT
	{
		return position;
	}

	void Model::SetPosition(const Vector3& position)
		ULTIMA_NOEXCEPT
	{
		this->position = position;

		sphere.SetPosition(position);
	}

	const Matrix& Model::GetRotation()
		const ULTIMA_NOEXCEPT
	{
		return rotation;
	}

	void Model::SetRotation(const Vector3& rotation)
		ULTIMA_NOEXCEPT
	{
		this->rotation = Matrix::CreateFromPitchYawRoll(
			rotation.GetX(),
			rotation.GetY(),
			rotation.GetZ());
	}

	void Model::SetRotation(const Matrix& rotation)
		ULTIMA_NOEXCEPT
	{
		this->rotation = rotation;
	}

	float Model::GetScale()
		const ULTIMA_NOEXCEPT
	{
		return scale;
	}

	void Model::SetScale(float scale)
		ULTIMA_NOEXCEPT
	{
		sphere.SetRadius(sphere.GetRadius() / this->scale);

		this->scale = scale;

		sphere.SetRadius(sphere.GetRadius() * this->scale);
	}

	void Model::MoveX(float f)
		ULTIMA_NOEXCEPT
	{
		position.SetX(position.GetX() + f);

		sphere.SetPosition(position);
	}

	void Model::MoveY(float f)
		ULTIMA_NOEXCEPT
	{
		position.SetY(position.GetY() + f);

		sphere.SetPosition(position);
	}

	void Model::MoveZ(float f)
		ULTIMA_NOEXCEPT
	{
		position.SetZ(position.GetZ() + f);

		sphere.SetPosition(position);
	}

	void Model::Move(const Vector3& moveVector)
		ULTIMA_NOEXCEPT
	{
		position += moveVector;

		sphere.SetPosition(position);
	}

	void Model::RotateX(float radians)
		ULTIMA_NOEXCEPT
	{
		rotation *= Matrix::CreateRotationX(radians);
	}

	void Model::RotateY(float radians)
		ULTIMA_NOEXCEPT
	{
		rotation *= Matrix::CreateRotationY(radians);
	}

	void Model::RotateZ(float radians)
		ULTIMA_NOEXCEPT
	{
		rotation *= Matrix::CreateRotationZ(radians);
	}

	void Model::Rotate(const Vector3& rotateVector)
		ULTIMA_NOEXCEPT
	{
		rotation *= Matrix::CreateRotationX(rotateVector.GetX());
		rotation *= Matrix::CreateRotationY(rotateVector.GetY());
		rotation *= Matrix::CreateRotationZ(rotateVector.GetZ());
	}

	const Material& Model::GetMaterial()
		const ULTIMA_NOEXCEPT
	{
		return material;
	}

	void Model::SetMaterial(const Material& material)
		ULTIMA_NOEXCEPT
	{
		this->material = material;
	}

	const std::string& Model::GetMeshPath()
		const ULTIMA_NOEXCEPT
	{
		return meshPath;
	}

	const std::string& Model::GetTexturePath()
		const ULTIMA_NOEXCEPT
	{
		return texturePath;
	}

	const std::string& Model::GetBumpMapPath()
		const ULTIMA_NOEXCEPT
	{
		return bumpMapPath;
	}

	const std::string& Model::GetDisplacementMapPath()
		const ULTIMA_NOEXCEPT
	{
		return displacementMapPath;
	}

	const Device* Model::GetDevice()
		const ULTIMA_NOEXCEPT
	{
		return device;
	}

	unsigned int Model::GetAnisotropicSampleCount()
		const ULTIMA_NOEXCEPT
	{
		return anisotropicSampleCount;
	}

	const Sphere& Model::GetSphere()
		const ULTIMA_NOEXCEPT
	{
		return sphere;
	}

	const BumpMappingFilter& Model::GetBumpMappingFilter()
		const ULTIMA_NOEXCEPT
	{
		return bumpMappingFilter;
	}

	void Model::SetBumpMappingFilter(const BumpMappingFilter& bmf)
		ULTIMA_NOEXCEPT
	{
		this->bumpMappingFilter = bmf;
	}

	const DisplacementMappingFilter& Model::GetDisplacementMappingFilter()
		const ULTIMA_NOEXCEPT
	{
		return displacementMappingFilter;
	}

	void Model::SetDisplacementMappingFilter(const DisplacementMappingFilter& dmf)
		ULTIMA_NOEXCEPT
	{
		this->displacementMappingFilter = dmf;
	}

	void Model::Update(float deltaTime)
	{
		UNREFERENCED_PARAMETER(deltaTime);
		//Write own implementation in child classes.
	}

	unsigned int Model::Render(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const ambientOcclusionMap,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		return Render(context, camera.GetView() * camera.GetProjection(), camera.GetPosition(),
			camera.GetFrustum(), light, fog, graphicsFilter, ambientOcclusionMap, shadowMap,
			lightViewProjection, shadowFilter);
	}

	unsigned int Model::Render(
		ID3D11DeviceContext* const context,
		const Matrix& viewProjection,
		const Vector3& cameraPosition,
		const Frustum& frustum,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const ambientOcclusionMap,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		if (!frustum.IsColliding(sphere))
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
				* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = viewProjection.ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = lightViewProjection.ToXMMATRIX();
		context->UpdateSubresource(lightViewProjectionBuffer, 0, 0, &tempXM, 0, 0);

		XMFLOAT3 eyePos = cameraPosition.ToXMFLOAT3();
		context->UpdateSubresource(cameraPositionBuffer, 0, 0, &eyePos, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);
		context->VSSetConstantBuffers(2, 1, &lightViewProjectionBuffer);
		context->VSSetConstantBuffers(3, 1, &cameraPositionBuffer);

		context->UpdateSubresource(lightBuffer, 0, 0, &light, 0, 0);

		context->UpdateSubresource(materialBuffer, 0, 0, &material, 0, 0);

		context->UpdateSubresource(fogBuffer, 0, 0, &fog, 0, 0);

		context->UpdateSubresource(graphicsFilterBuffer, 0, 0, &graphicsFilter, 0, 0);

		context->UpdateSubresource(shadowFilterBuffer, 0, 0, &shadowFilter, 0, 0);

		context->UpdateSubresource(bumpMappingFilterBuffer, 0, 0, &bumpMappingFilter, 0, 0);

		eyePos.x = (ambientOcclusionMap != nullptr);
		context->UpdateSubresource(ambientOcclusionBuffer, 0, 0, &eyePos, 0, 0);

		eyePos.x = (colorTex.GetSrv() != nullptr);
		context->UpdateSubresource(boolBuffer, 0, 0, &eyePos, 0, 0);

		context->PSSetConstantBuffers(0, 1, &lightBuffer);
		context->PSSetConstantBuffers(1, 1, &cameraPositionBuffer);
		context->PSSetConstantBuffers(2, 1, &materialBuffer);
		context->PSSetConstantBuffers(3, 1, &fogBuffer);
		context->PSSetConstantBuffers(4, 1, &graphicsFilterBuffer);
		context->PSSetConstantBuffers(5, 1, &shadowFilterBuffer);
		context->PSSetConstantBuffers(6, 1, &bumpMappingFilterBuffer);
		context->PSSetConstantBuffers(7, 1, &ambientOcclusionBuffer);
		context->PSSetConstantBuffers(8, 1, &boolBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());
		context->PSSetShaderResources(1, 1, normalTex.GetSrvPtr());
		context->PSSetShaderResources(2, 1, &ambientOcclusionMap);
		context->PSSetShaderResources(3, 1, &shadowMap);

		context->PSSetSamplers(0, 1, &anisotropicSampler);
		context->PSSetSamplers(1, 1, &comparisonSampler);
		context->PSSetSamplers(2, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShader.GetShader(), 0, 0);

		if (displacementTex.GetSrv() != nullptr
			&& displacementMappingFilter.IsActive)
		{
			context->UpdateSubresource(displacementMappingFilterBuffer, 0, 0,
				&displacementMappingFilter, 0, 0);

			context->DSSetConstantBuffers(0, 1, &viewBuffer);
			context->DSSetConstantBuffers(1, 1, &displacementMappingFilterBuffer);

			context->DSSetShaderResources(0, 1, displacementTex.GetSrvPtr());

			context->DSSetSamplers(0, 1, &bilinearSampler);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			context->HSSetShader(hullShaderStandard.GetShader(), 0, 0);
			context->DSSetShader(domainShaderStandard.GetShader(), 0, 0);
		}

		context->DrawIndexed(numberOfIndices, 0, 0);

		context->HSSetShader(nullptr, 0, 0);
		context->DSSetShader(nullptr, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Model::RenderDepth(
		ID3D11DeviceContext* const context,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		if (shadowFilter.Type == ShadowType::None)
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
				* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = lightViewProjection.ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);

		context->UpdateSubresource(shadowFilterBuffer, 0, 0, &shadowFilter, 0, 0);

		context->PSSetConstantBuffers(0, 1, &shadowFilterBuffer);

		context->VSSetShader(vertexShaderShadow.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderShadow.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Model::RenderPositionsAndNormals(
		ID3D11DeviceContext* const context,
		const Camera3D& camera)
		const ULTIMA_NOEXCEPT
	{
		return RenderPositionsAndNormals(context, camera.GetView(),
			camera.GetProjection(), camera.GetFrustum());
	}

	unsigned int Model::RenderPositionsAndNormals(
		ID3D11DeviceContext* const context,
		const Matrix& view,
		const Matrix& projection,
		const Frustum& frustum)
		const ULTIMA_NOEXCEPT
	{
		if (!frustum.IsColliding(this->sphere))
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
				* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = view.ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = projection.ToXMMATRIX();
		context->UpdateSubresource(projectionBuffer, 0, 0, &tempXM, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);
		context->VSSetConstantBuffers(2, 1, &projectionBuffer);

		context->PSSetSamplers(0, 1, &anisotropicSampler);

		context->VSSetShader(vertexShaderPositionsAndNormals.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderPositionsAndNormals.GetShader(), 0, 0);

		if (displacementTex.GetSrv() != nullptr
			&& displacementMappingFilter.IsActive)
		{
			context->UpdateSubresource(displacementMappingFilterBuffer, 0, 0,
				&displacementMappingFilter, 0, 0);

			context->DSSetConstantBuffers(0, 1, &viewBuffer);
			context->DSSetConstantBuffers(1, 1, &projectionBuffer);
			context->DSSetConstantBuffers(2, 1, &displacementMappingFilterBuffer);

			context->DSSetShaderResources(0, 1, displacementTex.GetSrvPtr());

			context->DSSetSamplers(0, 1, &bilinearSampler);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			context->HSSetShader(hullShaderPositionsAndNormals.GetShader(), 0, 0);
			context->DSSetShader(domainShaderPositionsAndNormals.GetShader(), 0, 0);
		}

		context->DrawIndexed(numberOfIndices, 0, 0);

		context->HSSetShader(nullptr, 0, 0);
		context->DSSetShader(nullptr, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Model::RenderDeferred(
		ID3D11DeviceContext* const context,
		const Camera3D& camera)
		const ULTIMA_NOEXCEPT
	{
		if (!camera.GetFrustum().IsColliding(sphere))
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
			* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = camera.GetView().ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = camera.GetProjection().ToXMMATRIX();
		context->UpdateSubresource(projectionBuffer, 0, 0, &tempXM, 0, 0);

		XMFLOAT3 eyePos = camera.GetPosition().ToXMFLOAT3();
		context->UpdateSubresource(cameraPositionBuffer, 0, 0, &eyePos, 0, 0);

		context->UpdateSubresource(materialBuffer, 0, 0, &material, 0, 0);

		context->UpdateSubresource(bumpMappingFilterBuffer, 0, 0, &bumpMappingFilter, 0, 0);

		eyePos.x = (colorTex.GetSrv() != nullptr);
		context->UpdateSubresource(boolBuffer, 0, 0, &eyePos, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);
		context->VSSetConstantBuffers(2, 1, &projectionBuffer);
		context->VSSetConstantBuffers(3, 1, &cameraPositionBuffer);

		context->PSSetConstantBuffers(0, 1, &materialBuffer);
		context->PSSetConstantBuffers(1, 1, &bumpMappingFilterBuffer);
		context->PSSetConstantBuffers(2, 1, &boolBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());
		context->PSSetShaderResources(1, 1, normalTex.GetSrvPtr());

		context->PSSetSamplers(0, 1, &anisotropicSampler);

		context->VSSetShader(vertexShaderDeferred.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderDeferred.GetShader(), 0, 0);

		if (displacementTex.GetSrv() != nullptr
			&& displacementMappingFilter.IsActive)
		{
			context->UpdateSubresource(displacementMappingFilterBuffer, 0, 0,
				&displacementMappingFilter, 0, 0);

			context->DSSetConstantBuffers(0, 1, &viewBuffer);
			context->DSSetConstantBuffers(1, 1, &projectionBuffer);
			context->DSSetConstantBuffers(2, 1, &displacementMappingFilterBuffer);

			context->DSSetShaderResources(0, 1, displacementTex.GetSrvPtr());

			context->DSSetSamplers(0, 1, &bilinearSampler);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			context->HSSetShader(hullShaderDeferred.GetShader(), 0, 0);
			context->DSSetShader(domainShaderDeferred.GetShader(), 0, 0);
		}

		context->DrawIndexed(numberOfIndices, 0, 0);

		context->HSSetShader(nullptr, 0, 0);
		context->DSSetShader(nullptr, 0, 0);

		return numberOfIndices / 3;
	}

#endif

}