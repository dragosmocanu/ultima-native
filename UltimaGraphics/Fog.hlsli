#ifndef FOG_HLSLI
#define FOG_HLSLI

struct Fog
{
	float4 Process(float4 color, float distanceToEye, float ambientPower)
	{
		if (this.Color.w == 0.0f)
			return color;
		else
			return lerp(color, this.Color * ambientPower,
				saturate((distanceToEye - this.Start) / (this.End - this.Start)));
	}

	float4 Color;
	float Start;
	float End;
};

#endif