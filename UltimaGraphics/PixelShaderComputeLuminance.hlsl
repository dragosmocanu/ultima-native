#include "HDR.hlsli"

Texture2D colorTexture : register(t0);
Texture2D luminanceTexture : register(t1);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)  
{
	float deltaTime;
	bool isAdaptive;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float main(VertexOut input) : SV_TARGET
{
	float luminance = CalculateLuminance(colorTexture.Sample(bilinearSampler, input.Texture).rgb);

	[flatten]
	if (isAdaptive)
	{
		float prevLuminance = luminanceTexture.Sample(bilinearSampler, input.Texture).r;

		luminance = prevLuminance + (luminance - prevLuminance) *
			(1 - exp(-deltaTime));
	}

	return luminance;
}