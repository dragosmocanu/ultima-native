#pragma once

#include "Macros.h"

#include "Color.h"

namespace Ultima
{
	/*!
	Defines a PBR (Physically Based Rendering) material.
	*/
	struct Material
	{
		/*!
		Creates a new instance of Material.
		*/
		ULTIMAGRAPHICS_API Material(
			const Color& base = Color(0.3f, 0.3f, 0.3f, 1.0f),
			float metallic = 0.2f,
			float roughness = 0.5f)
			ULTIMA_NOEXCEPT;

		XMFLOAT4 Base;

		float Metallic;
		float Roughness;
	};

}