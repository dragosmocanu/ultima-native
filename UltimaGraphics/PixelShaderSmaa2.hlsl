cbuffer cb0 : register(b0)
{
	float4 SMAA_RT_METRICS;
}

#define SMAA_HLSL_4_1
#define SMAA_PRESET_HIGH
#include "SMAA.hlsli"

Texture2D edgeTexture : register(t0);
Texture2D areaTexture : register(t1);
Texture2D searchTexture : register(t2);

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
	float2 Pixcoord : TEXCOORD1;
	float4 Offset[3] : TEXCOORD2;
};

float4 main(VertexOut input) : SV_TARGET
{
	//float4 output = (float4)0;

	float4 output = SMAABlendingWeightCalculationPS(
		input.Texture,
		input.Pixcoord,
		input.Offset,
		edgeTexture,
		areaTexture,
		searchTexture,
		float4(0.0f, 0.0f, 0.0f, 0.0f));

	//return SMAASample(edgeTexture, input.Texture);
	//return SMAASample(areaTexture, input.Texture);
	//return SMAASample(searchTexture, input.Texture);

	//output = SMAASample(searchTexture, input.Texture);

	//if (output.x != 0)
	//	output.x = 1.0f;
	//if (output.y != 0)
	//	output.y = 1.0f;
	//if (output.z != 0)
	//	output.z = 1.0f;

	return output;
}