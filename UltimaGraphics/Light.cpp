#include "stdafx.h"

#include "Light.h"

namespace Ultima
{
	Light::Light(
		const Ultima::Color& color,
		float specular,
		float diffuse,
		float ambient,
		const LightType& type,
		const Vector3& direction,
		float radius,
		const Vector3& position,
		float range,
		const Vector3& attenuation)
		ULTIMA_NOEXCEPT
		: Color(color.ToXMFLOAT4()),
		Specular(specular),
		Diffuse(diffuse),
		Ambient(ambient),
		Type(type),
		Direction(direction.ToXMFLOAT3()),
		Radius(radius),
		Position(position.ToXMFLOAT3()),
		Range(range),
		Attenuation(attenuation.ToXMFLOAT3())
	{

	}
}