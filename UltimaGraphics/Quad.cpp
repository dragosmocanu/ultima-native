#include "stdafx.h"

#include "Quad.h"

#include "ContentManager.h"
#include "MathExtensions.h"
#include "RandomNumberGenerator.h"
#include "Utilities.h"

#include "SMAAAreaTex.h"
#include "SMAASearchTex.h"

namespace Ultima
{
	Quad::Quad(
        const Device& device,
		float left,
		float right,
		float top,
		float bottom)
		ULTIMA_NOEXCEPT
	{
        auto dev = device.GetDev();

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{
				"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			},
			{
				"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			}
		};

		unsigned int totalLayoutElements = ARRAYSIZE(layout);

		unsigned int vsSize;
		char* vsData;

		device.LoadVertexShader(vertexShader, "UltimaShaders/VertexShaderQuad.cso", vsData, vsSize);

		Utilities::Assert(
            dev->CreateInputLayout(layout, totalLayoutElements,
			(const void*)vsData, (SIZE_T)vsSize, &inputLayout));

		delete[] vsData;

		device.LoadPixelShader(pixelShaderComputeAO, "UltimaShaders/PixelShaderComputeAO.cso");

		device.LoadPixelShader(pixelShaderFxaa, "UltimaShaders/PixelShaderFxaa.cso");

		device.LoadPixelShader(pixelShaderBlur, "UltimaShaders/PixelShaderBlur.cso");

		device.LoadPixelShader(pixelShaderComputeBloom, "UltimaShaders/PixelShaderComputeBloom.cso");

		device.LoadVertexShader(vertexShaderSmaa1, "UltimaShaders/VertexShaderSmaa1.cso", vsData, vsSize);

		delete[] vsData;

		device.LoadPixelShader(pixelShaderSmaa1, "UltimaShaders/PixelShaderSmaa1.cso");

		device.LoadVertexShader(vertexShaderSmaa2, "UltimaShaders/VertexShaderSmaa2.cso", vsData, vsSize);

		delete[] vsData;

		device.LoadPixelShader(pixelShaderSmaa2, "UltimaShaders/PixelShaderSmaa2.cso");

		// SMAA AreaTex
		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Width = AREATEX_WIDTH;
		texDesc.Height = AREATEX_HEIGHT;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = DXGI_FORMAT_R8G8_UNORM;
		texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA resourceData;
		SecureZeroMemory(&resourceData, sizeof(resourceData));
		resourceData.pSysMem = GetSmaaAreaTexBytes();
		resourceData.SysMemPitch = AREATEX_PITCH;
		resourceData.SysMemSlicePitch = 0;

		ID3D11Texture2D* areaTex = nullptr;
		Utilities::Assert(dev->CreateTexture2D(&texDesc, &resourceData, &areaTex));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
            dev->CreateShaderResourceView(areaTex, &srvDesc, &areaSRV));

		Utilities::Release(areaTex);
		// SMAA AreaTex

		// SMAA SearchTex
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Width = SEARCHTEX_WIDTH;
		texDesc.Height = SEARCHTEX_HEIGHT;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = DXGI_FORMAT_R8_UNORM;
		texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;

		SecureZeroMemory(&resourceData, sizeof(resourceData));
		resourceData.pSysMem = GetSmaaSearchTexBytes();
		resourceData.SysMemPitch = SEARCHTEX_PITCH;
		resourceData.SysMemSlicePitch = 0;

		ID3D11Texture2D* searchTex = nullptr;
		Utilities::Assert(dev->CreateTexture2D(&texDesc, &resourceData, &searchTex));

		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
            dev->CreateShaderResourceView(searchTex, &srvDesc, &searchSRV));

		Utilities::Release(searchTex);
		// SMAA SearchTex

		device.LoadVertexShader(vertexShaderSmaa3, "UltimaShaders/VertexShaderSmaa3.cso", vsData, vsSize);

		delete[] vsData;

		device.LoadPixelShader(pixelShaderSmaa3, "UltimaShaders/PixelShaderSmaa3.cso");

		device.LoadPixelShader(pixelShaderComputeLuminance, "UltimaShaders/PixelShaderComputeLuminance.cso");

		device.LoadPixelShader(pixelShaderShowLuminance, "UltimaShaders/PixelShaderShowLuminance.cso");

		device.LoadPixelShader(pixelShaderTone, "UltimaShaders/PixelShaderTone.cso");

		device.LoadPixelShader(pixelShaderCopy, "UltimaShaders/PixelShaderCopy.cso");

		device.LoadPixelShader(pixelShaderComputeIllumination, "UltimaShaders/PixelShaderComputeIllumination.cso");

		device.LoadPixelShader(pixelShaderIlluminate, "UltimaShaders/PixelShaderIlluminate.cso");

		std::vector<VertexPT> vertices;
		VertexPT v;

		v.Position = XMFLOAT3(left, top, 0.0f);
		v.Texture = XMFLOAT2(0.0f, 0.0f);
		vertices.push_back(v);

		bool isFullScreen = (left == -1) & (right == 1) & (top == 1) & (bottom == -1);

		if (isFullScreen)
		{
			v.Position = XMFLOAT3(right + (right - left), top, 0.0f);
			v.Texture = XMFLOAT2(1.0f * 2, 0.0f);
			vertices.push_back(v);

			v.Position = XMFLOAT3(left, bottom + (bottom - top), 0.0f);
			v.Texture = XMFLOAT2(0.0f, 1.0f * 2);
			vertices.push_back(v);
		}
		else
		{
			v.Position = XMFLOAT3(right, top, 0.0f);
			v.Texture = XMFLOAT2(1.0f, 0.0f);
			vertices.push_back(v);

			v.Position = XMFLOAT3(right, bottom, 0.0f);
			v.Texture = XMFLOAT2(1.0f, 1.0f);
			vertices.push_back(v);

			v.Position = XMFLOAT3(left, bottom, 0.0f);
			v.Texture = XMFLOAT2(0.0f, 1.0f);
			vertices.push_back(v);
		}

		std::vector<unsigned short int> indices;

		indices.push_back(0);
		indices.push_back(1);
		indices.push_back(2);

		if (!isFullScreen)
		{
			indices.push_back(0);
			indices.push_back(2);
			indices.push_back(3);
		}

		numberOfIndices = indices.size();

		D3D11_BUFFER_DESC vertexDesc;
		SecureZeroMemory(&vertexDesc, sizeof(vertexDesc));
		vertexDesc.Usage = D3D11_USAGE_DEFAULT;
		vertexDesc.ByteWidth = sizeof(VertexPT) * vertices.size();
		vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA data;
		SecureZeroMemory(&data, sizeof(data));
		data.pSysMem = &vertices[0];

		Utilities::Assert(
            dev->CreateBuffer(&vertexDesc, &data, &vertexBuffer));

		D3D11_BUFFER_DESC indexDesc;
		SecureZeroMemory(&indexDesc, sizeof(indexDesc));
		indexDesc.Usage = D3D11_USAGE_DEFAULT;
		indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexDesc.ByteWidth = sizeof(unsigned short int) * indices.size();

		data.pSysMem = &indices[0];

		Utilities::Assert(
            dev->CreateBuffer(&indexDesc, &data, &indexBuffer));

		D3D11_SAMPLER_DESC samplerDesc;
		SecureZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;

		Utilities::Assert(
            dev->CreateSamplerState(&samplerDesc, &bilinearSampler));

		SecureZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
		samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;

		Utilities::Assert(
            dev->CreateSamplerState(&samplerDesc, &comparisonSampler));

		antiAliasingFilterBuffer =
			Utilities::CreateBuffer(dev, sizeof(AntiAliasingFilter));

		ambientOcclusionBuffer =
			Utilities::CreateBuffer(dev, sizeof(AmbientOcclusionFilter));

		gaussianBlurFilterBuffer =
			Utilities::CreateBuffer(dev, sizeof(GaussianBlurFilter));

		smaaRtMetricsBuffer =
			Utilities::CreateBuffer(dev, sizeof(XMFLOAT4));

		bloomBuffer =
			Utilities::CreateBuffer(dev, sizeof(BloomFilter));

		toneMappingBuffer =
			Utilities::CreateBuffer(dev, sizeof(ToneMappingFilter));

		lightBuffer =
			Utilities::CreateBuffer(dev, sizeof(Light));

		shadowFilterBuffer =
			Utilities::CreateBuffer(dev, sizeof(ShadowFilter));

		matrixBuffer =
			Utilities::CreateBuffer(dev, sizeof(XMMATRIX));
	}

	Quad::~Quad()
		ULTIMA_NOEXCEPT
	{
		Utilities::Release(comparisonSampler);
		Utilities::Release(pointSampler);
		Utilities::Release(bilinearSampler);

		Utilities::Release(matrixBuffer);
		Utilities::Release(shadowFilterBuffer);
		Utilities::Release(lightBuffer);
		Utilities::Release(toneMappingBuffer);
		Utilities::Release(smaaRtMetricsBuffer);
		Utilities::Release(bloomBuffer);
		Utilities::Release(gaussianBlurFilterBuffer);
		Utilities::Release(antiAliasingFilterBuffer);
		Utilities::Release(ambientOcclusionBuffer);

		Utilities::Release(searchSRV);
		Utilities::Release(areaSRV);

		Utilities::Release(indexBuffer);
		Utilities::Release(vertexBuffer);
		Utilities::Release(inputLayout);
	}

	unsigned int Quad::ComputeAmbientOcclusion(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const positionSRV,
		ID3D11ShaderResourceView* const normalSRV,
		const AmbientOcclusionFilter& aoFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(ambientOcclusionBuffer, 0, 0, &aoFilter, 0, 0);

		context->PSSetConstantBuffers(0, 1, &ambientOcclusionBuffer);

		context->PSSetShaderResources(0, 1, &positionSRV);
		context->PSSetShaderResources(1, 1, &normalSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderComputeAO.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Blur(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		const GaussianBlurFilter& blurFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(gaussianBlurFilterBuffer, 0, 0, &blurFilter, 0, 0);

		context->PSSetConstantBuffers(0, 1, &gaussianBlurFilterBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderBlur.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::AntiAlias(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const input,
		ID3D11RenderTargetView* const output,
		const AntiAliasingFilter& aaFilter,
		ID3D11ShaderResourceView* const smaaSRV1,
		ID3D11RenderTargetView* const smaaRTV1,
		ID3D11ShaderResourceView* const smaaSRV2,
		ID3D11RenderTargetView* const smaaRTV2,
		const Vector2& screenSize)
		const ULTIMA_NOEXCEPT
	{
		unsigned int result = 0;

		if (aaFilter.Type == AntiAliasingType::None
			|| aaFilter.Type == AntiAliasingType::FXAA)
		{
			context->OMSetRenderTargets(1, &output, nullptr);
			result += this->Fxaa(context, input, aaFilter);
		}
		else if (aaFilter.Type == AntiAliasingType::SMAA)
		{
			float f[4];
			f[0] = f[1] = f[2] = f[3] = 0.0f;

			context->ClearRenderTargetView(smaaRTV1, f);
			context->ClearRenderTargetView(smaaRTV2, f);
			//context->ClearRenderTargetView(output, f);

			context->OMSetRenderTargets(1, &smaaRTV1, nullptr);
			result += this->Smaa1(context, input, screenSize);

			context->OMSetRenderTargets(1, &smaaRTV2, nullptr);
			result += this->Smaa2(context, smaaSRV1, screenSize);

			context->OMSetRenderTargets(1, &output, nullptr);
			result += this->Smaa3(context, input, smaaSRV2, screenSize);
		}

		return result;
	}

	unsigned int Quad::Fxaa(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		const AntiAliasingFilter& aaFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(antiAliasingFilterBuffer, 0, 0, &aaFilter, 0, 0);

		context->PSSetConstantBuffers(0, 1, &antiAliasingFilterBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderFxaa.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Smaa1(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		const Vector2& screenSize)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMFLOAT4 xm = XMFLOAT4(1.0f / screenSize.GetX(), 1.0f / screenSize.GetY(),
			screenSize.GetX(), screenSize.GetY());
		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &xm, 0, 0);

		context->VSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);
		context->PSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);

		context->VSSetShader(vertexShaderSmaa1.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderSmaa1.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Smaa2(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const smaaSRV1,
		const Vector2& screenSize)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMFLOAT4 xm = XMFLOAT4(1.0f / screenSize.GetX(), 1.0f / screenSize.GetY(),
			screenSize.GetX(), screenSize.GetY());
		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &xm, 0, 0);

		context->VSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);
		context->PSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &smaaSRV1);
		context->PSSetShaderResources(1, 1, &areaSRV);
		context->PSSetShaderResources(2, 1, &searchSRV);

		context->VSSetShader(vertexShaderSmaa2.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderSmaa2.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Smaa3(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		ID3D11ShaderResourceView* const smaaSRV2,
		const Vector2& screenSize)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMFLOAT4 xm = XMFLOAT4(1.0f / screenSize.GetX(), 1.0f / screenSize.GetY(),
			screenSize.GetX(), screenSize.GetY());
		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &xm, 0, 0);

		context->VSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);
		context->PSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);
		context->PSSetShaderResources(1, 1, &smaaSRV2);

		context->VSSetShader(vertexShaderSmaa3.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderSmaa3.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::ComputeLuminance(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		ID3D11ShaderResourceView* const previousLuminanceSRV,
		float deltaTime,
		bool isAdaptive,
		float adaptationRate)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMFLOAT2 xm;
		xm.x = deltaTime * adaptationRate;
		xm.y = isAdaptive;

		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &xm, 0, 0);

		context->PSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);
		context->PSSetShaderResources(1, 1, &previousLuminanceSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderComputeLuminance.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}


	unsigned int Quad::ShowLuminance(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		unsigned int mipLevel)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &mipLevel, 0, 0);
		context->PSSetConstantBuffers(0, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);

		context->PSSetSamplers(0, 1, &pointSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderShowLuminance.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::ComputeBloom(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		ID3D11ShaderResourceView* const luminanceSRV,
		unsigned int mipLevel,
		const BloomFilter& bloomFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(bloomBuffer, 0, 0, &bloomFilter, 0, 0);
		context->PSSetConstantBuffers(0, 1, &bloomBuffer);

		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &mipLevel, 0, 0);
		context->PSSetConstantBuffers(1, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);
		context->PSSetShaderResources(1, 1, &luminanceSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);
		context->PSSetSamplers(1, 1, &pointSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderComputeBloom.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Tone(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		ID3D11ShaderResourceView* const luminanceSRV,
		unsigned int mipLevel,
		ID3D11ShaderResourceView* const bloomSRV,
		const ToneMappingFilter& toneMappingFilter,
		const BloomFilter& bloomFilter,
		const GraphicsFilter& graphicsFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(toneMappingBuffer, 0, 0, &toneMappingFilter, 0, 0);
		context->PSSetConstantBuffers(0, 1, &toneMappingBuffer);

		context->UpdateSubresource(bloomBuffer, 0, 0, &bloomFilter, 0, 0);
		context->PSSetConstantBuffers(1, 1, &bloomBuffer);

		context->UpdateSubresource(lightBuffer, 0, 0, &graphicsFilter, 0, 0);
		context->PSSetConstantBuffers(2, 1, &lightBuffer);

		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &mipLevel, 0, 0);
		context->PSSetConstantBuffers(3, 1, &smaaRtMetricsBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);
		context->PSSetShaderResources(1, 1, &luminanceSRV);
		context->PSSetShaderResources(2, 1, &bloomSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);
		context->PSSetSamplers(1, 1, &pointSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderTone.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Copy(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const inputSRV,
		bool isSingleChannel)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMFLOAT4 tempXM;
		tempXM.x = isSingleChannel;
		context->UpdateSubresource(antiAliasingFilterBuffer, 0, 0, &tempXM, 0, 0);

		context->PSSetConstantBuffers(0, 1, &antiAliasingFilterBuffer);

		context->PSSetShaderResources(0, 1, &inputSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderCopy.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::ComputeIllumination(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		ID3D11ShaderResourceView* const positionSRV,
		ID3D11ShaderResourceView* const normalSRV,
		const Light& light,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(lightBuffer, 0, 0, &light, 0, 0);

		XMFLOAT3 camPos = camera.GetPosition().ToXMFLOAT3();
		context->UpdateSubresource(smaaRtMetricsBuffer, 0, 0, &camPos, 0, 0);

		context->UpdateSubresource(shadowFilterBuffer, 0, 0, &shadowFilter, 0, 0);

		XMMATRIX matrix = lightViewProjection.ToXMMATRIX();
		context->UpdateSubresource(matrixBuffer, 0, 0, &matrix, 0, 0);

		context->PSSetConstantBuffers(0, 1, &lightBuffer);
		context->PSSetConstantBuffers(1, 1, &smaaRtMetricsBuffer);
		context->PSSetConstantBuffers(2, 1, &shadowFilterBuffer);
		context->PSSetConstantBuffers(3, 1, &matrixBuffer);

		context->PSSetShaderResources(0, 1, &positionSRV);
		context->PSSetShaderResources(1, 1, &normalSRV);
		context->PSSetShaderResources(2, 1, &shadowMap);

		context->PSSetSamplers(0, 1, &bilinearSampler);
		context->PSSetSamplers(1, 1, &comparisonSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderComputeIllumination.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Quad::Illuminate(
		ID3D11DeviceContext* const context,
		ID3D11ShaderResourceView* const colorSRV,
		ID3D11ShaderResourceView* const diffuseSRV,
		ID3D11ShaderResourceView* const specularSRV,
		ID3D11ShaderResourceView* const ambientSRV,
		ID3D11ShaderResourceView* const materialSRV,
		ID3D11ShaderResourceView* const aoSRV,
		const AmbientOcclusionFilter& aoFilter,
		const GraphicsFilter& graphicsFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		context->UpdateSubresource(lightBuffer, 0, 0, &graphicsFilter, 0, 0);

		context->UpdateSubresource(ambientOcclusionBuffer, 0, 0, &aoFilter, 0, 0);

		context->PSSetConstantBuffers(0, 1, &lightBuffer);
		context->PSSetConstantBuffers(1, 1, &ambientOcclusionBuffer);

		context->PSSetShaderResources(0, 1, &colorSRV);
		context->PSSetShaderResources(1, 1, &diffuseSRV);
		context->PSSetShaderResources(2, 1, &specularSRV);
		context->PSSetShaderResources(3, 1, &ambientSRV);
		context->PSSetShaderResources(4, 1, &materialSRV);
		context->PSSetShaderResources(5, 1, &aoSRV);

		context->PSSetSamplers(0, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderIlluminate.GetShader(), 0, 0);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}
}