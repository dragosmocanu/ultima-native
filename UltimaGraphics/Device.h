#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "DeviceDx11.h"
namespace Ultima
{
    class Device : public DeviceDx11 { };
}
#endif