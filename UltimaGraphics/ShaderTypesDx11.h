#pragma once

#include "Macros.h"

#include "IShaderTypes.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
namespace Ultima
{
    class VertexShaderDx11 : public IVertexShader
    {
    public:
        ULTIMAGRAPHICS_API VertexShaderDx11() { vs = nullptr; }

        ULTIMAGRAPHICS_API ID3D11VertexShader* GetShader() const ULTIMA_NOEXCEPT { return vs; }
        ULTIMAGRAPHICS_API ID3D11VertexShader*& GetShader() ULTIMA_NOEXCEPT { return vs; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11VertexShader* shader) ULTIMA_NOEXCEPT { vs = shader; }

    protected:
        ID3D11VertexShader* vs;
    };

    class PixelShaderDx11 : public IPixelShader
    {
    public:
        ULTIMAGRAPHICS_API PixelShaderDx11() { ps = nullptr; }

        ULTIMAGRAPHICS_API ID3D11PixelShader* GetShader() const ULTIMA_NOEXCEPT { return ps; }
        ULTIMAGRAPHICS_API ID3D11PixelShader*& GetShader() ULTIMA_NOEXCEPT { return ps; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11PixelShader* shader) ULTIMA_NOEXCEPT { ps = shader; }

    protected:
        ID3D11PixelShader* ps;
    };

    class HullShaderDx11 : public IVertexShader
    {
    public:
        ULTIMAGRAPHICS_API HullShaderDx11() { hs = nullptr; }

        ULTIMAGRAPHICS_API ID3D11HullShader* GetShader() const ULTIMA_NOEXCEPT { return hs; }
        ULTIMAGRAPHICS_API ID3D11HullShader*& GetShader() ULTIMA_NOEXCEPT { return hs; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11HullShader* shader) ULTIMA_NOEXCEPT { hs = shader; }

    protected:
        ID3D11HullShader* hs;
    };

    class DomainShaderDx11 : public IVertexShader
    {
    public:
        ULTIMAGRAPHICS_API DomainShaderDx11() { ds = nullptr; }

        ULTIMAGRAPHICS_API ID3D11DomainShader* GetShader() const ULTIMA_NOEXCEPT { return ds; }
        ULTIMAGRAPHICS_API ID3D11DomainShader*& GetShader() ULTIMA_NOEXCEPT { return ds; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11DomainShader* shader) ULTIMA_NOEXCEPT { ds = shader; }

    protected:
        ID3D11DomainShader* ds;
    };

    class GeometryShaderDx11 : public IVertexShader
    {
    public:
        ULTIMAGRAPHICS_API GeometryShaderDx11() { gs = nullptr; }

        ULTIMAGRAPHICS_API ID3D11GeometryShader* GetShader() const ULTIMA_NOEXCEPT { return gs; }
        ULTIMAGRAPHICS_API ID3D11GeometryShader*& GetShader() ULTIMA_NOEXCEPT { return gs; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11GeometryShader* shader) ULTIMA_NOEXCEPT { gs = shader; }

    protected:
        ID3D11GeometryShader* gs;
    };

    class ComputeShaderDx11 : public IVertexShader
    {
    public:
        ULTIMAGRAPHICS_API ComputeShaderDx11() { cs = nullptr; }

        ULTIMAGRAPHICS_API ID3D11ComputeShader* GetShader() const ULTIMA_NOEXCEPT { return cs; }
        ULTIMAGRAPHICS_API ID3D11ComputeShader*& GetShader() ULTIMA_NOEXCEPT { return cs; }
        ULTIMAGRAPHICS_API void SetShader(ID3D11ComputeShader* shader) ULTIMA_NOEXCEPT { cs = shader; }

    protected:
        ID3D11ComputeShader* cs;
    };

}
#endif