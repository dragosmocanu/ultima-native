#include "stdafx.h"

#include "ShadowFilter.h"

namespace Ultima
{
	ShadowFilter::ShadowFilter(
		ShadowType type,
		unsigned int pcfNumberOfSamples,
		float pcfBias,
		float vsmMinVariance,
		float vsmBleedingReduction,
		Vector2 evsmExponents)
		ULTIMA_NOEXCEPT
		: Type(type),
		PcfNumberOfSamples(pcfNumberOfSamples),
		PcfBias(pcfBias),
		VsmMinVariance(vsmMinVariance),
		VsmBleedingReduction(vsmBleedingReduction),
		EvsmExponents(evsmExponents.ToXMFLOAT2())
	{ }
}