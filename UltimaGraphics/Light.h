#pragma once

#include "Macros.h"

#include "Color.h"
#include "Vector3.h"

namespace Ultima
{
	enum class LightType
	{
		None,
		Directional,
		Point,
		Spot
	};

	/*!
	Defines a light that can be used in a 3D scene.
	*/
	struct Light
	{
		/*!
		Creates a new instance of Light.
		*/
		ULTIMAGRAPHICS_API Light(
			const Ultima::Color& color = Ultima::Color(0.8f, 0.75f, 0.6f, 1.0f),
			float specular = 1.0f,
			float diffuse = 0.8f,
			float ambient = 0.4f,
			const LightType& type = LightType::Directional,
			const Vector3& direction = Vector3(-0.440706313f, -0.734510481f, -0.514157355f),
			float radius = 100.0f,
			const Vector3& position = Vector3(0.0f, 0.0f, 0.0f),
			float range = 10000.0f,
			const Vector3& attenuation = Vector3(0.001f, 0.001f, 0.001f))
			ULTIMA_NOEXCEPT;

		XMFLOAT4 Color;

		float Diffuse;
		float Specular;
		float Ambient;
		LightType Type;

		XMFLOAT3 Direction;
		float Radius;

		XMFLOAT3 Position;
		float Range;

		XMFLOAT3 Attenuation;
		float Padding;
	};
}