#ifndef SHADOWFILTER_HLSLI
#define SHADOWFILTER_HLSLI

#pragma warning (disable : 4000)

float2 WarpDepth(float depth, float2 exponents)
{
	depth = depth * 2.0f - 1.0f;

	return float2(
		exp(abs(exponents.x) * depth),
		-exp(-abs(exponents.y) * depth));
}

float ChebyshevUpperBound(
	float2 moments, float mean, float minVariance, float bleedingReduction)
{
	float result = 0.0f;

	[flatten]
	if (mean <= moments.x)
		result = 1.0f;

	float variance = moments.y - (moments.x * moments.x);
	variance = max(variance, minVariance);

	float d = mean - moments.x;
	float maxR = variance / (variance + d * d);

	maxR = saturate((maxR - bleedingReduction) / (1.0f - bleedingReduction));

	result = max(result, maxR);

	return result;
}

struct ShadowFilter
{
	float Process(
		float2 textureCoordinates,
		float depth,
		Texture2D depthMap,
		SamplerState bilinearSampler,
		SamplerComparisonState comparisonSampler)
	{
		float lightPower = 1.0f;

		[flatten]
		if (Type != 0
			&& textureCoordinates.x == saturate(textureCoordinates.x)
			&& textureCoordinates.y == saturate(textureCoordinates.y)
			&& depth == saturate(depth))
		{
			if (Type == 1)
			{
				depth -= this.PcfBias;

				float texelSizeX, texelSizeY;
				depthMap.GetDimensions(texelSizeX, texelSizeY);
				texelSizeX = 1.0f / texelSizeX;
				texelSizeY = 1.0f / texelSizeY;

				float samples = PcfNumberOfSamples / 2.0f - 0.5f;

				lightPower = 0.0f;

				for (float y = -samples; y <= samples; y++)
				{
					for (float x = -samples; x <= samples; x++)
					{
						lightPower += depthMap.SampleCmpLevelZero(
							comparisonSampler,
							textureCoordinates
							+ float2(texelSizeX * x, texelSizeY * y),
							depth);
					}
				}

				lightPower /= (PcfNumberOfSamples * PcfNumberOfSamples);
			}
			else if (Type == 2)
			{
				float2 vsm = depthMap.Sample(bilinearSampler, textureCoordinates).xy;

				lightPower = ChebyshevUpperBound(
					vsm,
					depth,
					VsmMinVariance,
					VsmBleedingReduction);
			}
			else if (Type == 3)
			{
				float2 evsm = depthMap.Sample(bilinearSampler, textureCoordinates).xy;

				float warpedDepth = WarpDepth(depth, EvsmExponents).x;

				lightPower = ChebyshevUpperBound(
					evsm,
					warpedDepth,
					VsmMinVariance,
					VsmBleedingReduction);
			}
			else if (Type == 4)
			{
				float4 evsm = depthMap.Sample(bilinearSampler, textureCoordinates);

				float2 warpedDepth = WarpDepth(depth, EvsmExponents);

				float2 depthScale = VsmMinVariance * EvsmExponents * warpedDepth;
				float2 minVariance = depthScale * depthScale;

				float lightPower1 = ChebyshevUpperBound(
					evsm.xz,
					warpedDepth.x,
					minVariance.x,
					VsmBleedingReduction);

				float lightPower2 = ChebyshevUpperBound(
					evsm.yw,
					warpedDepth.y,
					minVariance.y,
					VsmBleedingReduction);

				lightPower = min(lightPower1, lightPower2);
			}
		}

		return lightPower;
	}

	float Process(
		float4 positionInLightSpace,
		Texture2D depthMap,
		SamplerState bilinearSampler,
		SamplerComparisonState comparisonState)
	{
		if (Type == 0)
			return 1.0f;

		positionInLightSpace.x =
			0.5f + positionInLightSpace.x / positionInLightSpace.w * 0.5f;
		positionInLightSpace.y =
			0.5f - positionInLightSpace.y / positionInLightSpace.w * 0.5f;
		positionInLightSpace.z /= positionInLightSpace.w;

		return this.Process(
			positionInLightSpace.xy,
			positionInLightSpace.z,
			depthMap,
			bilinearSampler,
			comparisonState);
	}

	unsigned int Type;
	unsigned int PcfNumberOfSamples;
	float PcfBias;
	float VsmMinVariance;

	float VsmBleedingReduction;
	float2 EvsmExponents;
};

#endif