#pragma once

#include "Macros.h"

namespace Ultima
{
    /*!
    Defines the interface for a swap chain.
    */
    class ISwapChain
    {
    public:
        ULTIMAGRAPHICS_API virtual void Flip(bool isInVsync = true)
            ULTIMA_NOEXCEPT = 0;

        ULTIMAGRAPHICS_API virtual void Resize(unsigned int width, unsigned int height)
            ULTIMA_NOEXCEPT = 0;
    protected:

    };
}