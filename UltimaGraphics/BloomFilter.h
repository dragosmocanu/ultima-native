#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines bloom settings.
	*/
	struct BloomFilter
	{
		/*!
		Creates a new instance of BloomFilter.
		A higher threshold will result in less bloom.
		*/
		ULTIMAGRAPHICS_API BloomFilter(
			bool isActive = true,
			float threshold = 1.0f,
			float strength = 1.0f)
			ULTIMA_NOEXCEPT;

		float IsActive;
		float Threshold;
		float Strength;
	};
}