#pragma once

#include "Macros.h"

#include "Color.h"
#include "Fog.h"
#include "Light.h"

namespace Ultima
{
	/*!
	Defines a graphics filter that changes the output.
	*/
	struct GraphicsFilter
	{

		/*!
		Creates a new instance of GraphicsFilter.
		Brightness and contrast have values in the [0, 1] interval.
		*/
		ULTIMAGRAPHICS_API GraphicsFilter(
			bool isActive = false,
			float brightness = 0.5f, 
			float contrast = 0.5f,
			const Color& tintColor = Color::Transparent,
			bool isGrayscale = false, 
			bool isNegative = false)
			ULTIMA_NOEXCEPT;

		/*!
		Processes the final color after applying the filter.
		*/
		ULTIMAGRAPHICS_API Color Process(
			const Fog& fog,
			const Light& light)
			const ULTIMA_NOEXCEPT;

		XMFLOAT4 TintColor;

		float IsActive;
		float Brightness;
		float Contrast;
		float IsGrayscale;

		float IsNegative;
	};
}