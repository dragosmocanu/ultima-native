cbuffer cb0 : register(b0)
{
	float4 SMAA_RT_METRICS;
}

#define SMAA_HLSL_4_1
#define SMAA_PRESET_HIGH
#include "SMAA.hlsli"

Texture2D colorTexture : register(t0);
Texture2D blendTexture : register(t1);

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
	float4 Offset : TEXCOORD1;
};

float4 main(VertexOut input) : SV_TARGET
{
	float4 output = SMAANeighborhoodBlendingPS(
		input.Texture,
		input.Offset,
		colorTexture,
		blendTexture);

	//return SMAASample(blendTexture, input.Texture);

	return output;
}