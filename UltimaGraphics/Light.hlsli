#ifndef LIGHT_HLSLI
#define LIGHT_HLSLI

#include "PBR.hlsli"

struct Light
{
	void Process(
		float metallic,
		float roughness,
		float3 position,
		float3 normal,
		float3 toEye,
		float diffuseFactor,
		float specularFactor,
		inout float4 diffuse,
		inout float4 specular,
		inout float4 ambient)
	{
		if (this.Type == 0)
			return;

		ambient += this.Color * this.Ambient;

		if (this.Type == 1) //Directional
		{
			//PBR
			diffuse +=
				(1.0f - metallic)
				* saturate(dot(normal, -this.Direction))
				* this.Color
				* this.Diffuse
				* diffuseFactor;

			//PBR
			specular +=
				SpecularPBR(
					normal,
					toEye,
					-this.Direction,
					roughness,
					metallic)
				* this.Color
				* this.Specular
				* specularFactor;
		}
		else if (this.Type == 2) //Spot
		{
			float3 pos = this.Position - position;
			float d = length(pos);

			if (d > this.Range)
				return;

			pos /= d;

			float amount = dot(pos, normal);

			if (amount > 0.0f)
			{
				//used as spot power
				float attenuation = pow(max(dot(-pos, this.Direction), 0.0f), this.Radius);
				attenuation = attenuation / dot(this.Attenuation, float3(1.0f, d, d * d));

				float4 temp;

				//used as temporary diffuse
				temp = saturate(this.Color * this.Diffuse * amount)
					* attenuation * diffuseFactor
					* (1.0f - metallic);

				diffuse += temp;

				//used as specular power
				d = SpecularPBR(normal, toEye, pos, roughness, metallic);

				//used as temporary specular
				temp = d * this.Color * this.Specular
					* attenuation * specularFactor;

				specular += temp;
			}
		}
		else if (this.Type == 3) //Point
		{
			float3 pos = this.Position - position;
			float d = length(pos);

			if (d > this.Range)
				return;

			pos /= d;

			float amount = dot(pos, normal);

			if (amount > 0.0f)
			{
				float attenuation = 1.0f / dot(this.Attenuation, float3(1.0f, d, d * d));

				float4 temp;

				//used as temporary diffuse
				temp = saturate(this.Color * this.Diffuse * amount)
					* attenuation * diffuseFactor
					* (1.0f - metallic);

				diffuse += temp;

				//used as specular power
				d = SpecularPBR(normal, toEye, pos, roughness, metallic);

				//used as temporary specular
				temp = d * this.Color * this.Diffuse
					* attenuation * specularFactor;

				specular += temp;
			}
		}
	}

	float4 Color;

	float Diffuse;
	float Specular;
	float Ambient;
	unsigned int Type;

	float3 Direction;
	float Radius;

	float3 Position;
	float Range;

	float3 Attenuation;
	float Padding;
};

#endif