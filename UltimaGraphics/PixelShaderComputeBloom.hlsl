#include "BloomFilter.hlsli"

cbuffer cb0 : register(b0)
{
	BloomFilter bloomFilter;
}

cbuffer cb1 : register(b1)
{
	unsigned int mipLevel;
}

Texture2D colorTexture : register(t0);
Texture2D luminanceTexture : register(t1);

SamplerState bilinearSampler : register(s0);
SamplerState pointSampler : register(s1);

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float3 color = bloomFilter.Compute(colorTexture, luminanceTexture, bilinearSampler, 
		pointSampler, input.Texture, mipLevel);

	return float4(color, 1);
}