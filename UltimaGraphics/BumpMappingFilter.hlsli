#ifndef BUMPMAPPINGFILTER_HLSLI
#define BUMPMAPPINGFILTER_HLSLI

struct BumpMappingFilter
{
	float3 Process(
		Texture2D bumpTexture,
		SamplerState anisotropicSampler,
		float2 textureCoordinates,
		float3 normal,
		float3 tangent)
	{
		float3 result = normal;

		[flatten]
		if (this.IsActive)
		{
			float3 bumpMap = bumpTexture.Sample(
				anisotropicSampler,
				textureCoordinates).rgb;

			bumpMap = 2 * bumpMap - 1.0f;

			float3 T = normalize(normal - dot(tangent, normal) * normal);
			float3 B = cross(normal, T);

			float3x3 TBN = float3x3(T, B, normal);

			result = normalize(mul(bumpMap, TBN));
		}

		return result;
	}

	bool IsActive;
};

#endif