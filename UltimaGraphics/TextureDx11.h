#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "TextureBase.h"

namespace Ultima
{
    class TextureDx11 : public TextureBase
    {
    public:
        ULTIMAGRAPHICS_API TextureDx11(const Device* device = nullptr) : TextureBase(device), srv(nullptr), rtv(nullptr), dsv(nullptr) { }

        ULTIMAGRAPHICS_API ID3D11ShaderResourceView* GetSrv() const ULTIMA_NOEXCEPT { return srv; };

        ULTIMAGRAPHICS_API ID3D11ShaderResourceView** GetSrvPtr() const ULTIMA_NOEXCEPT { return &srv; }

        ULTIMAGRAPHICS_API void SetSrv(ID3D11ShaderResourceView* val) ULTIMA_NOEXCEPT { srv = val; }

        ULTIMAGRAPHICS_API ID3D11RenderTargetView* GetRtv() const ULTIMA_NOEXCEPT { return rtv; };

        ULTIMAGRAPHICS_API ID3D11RenderTargetView** GetRtvPtr() const ULTIMA_NOEXCEPT { return &rtv; }

        ULTIMAGRAPHICS_API void SetRtv(ID3D11RenderTargetView* val) ULTIMA_NOEXCEPT { rtv = val; }

        ULTIMAGRAPHICS_API ID3D11DepthStencilView* GetDsv() const ULTIMA_NOEXCEPT { return dsv; };

        ULTIMAGRAPHICS_API ID3D11DepthStencilView** GetDsvPtr() const ULTIMA_NOEXCEPT { return &dsv; }

        ULTIMAGRAPHICS_API void SetSrv(ID3D11DepthStencilView* val) ULTIMA_NOEXCEPT { dsv = val; }

        //ULTIMAGRAPHICS_API virtual ~TextureDx11()
        //{
        //    //device->Release(this);
        //}

    protected:
        mutable ID3D11ShaderResourceView* srv;
        mutable ID3D11RenderTargetView* rtv;
        mutable ID3D11DepthStencilView* dsv;
    };
}

#endif