#include "Lighting.hlsli"

cbuffer cb0 : register(b0)
{
	DirectionalLight dl;
}

cbuffer cb1 : register(b1)
{
	PointLight pl;
}

cbuffer cb2 : register(b2)
{
	SpotLight sl;
}

cbuffer cb3 : register(b3)
{
	float3 camPos;
}

cbuffer cb4 : register(b4)
{
	Material material;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
};

float4 main(VertexOut v) : SV_TARGET
{
	float4 output = float4(0.0f, 0.0f, 0.0f, 0.0f);

	v.Normal = normalize(v.Normal);

	float3 toEye = normalize(camPos - v.PositionW);

	float4 diffuse, specular, ambient;
	diffuse = specular = ambient = output;

	dl.Process(material, v.Normal, toEye, 1, 1, 1, diffuse, specular, ambient);

	pl.Process(material, v.PositionW, v.Normal, toEye, 1, 1, 1, diffuse, specular, ambient);

	sl.Process(material, v.PositionW, v.Normal, toEye, 1, 1, 1, diffuse, specular, ambient);

	output = material.Modulate(output, diffuse, specular, ambient);

	return output;
}