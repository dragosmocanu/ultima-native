#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "IDevice.h"

#include "ContentManager.h"
#include "Sphere.h"

namespace Ultima
{
    class DeviceDx11 : public IDevice
    {
    public:
        ULTIMAGRAPHICS_API ID3D11Device* GetDev()
            const ULTIMA_NOEXCEPT;

        ULTIMAGRAPHICS_API ID3D11Device*& GetDevRef()
            ULTIMA_NOEXCEPT;

        ULTIMAGRAPHICS_API void SetDev(ID3D11Device* dev)
            ULTIMA_NOEXCEPT;

        /*!
        Loads the texture data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadTexture(
            Texture& outTexture,
            const std::string& filePath,
            TextureViewDimension textureViewDimension = TextureViewDimension::TwoDimensional)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the vertex shader data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadVertexShader(
            VertexShader& outVS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the vertex shader data from a given file. Also returns the data as a new char* to be used in constructing an input assembler.
        */
        ULTIMAGRAPHICS_API void LoadVertexShader(
            VertexShader& outVS,
            const std::string& filePath,
            char*& data,
            unsigned int& size)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the pixel shader data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadPixelShader(
            PixelShader& outPS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the hull shader data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadHullShader(
            HullShader& outHS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the domain shader data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadDomainShader(
            DomainShader& outDS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT override;

        /*!
        Loads the compute shader data from a given file.
        */
        ULTIMAGRAPHICS_API void LoadComputeShader(
            ComputeShader& outCS,
            const std::string& filePath)
            const ULTIMA_NOEXCEPT override;

        /*!
        Fills up two ID3D11Buffers with data loaded from an FBX file.
        */
        ULTIMAGRAPHICS_API void LoadMesh(
            const std::string& filePath,
            ID3D11Buffer*& outVertexBuffer,
            ID3D11Buffer*& outIndexBuffer,
            unsigned int& outNumberOfIndices,
            Sphere& outSphere)
            ULTIMA_NOEXCEPT;

    protected:
        ID3D11Device* deviceDx11;

        mutable std::map<std::string, LoadedMesh> meshes;
        mutable std::map<std::string, std::pair<ID3D11ShaderResourceView*, unsigned int>> textures;
        mutable std::map<std::string, std::pair<ID3D11VertexShader*, unsigned int>> vertexShaders;
        mutable std::map<std::string, std::pair<ID3D11PixelShader*, unsigned int>> pixelShaders;
        mutable std::map<std::string, std::pair<ID3D11HullShader*, unsigned int>> hullShaders;
        mutable std::map<std::string, std::pair<ID3D11DomainShader*, unsigned int>> domainShaders;
        mutable std::map<std::string, std::pair<ID3D11ComputeShader*, unsigned int>> computeShaders;
    };
}
#endif