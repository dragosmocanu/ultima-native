#include "stdafx.h"

#include "GaussianBlurFilter.h"

namespace Ultima
{

	GaussianBlurFilter::GaussianBlurFilter(
		bool isHorizontal)
		ULTIMA_NOEXCEPT
		: IsHorizontal(isHorizontal)
	{ }

}