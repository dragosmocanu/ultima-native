#include "GraphicsFilter.hlsli"

TextureCube skymap : register(t0);
SamplerState cubeSampler : register(s0);

cbuffer cb0 : register(b0)
{
	GraphicsFilter graphicsFilter;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float4 output = skymap.Sample(cubeSampler, input.Texture);

	output = graphicsFilter.Process(output);

	return output;
}