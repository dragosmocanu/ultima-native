#include "stdafx.h"

#include "ToneMappingFilter.h"

namespace Ultima
{
	ToneMappingFilter::ToneMappingFilter(
		ToneMappingType type,
		float linearWhite,
		float hableShoulderStrength,
		float hableLinearStrength,
		float hableLinearAngle,
		float hableToeStrength,
		float hableToeNumerator,
		float hableToeDenominator)
		ULTIMA_NOEXCEPT
		: Type(type),
		LinearWhite(linearWhite),
		HableShoulderStrength(hableShoulderStrength),
		HableLinearStrength(hableLinearStrength),
		HableLinearAngle(hableLinearAngle),
		HableToeStrength(hableToeStrength),
		HableToeNumerator(hableToeNumerator),
		HableToeDenominator(hableToeDenominator)
	{ }
}