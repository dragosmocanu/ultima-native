#ifndef DISPLACEMENTMAPPINGFILTER_HLSLI
#define DISPLACEMENTMAPPINGFILTER_HLSLI

struct DisplacementMappingFilter
{
	float3 Process(
		Texture2D displacementTexture,
		SamplerState bilinearSampler,
		float2 textureCoordinates,
		float3 position,
		float3 normal)
	{
		float3 result = position;

		if (this.IsActive)
		{
			float h = displacementTexture.SampleLevel(
				bilinearSampler,
				textureCoordinates,
				0).r;

			h += this.Bias;

			result += h * normal * this.Factor;
		}

		return result;
	}

	bool IsActive;
	float Bias;
	float Factor;
};

#endif