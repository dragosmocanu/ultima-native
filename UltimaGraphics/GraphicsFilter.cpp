#include "stdafx.h"

#include "GraphicsFilter.h"

namespace Ultima
{

	GraphicsFilter::GraphicsFilter(
		bool isActive,
		float brightness, 
		float contrast, 
		const Color& tintColor, 
		bool isGrayscale, 
		bool isNegative)
		ULTIMA_NOEXCEPT
	{
		IsActive = isActive;
		TintColor = tintColor.ToXMFLOAT4();
		Brightness = brightness;
		Contrast = contrast;
		IsGrayscale = isGrayscale;
		IsNegative = isNegative;
	}

	Color GraphicsFilter::Process(
		const Fog& fog,
		const Light& light)
		const ULTIMA_NOEXCEPT
	{
		Color temp = fog.Color;
		
		if (light.Type == LightType::None)
			temp *= 0;

		temp = temp + (Color::Invert(temp)) * TintColor * TintColor.w;

		//
		//Color temp2 = TintColor;
		//temp2.SetFloatR(1.0f - temp2.GetFloatR());
		//temp2.SetFloatG(1.0f - temp2.GetFloatG());
		//temp2.SetFloatB(1.0f - temp2.GetFloatB());

		//temp = Color::Saturate(temp + (temp * temp2 
		//	* TintColor.w));
		//

		temp = Color::Saturate((temp - 0.5f) * (Contrast * 2.0f) + 0.5f);

		if (Brightness > 0.5f)
			temp = Color::Lerp(
				temp,
				Color::White,
				(Brightness - 0.5f) * 2.0f);
		else
			temp = Color::Lerp(
				temp,
				Color::Black,
				(0.5f - Brightness) * 2.0f);

		if (IsGrayscale)
		{
			float rgb = 
				temp.GetFloatR() * 0.334f
				+ temp.GetFloatG() * 0.333f
				+ temp.GetFloatB() * 0.333f;

			temp = Color(rgb, rgb, rgb, temp.GetFloatA());
		}

		if (IsNegative)
			temp.Invert();

		return temp;
	}

}