cbuffer cb0 : register(b0)
{
	row_major matrix m;
};

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture : TEXCOORD0;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 Texture : TEXCOORD0;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = mul(float4(input.Position, 1.0f), m).xyww;

	output.Texture = input.Position;

	return output;
}