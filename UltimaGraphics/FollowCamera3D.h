#pragma once

#include "Macros.h"

#include "Camera3D.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a camera that follows an object in 3D space.
	*/
	class FollowCamera3D : public Camera3D
	{
	public:
		/*!
		Creates a new instance of FollowCamera3D.
		*/
		ULTIMAGRAPHICS_API FollowCamera3D(float aspectRatio = 1.777777777777778f,
			float viewDistance = 10000.0f)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of FollowCamera3D.
		*/
		ULTIMAGRAPHICS_API FollowCamera3D(
			float aspectRatio, 
			float viewDistance, 
			float fieldOfView)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~FollowCamera3D()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the pitch rotation of the follow camera.
		*/
		ULTIMAGRAPHICS_API virtual float GetPitch()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the pitch rotation of the follow camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetPitch(float pitch)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the yaw rotation of the follow camera.
		*/
		ULTIMAGRAPHICS_API virtual float GetYaw()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the yaw rotation of the follow camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetYaw(float yaw)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the distance towards the followed 3D object.
		*/
		ULTIMAGRAPHICS_API virtual float GetDistance() const
			ULTIMA_NOEXCEPT;

		/*!
		Sets the distance towards the followed 3D object.
		*/
		ULTIMAGRAPHICS_API virtual void SetDistance(float distance)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetPosition(const Vector3& position)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotation(const Vector3& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the camera by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Move(const Vector3& moveVector)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the camera by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Rotate(const Vector3& rotateVector)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the bounding frustum of the camera.
		*/
		ULTIMAGRAPHICS_API virtual const Frustum& GetFrustum()
			const ULTIMA_NOEXCEPT override;

		/*!
		Updates the camera logic.
		*/
		ULTIMAGRAPHICS_API virtual void Update(float deltaTime,
			const Vector3& targetPosition, const Matrix& targetRotation)
			ULTIMA_NOEXCEPT;

		/*!
		Returns the view-projection matrix of the camera.
		*/
		ULTIMAGRAPHICS_API virtual const Matrix& GetViewProjection()
			const ULTIMA_NOEXCEPT override;

	protected:
		float pitch;
		float yaw;
		float distance;
		
		Matrix viewProjection;
	};
}