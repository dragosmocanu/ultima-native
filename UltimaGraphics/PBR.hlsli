#ifndef PBR_HLSLI
#define PBR_HLSLI

float F0(float x)
{
	x = (1 - x) / (1 + x);
	return x * x;
}

float Pow5(float x)
{
	//return x * x * x * x * x;
	float x2 = x * x;
	float x4 = x2 * x2;
	return x4 * x;
}

//Hable's optimized implementation - http://www.filmicworlds.com/2015/03/17/combined-approximation-of-fresnelvisibility/
float LightingFuncGGX_V90(float roughness)
{
	float alpha = roughness * roughness;

	// V
	//float vis;
	float k = alpha * 0.5f;
	//float k2 = k * k;
	//vis = rcp(k2);

	//return vis;
	return rcp(k * k);
}

float LightingFuncGGX_CheapHelper(float roughness, float F0)
{
	float V90 = LightingFuncGGX_V90(roughness);

	// optional fudge factor
	float fudgeFactor = 1.0;
	{
		float fudgeX0 = .1f;
		float fudgeX1 = .6f;
		float fudgeY0 = .65f;
		float fudgeY1 = 1.10f;
		float fudgeT = saturate((roughness - fudgeX0) / (fudgeX1 - fudgeX0));

		fudgeFactor = lerp(fudgeY0, fudgeY1, fudgeT);
	}

	float A = log2(V90 / F0) * fudgeFactor;

	return A;
}

float4 LightingFuncGGX_CheapFV_Precalc(float roughness, float F0)
{
	float alphaHelper = 0;
	float alphaSqr = 0;
	{
		float alpha = roughness * roughness;
		alphaSqr = alpha*alpha;

		float pi = 3.14159f;
		alphaHelper = (F0 * alphaSqr) / pi;
	}

	float A = LightingFuncGGX_CheapHelper(roughness, F0);

	float4 ret = float4(alphaSqr - 1.0f, alphaHelper, A, -2.0f * A);
	return ret;
}

float LightingFuncGGX_CheapFV_OPT1(float3 N, float3 V, float3 L, float4 preCalc, float dotNL)
{
	// These three lines are highly dependent on the instruction set.  The rest should be
	// mostly consistent across modern GPUs (except maybe DIV)
	float3 H = normalize(V + L);
	float dotNH = saturate(dot(N, H));
	float dotLH = saturate(dot(L, H));

	// Everything after here seems to be 9 scalar instructions.
	float D_denom = dotNH * (dotNH * preCalc.x) + 1.0f; // MUL, MAD
	float D = preCalc.y / (D_denom * D_denom);			// MUL, DIV

	float x = dotLH;
	float A = preCalc.z;
	float neg2A = preCalc.w;

	// no F0 because it is baked into D
	float FV = exp2((A * x + neg2A) * x + A);			// MAD, MAD, EXP2

	float specular = dotNL * D * FV;					// MUL, MUL
	return specular;
}

float LightingFuncGGX_CheapFV_FULL(float3 N, float3 V, float3 L, float roughness, float F0)
{
	float4 preCalc = LightingFuncGGX_CheapFV_Precalc(roughness, F0);
	float dotNL = saturate(dot(N, L));

	float specular = LightingFuncGGX_CheapFV_OPT1(N, V, L, preCalc, dotNL);
	return specular;
}
//Hable

float SpecularPBR(float3 n, float3 v, float3 l, float roughness, float metallic)
{
	return max(LightingFuncGGX_CheapFV_FULL(n, v, l, roughness, metallic), 0);
}

#endif