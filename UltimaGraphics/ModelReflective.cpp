#include "stdafx.h"

#include "ModelReflective.h"

#include "ContentManager.h"
#include "Utilities.h"

namespace Ultima
{
	ModelReflective::ModelReflective(
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const std::string& bumpmapPath,
		const std::string& displacementMapPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount,
		unsigned int reflectiveTextureSize)
		ULTIMA_NOEXCEPT
		: Model(device, meshPath, texturePath, bumpmapPath, displacementMapPath, 
		position, rotation, scale, anisotropicSampleCount),
		reflectiveTextureSize(reflectiveTextureSize)
	{ 
		buildReflectiveData();
	}

	ModelReflective::ModelReflective(
		const Device& device,
		const std::string& meshPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount,
		unsigned int reflectiveTextureSize)
		ULTIMA_NOEXCEPT
		: Model(device, meshPath, position, rotation, scale, anisotropicSampleCount),
		reflectiveTextureSize(reflectiveTextureSize)
	{
		buildReflectiveData();
	}

	ModelReflective::ModelReflective(const ModelReflective& other)
		ULTIMA_NOEXCEPT
		: Model(other)
	{
		if (this != &other)
		{
			destructReflectiveData();

			reflectiveTextureSize = other.GetReflectiveTextureSize();
			buildReflectiveData();
		}
	}

	ModelReflective& ModelReflective::operator=(const ModelReflective& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			destructReflectiveData();
			destructModel();

			position = other.GetPosition();
			rotation = other.GetRotation();
			scale = other.GetScale();
			material = other.GetMaterial();
			meshPath = other.GetMeshPath();
			texturePath = other.GetTexturePath();
			bumpMapPath = other.GetBumpMapPath();
			displacementMapPath = other.GetDisplacementMapPath();
			device = other.GetDevice();
			anisotropicSampleCount = other.GetAnisotropicSampleCount();
			reflectiveTextureSize = other.GetReflectiveTextureSize();

			buildModel();
			buildReflectiveData();
		}

		return *this;
	}

	void ModelReflective::buildReflectiveData()
		ULTIMA_NOEXCEPT
	{
		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));

		texDesc.Width = reflectiveTextureSize;
		texDesc.Height = reflectiveTextureSize;
		texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS |
			D3D11_RESOURCE_MISC_TEXTURECUBE;
		texDesc.MipLevels = 0;
		texDesc.ArraySize = 6;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET |
			D3D11_BIND_SHADER_RESOURCE;
		texDesc.CPUAccessFlags = 0;
		srvDesc.Format = rtvDesc.Format = texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

		ID3D11Texture2D* reflectanceTexture = nullptr;

		Utilities::Assert(device->GetDev()->CreateTexture2D(&texDesc, nullptr, &reflectanceTexture));

		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
		rtvDesc.Texture2DArray.MipSlice = 0;
		rtvDesc.Texture2DArray.ArraySize = 1;

		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
		srvDesc.TextureCube.MostDetailedMip = 0;
		srvDesc.TextureCube.MipLevels = 1;

		for (unsigned int i = 0; i < 6; i++)
		{
			rtvDesc.Texture2DArray.FirstArraySlice = i;
			Utilities::Assert(device->GetDev()->CreateRenderTargetView(reflectanceTexture, &rtvDesc,
				&reflectiveRTV[i]));
		}

		Utilities::Assert(device->GetDev()->CreateShaderResourceView(reflectanceTexture, &srvDesc,
			&reflectiveSRV));

		Utilities::Release(reflectanceTexture);

		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = DXGI_FORMAT_D32_FLOAT;
		texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		texDesc.MiscFlags = 0;

		ID3D11Texture2D* depthTexture = nullptr;

		Utilities::Assert(device->GetDev()->CreateTexture2D(&texDesc, nullptr, &depthTexture));

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		SecureZeroMemory(&dsvDesc, sizeof(dsvDesc));
		dsvDesc.Format = texDesc.Format;
		dsvDesc.Flags = 0;
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->GetDev()->CreateDepthStencilView(depthTexture, &dsvDesc, &reflectiveDSV));

		Utilities::Release(depthTexture);

		reflectiveViewport.TopLeftX = 0.0f;
		reflectiveViewport.TopLeftY = 0.0f;
		reflectiveViewport.Width = static_cast<float>(reflectiveTextureSize);
		reflectiveViewport.Height = static_cast<float>(reflectiveTextureSize);
		reflectiveViewport.MinDepth = 0.0f;
		reflectiveViewport.MaxDepth = 1.0f;

		device->LoadPixelShader(pixelShaderReflective, "UltimaShaders/PixelShaderReflective.cso");

		device->LoadPixelShader(pixelShaderDeferredReflective, "UltimaShaders/PixelShaderDeferredReflective.cso");

		XMFLOAT3 center(0.0f, 0.0f, 0.0f);
		XMFLOAT3 up(0.0f, 1.0f, 0.0f);

		float x, y, z;
		x = position.GetX();
		y = position.GetY();
		z = position.GetZ();

		XMFLOAT3 targets[6] =
		{
			XMFLOAT3(x + 1.0f, y, z),
			XMFLOAT3(x - 1.0f, y, z),
			XMFLOAT3(x, y + 1.0f, z),
			XMFLOAT3(x, y - 1.0f, z),
			XMFLOAT3(x, y, z + 1.0f),
			XMFLOAT3(x, y, z - 1.0f)
		};

		XMFLOAT3 ups[6] =
		{
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 0.0f, -1.0f),
			XMFLOAT3(0.0f, 0.0f, +1.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f)
		};

		for (unsigned int i = 0; i < 6; i++)
		{
			views[i] = Matrix::CreateLookAt(position, targets[i], ups[i]);
			projections[i] = Matrix::CreateProjection(XM_PI * 0.5f, 1.0f,
				sphere.GetRadius(), sphere.GetRadius() + 1000.0f);
			frustums[i] = Frustum(projections[i]);
			frustums[i].Transform(projections[i], position, views[i]);
		}
	}

	void ModelReflective::destructReflectiveData()
		ULTIMA_NOEXCEPT
	{
		Utilities::Release(reflectiveDSV);
		Utilities::Release(reflectiveSRV);

		for (unsigned int i = 0; i < 6; i++)
			Utilities::Release(reflectiveRTV[i]);
	}

	ModelReflective::~ModelReflective()
		ULTIMA_NOEXCEPT
	{
		destructReflectiveData();
	}

	void ModelReflective::SetPosition(const Vector3& position)
		ULTIMA_NOEXCEPT
	{
		Model::SetPosition(position);
		rebuildMatrices();
	}

	void ModelReflective::MoveX(float f)
		ULTIMA_NOEXCEPT
	{
		Model::MoveX(f);
		rebuildMatrices();
	}

	void ModelReflective::MoveY(float f)
		ULTIMA_NOEXCEPT
	{
		Model::MoveY(f);
		rebuildMatrices();
	}

	void ModelReflective::MoveZ(float f)
		ULTIMA_NOEXCEPT
	{
		Model::MoveZ(f);
		rebuildMatrices();
	}

	void ModelReflective::Move(const Vector3& moveVector)
		ULTIMA_NOEXCEPT
	{
		Model::Move(moveVector);
		rebuildMatrices();
	}

	unsigned int ModelReflective::GetReflectiveTextureSize()
		const ULTIMA_NOEXCEPT
	{
		return reflectiveTextureSize;
	}

	void ModelReflective::rebuildMatrices()
		ULTIMA_NOEXCEPT
	{
		float x, y, z;
		x = position.GetX();
		y = position.GetY();
		z = position.GetZ();

		XMFLOAT3 targets[6] =
		{
			XMFLOAT3(x + 1.0f, y, z),
			XMFLOAT3(x - 1.0f, y, z),
			XMFLOAT3(x, y + 1.0f, z),
			XMFLOAT3(x, y - 1.0f, z),
			XMFLOAT3(x, y, z + 1.0f),
			XMFLOAT3(x, y, z - 1.0f)
		};

		XMFLOAT3 ups[6] =
		{
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 0.0f, -1.0f),
			XMFLOAT3(0.0f, 0.0f, +1.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f),
			XMFLOAT3(0.0f, 1.0f, 0.0f)
		};

		for (unsigned int i = 0; i < 6; i++)
		{
			views[i] = Matrix::CreateLookAt(position, targets[i], ups[i]);
			frustums[i].Transform(projections[i], position, views[i]);
		}
	}

	unsigned int ModelReflective::ComputeReflections(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		const Color& clearColor,
		const std::vector<Model*>& models,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int noTriangles = 0;

		if (material.Metallic > 0.0f
			&& material.Roughness < 1.0f
			&& camera.GetFrustum().IsColliding(this->sphere))
		{
			context->RSSetViewports(1, &reflectiveViewport);

			float f[4] =
			{
				clearColor.GetFloatR(),
				clearColor.GetFloatG(),
				clearColor.GetFloatB(),
				clearColor.GetFloatA()
			};

			for (unsigned int i = 0; i < 6; i++)
			{
				context->ClearRenderTargetView(reflectiveRTV[i], f);
				context->ClearDepthStencilView(reflectiveDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);

				context->OMSetRenderTargets(1, &reflectiveRTV[i], reflectiveDSV);

				Matrix viewProjection = views[i] * projections[i];

				for (auto& x : models)
					noTriangles += x->Render(context, viewProjection,
						this->position, this->frustums[i], light, fog, graphicsFilter,
							nullptr, shadowMap, lightViewProjection, shadowFilter);
			}

			context->GenerateMips(reflectiveSRV);
		}

		return noTriangles;
	}

	unsigned int ModelReflective::ComputeReflections(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		const Color& clearColor,
		const Skybox* const skybox,
		const std::vector<Model*>& models,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		unsigned int noTriangles = 0;

		if (material.Metallic > 0.0f
			&& material.Roughness < 1.0f
			&& camera.GetFrustum().IsColliding(this->sphere))
		{
			context->RSSetViewports(1, &reflectiveViewport);

			float f[4] =
			{
				clearColor.GetFloatR(),
				clearColor.GetFloatG(),
				clearColor.GetFloatB(),
				clearColor.GetFloatA()
			};

			for (unsigned int i = 0; i < 6; i++)
			{
				context->ClearRenderTargetView(reflectiveRTV[i], f);
				context->ClearDepthStencilView(reflectiveDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);

				context->OMSetRenderTargets(1, &reflectiveRTV[i], reflectiveDSV);

				Matrix viewProjectionSkybox = views[i];
				viewProjectionSkybox.SetM30(0);
				viewProjectionSkybox.SetM31(0);
				viewProjectionSkybox.SetM32(0);
				viewProjectionSkybox *= projections[i];

				skybox->Render(context, viewProjectionSkybox, light, fog, graphicsFilter);

				Matrix viewProjection = views[i] * projections[i];

				for (auto& x : models)
					noTriangles += x->Render(context, viewProjection,
						this->position, this->frustums[i], light, fog, graphicsFilter,
							nullptr, shadowMap, lightViewProjection, shadowFilter);
			}

			context->GenerateMips(reflectiveSRV);
		}

		return noTriangles;
	}

	unsigned int ModelReflective::RenderReflective(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const ambientOcclusionMap,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		return RenderReflective(context, camera.GetViewProjection(), 
			camera.GetPosition(), camera.GetFrustum(), light, fog, graphicsFilter, 
			ambientOcclusionMap, shadowMap, lightViewProjection, shadowFilter);
	}

	unsigned int ModelReflective::RenderReflective(
		ID3D11DeviceContext* const context,
		const Matrix& viewProjection,
		const Vector3& cameraPosition,
		const Frustum& frustum,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter,
		ID3D11ShaderResourceView* const ambientOcclusionMap,
		ID3D11ShaderResourceView* const shadowMap,
		const Matrix& lightViewProjection,
		const ShadowFilter& shadowFilter)
		const ULTIMA_NOEXCEPT
	{
		if (!frustum.IsColliding(sphere))
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
			* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = viewProjection.ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = lightViewProjection.ToXMMATRIX();
		context->UpdateSubresource(lightViewProjectionBuffer, 0, 0, &tempXM, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);
		context->VSSetConstantBuffers(2, 1, &lightViewProjectionBuffer);

		context->UpdateSubresource(lightBuffer, 0, 0, &light, 0, 0);

		XMFLOAT3 eyePos = cameraPosition.ToXMFLOAT3();
		context->UpdateSubresource(cameraPositionBuffer, 0, 0, &eyePos, 0, 0);

		context->UpdateSubresource(materialBuffer, 0, 0, &material, 0, 0);

		context->UpdateSubresource(fogBuffer, 0, 0, &fog, 0, 0);

		context->UpdateSubresource(graphicsFilterBuffer, 0, 0, &graphicsFilter, 0, 0);

		context->UpdateSubresource(shadowFilterBuffer, 0, 0, &shadowFilter, 0, 0);

		context->UpdateSubresource(bumpMappingFilterBuffer, 0, 0, &bumpMappingFilter, 0, 0);

		eyePos.x = (ambientOcclusionMap != nullptr);
		context->UpdateSubresource(ambientOcclusionBuffer, 0, 0, &eyePos, 0, 0);

		eyePos.x = (colorTex.GetSrv() != nullptr);
		context->UpdateSubresource(boolBuffer, 0, 0, &eyePos, 0, 0);

		context->PSSetConstantBuffers(0, 1, &lightBuffer);
		context->PSSetConstantBuffers(1, 1, &cameraPositionBuffer);
		context->PSSetConstantBuffers(2, 1, &materialBuffer);
		context->PSSetConstantBuffers(3, 1, &fogBuffer);
		context->PSSetConstantBuffers(4, 1, &graphicsFilterBuffer);
		context->PSSetConstantBuffers(5, 1, &shadowFilterBuffer);
		context->PSSetConstantBuffers(6, 1, &bumpMappingFilterBuffer);
		context->PSSetConstantBuffers(7, 1, &ambientOcclusionBuffer);
		context->PSSetConstantBuffers(8, 1, &boolBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());
		context->PSSetShaderResources(1, 1, normalTex.GetSrvPtr());
		context->PSSetShaderResources(2, 1, &ambientOcclusionMap);
		context->PSSetShaderResources(3, 1, &shadowMap);
		context->PSSetShaderResources(4, 1, &reflectiveSRV);

		context->PSSetSamplers(0, 1, &anisotropicSampler);
		context->PSSetSamplers(1, 1, &comparisonSampler);
		context->PSSetSamplers(2, 1, &bilinearSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderReflective.GetShader(), 0, 0);

		if (displacementTex.GetSrv() != nullptr)
		{
			context->DSSetConstantBuffers(0, 1, &viewBuffer);
			context->DSSetConstantBuffers(1, 1, &projectionBuffer);

			context->DSSetShaderResources(0, 1, displacementTex.GetSrvPtr());

			context->DSSetSamplers(0, 1, &anisotropicSampler);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			context->HSSetShader(hullShaderStandard.GetShader(), 0, 0);
			context->DSSetShader(domainShaderStandard.GetShader(), 0, 0);
		}

		context->DrawIndexed(numberOfIndices, 0, 0);

		context->HSSetShader(nullptr, 0, 0);
		context->DSSetShader(nullptr, 0, 0);

		return numberOfIndices / 3;
	}


	unsigned int ModelReflective::RenderDeferredReflective(
		ID3D11DeviceContext* const context,
		const Camera3D& camera)
		const ULTIMA_NOEXCEPT
	{
		if (!camera.GetFrustum().IsColliding(sphere))
			return 0;

		unsigned int stride = sizeof(Vertex);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world;
		XMMATRIX tempXM;

		if (scale == 1.0f)
			world = rotation * Matrix::CreateTranslation(position);
		else
			world = Matrix::CreateScale(scale) * rotation
			* Matrix::CreateTranslation(position);

		tempXM = world.ToXMMATRIX();
		context->UpdateSubresource(worldBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = camera.GetView().ToXMMATRIX();
		context->UpdateSubresource(viewBuffer, 0, 0, &tempXM, 0, 0);

		tempXM = camera.GetProjection().ToXMMATRIX();
		context->UpdateSubresource(projectionBuffer, 0, 0, &tempXM, 0, 0);

		XMFLOAT3 eyePos = camera.GetPosition().ToXMFLOAT3();
		context->UpdateSubresource(cameraPositionBuffer, 0, 0, &eyePos, 0, 0);

		context->UpdateSubresource(materialBuffer, 0, 0, &material, 0, 0);

		context->UpdateSubresource(bumpMappingFilterBuffer, 0, 0, &bumpMappingFilter, 0, 0);

		eyePos.x = (colorTex.GetSrv() != nullptr);

		context->UpdateSubresource(boolBuffer, 0, 0, &eyePos, 0, 0);

		context->VSSetConstantBuffers(0, 1, &worldBuffer);
		context->VSSetConstantBuffers(1, 1, &viewBuffer);
		context->VSSetConstantBuffers(2, 1, &projectionBuffer);
		context->VSSetConstantBuffers(3, 1, &cameraPositionBuffer);

		context->PSSetConstantBuffers(0, 1, &materialBuffer);
		context->PSSetConstantBuffers(1, 1, &bumpMappingFilterBuffer);
		context->PSSetConstantBuffers(2, 1, &cameraPositionBuffer);
		context->PSSetConstantBuffers(3, 1, &boolBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());
		context->PSSetShaderResources(1, 1, normalTex.GetSrvPtr());
		context->PSSetShaderResources(2, 1, &reflectiveSRV);

		context->PSSetSamplers(0, 1, &anisotropicSampler);

		context->VSSetShader(vertexShaderDeferred.GetShader(), 0, 0);
		context->PSSetShader(pixelShaderDeferredReflective.GetShader(), 0, 0);

		if (displacementTex.GetSrv() != nullptr)
		{
			context->DSSetConstantBuffers(0, 1, &viewBuffer);
			context->DSSetConstantBuffers(1, 1, &projectionBuffer);

			context->DSSetShaderResources(0, 1, displacementTex.GetSrvPtr());

			context->DSSetSamplers(0, 1, &bilinearSampler);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST);
			context->HSSetShader(hullShaderDeferred.GetShader(), 0, 0);
			context->DSSetShader(domainShaderDeferred.GetShader(), 0, 0);
		}

		context->DrawIndexed(numberOfIndices, 0, 0);

		context->HSSetShader(nullptr, 0, 0);
		context->DSSetShader(nullptr, 0, 0);

		return numberOfIndices / 3;
	}

}