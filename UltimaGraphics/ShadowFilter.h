#pragma once

#include "Macros.h"

#include "Vector2.h"

namespace Ultima
{
	/*!
	Defines the different algorithms used to filter shadows.
	*/
	enum class ShadowType
	{
		None, //No shadows
		PCF, //Percentage Closer Filtering
		VSM, //Variance Shadow Maps
		EVSM, //Exponential Variance Shadow Maps
		EVSM4 //Exponential Variance Shadow Maps 4
	};

	/*!
	Defines a shadow filter filled with options for generating shadows.
	PCF, VSM, EVSM & EVSM4 implementations.
	*/
	struct ShadowFilter
	{
		/*!
		Creates a new instance of ShadowFilter.
		*/
		ULTIMAGRAPHICS_API ShadowFilter(
			ShadowType type = ShadowType::None,
			unsigned int pcfNumberOfSamples = 4,
			float pcfBias = 0.001f,
			float vsmMinVariance = 0.001f,
			float vsmBleedingReduction = 0.999f,
			Vector2 evsmExponents = Vector2(8.0f, 5.0f))
			ULTIMA_NOEXCEPT;

		ShadowType Type;
		unsigned int PcfNumberOfSamples;
		float PcfBias;
		float VsmMinVariance;

		float VsmBleedingReduction;
		XMFLOAT2 EvsmExponents;
	};
}