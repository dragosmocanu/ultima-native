#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines a Gaussian blur filter.
	*/
	struct GaussianBlurFilter
	{
		/*!
		Creates a new instance of GaussianBlurFilter.
		*/
		ULTIMAGRAPHICS_API GaussianBlurFilter(
			bool isHorizontal = true)
			ULTIMA_NOEXCEPT;

		float IsHorizontal;
	};
}