cbuffer cb0 : register(b0)
{
	float4 SMAA_RT_METRICS;
}

#define SMAA_HLSL_4_1
#define SMAA_PRESET_HIGH
#include "SMAA.hlsli"

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture : TEXCOORD0;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
	float2 Pixcoord : TEXCOORD1;
	float4 Offset[3] : TEXCOORD2;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = float4(input.Position, 1.0f);
	output.Texture = input.Texture;

	SMAABlendingWeightCalculationVS(
		output.Texture,
		output.Pixcoord,
		output.Offset);

	return output;
}