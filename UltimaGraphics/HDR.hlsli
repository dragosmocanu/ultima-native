#ifndef HDR_HLSLI
#define HDR_HLSLI

#pragma warning (disable : 4000)

//http://content.gpwiki.org/index.php/D3DBook:High-Dynamic_Range_Rendering
float3 CalculateExposedColor(float3 color, float averageLuminance, float threshold)
{
	averageLuminance = max(averageLuminance, 0.001f);

	float middleGrey = 1.03f - (2.0f / (2 + log10(averageLuminance + 1)));

	float linearExposure = (middleGrey / averageLuminance);
	float exposure = log2(max(linearExposure, 0.0001f));

	exposure -= threshold;
	color = exp2(exposure) * color;
	return color;
}

//http://www.w3.org/TR/AERT#color-contrast
float CalculateLuminance(float3 color)
{
	return max(dot(color, float3(0.299f, 0.587f, 0.114f)), 0.0001f);
}

float3 CorrectGamma(float3 color)
{
	return pow(abs(color), 0.454545438f);
}

#endif