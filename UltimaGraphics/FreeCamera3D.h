#pragma once

#include "Macros.h"

#include "Camera3D.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a camera that can be moved and rotated in 3D space.
	*/
	class FreeCamera3D : public Camera3D
	{
	public:
		/*!
		Creates a new instance of FreeCamera3D.
		*/
		ULTIMAGRAPHICS_API FreeCamera3D(float aspectRatio = 1.777777777777778f,
			float viewDistance = 10000.0f)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of FreeCamera3D.
		*/
		ULTIMAGRAPHICS_API FreeCamera3D(
			float aspectRatio, 
			float viewDistance, 
			float fieldOfView)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~FreeCamera3D()
			ULTIMA_NOEXCEPT;

		/*!
		Moves the camera along its X axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveX(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the camera along its Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveY(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the camera along its Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveZ(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Moves the camera by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Move(const Vector3& moveVector)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the free camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetPosition(const Vector3& position)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the camera along its X axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateX(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the camera along its Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateY(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the camera along its Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void RotateZ(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Rotates the camera by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Rotate(const Vector3& rotateVector)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation along the camera's X axis.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotationX(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation along the camera's Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotationY(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation along the camera's Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotationZ(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the free camera.
		*/
		ULTIMAGRAPHICS_API virtual void SetRotation(const Vector3& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the bounding frustum of the camera.
		*/
		ULTIMAGRAPHICS_API virtual const Frustum& GetFrustum()
			const ULTIMA_NOEXCEPT override;

		/*!
		Updates the camera logic.
		*/
		ULTIMAGRAPHICS_API virtual void Update(float deltaTime)
			ULTIMA_NOEXCEPT;

		/*!
		Reinitializes the camera with a different aspect ratio and view distance.
		*/
		ULTIMAGRAPHICS_API virtual void Reinitialize(
			float aspectRatio,
			float viewDistance = 10000.0f)
			ULTIMA_NOEXCEPT override;

		/*!
		Returns the view-projection matrix of the camera.
		*/
		ULTIMAGRAPHICS_API virtual const Matrix& GetViewProjection()
			const ULTIMA_NOEXCEPT override;

	protected:
		Vector3 movement;
		Vector3 target;

		float pitch;
		float yaw;
		float roll;

		bool wasModified;
		Matrix viewProjection;
	};
}