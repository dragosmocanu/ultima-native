#ifndef ANTIALIASINGFILTER_HLSLI
#define ANTIALIASINGFILTER_HLSLI

#pragma warning (disable : 4000)

#define FXAA_PC 1
#define FXAA_HLSL_5 1
#define FXAA_QUALITY__PRESET 25
#define FXAA_GREEN_AS_LUMA 1
#include "FXAA.hlsli"

struct AntiAliasingFilter
{
	float4 Process(
		float2 pixelPosition,
		SamplerState bilinearSampler,
		Texture2D colorTexture,
		float2 screenSizeInPixels)
	{
		if (Type == 0)
			return colorTexture.Sample(bilinearSampler, pixelPosition);

		float4 zero = float4(0.0f, 0.0f, 0.0f, 0.0f);

			FxaaTex fxaaTex = { bilinearSampler, colorTexture };

		return FxaaPixelShader(
			pixelPosition,
			zero, //not used on PC
			fxaaTex,
			fxaaTex, //not used on PC
			fxaaTex, //not used on PC
			1.0f / screenSizeInPixels, //transforming them into texels
			zero, //not used on PC
			zero, //not used on PC
			zero, //not used on PC
			this.FxaaSubPixel,
			this.FxaaEdgeThreshHold,
			0, //not used in nonLUMA calculations
			0, //not used on PC
			0, //not used on PC
			0, //not used on PC
			0  //not used on PC
			);
	}

	unsigned int Type;
	float FxaaSubPixel;
	float FxaaEdgeThreshHold;
};

#endif