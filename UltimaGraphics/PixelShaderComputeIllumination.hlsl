#include "Light.hlsli"
#include "Material.hlsli"
#include "ShadowFilter.hlsli"

Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D depthMapTexture : register(t2);

SamplerState bilinearSampler : register(s0);
SamplerComparisonState comparisonSampler : register(s1);

cbuffer cb0 : register(b0)
{
	Light light;
}

cbuffer cb1 : register(b1)
{
	float3 camPos;
}

cbuffer cb2 : register(b2)
{
	ShadowFilter shadowFilter;
}

cbuffer cb3 : register(b3)
{
	row_major matrix lightViewProjection;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

struct PixelOut
{
	float4 Diffuse : SV_TARGET0;
	float4 Specular : SV_TARGET1;
	float4 Ambient : SV_TARGET2;
};

PixelOut main(VertexOut input)
{
	Material material;
	material.Base = float4(0, 0, 0, 0);
	material.Metallic = material.Roughness = 0;
	
	float4 data2 = normalTexture.Sample(bilinearSampler, input.Texture);

	float3 normal = data2.xyz;
	material.Roughness = data2.w;

	if (normal.x == 0 && normal.y == 0 && normal.z == 0)
	{
		PixelOut fast;
		fast.Diffuse = (float4)0;
		fast.Specular = (float4)0;
		fast.Ambient = float4(1, 1, 1, 1);
		return fast;
	}

	float4 data1 = positionTexture.Sample(bilinearSampler, input.Texture);

	float3 position = data1.xyz;
	material.Metallic = data1.w;

	float4 positionL = float4(position, 1);
	positionL = mul(positionL, lightViewProjection);

	float lightPower = shadowFilter.Process(
		positionL,
		depthMapTexture,
		bilinearSampler,
		comparisonSampler);

	float3 toEye = camPos - position;
	float distanceToEye = length(toEye);
	toEye /= distanceToEye;

	float4 diffuse, specular, ambient;
	diffuse = specular = ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);

	light.Process(material.Metallic, material.Roughness, position, normal, toEye, lightPower, 
		Pow5(lightPower), diffuse, specular, ambient);

	PixelOut output;
	output.Diffuse = diffuse;
	output.Specular = specular;
	output.Ambient = ambient;

	return output;
}