#include "DisplacementMappingFilter.hlsli"

Texture2D displacementTexture : register(t0);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	row_major matrix view;
}

cbuffer cb1 : register(b1)
{
	row_major matrix projection;
}

cbuffer cb2 : register(b2)
{
	DisplacementMappingFilter dmf;
}

// Output control point
struct HS_CONTROL_POINT_OUTPUT
{
	float3 PositionW : POSITION;
	float3 NormalW : NORMAL;
	float3 PositionV : POSITIONV;
	float3 NormalV : NORMALV;
	float2 Texture : TEXCOORD0;
	float3 TangentW : TANGENT;
};

struct DS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 NormalW : NORMAL;
	float3 PositionV : POSITIONV;
	float3 NormalV : NORMALV;
	float2 Texture : TEXCOORD0;
	float3 TangentW : TANGENT;
};

// Output patch constant data.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]			: SV_TessFactor; // e.g. would be [4] for a quad domain
	float InsideTessFactor : SV_InsideTessFactor; // e.g. would be Inside[2] for a quad domain
	// TODO: change/add other stuff
};

#define NUM_CONTROL_POINTS 3

[domain("tri")]
DS_OUTPUT main(
	HS_CONSTANT_DATA_OUTPUT input,
	float3 domain : SV_DomainLocation,
	const OutputPatch<HS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> patch)
{
	DS_OUTPUT output = (DS_OUTPUT)0;

	output.PositionW = domain.x * patch[0].PositionW
		+ domain.y * patch[1].PositionW
		+ domain.z * patch[2].PositionW;

	output.NormalW = domain.x * patch[0].NormalW
		+ domain.y * patch[1].NormalW
		+ domain.z * patch[2].NormalW;

	output.NormalV = domain.x * patch[0].NormalV
		+ domain.y * patch[1].NormalV
		+ domain.z * patch[2].NormalV;

	output.Texture = domain.x * patch[0].Texture
		+ domain.y * patch[1].Texture
		+ domain.z * patch[2].Texture;

	output.TangentW = domain.x * patch[0].TangentW
		+ domain.y * patch[1].TangentW
		+ domain.z * patch[2].TangentW;

	output.PositionW = dmf.Process(displacementTexture, bilinearSampler, output.Texture,
		output.PositionW, output.NormalW);

	output.Position = mul(float4(output.PositionW, 1.0f), view);

	output.PositionV = output.Position.xyz;

	output.Position = mul(output.Position, projection);

	return output;
}
