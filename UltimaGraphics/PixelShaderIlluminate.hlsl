#include "AmbientOcclusionFilter.hlsli"
#include "GraphicsFilter.hlsli"
#include "Material.hlsli"

Texture2D colorTexture : register(t0);
Texture2D diffuseTexture : register(t1);
Texture2D specularTexture : register(t2);
Texture2D ambientTexture : register(t3);
Texture2D materialTexture : register(t4);
Texture2D<float> aoTexture : register(t5);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	GraphicsFilter gf;
}

cbuffer cb1 : register(b1)
{
	AmbientOcclusionFilter aoFilter;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float4 color = colorTexture.Sample(bilinearSampler, input.Texture);
	float4 diffuse = diffuseTexture.Sample(bilinearSampler, input.Texture);
	float4 specular = specularTexture.Sample(bilinearSampler, input.Texture);
	float4 ambient = ambientTexture.Sample(bilinearSampler, input.Texture);

	Material material;
	material.Base = materialTexture.Sample(bilinearSampler, input.Texture);
	material.Roughness = material.Metallic = 0;

	float ao = 1;

	if (aoFilter.Type != 0)
	{
		ao = aoTexture.Sample(bilinearSampler, input.Texture);
	}

	float4 output = material.Modulate(color, diffuse, specular, ambient * ao);

	return output;
}