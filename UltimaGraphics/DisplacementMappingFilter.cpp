#include "stdafx.h"

#include "DisplacementMappingFilter.h"

namespace Ultima
{
	DisplacementMappingFilter::DisplacementMappingFilter(
		bool isActive,
		float bias,
		float factor)
		ULTIMA_NOEXCEPT
	{
		IsActive = isActive;
		Bias = bias;
		Factor = factor;
	}
}