#pragma once

#include "Macros.h"

#include "AmbientOcclusionFilter.h"
#include "AntiAliasingFilter.h"
#include "BloomFilter.h"
#include "Camera3D.h"
#include "Device.h"
#include "GaussianBlurFilter.h"
#include "GraphicsFilter.h"
#include "ToneMappingFilter.h"
#include "ShadowFilter.h"
#include "Vector2.h"

namespace Ultima
{
	/*!
	Defines a quad that is used to draw to the screen.
	If it is a full screen quad, it will be implemented as a triangle.
	*/
	class Quad
	{
	public:

		/*!
		Creates a new instance of Quad.
		*/
		ULTIMAGRAPHICS_API Quad(
			const Device& device,
			float left = -1.0f, 
			float right = 1.0f, 
			float top = 1.0f, 
			float bottom = -1.0f)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~Quad()
			ULTIMA_NOEXCEPT;

		/*!
		Computes ambient occlusion from given positions and normals SRVs.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeAmbientOcclusion(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const positionSRV,
			ID3D11ShaderResourceView* const normalSRV, 
			const AmbientOcclusionFilter& aoFilter = AmbientOcclusionFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Performs a Gaussian blur on a specified texture.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Blur(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			const GaussianBlurFilter& blurFilter)
			const ULTIMA_NOEXCEPT;

		/*!
		Anti-aliases a given input SRV and renders to a given output RTV.
		Parameters smaaSRV1, smaaRTV1, smaaSRV2, and smaaRTV2 are used only when antiAliasingFilter's Type is set to SMAA.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int AntiAlias(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const input,
			ID3D11RenderTargetView* const output,
			const AntiAliasingFilter& antiAliasingFilter,
			ID3D11ShaderResourceView* const smaaSRV1 = nullptr,
			ID3D11RenderTargetView* const smaaRTV1 = nullptr,
			ID3D11ShaderResourceView* const smaaSRV2 = nullptr,
			ID3D11RenderTargetView* const smaaRTV2 = nullptr,
			const Vector2& screenSize = Vector2(1280.0f, 720.0f))
			const ULTIMA_NOEXCEPT;

		/*!
		Anti-aliases a given colorSRV using the FXAA algorithm.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Fxaa(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			const AntiAliasingFilter& antiAliasingFilter)
			const ULTIMA_NOEXCEPT;

		/*!
		Performs the first pass (edge detection) of the SMAA algorithm.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Smaa1(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			const Vector2& screenSize)
			const ULTIMA_NOEXCEPT;

		/*!
		Performs the second pass (blend weight calculation) of the SMAA algorithm.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Smaa2(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const smaaSRV1,
			const Vector2& screenSize)
			const ULTIMA_NOEXCEPT;

		/*!
		Performs the third pass (neightbourhood blending) of the SMAA algorithm.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Smaa3(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			ID3D11ShaderResourceView* const smaaSRV2,
			const Vector2& screenSize)
			const ULTIMA_NOEXCEPT;

		/*!
		Computes the luminance of a given SRV.
		Delta time is in milliseconds.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeLuminance(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			ID3D11ShaderResourceView* const previousLuminanceSRV,
			float deltaTime,
			bool isAdaptive = true,
			float adaptationRate = 1.0f)
			const ULTIMA_NOEXCEPT;

		/*!
		Shows the luminance map.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ShowLuminance(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const luminanceSRV,
			unsigned int mipLevel)
			const ULTIMA_NOEXCEPT;

		/*!
		Computes the bloom effect on a given color SRV.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeBloom(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			ID3D11ShaderResourceView* const luminanceSRV,
			unsigned int mipLevel,
			const BloomFilter& bloomFilter = BloomFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Computes the tone map of a given color SRV using a luminance SRV.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Tone(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			ID3D11ShaderResourceView* const luminanceSRV,
			unsigned int mipLevel,
			ID3D11ShaderResourceView* const bloomSRV,
			const ToneMappingFilter& toneMappingFilter,
			const BloomFilter& bloomFilter,
			const GraphicsFilter& graphicsFilter)
			const ULTIMA_NOEXCEPT;

		/*!
		Copies the data from a given SRV.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Copy(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const inputSRV,
			bool isSingleChannel = false)
			const ULTIMA_NOEXCEPT;

		/*!
		Computes the illumation factors (ambient, diffuse, and specular) into three textures.
		Three different HDR textures should be set as a render targets before calling this function.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeIllumination(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			ID3D11ShaderResourceView* const positionSRV,
			ID3D11ShaderResourceView* const normalSRV,
			const Light& light = Light(),
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Illuminates a given texture by using three different textures that have the illumination data.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Illuminate(
			ID3D11DeviceContext* const context,
			ID3D11ShaderResourceView* const colorSRV,
			ID3D11ShaderResourceView* const diffuseSRV,
			ID3D11ShaderResourceView* const specularSRV,
			ID3D11ShaderResourceView* const ambientSRV,
			ID3D11ShaderResourceView* const materialSRV,
			ID3D11ShaderResourceView* const aoSRV,
			const AmbientOcclusionFilter& aoFilter = AmbientOcclusionFilter(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter())
			const ULTIMA_NOEXCEPT;

	protected:
		unsigned int numberOfIndices;

		ID3D11InputLayout* inputLayout;

		VertexShader vertexShader;

		PixelShader pixelShaderComputeAO;

        PixelShader pixelShaderBlur;

        PixelShader pixelShaderFxaa;

        VertexShader vertexShaderSmaa1;
        PixelShader pixelShaderSmaa1;

        VertexShader vertexShaderSmaa2;
        PixelShader pixelShaderSmaa2;
		ID3D11ShaderResourceView* areaSRV;
		ID3D11ShaderResourceView* searchSRV;

        VertexShader vertexShaderSmaa3;
        PixelShader pixelShaderSmaa3;

		PixelShader pixelShaderComputeLuminance;
		PixelShader pixelShaderShowLuminance;
		PixelShader pixelShaderComputeBloom;
		PixelShader pixelShaderTone;
        
		PixelShader pixelShaderCopy;
        
		PixelShader pixelShaderComputeIllumination;
		PixelShader pixelShaderIlluminate;

		ID3D11Buffer* vertexBuffer;
		ID3D11Buffer* indexBuffer;

		ID3D11Buffer* gaussianBlurFilterBuffer;
		ID3D11Buffer* bloomBuffer;

		ID3D11Buffer* antiAliasingFilterBuffer;
		ID3D11Buffer* ambientOcclusionBuffer;

		ID3D11Buffer* smaaRtMetricsBuffer;

		ID3D11Buffer* toneMappingBuffer;

		ID3D11Buffer* lightBuffer;

		ID3D11Buffer* shadowFilterBuffer;
		ID3D11Buffer* matrixBuffer;

		ID3D11SamplerState* bilinearSampler;
		ID3D11SamplerState* pointSampler;
		ID3D11SamplerState* comparisonSampler;
	};
}