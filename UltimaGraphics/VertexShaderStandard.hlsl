cbuffer cb0 : register(b0)
{
	row_major matrix world;
};

cbuffer cb1 : register(b1)
{
	row_major matrix viewProjection;
};

cbuffer cb2 : register(b2)
{
	row_major matrix lightViewProjection;
}

cbuffer cb3 : register(b3)
{
	float3 cameraPosition;
}

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
	float TessellationFactor : TESSELLATION;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;
	
	output.Position = float4(input.Position, 1);
	output.Position = mul(output.Position, world);

	output.PositionW = output.Position.xyz;

	output.PositionL = mul(output.Position, lightViewProjection);

	output.Position = mul(output.Position, viewProjection);

	output.Normal = mul(input.Normal, (float3x3)world);

	output.Texture = input.Texture;

	output.Tangent = mul(input.Tangent, (float3x3)world);

	output.PositionDepth = output.Position;

	float dist = distance(output.PositionW, cameraPosition);
	float tessellation = saturate((500.0f - dist) / (500.0f - 20.0f));

	output.TessellationFactor = 2.0f + tessellation * (25.0f - 1.0f);

	return output;
}