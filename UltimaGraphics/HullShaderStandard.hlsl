// Input control point
struct VS_CONTROL_POINT_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
	float TessellationFactor : TESSELLATION;
};

// Output control point
struct HS_CONTROL_POINT_OUTPUT
{
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
};

// Output patch constant data.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]	: SV_TessFactor; // e.g. would be [4] for a quad domain
	float InsideTessFactor : SV_InsideTessFactor; // e.g. would be Inside[2] for a quad domain
	// TODO: change/add other stuff
};

#define NUM_CONTROL_POINTS 3

// Patch Constant Function
HS_CONSTANT_DATA_OUTPUT CalcHSPatchConstants(
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip,
	uint PatchID : SV_PrimitiveID)
{
	HS_CONSTANT_DATA_OUTPUT output;

	output.EdgeTessFactor[0] = 0.5f * (ip[1].TessellationFactor + ip[2].TessellationFactor);
	output.EdgeTessFactor[1] = 0.5f * (ip[0].TessellationFactor + ip[2].TessellationFactor);
	output.EdgeTessFactor[2] = 0.5f * (ip[1].TessellationFactor + ip[0].TessellationFactor);

	output.InsideTessFactor = output.EdgeTessFactor[0];

	return output;
}

[domain("tri")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("CalcHSPatchConstants")]
HS_CONTROL_POINT_OUTPUT main( 
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip, 
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID )
{
	HS_CONTROL_POINT_OUTPUT output = (HS_CONTROL_POINT_OUTPUT)0;

	output.PositionW = ip[i].PositionW;
	output.Normal = ip[i].Normal;
	output.Texture = ip[i].Texture;

	output.PositionL = ip[i].PositionL;

	output.Tangent = ip[i].Tangent;

	output.PositionDepth = ip[i].PositionDepth;

	return output;
}
