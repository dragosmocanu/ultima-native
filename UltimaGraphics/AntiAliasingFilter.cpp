#include "stdafx.h"

#include "AntiAliasingFilter.h"

namespace Ultima
{
	AntiAliasingFilter::AntiAliasingFilter(
		AntiAliasingType type,
		float fxaaSubPixel,
		float fxaaEdgeThreshHold)
		ULTIMA_NOEXCEPT
		: Type(type),
		FxaaSubPixel(fxaaSubPixel),
		FxaaEdgeThreshHold(fxaaEdgeThreshHold)
	{ }
}