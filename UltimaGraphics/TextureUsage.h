#pragma once

#include "Macros.h"

namespace Ultima
{
    /*!
    Defines the texture usage scenarios.
    */
    enum /*class*/ TextureUsage
    {
        SRV = (1 << 0),
        RTV = (1 << 1),
        UAV = (1 << 2),
    };
}