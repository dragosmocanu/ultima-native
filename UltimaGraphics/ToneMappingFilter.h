#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines the different tone mapping algorithms.
	*/
	enum class ToneMappingType
	{
		None,
		Linear,
		Reinhard,
		Reinhard2,
		Hejl,
		Hable
	};

	/*!
	Defines tone mapping settings.
	*/
	struct ToneMappingFilter
	{
		/*!
		Creates a new instance of ToneMappingFilter.
		*/
		ULTIMAGRAPHICS_API ToneMappingFilter(
			ToneMappingType type = ToneMappingType::Hable,
			float linearWhite = 11.2f,
			float hableShoulderStrength = 0.15f,
			float hableLinearStrength = 0.50f,
			float hableLinearAngle = 0.10f,
			float hableToeStrength = 0.20f,
			float hableToeNumerator = 0.02f,
			float hableToeDenominator = 0.30f)
			ULTIMA_NOEXCEPT;

		ToneMappingType Type;
		float LinearWhite;
		float HableShoulderStrength;
		float HableLinearStrength;
		float HableLinearAngle;
		float HableToeStrength;
		float HableToeNumerator;
		float HableToeDenominator;
	};
}