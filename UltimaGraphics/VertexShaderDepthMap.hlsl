cbuffer cb0 : register(b0)
{
	row_major matrix world;
};

cbuffer cb1 : register(b1)
{
	row_major matrix lightViewProjection;
};

struct VertexIn
{
	float3 Position : POSITION;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = mul(float4(input.Position, 1), world);
	output.Position = mul(output.Position, lightViewProjection);

	return output;
}