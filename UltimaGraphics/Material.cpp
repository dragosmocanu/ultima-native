#include "stdafx.h"

#include "Material.h"

namespace Ultima
{
	Material::Material(
		const Color& base,
		float metallic,
		float roughness)
		ULTIMA_NOEXCEPT
	{
		Base = base.ToXMFLOAT4();
		Metallic = metallic;
		Roughness = roughness;
	}
}