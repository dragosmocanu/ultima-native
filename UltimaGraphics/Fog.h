#pragma once

#include "Macros.h"

#include "Color.h"

namespace Ultima
{
	/*!
	Defines a fog effect.
	The Color's alpha channel is used to determine whether the fog is active or not (0.0f is inactive).
	*/
	struct Fog
	{
		/*!
		Creates a new instance of Fog.
		By default the color's alpha channel is 0, so the fog is deactivated.
		*/
		ULTIMAGRAPHICS_API Fog(
			const Color& color = Ultima::Color(0.6f, 0.6f, 0.6f, 0.0f),
			float start = 1000.0f,
			float end = 4000.0f)
			ULTIMA_NOEXCEPT;

		XMFLOAT4 Color;

		float Start;
		float End;
	};
}