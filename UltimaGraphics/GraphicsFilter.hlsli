#ifndef GRAPHICSFILTER_HLSLI
#define GRAPHICSFILTER_HLSLI

#pragma warning (disable : 4000)

struct GraphicsFilter
{
	float4 Process(float4 color)
	{
		if (!IsActive)
			return color;

		color = color + (1.0f - color) * this.TintColor * this.TintColor.w;

		//color.xyz = color.xyz - color.xyz * (1.0f - this.TintColor.xyz) 
		//	* this.TintColor.w;

		color = (color - 0.5f) * (this.Contrast * 2.0f) + 0.5f;

		if (this.Brightness > 0.5f)
			color.xyz = lerp(
				color.xyz,
				float3(1, 1, 1),
				(this.Brightness - 0.5f) * 2.0f);
		else
			color.xyz = lerp(
				color.xyz,
				float3(0, 0, 0),
				(0.5f - this.Brightness) * 2.0f);

		if (IsGrayscale)
		{
			//float rgb = color.x * 0.334f + color.y * 0.333f + color.z * 0.333f;
			// http://www.ee-techs.com/circuit/video-demy5.pdf
			float rgb = color.x * 0.299f + color.y * 0.587f + color.z * 0.144f;
			color.x = color.y = color.z = rgb;
		}

		if (IsNegative)
		{
			float alpha = color.a;
			color = 1.0f - color;
			color.a = alpha;
		}

		return color;
	}

	float4 TintColor;
	bool IsActive;
	float Brightness;
	float Contrast;
	float IsGrayscale;
	float IsNegative;
};

#endif