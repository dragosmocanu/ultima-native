#pragma once

#include "Macros.h"

namespace Ultima
{
    class IShader
    {
    public:
        ULTIMAGRAPHICS_API IShader(const Device* device = nullptr) : device(device) { }

        ULTIMAGRAPHICS_API virtual ~IShader() { /* device->Release()*/ }

    protected:
        const Device* device;
    };

    class IVertexShader { };

    class IPixelShader { };

    class IHullShader { };

    class IDomainShader { };

    class IGeometryShader { };

    class IComputeShader { };
}
