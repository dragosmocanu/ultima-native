#pragma once

#include "Macros.h"

#include "Camera3D.h"
#include "Device.h"
#include "Fog.h"
#include "GraphicsFilter.h"
#include "Light.h"
#include "Matrix.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a skybox in the shape of a sphere.
	*/
	class Skybox
	{
	public:
		/*!
		Creates a new instance of Skybox.
		The lines and columns variables are used to contruct the sphere. Higher numbers will create a better defined geometry.
		*/
		ULTIMAGRAPHICS_API Skybox(
			const Device& device,
			const std::string& texturePath,
			unsigned short int lines = 4,
			unsigned short int columns = 4,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*
		Copies an instance of Skybox.
		*/
		ULTIMAGRAPHICS_API Skybox(const Skybox& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API Skybox& operator=(const Skybox& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~Skybox()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the number of lines.
		*/
		ULTIMAGRAPHICS_API virtual unsigned short int GetLines()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the number of columns.
		*/
		ULTIMAGRAPHICS_API virtual unsigned short int GetColumns()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the path file of the texture.
		*/
		ULTIMAGRAPHICS_API virtual const std::string& GetTexturePath()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the count of anisotropic samples.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int GetAnisotropicSampleCount()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the ID3D11Device* object.
		*/
		ULTIMAGRAPHICS_API virtual const Device* GetDevice()
			const ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Renders the skybox.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Skybox::Render(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			const Light& light = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the skybox.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int Skybox::Render(
			ID3D11DeviceContext* const context,
			const Matrix& viewProjection,
			const Light& light = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter())
			const ULTIMA_NOEXCEPT;
#endif

	protected:

		/*!
		Builds the vertex and index buffers and loads the required assets.
		*/
		ULTIMAGRAPHICS_API virtual void build()
			ULTIMA_NOEXCEPT;

		unsigned int numberOfIndices;

		unsigned short int lines;
		unsigned short int columns;
		std::string texturePath;
		unsigned int anisotropicSampleCount;

		const Device* device;

		ID3D11InputLayout* inputLayout;

		ID3D11Buffer* vertexBuffer;
		ID3D11Buffer* indexBuffer;

		VertexShader vertexShader;
        PixelShader pixelShader;

        Texture colorTex;

		ID3D11Buffer* matrixBuffer;
		ID3D11Buffer* graphicsFilterBuffer;

		ID3D11SamplerState* anisotropicSampler;
	};

}