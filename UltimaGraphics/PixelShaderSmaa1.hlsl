cbuffer cb0 : register(b0)
{
	float4 SMAA_RT_METRICS;
}

#define SMAA_HLSL_4_1
#define SMAA_PRESET_HIGH
#include "SMAA.hlsli"

Texture2D colorTexture : register(t0);

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
	float4 Offset[3] : TEXCOORD1;
};

float2 main(VertexOut input) : SV_TARGET
{
	float2 output = SMAALumaEdgeDetectionPS(
		input.Texture,
		input.Offset,
		colorTexture);

	return output;
}