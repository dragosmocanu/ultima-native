// Input control point
struct VS_CONTROL_POINT_OUTPUT
{
	float4 Position : SV_POSITION;

	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
	float TessellationFactor : TESSELLATION;
	float2 Texture : TEXCOORD3;
};

// Output control point
struct HS_CONTROL_POINT_OUTPUT
{
	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
	float2 Texture : TEXCOORD3;
};

// Output patch constant data.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]	: SV_TessFactor; // e.g. would be [4] for a quad domain
	float InsideTessFactor : SV_InsideTessFactor; // e.g. would be Inside[2] for a quad domain
	// TODO: change/add other stuff
};

#define NUM_CONTROL_POINTS 3

// Patch Constant Function
HS_CONSTANT_DATA_OUTPUT CalcHSPatchConstants(
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip,
	uint PatchID : SV_PrimitiveID)
{
	HS_CONSTANT_DATA_OUTPUT output;

	output.EdgeTessFactor[0] = 0.5f * (ip[1].TessellationFactor + ip[2].TessellationFactor);
	output.EdgeTessFactor[1] = 0.5f * (ip[0].TessellationFactor + ip[2].TessellationFactor);
	output.EdgeTessFactor[2] = 0.5f * (ip[1].TessellationFactor + ip[0].TessellationFactor);

	output.InsideTessFactor = output.EdgeTessFactor[0];

	return output;
}

[domain("tri")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(3)]
[patchconstantfunc("CalcHSPatchConstants")]
HS_CONTROL_POINT_OUTPUT main(
	InputPatch<VS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> ip,
	uint i : SV_OutputControlPointID,
	uint PatchID : SV_PrimitiveID)
{
	HS_CONTROL_POINT_OUTPUT output = (HS_CONTROL_POINT_OUTPUT)0;

	output.PositionV = ip[i].PositionV;
	output.NormalV = ip[i].NormalV;
	output.Texture = ip[i].Texture;

	return output;
}
