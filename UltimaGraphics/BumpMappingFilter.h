#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines a structure that holds all the bump mapping parameters.
	*/
	struct BumpMappingFilter
	{
	public:
		/*!
		Creates a new instance of BumpMappingFilter.
		*/
		ULTIMAGRAPHICS_API BumpMappingFilter(
			bool isActive = false)
			ULTIMA_NOEXCEPT;

		float IsActive;
	};
}