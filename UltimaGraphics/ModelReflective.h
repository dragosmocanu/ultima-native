#pragma once

#include "Macros.h"

#include "Model.h"
#include "Skybox.h"

namespace Ultima
{
	class ModelReflective : public Model
	{
	public:
		/*!
		Creates a new instance of ModelReflective.
		*/
		ULTIMAGRAPHICS_API ModelReflective(
			const Device& device,
			const std::string& meshPath,
			const std::string& texturePath,
			const std::string& bumpmapPath,
			const std::string& displacementMapPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4,
			unsigned int reflectiveTextureSize = 256)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of ModelReflective.
		*/
		ULTIMAGRAPHICS_API ModelReflective(
			const Device& device,
			const std::string& meshPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4,
			unsigned int reflectiveTextureSize = 256)
			ULTIMA_NOEXCEPT;

		/*
		Copies an instance of ModelReflective.
		*/
		ULTIMAGRAPHICS_API ModelReflective(const ModelReflective& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API ModelReflective& operator=(const ModelReflective& other)
			ULTIMA_NOEXCEPT;

		ULTIMAGRAPHICS_API virtual ~ModelReflective()
			ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the model.
		*/
		ULTIMAGRAPHICS_API virtual void SetPosition(const Vector3& position)
			ULTIMA_NOEXCEPT override;

		/*!
		Moves the model along its X axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveX(float f)
			ULTIMA_NOEXCEPT override;

		/*!
		Moves the model along its Y axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveY(float f)
			ULTIMA_NOEXCEPT override;

		/*!
		Moves the model along its Z axis.
		*/
		ULTIMAGRAPHICS_API virtual void MoveZ(float f)
			ULTIMA_NOEXCEPT override;

		/*!
		Moves the model by a given Vector3.
		*/
		ULTIMAGRAPHICS_API virtual void Move(const Vector3& moveVector)
			ULTIMA_NOEXCEPT override;

		/*!
		Gets the size of the reflective texture.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int GetReflectiveTextureSize()
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the reflection data in the RTVs.
		Should be called before RenderReflective.
		Resets the viewport.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeReflections(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			const Color& clearColor,
			const std::vector<Model*>& models,
			const Light& dl = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the reflection data in the RTVs.
		Should be called before RenderReflective.
		Resets the viewport.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int ComputeReflections(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			const Color& clearColor,
			const Skybox* const skybox,
			const std::vector<Model*>& models,
			const Light& dl = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the model with reflectance.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderReflective(
			ID3D11DeviceContext* const context,
			const Camera3D& camera,
			const Light& dl = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const ambientOcclusionMap = nullptr,
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;

		/*!
		Renders the model with reflections.
		Returns the number of rendered triangles.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderReflective(
			ID3D11DeviceContext* const context,
			const Matrix& viewProjection,
			const Vector3& cameraPosition,
			const Frustum& frustum,
			const Light& dl = Light(),
			const Fog& fog = Fog(),
			const GraphicsFilter& graphicsFilter = GraphicsFilter(),
			ID3D11ShaderResourceView* const ambientOcclusionMap = nullptr,
			ID3D11ShaderResourceView* const shadowMap = nullptr,
			const Matrix& lightViewProjection = Matrix::Identity,
			const ShadowFilter& shadowFilter = ShadowFilter())
			const ULTIMA_NOEXCEPT;


		/*!
		Renders the color (with reflection), position, normal (both in world and in view space) and material to six textures.
		Returns the number of rendered triangles.
		Six different HDR textures should be set as a render targets before calling this function.
		Position and normal will be both in world and in view space.
		*/
		ULTIMAGRAPHICS_API virtual unsigned int RenderDeferredReflective(
			ID3D11DeviceContext* const context,
			const Camera3D& camera)
			const ULTIMA_NOEXCEPT;

	protected:
		/*!
		Builds the reflective matrices, RTVs, and SRVs.
		*/
		ULTIMAGRAPHICS_API virtual void buildReflectiveData()
			ULTIMA_NOEXCEPT;

		/*!
		Destroys the reflective data. Called by the actual destructor and the copy/move constructors.
		*/
		ULTIMAGRAPHICS_API virtual void destructReflectiveData()
			ULTIMA_NOEXCEPT;

		/*!
		Rebuilds the view and projection matrices.
		Also rebuilds the view frustums.
		*/
		ULTIMAGRAPHICS_API virtual void rebuildMatrices()
			ULTIMA_NOEXCEPT;

		Matrix views[6];
		Matrix projections[6];
		Frustum frustums[6];

		unsigned int reflectiveTextureSize;

		D3D11_VIEWPORT reflectiveViewport;

		PixelShader pixelShaderReflective;
		PixelShader pixelShaderDeferredReflective;

		ID3D11RenderTargetView* reflectiveRTV[6];
		ID3D11ShaderResourceView* reflectiveSRV;
		ID3D11DepthStencilView* reflectiveDSV;

	};
}