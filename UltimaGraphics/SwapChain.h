#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "SwapChainDx11.h"
namespace Ultima
{
    class SwapChain : public SwapChainDx11
    {
    public:

    };
}
#endif