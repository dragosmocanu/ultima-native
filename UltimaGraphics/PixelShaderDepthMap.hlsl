#include "ShadowFilter.hlsli"

cbuffer cb0 : register(b0)
{
	ShadowFilter shadowFilter;
}

float4 main(float4 input : SV_POSITION) : SV_TARGET
{
	float4 output = (float4)0;

	[flatten]
	if (shadowFilter.Type > 1)
	{
		float depth = input.z / input.w;

		[flatten]
		if (shadowFilter.Type == 2)
		{
			float depth2 = depth * depth;

			float dx = ddx(depth);
			float dy = ddy(depth);
			depth2 += 0.25f * (dx * dx + dy * dy);

			output = float4(depth, depth2, 0.0f, 0.0f);
		}
		else if (shadowFilter.Type == 3)
		{
			float warpedDepth = WarpDepth(depth, shadowFilter.EvsmExponents).x;
			float warpedDepth2 = warpedDepth * warpedDepth;

			float dx = ddx(warpedDepth);
			float dy = ddy(warpedDepth);
			warpedDepth += 0.25f * (dx * dx + dy * dy);

			output = float4(warpedDepth, warpedDepth2, 0, 0);
		}
		else if (shadowFilter.Type == 4)
		{
			float2 warpedDepth = WarpDepth(depth, shadowFilter.EvsmExponents);
			float2 warpedDepth2 = warpedDepth * warpedDepth;

			float2 dx = ddx(warpedDepth);
			float2 dy = ddy(warpedDepth);
			warpedDepth2 += 0.25f * (dx * dx + dy * dy);

			output = float4(warpedDepth, warpedDepth2);
		}
	}

	return output;
}