#include "stdafx.h"

#include "FreeCamera3D.h"

#include "MathExtensions.h"

namespace Ultima
{

	FreeCamera3D::FreeCamera3D(float aspectRatio, float viewDistance)
		ULTIMA_NOEXCEPT
	{
		position = Vector3::Zero;
		view = Matrix::Identity;
		Reinitialize(aspectRatio, viewDistance);
		pitch = 0.0f;
		yaw = 0.0f;
		roll = 0;
		movement = Vector3::Zero;
		target = Vector3::Forward + position;
		wasModified = true;
		viewProjection = view * projection;
	}

	FreeCamera3D::FreeCamera3D(float aspectRatio, float viewDistance, 
		float fieldOfView)
		ULTIMA_NOEXCEPT
	{
		position = Vector3::Zero;
		view = Matrix::Identity;
		projection = Matrix(fieldOfView, aspectRatio, 0.001f, viewDistance);
		//projection.Transpose();
		pitch = 0.0f;
		yaw = 0.0f;
		roll = 0;
		movement = Vector3::Zero;
		target = Vector3::Forward + position;
		wasModified = true;
		viewProjection = view * projection;
		frustum = Frustum(projection);
	}

	FreeCamera3D::~FreeCamera3D()
		ULTIMA_NOEXCEPT
	{ }

	void FreeCamera3D::MoveX(float f)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		movement.SetX(f);
	}

	void FreeCamera3D::MoveY(float f)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		movement.SetY(f);
	}

	void FreeCamera3D::MoveZ(float f)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		movement.SetZ(f);
	}

	void FreeCamera3D::Move(const Vector3& moveVector)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		movement += moveVector;
	}

	void FreeCamera3D::SetPosition(const Vector3& position)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		this->position = position;
	}

	void FreeCamera3D::RotateX(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		SetRotationX(pitch + radians);
	}

	void FreeCamera3D::RotateY(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		SetRotationY(yaw + radians);
	}

	void FreeCamera3D::RotateZ(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		SetRotationZ(roll + radians);
	}

	void FreeCamera3D::Rotate(const Vector3& rotateVector)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		SetRotationX(pitch + rotateVector.GetX());
		SetRotationY(yaw + rotateVector.GetY());
		SetRotationZ(roll + rotateVector.GetZ());
	}

	void FreeCamera3D::SetRotationX(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		pitch = MathExtensions::Clamp(
			radians, 
			MathExtensions::ToRadians(-90.0f), 
			MathExtensions::ToRadians(90.0f));
	}

	void FreeCamera3D::SetRotationY(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		yaw = radians;

		while (yaw > 6.28318531f)
			yaw -= 6.28318531f;

		while (yaw < 0)
			yaw += 6.28318531f;
	}

	void FreeCamera3D::SetRotationZ(float radians)
		ULTIMA_NOEXCEPT
	{
		wasModified = true;

		roll = radians;

		while (roll > 6.28318531f)
			roll -= 6.28318531f;

		while (roll < 0)
			roll += 6.28318531f;
	}

	void FreeCamera3D::SetRotation(const Vector3& rotation)
		ULTIMA_NOEXCEPT
	{
		SetRotationX(rotation.GetX());
		SetRotationY(rotation.GetY());
		SetRotationZ(rotation.GetZ());
	}

	const Frustum& FreeCamera3D::GetFrustum() 
		const ULTIMA_NOEXCEPT
	{
		return frustum;
	}

	const Matrix& FreeCamera3D::GetViewProjection()
		const ULTIMA_NOEXCEPT
	{
		return viewProjection;
	}

	void FreeCamera3D::Reinitialize(
		float aspectRatio,
		float viewDistance)
		ULTIMA_NOEXCEPT
	{
		Camera3D::Reinitialize(aspectRatio, viewDistance);
		wasModified = true;
	}

	void FreeCamera3D::Update(float deltaTime)
		ULTIMA_NOEXCEPT
	{
		UNREFERENCED_PARAMETER(deltaTime);

		if (wasModified)
		{
			movement.SetZ(movement.GetZ() * Vector3::Forward.GetZ());

			Matrix rotation;

			if (roll == 0)
				rotation = Matrix::CreateRotationX(pitch) * Matrix::CreateRotationY(yaw);
			else
				rotation = Matrix::CreateFromPitchYawRoll(pitch, yaw, roll);

			Vector3 transformed = Vector3::Transform(Vector3::Forward, rotation);
			position += Vector3::Transform(movement, rotation);
			target = transformed + position;

			transformed = Vector3::Transform(Vector3::Up, rotation);
			view = Matrix::CreateLookAt(position, target, transformed);

			movement = Vector3::Zero;

			viewProjection = view * projection;
			frustum.Transform(projection, position, view);
		}

		wasModified = false;
	}

}