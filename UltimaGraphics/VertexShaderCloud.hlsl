cbuffer cb0 : register(b0)
{
	row_major matrix viewProjection;
}; 

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture: TEXCOORD;
};

struct VertexOut
{
	float4 Position: SV_POSITION;
	float2 Texture : TEXCOORD;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = mul(float4(input.Position, 1.0f), viewProjection);

	output.Texture = input.Texture;

	return output;
}