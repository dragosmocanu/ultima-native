#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines the different anti-aliasing algorithms.
	*/
	enum class AntiAliasingType
	{
		None, //No anti-aliasing
		FXAA, //Fast Approximate Anti-Aliasing
		SMAA //Enhanced Subpixel Morphological Anti-Aliasing
	};

	/*!
	Defines the AA settings that will be applied to a quad used in deferred rendering.
	FXAA and SMAA implementation.
	*/
	struct AntiAliasingFilter
	{
	public:

		/*!
		Creates a new isntance of AntiAliasingFilter.
		subPixel should be between 0 and 1, where higher values denote softer edges.
		edgeThreshHold should be between 0.06f and 0.333f, where lower values denote higher quality.
		*/
		ULTIMAGRAPHICS_API AntiAliasingFilter(
			AntiAliasingType type = AntiAliasingType::FXAA,
			float fxaaSubPixel = 1.0f,
			float fxaaEdgeThreshHold = 0.333f)
			ULTIMA_NOEXCEPT;

		AntiAliasingType Type;
		float FxaaSubPixel;
		float FxaaEdgeThreshHold;

	};
}