#pragma once

#include "Macros.h"

#include "ISwapChain.h"

#include "Utilities.h"

namespace Ultima
{
    class SwapChainDx11 : public ISwapChain
    {
    public:
        ULTIMAGRAPHICS_API virtual ~SwapChainDx11()
            ULTIMA_NOEXCEPT
        { Utilities::Release(swapChain); }

        ULTIMAGRAPHICS_API void Flip(bool isInVsync = true)
            ULTIMA_NOEXCEPT override
        { swapChain->Present(isInVsync ? 1 : 0, 0); }

        ULTIMAGRAPHICS_API void Resize(unsigned int width, unsigned int height)
            ULTIMA_NOEXCEPT override
        { Utilities::Assert(swapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0)); }

        ULTIMAGRAPHICS_API IDXGISwapChain*& GetSwapChain()
            ULTIMA_NOEXCEPT
        { return swapChain; }

    protected:
        IDXGISwapChain* swapChain;
    };
}