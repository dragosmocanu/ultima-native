cbuffer cb0 : register(b0)
{
	matrix world;
};

cbuffer cb1 : register(b1)
{
	matrix viewProj;
};

//cbuffer cb2 : register(b2)
//{
//	matrix projection;
//};

struct VertexIn
{
	float3 Position : POSITION;
	float4 Color : COLOR;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;
	output.Position = float4(input.Position, 1);
	output.Position = mul(output.Position, world);
	output.Position = mul(output.Position, viewProj);
	//output.Position = mul(output.Position, projection);
	output.Color = input.Color;
	return output;
}