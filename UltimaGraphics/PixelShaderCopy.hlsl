#include "GaussianBlurFilter.hlsli"

Texture2D colorTexture : register(t0);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	bool isSingleChannel;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float4 output = colorTexture.Sample(bilinearSampler, input.Texture);

	if (isSingleChannel)
		output.y = output.z = output.x;

	return output;
}