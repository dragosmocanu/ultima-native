#ifndef TONEMAPPINGFILTER_HLSLI
#define TONEMAPPINGFILTER_HLSLI

struct ToneMappingFilter
{
	float3 ToneMapLinear(float3 color)
	{
		return CorrectGamma(color);
	}

	float3 ToneMapReinhard(float3 color)
	{
		color = color / (1 + color);
		return CorrectGamma(color);
	}

	float3 ToneMapReinhard2(float3 color)
	{
		color = (color * (1 + color / (LinearWhite * LinearWhite))) / (1 + color);
		return CorrectGamma(color);
	}

	float3 ToneMapHejl(float3 color)
	{
		float3 x = max(0, color - 0.004);
			color = (x * (6.2f * x + 0.5f)) / (x * (6.2f * x + 1.7f) + 0.06f);
		return color;
	}

	float3 U2Function(float3 x)
	{
		float A = HableShoulderStrength;
		float B = HableLinearStrength;
		float C = HableLinearAngle;
		float D = HableToeStrength;
		float E = HableToeNumerator;
		float F = HableToeDenominator;

		return ((x * (A * x + C * B) + D * E) / (x * (A * x + B) + D * F)) - E / F;
	}

	float3 ToneMapHable(float3 color)
	{
		float3 numerator = U2Function(color);
			float3 denominator = U2Function(float3(LinearWhite, LinearWhite, LinearWhite));

			color = numerator / denominator;

		return CorrectGamma(color);
	}

	float3 Process(float3 color, float avgLuminance, float threshold)
	{
		if (Type == 0)
			return color;

		color = CalculateExposedColor(color, avgLuminance, threshold);

		if (Type == 1)
			color = ToneMapLinear(color);
		else if (Type == 2)
			color = ToneMapReinhard(color);
		else if (Type == 3)
			color = ToneMapReinhard2(color);
		else if (Type == 4)
			color = ToneMapHejl(color);
		else if (Type == 5)
			color = ToneMapHable(color);

		return color;
	}

	unsigned int Type;
	float LinearWhite;
	float HableShoulderStrength;
	float HableLinearStrength;
	float HableLinearAngle;
	float HableToeStrength;
	float HableToeNumerator;
	float HableToeDenominator;
};

#endif