cbuffer cb0 : register(b0)
{
	matrix world;
};

cbuffer cb1 : register(b1)
{
	matrix worldInverseTranspose;
};

cbuffer cb2 : register(b2)
{
	matrix view;
};

cbuffer cb3 : register(b3)
{
	matrix projection;
};

struct VertexIn
{
	float3 Position : POSITION;
	float3 Normal : NORMAL;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.PositionW = mul(float4(input.Position, 1.0f), world).xyz;

	output.Normal = mul(input.Normal, (float3x3)worldInverseTranspose);

	output.Position = float4(input.Position, 1);
	output.Position = mul(output.Position, world);
	output.Position = mul(output.Position, view);
	output.Position = mul(output.Position, projection);

	return output;
}