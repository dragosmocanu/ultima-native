#include "stdafx.h"

#include "Fog.h"

namespace Ultima
{

	Fog::Fog(
		const Ultima::Color& color,
		float start,
		float end)
		ULTIMA_NOEXCEPT
	{
		Color = color.ToXMFLOAT4();
		Start = start;
		End = end;
	}

}