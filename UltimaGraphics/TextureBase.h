#pragma once

#include "Macros.h"

#include "TextureFormat.h"
#include "TextureUsage.h"

namespace Ultima
{
    class Device;

    /*!
    Defines the base class for graphic textures.
    */
    class TextureBase
    {
    public:
        ULTIMAGRAPHICS_API TextureBase(const Device* device) : device(device) { }

        ULTIMAGRAPHICS_API virtual void SetDevice(const Device& device) ULTIMA_NOEXCEPT { this->device = &device; }

        ULTIMAGRAPHICS_API virtual unsigned int GetWidth()
            const ULTIMA_NOEXCEPT
        { return width; };

        ULTIMAGRAPHICS_API virtual unsigned int GetHeight()
            const ULTIMA_NOEXCEPT
        { return height; };

        ULTIMAGRAPHICS_API virtual unsigned int GetDepth()
            const ULTIMA_NOEXCEPT
        { return depth; };

        ULTIMAGRAPHICS_API virtual unsigned int GetMipLevels()
            const ULTIMA_NOEXCEPT
        { return mipLevels; };

        ULTIMAGRAPHICS_API virtual TextureFormat GetTextureFormat()
            const ULTIMA_NOEXCEPT
        { return texFormat; };

        ULTIMAGRAPHICS_API virtual TextureUsage GetTextureUsage()
            const ULTIMA_NOEXCEPT
        { return texUsage; };

        ULTIMAGRAPHICS_API virtual TextureViewDimension GetTextureViewDimensions()
            const ULTIMA_NOEXCEPT
        { return texViewDim; };

    private:
        const Device* device;

        unsigned int width;
        unsigned int height;
        unsigned int depth;
        unsigned int mipLevels;

        TextureFormat texFormat;
        TextureUsage texUsage;
        TextureViewDimension texViewDim;
    };
}