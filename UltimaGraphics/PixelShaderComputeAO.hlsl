#include "AmbientOcclusionFilter.hlsli"

Texture2D positionTexture : register(t0);
Texture2D normalTexture : register(t1);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	AmbientOcclusionFilter aoFilter;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float main(VertexOut input) : SV_TARGET
{
	float2 screenSize;
	positionTexture.GetDimensions(screenSize.x, screenSize.y);

	float output;

	output = aoFilter.Process(
		positionTexture, normalTexture,
		bilinearSampler, input.Texture, screenSize);

	return output;
}