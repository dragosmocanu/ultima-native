#include "stdafx.h"

#include "DeviceDx11.h"

#include "Utilities.h"

namespace Ultima
{
    ID3D11Device* DeviceDx11::GetDev()
        const ULTIMA_NOEXCEPT
    {
        return deviceDx11;
    }

    ID3D11Device*& DeviceDx11::GetDevRef()
        ULTIMA_NOEXCEPT
    {
        return deviceDx11;
    }

    void DeviceDx11::SetDev(ID3D11Device* dev)
        ULTIMA_NOEXCEPT
    {
        deviceDx11 = dev;
    }

    void DeviceDx11::LoadTexture(
        Texture& outTexture,
        const std::string& filePath,
        TextureViewDimension textureViewDimension)
        const ULTIMA_NOEXCEPT
    {
        ID3D11ShaderResourceView* resultSRV = nullptr;

        for (auto& x : textures)
        {
            if (x.first == filePath)
            {
                resultSRV = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (resultSRV == nullptr
            && Utilities::FileExists(filePath))
        {
            D3DX11_IMAGE_LOAD_INFO info;
            if (textureViewDimension == TextureViewDimension::Cube)
                info.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

            ID3D11Texture2D* texture = nullptr;
            Utilities::Assert(
                D3DX11CreateTextureFromFile(GetDev(), filePath.c_str(),
                    &info, 0, (ID3D11Resource**)&texture, 0));

            D3D11_TEXTURE2D_DESC textureDesc;
            texture->GetDesc(&textureDesc);

            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
            viewDesc.Format = textureDesc.Format;
            viewDesc.TextureCube.MipLevels = textureDesc.MipLevels;
            viewDesc.TextureCube.MostDetailedMip = 0;

            if (textureViewDimension == TextureViewDimension::Cube)
                viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
            else if (textureViewDimension == TextureViewDimension::OneDimensional)
                viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
            else if (textureViewDimension == TextureViewDimension::TwoDimensional)
                viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
            else if (textureViewDimension == TextureViewDimension::ThreeDimensional)
                viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
            else if (textureViewDimension == TextureViewDimension::Array)
                Utilities::Assert(false);

            Utilities::Assert(
                GetDev()->CreateShaderResourceView(
                    texture, &viewDesc, &resultSRV));

            auto p = std::pair<ID3D11ShaderResourceView*, unsigned int>(resultSRV, 2);

            auto p2 = std::pair<
                std::string,
                std::pair<ID3D11ShaderResourceView*, unsigned int>>
                (filePath, p);

            textures.insert(p2);
        }

        outTexture.SetSrv(resultSRV);
    }

    void DeviceDx11::LoadVertexShader(
        VertexShader& outVS,
        const std::string& filePath,
        char*& data,
        unsigned int& size)
        const ULTIMA_NOEXCEPT
    {
    	ID3D11VertexShader* vertexShader = nullptr;

    	for (auto& x : vertexShaders)
    	{
    		if (x.first == filePath)
    		{
    			vertexShader = x.second.first;
    			x.second.second++;
    			break;
    		}
    	}

    	std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);
    	size = 0;

    	if (ifs.good())
    	{
    		ifs.seekg(0, std::ifstream::end);
    		size = static_cast<unsigned int>(ifs.tellg());
    		data = new char[size];

    		ifs.seekg(0, std::ifstream::beg);
    		ifs.read(&data[0], size);
    		ifs.close();

    		if (vertexShader == nullptr)
    		{
    			Utilities::Assert(
    				GetDev()->CreateVertexShader(data, size, 0, &vertexShader));

    			auto p = std::pair<ID3D11VertexShader*, unsigned int>(vertexShader, 2);

    			auto p2 = std::pair<
    				std::string,
    				std::pair<ID3D11VertexShader*, unsigned int>>
    				(filePath, p);

    			vertexShaders.insert(p2);
    		}
    	}

        outVS.SetShader(vertexShader);
    }

    void DeviceDx11::LoadVertexShader(
        VertexShader& outVS,
        const std::string& filePath)
        const ULTIMA_NOEXCEPT
    {
        ID3D11VertexShader* vertexShader = nullptr;

        for (auto& x : vertexShaders)
        {
            if (x.first == filePath)
            {
                vertexShader = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (vertexShader == nullptr)
        {
            char* data;
            unsigned int size;

            LoadVertexShader(outVS, filePath, data, size);

            delete[] data;

            return;
        }

        outVS.SetShader(vertexShader);
    }

    void DeviceDx11::LoadPixelShader(
        PixelShader& outPS,
        const std::string& filePath)
        const ULTIMA_NOEXCEPT
    {
        ID3D11PixelShader* pixelShader = nullptr;

        for (auto& x : pixelShaders)
        {
            if (x.first == filePath)
            {
                pixelShader = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (pixelShader == nullptr)
        {
            std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

            if (ifs.good())
            {
                unsigned int size = 0;
                char* data = nullptr;

                ifs.seekg(0, std::ifstream::end);
                size = static_cast<unsigned int>(ifs.tellg());
                data = new char[size];

                ifs.seekg(0, std::ifstream::beg);
                ifs.read(&data[0], size);
                ifs.close();

                Utilities::Assert(
                    GetDev()->CreatePixelShader(data, size, 0, &pixelShader));

                auto p = std::pair<ID3D11PixelShader*, unsigned int>(pixelShader, 2);

                auto p2 = std::pair<
                    std::string,
                    std::pair<ID3D11PixelShader*, unsigned int>>
                    (filePath, p);

                pixelShaders.insert(p2);

                delete[] data;
            }
        }

        outPS.SetShader(pixelShader);
    }

    void DeviceDx11::LoadHullShader(
        HullShader& outHS,
        const std::string& filePath)
        const ULTIMA_NOEXCEPT
    {
        ID3D11HullShader* hullShader = nullptr;

        for (auto& x : hullShaders)
        {
            if (x.first == filePath)
            {
                hullShader = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (hullShader == nullptr)
        {
            std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

            if (ifs.good())
            {
                unsigned int size = 0;
                char* data = nullptr;

                ifs.seekg(0, std::ifstream::end);
                size = static_cast<unsigned int>(ifs.tellg());
                data = new char[size];

                ifs.seekg(0, std::ifstream::beg);
                ifs.read(&data[0], size);
                ifs.close();

                Utilities::Assert(
                    GetDev()->CreateHullShader(data, size, 0, &hullShader));

                auto p = std::pair<ID3D11HullShader*, unsigned int>(hullShader, 2);

                auto p2 = std::pair<
                    std::string,
                    std::pair<ID3D11HullShader*, unsigned int>>
                    (filePath, p);

                hullShaders.insert(p2);

                delete[] data;
            }
        }

        outHS.SetShader(hullShader);
    }

    void DeviceDx11::LoadDomainShader(
        DomainShader& outDS,
        const std::string& filePath)
        const ULTIMA_NOEXCEPT
    {
        ID3D11DomainShader* domainShader = nullptr;

        for (auto& x : domainShaders)
        {
            if (x.first == filePath)
            {
                domainShader = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (domainShader == nullptr)
        {
            std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

            if (ifs.good())
            {
                unsigned int size = 0;
                char* data = nullptr;

                ifs.seekg(0, std::ifstream::end);
                size = static_cast<unsigned int>(ifs.tellg());
                data = new char[size];

                ifs.seekg(0, std::ifstream::beg);
                ifs.read(&data[0], size);
                ifs.close();

                Utilities::Assert(
                    GetDev()->CreateDomainShader(data, size, 0, &domainShader));

                auto p = std::pair<ID3D11DomainShader*, unsigned int>(domainShader, 2);

                auto p2 = std::pair<std::string, std::pair<ID3D11DomainShader*, unsigned int>>(filePath, p);

                domainShaders.insert(p2);

                delete[] data;
            }
        }

        outDS.SetShader(domainShader);
    }

    void DeviceDx11::LoadComputeShader(
        ComputeShader& outCS,
        const std::string& filePath)
        const ULTIMA_NOEXCEPT
    {
        ID3D11ComputeShader* computeShader = nullptr;

        for (auto& x : computeShaders)
        {
            if (x.first == filePath)
            {
                computeShader = x.second.first;
                x.second.second++;
                break;
            }
        }

        if (computeShader == nullptr)
        {
            std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

            if (ifs.good())
            {
                unsigned int size = 0;
                char* data = nullptr;

                ifs.seekg(0, std::ifstream::end);
                size = static_cast<unsigned int>(ifs.tellg());
                data = new char[size];

                ifs.seekg(0, std::ifstream::beg);
                ifs.read(&data[0], size);
                ifs.close();

                Utilities::Assert(
                    GetDev()->CreateComputeShader(data, size, 0, &computeShader));

                auto p = std::pair<ID3D11ComputeShader*, unsigned int>(computeShader, 2);

                auto p2 = std::pair<std::string, std::pair<ID3D11ComputeShader*, unsigned int>>(filePath, p);

                computeShaders.insert(p2);

                delete[] data;
            }
        }

        outCS.SetShader(computeShader);
    }
}