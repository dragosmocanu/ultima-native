#pragma once

#include "Macros.h"

#include "ContentManager.h"
#include "InputManager.h"
#include "Device.h"
#include "IContext.h"
#include "Texture.h"
#include "Timer.h"
#include "Quad.h"
#include "SwapChain.h"

namespace Ultima
{
	/*!
	Defines a multimedia application, providing basic logic and drawing.
	*/
	class Application
	{
	public:
#if DIRECTX
		/*!
		Creates a new instance of Application.
		*/
		ULTIMAGRAPHICS_API Application(
			const HINSTANCE& hInstance,
			const HINSTANCE& prevInstance,
			const LPWSTR& cmdLine,
			int cmdShow,
			const std::string& title, 
			unsigned int width = 1280, 
			unsigned int height = 720,
			bool isInVsync = false,
			bool isShowingFps = true, 
			unsigned int antiAliasingSampleCount = 4,
			const WNDPROC& messageLoop = Application::MessageLoop)
			ULTIMA_NOEXCEPT;
#endif
		ULTIMAGRAPHICS_API virtual ~Application()
			ULTIMA_NOEXCEPT;

		/*!
		Called when the application was resized.
		When overriden it is advised to also call Application's buildTextures() function.
		*/
		ULTIMAGRAPHICS_API virtual void OnResize()
			ULTIMA_NOEXCEPT;

		/*!
		Runs the application.
		*/
		ULTIMAGRAPHICS_API virtual int Run()
			ULTIMA_NOEXCEPT;

		/*!
		Updates the Aplication logic. Automatically called by Run().
		*/
		ULTIMAGRAPHICS_API virtual void Update(float deltaTime)
			ULTIMA_NOEXCEPT;

		/*!
		Renders on the screen. Automatically called by Run().
		*/
		ULTIMAGRAPHICS_API virtual void Render(float deltaTime)
			ULTIMA_NOEXCEPT;

        /*!
        Swaps the back buffer with the front buffer.
        */
        ULTIMAGRAPHICS_API virtual void SwapBuffers()
            ULTIMA_NOEXCEPT;

		/*!
		Clears the backbuffer and the RenderTargetView objects with a specified color.
		*/
		ULTIMAGRAPHICS_API virtual void Clear(const Color& color)
			const ULTIMA_NOEXCEPT;

		/*!
		Turns on or off the rendering to the depth map.
		*/
		ULTIMAGRAPHICS_API virtual void SetRenderingToDepth(
			bool isDepthRendering,
			ID3D11RenderTargetView* const depthRTV = nullptr,
			ID3D11DepthStencilView* const depthDSV = nullptr)
			ULTIMA_NOEXCEPT;

		/*!
		Turns on or off the blend rendering mode.
		*/
		ULTIMAGRAPHICS_API virtual void SetBlend(bool isBlendRendering)
			ULTIMA_NOEXCEPT;

		/*!
		Turns on or off the depth rendering mode.
		*/
		ULTIMAGRAPHICS_API virtual void SetDepth(bool hasDepth)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Default implementation for a message loop handler.
		*/
		ULTIMAGRAPHICS_API static LRESULT CALLBACK MessageLoop(
			HWND hwnd, 
			UINT message, 
			WPARAM wParam, 
			LPARAM lParam)
			ULTIMA_NOEXCEPT;

		/*!
		Processes the messages from the OS.
		*/
		ULTIMAGRAPHICS_API LRESULT CALLBACK MessageDispatcher(
			HWND hwnd, 
			UINT message, 
			WPARAM wParam, 
			LPARAM lParam)
			ULTIMA_NOEXCEPT;
#endif

	protected:
		/*!
		Builds (or rebuilds) the necessary textures.
		*/
		ULTIMAGRAPHICS_API virtual void buildTextures()
			ULTIMA_NOEXCEPT;

		bool isRunning;
		bool isInVsync;

		Timer timer;

		bool isShowingFPS;
		std::string title;

		unsigned int width;
		unsigned int height;
		float aspectRatio;
		unsigned int antiAliasingSampleCount;

		bool hasFocus;

		float timeFactor;

		HINSTANCE hInstance;
		HWND hwnd;

		//ID3D11Device* device;
        Device device;
		ID3D11DeviceContext* context;
		//IDXGISwapChain* swapChain;
        SwapChain swapChain;

		//ID3D11RenderTargetView* backBufferRTV;
		//ID3D11DepthStencilView* backBufferDSV;
        Texture backBuffer;

		D3D11_VIEWPORT viewport;
		D3D11_VIEWPORT viewportHalved;

		ID3D11DepthStencilState* depthEnabled;
		ID3D11DepthStencilState* depthDisabled;

		ID3D11BlendState* blendEnabled;
		ID3D11BlendState* blendDisabled;

		ID3D11RasterizerState* cullBack;
		ID3D11RasterizerState* cullFront;
		ID3D11RasterizerState* wireframe;

		Quad* quad;
	};
}