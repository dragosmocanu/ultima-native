#include "stdafx.h"

#include "Skybox.h"

#include "ContentManager.h"
#include "MathExtensions.h"
#include "Utilities.h"
#include "Vertex.h"

namespace Ultima
{
	Skybox::Skybox(
			const Device& device,
			const std::string& texturePath,
			unsigned short int noLines,
			unsigned short int noColumns,
			unsigned int anisotropicSampleCount)
			ULTIMA_NOEXCEPT
			: texturePath(texturePath),
            device(&device)
	{
		lines = max(noLines, 4);
		columns = max(noColumns, 4);
		this->anisotropicSampleCount = anisotropicSampleCount;

		build();
	}

	Skybox::Skybox(const Skybox& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Skybox& Skybox::operator=(const Skybox& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			lines = other.GetLines();
			columns = other.GetColumns();
			texturePath = other.GetTexturePath();
			device = other.GetDevice();
			anisotropicSampleCount = other.GetAnisotropicSampleCount();

			build();
		}

		return *this;
	}

	void Skybox::build()
		ULTIMA_NOEXCEPT
	{
		unsigned short int noVertices = (lines - 2) * columns + 2;
		unsigned short int noFaces = (lines - 3) * columns * 2 + lines * 2;

		float yaw = 0.0f;
		float pitch = 0.0f;

		Matrix rotationX, rotationZ;

		std::vector<VertexPT> vertices(noVertices);

		Vector3 current(0.0f, 0.0f, 1.0f);

		vertices[0].Position = current.ToXMFLOAT3();

		for (unsigned short int i = 0; i < lines - 2; i++)
		{
			pitch = (i + 1) * (MathExtensions::Pi / (columns - 1));
			rotationX = Matrix::CreateRotationX(pitch);

			for (unsigned int j = 0; j < columns; j++)
			{
				yaw = j * MathExtensions::TwoPi / columns;
				rotationZ = Matrix::CreateRotationZ(yaw);
				current = Vector3::Transform(Vector3::Forward, (rotationX * rotationZ));
				current.Normalize();
				vertices[i * columns + j + 1].Position = current.ToXMFLOAT3();
			}
		}

		vertices[noVertices - 1].Position.x = 0.0f;
		vertices[noVertices - 1].Position.y = 0.0f;
		vertices[noVertices - 1].Position.z = -1.0f;

		D3D11_BUFFER_DESC vertexDesc;
		SecureZeroMemory(&vertexDesc, sizeof(vertexDesc));
		vertexDesc.Usage = D3D11_USAGE_DEFAULT;
		vertexDesc.ByteWidth = sizeof(Vertex) * noVertices;
		vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexDesc.CPUAccessFlags = 0;
		vertexDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA resourceData;
		SecureZeroMemory(&resourceData, sizeof(resourceData));
		resourceData.pSysMem = &vertices[0];

		Utilities::Assert(
			device->GetDev()->CreateBuffer(&vertexDesc, &resourceData, &vertexBuffer));

		numberOfIndices = noFaces * 3;
		std::vector<unsigned short int> indices(numberOfIndices);

		unsigned int k = 0;

		for (unsigned short int i = 0; i < columns - 1; i++)
		{
			indices[k] = 0;
			indices[k + 1] = i + 1;
			indices[k + 2] = i + 2;
			k += 3;
		}

		indices[k] = 0;
		indices[k + 1] = columns;
		indices[k + 2] = 1;
		k += 3;

		for (unsigned short int i = 0; i < lines - 3; i++)
		{
			for (unsigned short int j = 0; j < columns - 1; j++)
			{
				indices[k] = i * columns + j + 1;
				indices[k + 1] = (i + 1) * columns + j + 1;
				indices[k + 2] = i * columns + j + 2;

				indices[k + 3] = (i + 1) * columns + j + 1;
				indices[k + 4] = (i + 1) * columns + j + 2;
				indices[k + 5] = i * columns + j + 2;

				k += 6;
			}

			indices[k] = (i + 1) * columns;
			indices[k + 1] = (i + 2) * columns;
			indices[k + 2] = i * columns + 1;

			indices[k + 3] = (i + 2) * columns;
			indices[k + 4] = (i + 1) * columns + 1;
			indices[k + 5] = i * columns + 1;

			k += 6;
		}

		for (unsigned short int i = 0; i < columns - 1; i++)
		{
			indices[k] = noVertices - 1;
			indices[k + 1] = (noVertices - 1) - (i + 1);
			indices[k + 2] = (noVertices - 1) - (i + 2);
			k += 3;
		}

		indices[k] = noVertices - 1;
		indices[k + 1] = (noVertices - 1) - columns;
		indices[k + 2] = noVertices - 2;

		D3D11_BUFFER_DESC indexDesc;
		SecureZeroMemory(&indexDesc, sizeof(indexDesc));
		indexDesc.Usage = D3D11_USAGE_DEFAULT;
		indexDesc.ByteWidth = sizeof(unsigned short int) * numberOfIndices;
		indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexDesc.CPUAccessFlags = 0;
		indexDesc.MiscFlags = 0;

		resourceData.pSysMem = &indices[0];

		Utilities::Assert(
			device->GetDev()->CreateBuffer(&indexDesc, &resourceData, &indexBuffer));

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{
				"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			},
			{
				"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12,
				D3D11_INPUT_PER_VERTEX_DATA, 0
			}
		};

		unsigned int totalLayoutElements = ARRAYSIZE(layout);

		unsigned int vsSize;
		char* vsData;

		device->LoadVertexShader(vertexShader, "UltimaShaders/VertexShaderSkybox.cso", vsData, vsSize);

		Utilities::Assert(
			device->GetDev()->CreateInputLayout(layout, totalLayoutElements,
			(const void*)vsData, (SIZE_T)vsSize, &inputLayout));

		delete[] vsData;

        device->LoadPixelShader(pixelShader, "UltimaShaders/PixelShaderSkybox.cso");
		//pixelShader = ContentManager::GetInstance().LoadPixelShader(
		//	"UltimaShaders/PixelShaderSkybox.cso", device->GetDev());

        device->LoadTexture(colorTex, texturePath, TextureViewDimension::Cube);
		//textureSRV = ContentManager::GetInstance().LoadSkyboxTexture(device->GetDev(), texturePath);

		matrixBuffer = Utilities::CreateBuffer(device->GetDev(), sizeof(XMMATRIX));
		graphicsFilterBuffer = Utilities::CreateBuffer(
			device->GetDev(), sizeof(GraphicsFilter));

		D3D11_SAMPLER_DESC colorMapDesc;
		SecureZeroMemory(&colorMapDesc, sizeof(colorMapDesc));
		colorMapDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		colorMapDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		colorMapDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		colorMapDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		colorMapDesc.MaxLOD = D3D11_FLOAT32_MAX;
		colorMapDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		colorMapDesc.MaxAnisotropy = anisotropicSampleCount;

		Utilities::Assert(
			device->GetDev()->CreateSamplerState(&colorMapDesc, &anisotropicSampler));
	}

	Skybox::~Skybox()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		Utilities::Release(anisotropicSampler);
		Utilities::Release(graphicsFilterBuffer);
		Utilities::Release(matrixBuffer);

		//ContentManager::GetInstance().Unregister(colorTex);

		//ContentManager::GetInstance().Unregister(pixelShader);
		//ContentManager::GetInstance().Unregister(vertexShader);

		Utilities::Release(indexBuffer);
		Utilities::Release(vertexBuffer);
		Utilities::Release(inputLayout);
#endif
	}

	unsigned int short Skybox::GetLines()
		const ULTIMA_NOEXCEPT
	{
		return lines;
	}

	unsigned int short Skybox::GetColumns()
		const ULTIMA_NOEXCEPT
	{
		return columns;
	}

	const std::string& Skybox::GetTexturePath()
		const ULTIMA_NOEXCEPT
	{
		return texturePath;
	}

	const Device* Skybox::GetDevice()
		const ULTIMA_NOEXCEPT
	{
		return device;
	}

	unsigned int Skybox::GetAnisotropicSampleCount()
		const ULTIMA_NOEXCEPT
	{
		return anisotropicSampleCount;
	}

	unsigned int Skybox::Render(
		ID3D11DeviceContext* const context,
		const Camera3D& camera,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter)
		const ULTIMA_NOEXCEPT
	{
		if (fog.Color.w > 0.0f || light.Type == LightType::None)
			return 0;

		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Matrix world = camera.GetView();
		
		world.SetM30(0.0f);
		world.SetM31(0.0f);
		world.SetM32(0.0f);

		world *= camera.GetProjection();

		XMMATRIX tempXM = world.ToXMMATRIX();

		context->UpdateSubresource(matrixBuffer, 0, 0, &tempXM, 0, 0);

		context->VSSetConstantBuffers(0, 1, &matrixBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());

		context->PSSetSamplers(0, 1, &anisotropicSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShader.GetShader(), 0, 0);

		context->UpdateSubresource(graphicsFilterBuffer, 0, 0, &graphicsFilter, 0, 0);
		context->PSSetConstantBuffers(0, 1, &graphicsFilterBuffer);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}

	unsigned int Skybox::Render(
		ID3D11DeviceContext* const context,
		const Matrix& viewProjection,
		const Light& light,
		const Fog& fog,
		const GraphicsFilter& graphicsFilter)
		const ULTIMA_NOEXCEPT
	{
		if (fog.Color.w > 0.0f || light.Type == LightType::None)
			return 0;

		unsigned int stride = sizeof(VertexPT);
		unsigned int offset = 0;

		context->IASetInputLayout(inputLayout);
		context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
		context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		XMMATRIX tempXM = viewProjection.ToXMMATRIX();

		context->UpdateSubresource(matrixBuffer, 0, 0, &tempXM, 0, 0);

		context->VSSetConstantBuffers(0, 1, &matrixBuffer);

		context->PSSetShaderResources(0, 1, colorTex.GetSrvPtr());

		context->PSSetSamplers(0, 1, &anisotropicSampler);

		context->VSSetShader(vertexShader.GetShader(), 0, 0);
		context->PSSetShader(pixelShader.GetShader(), 0, 0);

		context->UpdateSubresource(graphicsFilterBuffer, 0, 0, &graphicsFilter, 0, 0);
		context->PSSetConstantBuffers(0, 1, &graphicsFilterBuffer);

		context->DrawIndexed(numberOfIndices, 0, 0);

		return numberOfIndices / 3;
	}
}