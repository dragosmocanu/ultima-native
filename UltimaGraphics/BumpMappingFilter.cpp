#include "stdafx.h"

#include "BumpMappingFilter.h"

namespace Ultima
{
	BumpMappingFilter::BumpMappingFilter(
		bool isActive)
		ULTIMA_NOEXCEPT
	{
		IsActive = isActive;
	}
}