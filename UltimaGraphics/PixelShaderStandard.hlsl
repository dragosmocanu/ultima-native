#include "BumpMappingFilter.hlsli"
#include "Fog.hlsli"
#include "GraphicsFilter.hlsli"
#include "Light.hlsli"
#include "Material.hlsli"
#include "ShadowFilter.hlsli"

Texture2D modelTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D aoTexture : register(t2);
Texture2D depthMapTexture : register(t3);

SamplerState anisotropicSampler : register(s0);
SamplerComparisonState comparisonSampler : register(s1);
SamplerState bilinearSampler : register(s2);

cbuffer cb0 : register(b0)
{
	Light light;
}

cbuffer cb1 : register(b1)
{
	float3 camPos;
}

cbuffer cb2 : register(b2)
{
	Material material;
}

cbuffer cb3 : register(b3)
{
	Fog fog;
}

cbuffer cb4 : register(b4)
{
	GraphicsFilter graphicsFilter;
}

cbuffer cb5 : register(b5)
{
	ShadowFilter shadowFilter;
}

cbuffer cb6 : register(b6)
{
	BumpMappingFilter bumpMappingFilter;
}

cbuffer cb7 : register(b7)
{
	bool isAmbientOcclusionActive;
}

cbuffer cb8 : register(b8)
{
	bool hasTexture;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
};

float4 main(VertexOut input) : SV_TARGET
{
	float lightPower = shadowFilter.Process(
		input.PositionL,
		depthMapTexture,
		anisotropicSampler,
		comparisonSampler);

	float4 output = float4(1.0f, 1.0f, 1.0f, 1.0f);
	
	if (hasTexture)
		output = modelTexture.Sample(anisotropicSampler, input.Texture);

	float4 diffuse, specular, ambient;
	diffuse = specular = ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);

	input.Normal = normalize(input.Normal);
	input.Tangent = normalize(input.Tangent);

	input.Normal = bumpMappingFilter.Process(
		normalTexture,
		anisotropicSampler,
		input.Texture,
		input.Normal,
		input.Tangent);

	float3 toEye = camPos - input.PositionW;
	float distanceToEye = length(toEye);
	toEye /= distanceToEye;
	
	float ambientFactor = 1.0f;
	
	if (isAmbientOcclusionActive)
	{
		float2 textureCoordinates = input.PositionDepth.xy / input.PositionDepth.w;
		textureCoordinates.y = -textureCoordinates.y;
		textureCoordinates += 1.0f;
		textureCoordinates *= 0.5f;
		ambientFactor = aoTexture.Sample(bilinearSampler, textureCoordinates).r;
	}

	light.Process(material.Metallic, material.Roughness, input.PositionW, input.Normal, toEye, 
		lightPower, Pow5(lightPower), diffuse, specular, ambient);

	output = material.Modulate(output, diffuse, specular, ambient * ambientFactor);

	output = fog.Process(output, distanceToEye, ambientFactor);

	output = graphicsFilter.Process(output);

	return output;
}