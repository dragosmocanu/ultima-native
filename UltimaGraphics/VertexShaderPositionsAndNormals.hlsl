cbuffer cb0 : register(b0)
{
	row_major matrix world;
};

cbuffer cb1 : register(b1)
{
	row_major matrix view;
};

cbuffer cb2 : register(b2)
{
	row_major matrix projection;
};

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
};

struct VertexOut
{
	float4 Position : SV_POSITION;

	float3 PositionV : TEXCOORD0;
	float3 NormalV : TEXCOORD1;
	float TessellationFactor : TESSELLATION;
	float2 Texture : TEXCOORD3;
};

VertexOut main(VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = float4(input.Position, 1);
	output.Position = mul(output.Position, world);
	output.Position = mul(output.Position, view);

	output.PositionV = output.Position.xyz;

	output.Position = mul(output.Position, projection);

	output.NormalV = input.Normal;
	output.NormalV = mul(output.NormalV, (float3x3)world);
	output.NormalV = mul(output.NormalV, (float3x3)view);

	output.Texture = input.Texture;

	float dist = length(output.PositionV);
	float tessellation = saturate((500.0f - dist) / (500.0f - 20.0f));

	output.TessellationFactor = 2.0f + tessellation * (25.0f - 1.0f);

	return output;
}