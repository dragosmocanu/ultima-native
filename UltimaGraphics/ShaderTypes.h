#pragma once

#include "Macros.h"

#if defined(DIRECTX) && !defined(DIRECTX12)
#include "ShaderTypesDx11.h"
namespace Ultima
{
    class VertexShader : public VertexShaderDx11 { };
    class PixelShader : public PixelShaderDx11 { };
    class HullShader : public HullShaderDx11 { };
    class DomainShader : public DomainShaderDx11 { };
    class GeometryShader : public GeometryShaderDx11 { };
    class ComputeShader : public ComputeShaderDx11 { };
}
#endif