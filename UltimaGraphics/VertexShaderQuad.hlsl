//cbuffer cb0 : register(b0)
//{
//	float2 offset;
//};

struct VertexIn
{
	float3 Position : POSITION;
	float2 Texture : TEXCOORD0;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

VertexOut main (VertexIn input)
{
	VertexOut output = (VertexOut)0;

	output.Position = float4(input.Position, 1.0f);
	//output.Position.x += offset.x;
	//output.Position.y -= offset.y;

	output.Texture = input.Texture;

	return output;
}