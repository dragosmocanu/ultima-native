#include "BloomFilter.hlsli"
#include "ToneMappingFilter.hlsli"
#include "GraphicsFilter.hlsli"

Texture2D colorTexture : register(t0);
Texture2D luminanceTexture : register(t1);
Texture2D bloomTexture : register(t2);

SamplerState bilinearSampler : register(s0);
SamplerState pointSampler : register(s1);

cbuffer cb0 : register(b0)
{
	ToneMappingFilter toneMappingFilter;
}

cbuffer cb1 : register(b1)
{
	BloomFilter bloomFilter;
}

cbuffer cb2 : register(b2)
{
	GraphicsFilter graphicsFilter;
}

cbuffer cb3 : register(b3)
{
	unsigned int mipLevel;
}

struct VertexOut
{
	float4 Position : SV_POSITION;
	float2 Texture : TEXCOORD0;
};

float4 main(VertexOut input) : SV_TARGET
{
	float avgLuminance = luminanceTexture.SampleLevel(pointSampler, input.Texture, mipLevel).r;
	float3 color = colorTexture.Sample(bilinearSampler, input.Texture).rgb;

	color = toneMappingFilter.Process(color, avgLuminance, 0)
		+ bloomFilter.Process(bloomTexture, bilinearSampler, input.Texture);

	graphicsFilter.Process(float4(color, 1.0f));

	return float4(color, 1.0f);
}