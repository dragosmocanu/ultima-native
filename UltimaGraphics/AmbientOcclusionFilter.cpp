#include "stdafx.h"

#include "AmbientOcclusionFilter.h"

namespace Ultima
{
	AmbientOcclusionFilter::AmbientOcclusionFilter(
		AmbientOcclusionType type,
		float intensity,
		float ssaoSampleRadius,
		float ssaoScale,
		float ssaoBias,
		unsigned int hbaoNumberOfSteps,
		unsigned int hbaoNumberOfDirections,
		float hbaoR,
		float hbaoAngleBias,
		float hbaoMaxRadiusPixels,
		Vector2 hbaoFocalLen)
		ULTIMA_NOEXCEPT
		: Type(type),
		Intensity(intensity),
		SsaoSampleRadius(ssaoSampleRadius),
		SsaoScale(ssaoScale),
		SsaoBias(ssaoBias),
		HbaoNumberOfSteps(hbaoNumberOfSteps),
		HbaoNumberOfDirections(hbaoNumberOfDirections),
		HbaoR(hbaoR),
		HbaoAngleBias(hbaoAngleBias),
		HbaoMaxRadiusPixels(hbaoMaxRadiusPixels),
		HbaoFocalLen(hbaoFocalLen.ToXMFLOAT2())
	{ }
}