// Copyright (c) 2011 NVIDIA Corporation. All rights reserved.
//
// TO  THE MAXIMUM  EXTENT PERMITTED  BY APPLICABLE  LAW, THIS SOFTWARE  IS PROVIDED
// *AS IS*  AND NVIDIA AND  ITS SUPPLIERS DISCLAIM  ALL WARRANTIES,  EITHER  EXPRESS
// OR IMPLIED, INCLUDING, BUT NOT LIMITED  TO, NONINFRINGEMENT,IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL  NVIDIA 
// OR ITS SUPPLIERS BE  LIABLE  FOR  ANY  DIRECT, SPECIAL,  INCIDENTAL,  INDIRECT,  OR  
// CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION,  DAMAGES FOR LOSS 
// OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY 
// OTHER PECUNIARY LOSS) ARISING OUT OF THE  USE OF OR INABILITY  TO USE THIS SOFTWARE, 
// EVEN IF NVIDIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//
// Please direct any bugs or questions to SDKFeedback@nvidia.com

float3 FetchEyePos(float2 uv, Texture2D positionTexture, SamplerState bilinearSampler)
{
	return positionTexture.Sample(bilinearSampler, uv).xyz;
}

//----------------------------------------------------------------------------------
float InvLength(float2 v)
{
	return rsqrt(dot(v, v));
}

//----------------------------------------------------------------------------------
float Tangent(float3 P, float3 S)
{
	return (P.z - S.z) * InvLength(S.xy - P.xy);
}

//----------------------------------------------------------------------------------
float Length2(float3 v)
{
	return dot(v, v);
}

//----------------------------------------------------------------------------------
float3 MinDiff(float3 P, float3 Pr, float3 Pl)
{
	float3 V1 = Pr - P;
		float3 V2 = P - Pl;
		return (Length2(V1) < Length2(V2)) ? V1 : V2;
}

//----------------------------------------------------------------------------------
float Falloff(float d2, float negativeInverseR2)
{
	// 1 scalar mad instruction
	return d2 * negativeInverseR2 + 1.0f;
}

//----------------------------------------------------------------------------------
float2 SnapUVOffset(float2 uv, float2 screenSize, float2 texelSize)
{
	return round(uv * screenSize) * texelSize;
}

//----------------------------------------------------------------------------------
float TanToSin(float x)
{
	return x * rsqrt(x*x + 1.0f);
}

//----------------------------------------------------------------------------------
float3 TangentVector(float2 deltaUV, float3 dPdu, float3 dPdv)
{
	return deltaUV.x * dPdu + deltaUV.y * dPdv;
}

//----------------------------------------------------------------------------------
float Tangent(float3 T)
{
	return -T.z * InvLength(T.xy);
}

//----------------------------------------------------------------------------------
float BiasedTangent(float3 T, float tanAngleBias)
{
	// Do not use atan() because it gets expanded by fxc to many math instructions
	return Tangent(T) + tanAngleBias;
}

#if USE_NORMAL_FREE_HBAO

//----------------------------------------------------------------------------------
void IntegrateDirection(inout float ao, float3 P, float2 uv, float2 deltaUV,
	float numSteps, float tanH, float sinH)
{
	[unroll(10)]
	for (float j = 1; j <= numSteps; ++j)
	{
		uv += deltaUV;
		float3 S = FetchEyePos(uv);
			float tanS = Tangent(P, S);
		float d2 = Length2(S - P);

		[branch]
		if ((d2 < g_R2) && (tanS > tanH))
		{
			// Accumulate AO between the horizon and the sample
			float sinS = TanToSin(tanS);
			ao += Falloff(d2) * (sinS - sinH);

			// Update the current horizon angle
			tanH = tanS;
			sinH = sinS;
		}
	}
}

//----------------------------------------------------------------------------------
float NormalFreeHorizonOcclusion(float2 deltaUV,
	float2 texelDeltaUV,
	float2 uv0,
	float3 P,
	float numSteps,
	float randstep)
{

	float tanT = tan(-3.14159f * 0.5f + g_AngleBias);
	float sinT = TanToSin(tanT);

#if SAMPLE_FIRST_STEP
	float ao1 = 0;

	// Take a first sample between uv0 and uv0 + deltaUV
	float2 deltaUV1 = SnapUVOffset(randstep * deltaUV + texelDeltaUV);
		IntegrateDirection(ao1, P, uv0, deltaUV1, 1, tanT, sinT);
	IntegrateDirection(ao1, P, uv0, -deltaUV1, 1, tanT, sinT);

	ao1 = max(ao1 * 0.5 - 1.0, 0.0);
	--numSteps;
#endif

	float ao = 0;
	float2 uv = uv0 + SnapUVOffset(randstep * deltaUV);
		deltaUV = SnapUVOffset(deltaUV);
	IntegrateDirection(ao, P, uv, deltaUV, numSteps, tanT, sinT);

	// Integrate opposite directions together
	deltaUV = -deltaUV;
	uv = uv0 + SnapUVOffset(randstep * deltaUV);
	IntegrateDirection(ao, P, uv, deltaUV, numSteps, tanT, sinT);

	// Divide by 2 because we have integrated 2 directions together
	// Subtract 1 and clamp to remove the part below the surface
#if SAMPLE_FIRST_STEP
	return max(ao * 0.5 - 1.0, ao1);
#else
	return max(ao * 0.5 - 1.0, 0.0);
#endif
}

#else //USE_NORMAL_FREE_HBAO

//----------------------------------------------------------------------------------
float IntegerateOcclusion(float2 uv0,
	float2 snapped_duv,
	float3 P,
	float3 dPdu,
	float3 dPdv,
	inout float tanH,
	float tanAngleBias,
	float R2,
	float negativeInverseR2,
	Texture2D positionTexture,
	SamplerState bilinearSampler)
{
	float ao = 0;

	// Compute a tangent vector for snapped_duv
	float3 T1 = TangentVector(snapped_duv, dPdu, dPdv);
		float tanT = BiasedTangent(T1, tanAngleBias);
	float sinT = TanToSin(tanT);

	float3 S = positionTexture.Sample(bilinearSampler, uv0 + snapped_duv).xyz;
		float tanS = Tangent(P, S);

	float sinS = TanToSin(tanS);
	float d2 = Length2(S - P);

	if ((d2 < R2) && (tanS > tanT))
	{
		// Compute AO between the tangent plane and the sample
		ao = Falloff(d2, negativeInverseR2) * (sinS - sinT);

		// Update the horizon angle
		tanH = max(tanH, tanS);
	}

	return ao;
}

//----------------------------------------------------------------------------------
float horizon_occlusion(
	float2 deltaUV,
	float2 texelDeltaUV,
	float2 uv0,
	float3 P,
	float numSteps,
	float randstep,
	float3 dPdu,
	float3 dPdv,
	float2 screenSize,
	float2 texelSize,
	float tanAngleBias,
	float R2,
	float negativeInverseR2,
	Texture2D positionTexture,
	SamplerState bilinearSampler)
{
	float ao = 0;

	// Randomize starting point within the first sample distance
	float2 uv = uv0 + SnapUVOffset(randstep * deltaUV, screenSize, texelSize);

		// Snap increments to pixels to avoid disparities between xy
		// and z sample locations and sample along a line
		deltaUV = SnapUVOffset(deltaUV, screenSize, texelSize);

	// Compute tangent vector using the tangent plane
	float3 T = deltaUV.x * dPdu + deltaUV.y * dPdv;

		float tanH = BiasedTangent(T, tanAngleBias);

#if SAMPLE_FIRST_STEP
	// Take a first sample between uv0 and uv0 + deltaUV
	float2 snapped_duv = SnapUVOffset(randstep * deltaUV + texelDeltaUV, screenSize, texelSize);
		ao = IntegerateOcclusion(uv0, snapped_duv, P, dPdu, dPdv, tanH, tanAngleBias, R2, negativeInverseR2);
	--numSteps;
#endif

	float sinH = tanH / sqrt(1.0f + tanH*tanH);

	[unroll(10)]
	for (float j = 1; j <= numSteps; ++j)
	{
		uv += deltaUV;
		float3 S = FetchEyePos(uv, positionTexture, bilinearSampler);
		float tanS = Tangent(P, S);
		float d2 = Length2(S - P);

		// Use a merged dynamic branch
		if ((d2 < R2) && (tanS > tanH))
		{
			// Accumulate AO between the horizon and the sample
			float sinS = tanS / sqrt(1.0f + tanS*tanS);
			ao += Falloff(d2, negativeInverseR2) * (sinS - sinH);

			// Update the current horizon angle
			tanH = tanS;
			sinH = sinS;
		}
	}

	return ao;
}

#endif //USE_NORMAL_FREE_HBAO

//----------------------------------------------------------------------------------
float2 RotateDirections(float2 Dir, float2 CosSin)
{
	return float2(Dir.x*CosSin.x - Dir.y*CosSin.y,
		Dir.x*CosSin.y + Dir.y*CosSin.x);
}

//----------------------------------------------------------------------------------
void ComputeSteps(inout float2 step_size_uv, inout float numSteps, float ray_radius_pix, float rand, float initialNumSteps, float maxRadiusPixels, float2 texelSize)
{
	// Avoid oversampling if NUM_STEPS is greater than the kernel radius in pixels
	numSteps = min(initialNumSteps, ray_radius_pix);

	// Divide by Ns+1 so that the farthest samples are not fully attenuated
	float step_size_pix = ray_radius_pix / (numSteps + 1);

	// Clamp numSteps if it is greater than the max kernel footprint
	float maxNumSteps = maxRadiusPixels / step_size_pix;
	if (maxNumSteps < numSteps)
	{
		// Use dithering to avoid AO discontinuities
		numSteps = floor(maxNumSteps + rand);
		numSteps = max(numSteps, 1);
		step_size_pix = maxRadiusPixels / numSteps;
	}

	// Step size in uv space
	step_size_uv = step_size_pix * texelSize;
}
