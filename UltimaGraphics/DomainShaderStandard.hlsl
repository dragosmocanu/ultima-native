#include "DisplacementMappingFilter.hlsli"

Texture2D displacementTexture : register(t0);

SamplerState bilinearSampler : register(s0);

cbuffer cb0 : register(b0)
{
	row_major matrix viewProjection;
}

cbuffer cb1 : register(b1)
{
	DisplacementMappingFilter dmf;
}

// Output control point
struct HS_CONTROL_POINT_OUTPUT
{
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
};

struct DS_OUTPUT
{
	float4 Position : SV_POSITION;
	float3 PositionW : POSITION;
	float3 Normal : NORMAL;
	float2 Texture : TEXCOORD0;
	float3 Tangent : TANGENT;

	float4 PositionL : TEXCOORD1;
	float4 PositionDepth : TEXCOORD2;
};

// Output patch constant data.
struct HS_CONSTANT_DATA_OUTPUT
{
	float EdgeTessFactor[3]			: SV_TessFactor; // e.g. would be [4] for a quad domain
	float InsideTessFactor			: SV_InsideTessFactor; // e.g. would be Inside[2] for a quad domain
	// TODO: change/add other stuff
};

#define NUM_CONTROL_POINTS 3

[domain("tri")]
DS_OUTPUT main(
	HS_CONSTANT_DATA_OUTPUT input,
	float3 domain : SV_DomainLocation,
	const OutputPatch<HS_CONTROL_POINT_OUTPUT, NUM_CONTROL_POINTS> patch)
{
	DS_OUTPUT output = (DS_OUTPUT)0;

	output.PositionW = domain.x * patch[0].PositionW
		+ domain.y * patch[1].PositionW
		+ domain.z * patch[2].PositionW;

	output.Normal = domain.x * patch[0].Normal
		+ domain.y * patch[1].Normal
		+ domain.z * patch[2].Normal;

	output.Texture = domain.x * patch[0].Texture
		+ domain.y * patch[1].Texture
		+ domain.z * patch[2].Texture;

	output.PositionL = domain.x * patch[0].PositionL
		+ domain.y * patch[1].PositionL
		+ domain.z * patch[2].PositionL;

	output.Tangent = domain.x * patch[0].Tangent
		+ domain.y * patch[1].Tangent
		+ domain.z * patch[2].Tangent;

	output.PositionW = dmf.Process(displacementTexture, bilinearSampler, output.Texture,
		output.PositionW, output.Normal);

	output.Position = mul(float4(output.PositionW, 1.0f), viewProjection);

	output.PositionDepth = output.Position;

	return output;
}
