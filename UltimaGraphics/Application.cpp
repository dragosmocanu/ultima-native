#include "stdafx.h"

#include "Application.h"

#include <Windows.h>
#include <WinUser.h>

#if DIRECTX
#include <stdlib.h>
#endif

#include "Utilities.h"

namespace Ultima
{
	Application* app;

	Application::Application(
		const HINSTANCE& hInstance,
		const HINSTANCE& prevInstance,
		const LPWSTR& cmdLine,
		int cmdShow,
		const std::string& title, 
		unsigned int width, 
		unsigned int height, 
		bool isInVsync,
		bool isShowingFPS, 
		unsigned int antiAliasingSampleCount,
		const WNDPROC& messageLoop)
		ULTIMA_NOEXCEPT
		: hInstance(hInstance),
		title(title),
		isShowingFPS(isShowingFPS),
		width(width),
		height(height),
		aspectRatio(width / static_cast<float>(height)),
		antiAliasingSampleCount(antiAliasingSampleCount),
		isInVsync(isInVsync),
		hasFocus(false),
		timeFactor(1.0f),
		isRunning(true)
	{
		UNREFERENCED_PARAMETER(prevInstance);
		UNREFERENCED_PARAMETER(cmdLine);

		app = this;

		if (this->title.empty())
			this->title = "Ultima Fiery Orange Application";

		if (this->antiAliasingSampleCount < 1)
			this->antiAliasingSampleCount = 1;
		
		WNDCLASSEXA wndClass = { 0 };
		wndClass.cbSize = sizeof(WNDCLASSEX);
		wndClass.style = CS_HREDRAW | CS_VREDRAW;
		wndClass.lpfnWndProc = messageLoop;
		wndClass.hInstance = this->hInstance;
		wndClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wndClass.lpszMenuName = nullptr;
		wndClass.lpszClassName = this->title.c_str();

		ATOM atom = RegisterClassExA(&wndClass);

		if (!atom)
			return;

		RECT rc = { 0, 0, width, height };
		AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

		this->hwnd = CreateWindowA((LPCSTR)atom, 
			this->title.c_str(),
			WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 
			rc.right - rc.left, rc.bottom - rc.top, 
			nullptr, nullptr, this->hInstance, nullptr);

		if (!hwnd)
			return;

		ShowWindow(hwnd, cmdShow);

		InputManager::GetInstance().Initialize(this->hInstance, hwnd);

		//AudioManager::GetInstance().Initialize(hwnd);

#ifdef DIRECTX
		D3D_DRIVER_TYPE driverTypes[] = { D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_SOFTWARE };

		unsigned int totalDriverTypes = ARRAYSIZE(driverTypes);

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1
		};

		unsigned int totalFeatureLevels = ARRAYSIZE(featureLevels);

		DXGI_SWAP_CHAIN_DESC swapChainDesc;
		SecureZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
		swapChainDesc.BufferCount = 1;
		swapChainDesc.BufferDesc.Width = this->width;
		swapChainDesc.BufferDesc.Height = this->height;
		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.OutputWindow = hwnd;
		swapChainDesc.Windowed = true;
		swapChainDesc.SampleDesc.Count = this->antiAliasingSampleCount;
		swapChainDesc.SampleDesc.Quality = 0;
		
		unsigned int creationFlags = 0;

		HRESULT result = 1;
		unsigned int driver = 0;

		D3D_DRIVER_TYPE driverType;
		D3D_FEATURE_LEVEL featureLevel;

		for (driver = 0; driver < totalDriverTypes; driver++)
		{
			result = D3D11CreateDeviceAndSwapChain(0, driverTypes[driver], 0,
				creationFlags, featureLevels, totalFeatureLevels,
				D3D11_SDK_VERSION, &swapChainDesc, &swapChain.GetSwapChain(), &device.GetDevRef(),
				&featureLevel, &context);
			
			if SUCCEEDED(result)
			{
				driverType = driverTypes[driver];
				break;
			}
		}

		Utilities::Assert(result);

		D3D11_DEPTH_STENCIL_DESC rDesc;
		SecureZeroMemory(&rDesc, sizeof(rDesc));
		rDesc.DepthEnable = true;
		rDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		rDesc.DepthFunc = D3D11_COMPARISON_LESS;
		rDesc.StencilEnable = true;
		rDesc.StencilReadMask = 0xFF;
		rDesc.StencilWriteMask = 0xFF;
		rDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		rDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		rDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		rDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		rDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		rDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		rDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		rDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		Utilities::Assert(device.GetDev()->CreateDepthStencilState(&rDesc, &depthEnabled));

		rDesc.DepthEnable = false;

		Utilities::Assert(device.GetDev()->CreateDepthStencilState(&rDesc, &depthDisabled));

		D3D11_RASTERIZER_DESC drawingStateDesc;
		SecureZeroMemory(&drawingStateDesc, sizeof(drawingStateDesc));
		drawingStateDesc.CullMode = D3D11_CULL_BACK;
		drawingStateDesc.FillMode = D3D11_FILL_SOLID;
		drawingStateDesc.DepthClipEnable = true;

		Utilities::Assert(
			device.GetDev()->CreateRasterizerState(&drawingStateDesc, &cullBack));

		drawingStateDesc.CullMode = D3D11_CULL_FRONT;

		Utilities::Assert(
			device.GetDev()->CreateRasterizerState(&drawingStateDesc, &cullFront));

		drawingStateDesc.CullMode = D3D11_CULL_NONE;
		drawingStateDesc.FillMode = D3D11_FILL_WIREFRAME;

		Utilities::Assert(
			device.GetDev()->CreateRasterizerState(&drawingStateDesc, &wireframe));

		D3D11_BLEND_DESC blendStateDesc;
		SecureZeroMemory(&blendStateDesc, sizeof(blendStateDesc));

		blendStateDesc.RenderTarget[0].BlendEnable = true;
		blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendStateDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

		Utilities::Assert(
			device.GetDev()->CreateBlendState(&blendStateDesc, &blendEnabled));

		blendStateDesc.RenderTarget[0].BlendEnable = false;

		Utilities::Assert(
			device.GetDev()->CreateBlendState(&blendStateDesc, &blendDisabled));

		buildTextures();

		quad = new Quad(device);
#endif

		Render(0.0f);
	}

	void Application::OnResize()
		ULTIMA_NOEXCEPT
	{
		buildTextures();
	}

	void Application::buildTextures()
		ULTIMA_NOEXCEPT
	{
        // TODO: Nicer way of doing this :/
		Utilities::Release(backBuffer.GetDsv());
		Utilities::Release(backBuffer.GetRtv());


		ID3D11Texture2D* backBufferNew;
		ID3D11Texture2D* depthTextureNew;

		//Utilities::Assert(swapChain->ResizeBuffers(1, width, height,
		//	DXGI_FORMAT_R8G8B8A8_UNORM, 0));
        swapChain.Resize(width, height);

		Utilities::Assert(swapChain.GetSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D),
			reinterpret_cast<void**>(&backBufferNew)));

		Utilities::Assert(
			device.GetDev()->CreateRenderTargetView(backBufferNew, 0, backBuffer.GetRtvPtr()));

		D3D11_TEXTURE2D_DESC depthTextureDesc;
		SecureZeroMemory(&depthTextureDesc, sizeof(depthTextureDesc));
		depthTextureDesc.Width = this->width;
		depthTextureDesc.Height = this->height;
		depthTextureDesc.MipLevels = 1;
		depthTextureDesc.ArraySize = 1;
		depthTextureDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		depthTextureDesc.SampleDesc.Count = this->antiAliasingSampleCount;
		depthTextureDesc.SampleDesc.Quality = 0;
		depthTextureDesc.Usage = D3D11_USAGE_DEFAULT;
		depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthTextureDesc.CPUAccessFlags = 0;
		depthTextureDesc.MiscFlags = 0;

		Utilities::Assert(
			device.GetDev()->CreateTexture2D(&depthTextureDesc, nullptr, &depthTextureNew));

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		SecureZeroMemory(&dsvDesc, sizeof(dsvDesc));
		dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
		dsvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(device.GetDev()->CreateDepthStencilView(depthTextureNew,
			&dsvDesc, backBuffer.GetDsvPtr()));

		context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), backBuffer.GetDsv());

		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = static_cast<float>(width);
		viewport.Height = static_cast<float>(height);
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;

		memcpy(&viewportHalved, &viewport, sizeof(viewport));

		viewportHalved.Width = width / 2.0f;
		viewportHalved.Height = height / 2.0f;

		context->RSSetViewports(1, &viewport);

		aspectRatio = width / static_cast<float>(height);

		Utilities::Release(backBufferNew);
		Utilities::Release(depthTextureNew);
	}

	Application::~Application()
		ULTIMA_NOEXCEPT
	{
		delete quad;

		ContentManager::GetInstance().UnloadUnused();

		//Utilities::Release(backBufferDSV);
		//Utilities::Release(backBufferRTV);

		Utilities::Release(blendEnabled);
		Utilities::Release(blendDisabled);
		Utilities::Release(wireframe);
		Utilities::Release(cullFront);
		Utilities::Release(cullBack);

		Utilities::Release(depthEnabled);
		Utilities::Release(depthDisabled);
		//Utilities::Release(swapChain);
		//Utilities::Release(device);
		Utilities::Release(context);
	}

	LRESULT CALLBACK Application::MessageLoop(
		HWND hwnd, UINT message, 
		WPARAM wParam, LPARAM lParam)
		ULTIMA_NOEXCEPT
	{
		return app->MessageDispatcher(hwnd, message, wParam, lParam);
	}

	LRESULT CALLBACK Application::MessageDispatcher(
		HWND hwnd, UINT message, 
		WPARAM wParam, LPARAM lParam)
		ULTIMA_NOEXCEPT
	{
		switch (message)
		{
		case WM_ACTIVATE:
			if (LOWORD(wParam) != WA_INACTIVE)
				timer.Update();
			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_SIZE:
			width = LOWORD(lParam);
			height = HIWORD(lParam);
			
			if (device.GetDev() != nullptr && wParam == SIZE_MAXIMIZED)
				OnResize();
			
			return 0;

		case WM_EXITSIZEMOVE:
			timer.Update();
			OnResize();
			return 0;

		case WM_SYSCOMMAND:
			if (wParam == SC_KEYMENU && (lParam >> 16) <= 0)
				return 0;
			else
				return DefWindowProc(hwnd, message, wParam, lParam);

		case WM_MENUCHAR:
			return MAKELRESULT(0, MNC_CLOSE);

		case WM_GETMINMAXINFO:
			((MINMAXINFO*)lParam)->ptMinTrackSize.x = 320;
			((MINMAXINFO*)lParam)->ptMinTrackSize.y = 160;
			return 0;

		default:
			return DefWindowProc(hwnd, message, wParam, lParam);
		}

		return 0;
	}

	int Application::Run()
		ULTIMA_NOEXCEPT
	{
		MSG msg = { 0 };

		double time = 0;
		long long fps = 0;
		long long fpsDisplayed = 60;

		timer.Restart();

		while (msg.message != WM_QUIT && isRunning)
		{
			if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if (hwnd == GetFocus())
			{
				hasFocus = true;
				InputManager::GetInstance().Update();
			}
			else
			{
				hasFocus = false;
				InputManager::GetInstance().Flush();
			}

			timer.Update();
			float dt = timer.GetDeltaTime();
			time += dt;
			fps++;

			if (isShowingFPS)
			{
				if (time >= 1000.0f)
				{
					fpsDisplayed = fps;
					time -= 1000.0f;
					fps = 0;
				}

				std::string str = title
					+ " | FPS: " + std::to_string(fpsDisplayed)
					+ " | Delta: " + std::to_string(1000.0f / fpsDisplayed);

				SetWindowTextA(hwnd, str.c_str());
			}
			else
				SetWindowTextA(hwnd, title.c_str());

			dt *= timeFactor;

			Update(dt);
			Render(dt);

			SwapBuffers();
		}

		return static_cast<int>(msg.wParam);
	}

	void Application::Update(float deltaTime)
		ULTIMA_NOEXCEPT
	{
		UNREFERENCED_PARAMETER(deltaTime);
		//Write own implementation in child class.
	}

	void Application::Render(float deltaTime)
		ULTIMA_NOEXCEPT
	{
		UNREFERENCED_PARAMETER(deltaTime);

		Clear(Color::Black);

		SwapBuffers();
	}

	void Application::Clear(const Color& color) 
		const ULTIMA_NOEXCEPT
	{
		Utilities::Clear(context, backBuffer.GetRtv(), color);

		Utilities::Clear(context, backBuffer.GetDsv());
	}

	void Application::SwapBuffers() 
		ULTIMA_NOEXCEPT
	{
        swapChain.Flip(isInVsync);
	}

	void Application::SetRenderingToDepth(
		bool isDepthRendering,
		ID3D11RenderTargetView* const depthRTV,
		ID3D11DepthStencilView* const depthDSV)
		ULTIMA_NOEXCEPT
	{
		if (isDepthRendering)
		{
			context->RSSetViewports(1, &viewportHalved);
			context->OMSetRenderTargets(1, &depthRTV, depthDSV);
			context->RSSetState(cullFront);
		}
		else
		{
			context->RSSetViewports(1, &viewport);
			context->OMSetRenderTargets(1, backBuffer.GetRtvPtr(), backBuffer.GetDsv());
			context->RSSetState(cullBack);
		}
	}

	void Application::SetBlend(bool isBlendRendering)
		ULTIMA_NOEXCEPT
	{
		float blend[4];
		blend[0] = blend[1] = blend[2] = blend[3] = 0.0f;

		if (isBlendRendering)
		{
			context->OMSetBlendState(blendEnabled, blend, 0xffffffff);
		}
		else
		{
			context->OMSetBlendState(blendDisabled, blend, 0xffffffff);
			context->RSSetState(cullBack);
		}
	}

	void Application::SetDepth(bool hasDepth)
		ULTIMA_NOEXCEPT
	{
		if (hasDepth)
		{
			context->OMSetDepthStencilState(depthEnabled, 1);
		}
		else
		{
			context->OMSetDepthStencilState(depthDisabled, 1);
		}
	}

}