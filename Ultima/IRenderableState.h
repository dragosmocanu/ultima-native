#pragma once

#include "Macros.h"

#include "StateManager.h"

namespace Ultima
{
	class StateManager;

	/*!
	Defines an interface for a state that can be rendered, not only updated.
	*/
	class IRenderableState : public IState
	{
	public:

		/*!
		Draws the state on the screen.
		*/
		ULTIMA_API virtual void Render(
			ID3D11DeviceContext* const context,
			const StateManager& stateManager) 
			ULTIMA_NOEXCEPT = 0;
	};
}