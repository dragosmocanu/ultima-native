#pragma once

#include "Macros.h"

#include "ICollisionable.h"
#include "Matrix.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a bounding box which can be checked for collision with other bounding volumes.
	*/
	class Box : public ICollisionable
	{
	public:
		/*!
		Creates a new instance of Box.
		*/
		ULTIMA_API Box()
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Box.
		*/
		ULTIMA_API Box(
			const Vector3& position,
			const Vector3& size, 
			const Matrix& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Box.
		*/
		ULTIMA_API Box(
			const Vector3& position,
			const Vector3& size,
			const Quaternion& orientation)
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~Box()
			ULTIMA_NOEXCEPT;

		/*!
		Returns true if the frustum is colliding with another collisionable volume.
		*/
		ULTIMA_API virtual bool IsColliding(const ICollisionable& other)
			const ULTIMA_NOEXCEPT override;

		/*!
		Transforms the box by a given projection, position and rotation.
		*/
		ULTIMA_API virtual void Transform(
			const Vector3& position,
			const Matrix& rotation)
			ULTIMA_NOEXCEPT;

		/*
		Sets the global position of the box.
		*/
		ULTIMA_API virtual void SetPosition(
			const Vector3& position)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the box.
		*/
		ULTIMA_API virtual void SetRotation(
			const Matrix& rotation)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the box.
		*/
		ULTIMA_API virtual void SetRotation(
			const Quaternion& quaterion)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Gets the inner bounding box object.
		*/
		ULTIMA_API virtual const DirectX::BoundingOrientedBox& GetInnerBox()
			const ULTIMA_NOEXCEPT;
#endif

	protected:
#if DIRECTX
		Vector3 center;
		DirectX::BoundingOrientedBox box;
#endif
	};

}