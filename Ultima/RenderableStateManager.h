#pragma once

#include "Macros.h"

#include "StateManager.h"

namespace Ultima
{
	/*!
	Defines a renderable state manager that can manage renderable states.
	*/
	class RenderableStateManager : public StateManager
	{
	public:
		/*!
		Creates a new instance of DrawableStateManager.
		*/
		ULTIMA_API RenderableStateManager()
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~RenderableStateManager()
			ULTIMA_NOEXCEPT;

		/*!
		Draws on the screen.
		*/
		ULTIMA_API virtual void Render(ID3D11DeviceContext* const context)
			ULTIMA_NOEXCEPT;
	};
}