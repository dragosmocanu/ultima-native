#pragma once

#include "Macros.h"

#include "Vector2.h"
#include "Vector3.h"
#include "VertexPTN.h"

namespace Ultima
{
	/*!
	Defines a structure for a Vertex with a Position, Texture Coordinates, a Normal, a Tangent, and a Binormal.
	*/
	struct Vertex : public VertexPTN
	{
		/*!
		Creates a new instance of Vertex.
		*/
		ULTIMA_API Vertex()
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vertex.
		*/
		ULTIMA_API Vertex(
			const Vector3& position,
			const Vector2& texture,
			const Vector3& normal,
			const Vector3& tangent)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vertex.
		*/
		ULTIMA_API Vertex(
			float positionX, float positionY, float positionZ,
			float textureU, float textureV,
			float normalX, float normalY, float normalZ,
			float tangentX, float tangentY, float tangentZ)
			ULTIMA_NOEXCEPT;

		ULTIMA_API bool operator==(const Vertex& other)
			const ULTIMA_NOEXCEPT;

		XMFLOAT3 Tangent;
		//XMFLOAT3 Binormal;
	};
}