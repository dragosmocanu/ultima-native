#include "stdafx.h"

#include "StateManager.h"

#include "MathExtensions.h"

namespace Ultima
{
	StateManager::StateManager()
		ULTIMA_NOEXCEPT
	{
		stateIndex = 0;
	}

	StateManager::~StateManager()
		ULTIMA_NOEXCEPT
	{
		for (unsigned int i = 0; i < states.size(); i++)
		{
			IState* temp = states[i];
			delete temp;
		}

		states.clear();
	}

	unsigned int StateManager::GetStateIndex() 
		const ULTIMA_NOEXCEPT
	{
		return stateIndex;
	}

	const std::vector<IState*>& StateManager::GetStates() 
		const ULTIMA_NOEXCEPT
	{
		return states;
	}

	void StateManager::Update(float deltaTime)
		ULTIMA_NOEXCEPT
	{
		states[stateIndex]->Update(deltaTime, *this);
	}

	void StateManager::ChangeStateTo(unsigned int futureStateIndex)
		ULTIMA_NOEXCEPT
	{
		stateIndex = static_cast<unsigned int>(MathExtensions::Clamp(
			static_cast<float>(futureStateIndex), 
			0, 
			static_cast<float>(states.size())));
	}

	void StateManager::AddState(IState* const newState)
		ULTIMA_NOEXCEPT
	{
		states.push_back(newState);
	}
	
	void StateManager::RemoveState(unsigned int stateIndex)
		ULTIMA_NOEXCEPT
	{
		if (stateIndex < states.size()
			&& stateIndex >= 0)
		{
			IState* temp = states[stateIndex];
			delete temp;
			states.erase(states.begin() + stateIndex);
		}

		stateIndex = static_cast<unsigned int>(MathExtensions::Clamp(
			static_cast<float>(stateIndex), 
			0, 
			static_cast<float>(states.size())));
	}

	void StateManager::RemoveState(const IState* const state)
		ULTIMA_NOEXCEPT
	{
		for (unsigned int i = 0; i < states.size(); i++)
		{
			if (states[i] == state)
			{
				states.erase(states.begin() + i);
				break;
			}
		}

		stateIndex = static_cast<unsigned int>(MathExtensions::Clamp(
			static_cast<float>(stateIndex), 
			0, 
			static_cast<float>(states.size())));
	}
}