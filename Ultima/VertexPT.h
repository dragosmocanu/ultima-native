#pragma once

#include "Macros.h"

#include "Vector2.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines a structure for a Vertex that has only a Position, and Texture Coordinates.
	*/
	struct VertexPT
	{
		/*!
		Creates a new instance of VertexPT.
		*/
		ULTIMA_API VertexPT()
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of VertexPT.
		*/
		ULTIMA_API VertexPT(
			const Vector3& position,
			const Vector2& texture)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of VertexPT.
		*/
		ULTIMA_API VertexPT(
			float positionX, float positionY, float positionZ,
			float textureU, float textureV)
			ULTIMA_NOEXCEPT;

		ULTIMA_API bool operator==(const VertexPT& other)
			const ULTIMA_NOEXCEPT;

		XMFLOAT3 Position;
		XMFLOAT2 Texture;
	};
}