#pragma once

#include "Macros.h"

#include "Vector2.h"
#include "Vector3.h"
#include "VertexPT.h"

namespace Ultima
{
	/*!
	Defines a structure for a Vertex that has only a Position, Texture Coordinates, and a Normal.
	*/
	struct VertexPTN : public VertexPT
	{
		/*!
		Creates a new instance of VertexPTN.
		*/
		ULTIMA_API VertexPTN()
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of VertexPTN.
		*/
		ULTIMA_API VertexPTN(
			const Vector3& position, 
			const Vector2& texture,
			const Vector3& normal)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of VertexPTN.
		*/
		ULTIMA_API VertexPTN(
			float positionX, float positionY, float positionZ,
			float textureU, float textureV,
			float normalX, float normalY, float normalZ)
			ULTIMA_NOEXCEPT;

		ULTIMA_API bool operator==(const VertexPTN& other)
			const ULTIMA_NOEXCEPT;

		XMFLOAT3 Normal;
	};
}