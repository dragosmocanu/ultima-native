#include "stdafx.h"

#include <algorithm>
#include <locale>

#include "Utilities.h"

namespace Ultima
{
	void Utilities::Split(
		const std::string &str, 
		char delimitator,
		std::vector<std::string>& elements) 
		ULTIMA_NOEXCEPT
	{
		elements.clear();

		std::stringstream ss(str);
		std::string item;

		while (std::getline(ss, item, delimitator))
			if (!item.empty() || delimitator != ' ')
				elements.push_back(item);
	}

	std::vector<std::string> Utilities::Split(
		const std::string &str, 
		char delimitator)
		ULTIMA_NOEXCEPT
	{
		std::vector<std::string> temp;
		Split(str, delimitator, temp);
		return temp;
	}

	std::string Utilities::Replace(
		const std::string& str,
		char replaced, char replacing)
		ULTIMA_NOEXCEPT
	{
		std::string ret = str;
		std::replace(ret.begin(), ret.end(), replaced, replacing);
		return ret;
	}

	std::string Utilities::Remove(
		const std::string& str, 
		char removed)
		ULTIMA_NOEXCEPT
	{
		std::string ret = str;
		ret.erase(std::remove(ret.begin(), ret.end(), removed), ret.end());
		return ret;
	}

	bool Utilities::IsUnsignedInt(const std::string& str) 
		ULTIMA_NOEXCEPT
	{
		bool ret = true;

		if (str.size() == 0)
			ret = false;
		else
		{
			const char* c = str.c_str();

			for (unsigned int i = 0; i < str.size(); i++)
			{
				if (c[i] < '0' || c[i] > '9')
				{
					ret = false;
					break;
				}
			}
		}

		return ret;
	}

	bool Utilities::FileExists(const std::string& str) 
		ULTIMA_NOEXCEPT
	{
		struct stat buffer;
		return (stat(str.c_str(), &buffer) == 0);
	}

	unsigned int Utilities::GetNumberOfMipLevels(
		unsigned int width,
		unsigned int height)
		ULTIMA_NOEXCEPT
	{
		unsigned int result = 1;

		while ((width > 1) || (height > 1))
		{
			width = max(width / 2, 1);
			height = max(height / 2, 1);

			result++;
		}

		return result;
	}

#if DIRECTX
	void Utilities::Release(IUnknown* object) 
		ULTIMA_NOEXCEPT
	{
		if (object)
		{
			object->Release();
			object = 0;
		}
	}

	void Utilities::Clear(
		ID3D11DeviceContext* const context,
		ID3D11RenderTargetView* const rtv,
		const Color& color)
		ULTIMA_NOEXCEPT
	{
		float f[4];
		f[0] = color.GetFloatR();
		f[1] = color.GetFloatG();
		f[2] = color.GetFloatB();
		f[3] = color.GetFloatA();

		context->ClearRenderTargetView(rtv, f);
	}

	void Utilities::Clear(
		ID3D11DeviceContext* const context,
		ID3D11DepthStencilView* const dsv,
		float f)
		ULTIMA_NOEXCEPT
	{
		context->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, f, 0);
	}

	ID3D11Buffer* Utilities::CreateBuffer(
		ID3D11Device* const device, 
		unsigned int size,
		D3D11_BIND_FLAG bufferType, 
		D3D11_USAGE usageType)
		ULTIMA_NOEXCEPT
	{
		ID3D11Buffer* buffer = nullptr;

		if ((size % 16) != 0)
			size += 16 - (size % 16);

		D3D11_BUFFER_DESC bufferDesc;
		SecureZeroMemory(&bufferDesc, sizeof(bufferDesc));
		bufferDesc.ByteWidth = size;
		bufferDesc.BindFlags = bufferType;
		bufferDesc.Usage = usageType;

		device->CreateBuffer(&bufferDesc, 0, &buffer);

		return buffer;
	}

	void Utilities::CreateColorTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11RenderTargetView** colorRTV,
		ID3D11ShaderResourceView** colorSRV)
		ULTIMA_NOEXCEPT
	{
		ID3D11Texture2D* color = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.Width = width;
		texDesc.Height = height;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &color));

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateRenderTargetView(color, &rtvDesc, colorRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = rtvDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(color, &srvDesc, colorSRV));

		Utilities::Release(color);
	}

	void Utilities::CreatePcfTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11DepthStencilView** pcfDSV,
		ID3D11ShaderResourceView** pcfSRV)
		ULTIMA_NOEXCEPT
	{
		ID3D11Texture2D* pcf = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		texDesc.Width = width;
		texDesc.Height = height;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &pcf));

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		SecureZeroMemory(&dsvDesc, sizeof(dsvDesc));
		dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateDepthStencilView(pcf, &dsvDesc, pcfDSV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(pcf, &srvDesc, pcfSRV));

		Utilities::Release(pcf);
	}

	void Utilities::CreateVsmTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11RenderTargetView** vsmRTV,
		ID3D11ShaderResourceView** vsmSRV)
		ULTIMA_NOEXCEPT
	{
		ID3D11Texture2D* vsm = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		texDesc.Width = width;
		texDesc.Height = height;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &vsm));

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateRenderTargetView(vsm, &rtvDesc, vsmRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(vsm, &srvDesc, vsmSRV));

		Utilities::Release(vsm);
	}

	void Utilities::CreateHdrTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11RenderTargetView** hdrRTV,
		ID3D11ShaderResourceView** hdrSRV)
		ULTIMA_NOEXCEPT
	{
		ID3D11Texture2D* hdr = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		texDesc.Width = width;
		texDesc.Height = height;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &hdr));

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateRenderTargetView(hdr, &rtvDesc, hdrRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(hdr, &srvDesc, hdrSRV));

		Utilities::Release(hdr);
	}

	void Utilities::CreateLuminanceTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11RenderTargetView** luminanceRTV,
		ID3D11ShaderResourceView** luminanceSRV,
		unsigned int& mipLevel)
		ULTIMA_NOEXCEPT
	{
		unsigned int mipLevels = Utilities::GetNumberOfMipLevels(width, height);
		mipLevel = mipLevels - 1;

		ID3D11Texture2D* luminanceTexture = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R32_FLOAT;
		texDesc.MipLevels = mipLevels;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		texDesc.Width = width;
		texDesc.Height = height;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &luminanceTexture));

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = texDesc.Format;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateRenderTargetView(luminanceTexture, &rtvDesc, luminanceRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(luminanceTexture, &srvDesc, luminanceSRV));

		Utilities::Release(luminanceTexture);
	}

	void Utilities::CreateAmbientOcclusionTexture(
		ID3D11Device* device,
		unsigned int width,
		unsigned int height,
		ID3D11RenderTargetView** aoRTV,
		ID3D11ShaderResourceView** aoSRV)
		ULTIMA_NOEXCEPT
	{
		ID3D11Texture2D* ao = nullptr;

		D3D11_TEXTURE2D_DESC texDesc;
		SecureZeroMemory(&texDesc, sizeof(texDesc));
		texDesc.Format = DXGI_FORMAT_R32_FLOAT;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.SampleDesc.Count = 1;
		texDesc.Width = width;
		texDesc.Height = height;
		texDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;

		Utilities::Assert(device->CreateTexture2D(&texDesc, nullptr, &ao));

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		SecureZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = DXGI_FORMAT_R32_FLOAT;
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;

		Utilities::Assert(
			device->CreateRenderTargetView(ao, &rtvDesc, aoRTV));

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		SecureZeroMemory(&srvDesc, sizeof(srvDesc));
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MipLevels = texDesc.MipLevels;

		Utilities::Assert(
			device->CreateShaderResourceView(ao, &srvDesc, aoSRV));

		Utilities::Release(ao);
	}

	bool Utilities::Assert(HRESULT hr)
		ULTIMA_NOEXCEPT
	{
		if SUCCEEDED(hr)
			return true;
		else
		{
#if defined DEBUG || defined _DEBUG
			_com_error err(hr);
			LPCSTR errMsg = err.ErrorMessage();
			OutputDebugStringA(errMsg);
#endif
			return false;
		}
	}

	void Utilities::CatchError(const std::string& errorMessage)
		ULTIMA_NOEXCEPT
	{
		OutputDebugStringA(errorMessage.c_str());
		return;
	}
#endif
}