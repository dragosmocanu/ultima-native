#pragma once

#include "Macros.h"

#include "Frustum.h"
#include "Matrix.h"

namespace Ultima
{
	/*!
	Defines an interface for standardized objects that can be used as a point of reference when rendering.
	*/
	class IViewable
	{
	public:

		/*!
		Gets the view matrix.
		*/
		ULTIMA_API virtual const Matrix& GetView() 
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Gets the projection matrix.
		*/
		ULTIMA_API virtual const Matrix& GetProjection()
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Gets the view-projection matrix.
		*/
		ULTIMA_API virtual const Matrix& GetViewProjection()
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Gets the frustum.
		*/
		ULTIMA_API virtual const Frustum& GetFrustum()
			const ULTIMA_NOEXCEPT = 0;

	};
}