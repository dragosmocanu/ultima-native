#pragma once

#include "Macros.h"

#include "ICollisionable.h"
#include "Matrix.h"

namespace Ultima
{
	/*!
	Defines a bounding frustum which can be checked for collision with other bounding volumes.
	*/
	class Frustum : public ICollisionable
	{
	public:
		/*!
		Creates a new instance of Frustum.
		*/
		ULTIMA_API Frustum()
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Frustum.
		*/
		ULTIMA_API Frustum(const Matrix& projection)
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~Frustum()
			ULTIMA_NOEXCEPT;

		/*!
		Returns true if the frustum is colliding with another collisionable volume.
		*/
		ULTIMA_API virtual bool IsColliding(const ICollisionable& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Transforms the frustum by a given projection, position and rotation.
		*/
		ULTIMA_API virtual void Transform(
			const Matrix& projection,
			const Vector3& position,
			const Matrix& rotation)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Gets the inner bounding frustum object.
		*/
		ULTIMA_API virtual const DirectX::BoundingFrustum& GetInnerFrustum() 
			const ULTIMA_NOEXCEPT;
#endif

	protected:
#if DIRECTX
		DirectX::BoundingFrustum frustum;
#endif
	};

}