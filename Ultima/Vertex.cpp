#include "stdafx.h"

#include "Vertex.h"

namespace Ultima
{
	Vertex::Vertex()
		ULTIMA_NOEXCEPT
		: VertexPTN()
	{
		Tangent = Vector3().ToXMFLOAT3();
		//Binormal = Vector3().ToXMFLOAT3();
	}

	Vertex::Vertex(
		const Vector3& position,
		const Vector2& texture,
		const Vector3& normal,
		const Vector3& tangent)
		ULTIMA_NOEXCEPT
		: VertexPTN(position, texture, normal)
	{
		Tangent = tangent.ToXMFLOAT3();
		//Binormal = binormal.ToXMFLOAT3();
	}

	Vertex::Vertex(
		float positionX, float positionY, float positionZ,
		float textureU, float textureV,
		float normalX, float normalY, float normalZ,
		float tangentX, float tangentY, float tangentZ)
		ULTIMA_NOEXCEPT
		: VertexPTN(positionX, positionY, positionZ,
		textureU, textureV,
		normalX, normalY, normalZ)
	{
		Tangent = XMFLOAT3(tangentX, tangentY, tangentZ);
		//Binormal = XMFLOAT3(binormalX, binormalY, binormalZ);
	}

	bool Vertex::operator==(const Vertex& other)
		const ULTIMA_NOEXCEPT
	{
		bool result = true;

		if (Position.x != other.Position.x)
			result = false;
		else if (Position.y != other.Position.y)
			result = false;
		else if (Position.z != other.Position.z)
			result = false;
		else if (Texture.x != other.Texture.x)
			result = false;
		else if (Texture.y != other.Texture.y)
			result = false;
		else if (Normal.x != other.Normal.x)
			result = false;
		else if (Normal.y != other.Normal.y)
			result = false;
		else if (Normal.z != other.Normal.z)
			result = false;
		else if (Tangent.x != other.Tangent.x)
			result = false;
		else if (Tangent.y != other.Tangent.y)
			result = false;
		else if (Tangent.z != other.Tangent.z)
			result = false;
		//else if (Binormal.x != other.Binormal.x)
		//	result = false;
		//else if (Binormal.y != other.Binormal.y)
		//	result = false;
		//else if (Binormal.z != other.Binormal.z)
		//	result = false;

		return result;
	}

}