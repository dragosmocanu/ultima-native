#include "stdafx.h"

#include "Color.h"

#include "MathExtensions.h"

namespace Ultima
{

	Color::Color()
		ULTIMA_NOEXCEPT
	{
		SetFloatR(0.5f);
		SetFloatG(0.5f);
		SetFloatB(0.5f);
		SetFloatA(0.5f);
	}

	Color::Color(const Color& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Color::Color(Color&& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Color::Color(float r, float g, float b, float a)
		ULTIMA_NOEXCEPT
	{
		SetFloatR(r);
		SetFloatG(g);
		SetFloatB(b);
		SetFloatA(a);
	}

	Color::Color(int r, int g, int b, int a)
		ULTIMA_NOEXCEPT
	{
		SetByteR(r);
		SetByteG(g);
		SetByteB(b);
		SetByteA(a);
	}

	Color::Color(const float* rgba)
		ULTIMA_NOEXCEPT
	{
		SetFloatR(rgba[0]);
		SetFloatG(rgba[1]);
		SetFloatB(rgba[2]);
		SetFloatA(rgba[3]);
	}

	Color::Color(const int* rgba)
		ULTIMA_NOEXCEPT
	{
		SetByteR(rgba[0]);
		SetByteG(rgba[1]);
		SetByteB(rgba[2]);
		SetByteA(rgba[3]);
	}

	Color::~Color() 
		ULTIMA_NOEXCEPT
	{ }
	
	float Color::GetFloatR() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return color.x;
#else
		return r;
#endif
	}

	float Color::GetFloatG() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return color.y;
#else
		return g;
#endif
	}

	float Color::GetFloatB() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return color.z;
#else
		return b;
#endif
	}

	float Color::GetFloatA() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return color.w;
#else
		return a;
#endif
	}

	char Color::GetByteR() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return (char)(color.x * 255);
#else
		return (char)(r * 255);
#endif
	}

	char Color::GetByteG() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return (char)(color.y * 255);
#else
		return (char)(g * 255);
#endif
	}

	char Color::GetByteB() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return (char)(color.z * 255);
#else
		return (char)(b * 255);
#endif
	}

	char Color::GetByteA() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return (char)(color.w * 255);
#else
		return (char)(a * 255);
#endif
	}

	void Color::SetFloatR(float r)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x = r;
#else
		this->r = r;
#endif
	}

	void Color::SetFloatG(float g)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.y = g;
#else
		this->g = g;
#endif
	}

	void Color::SetFloatB(float b)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.z = b;
#else
		this->b = b;
#endif
	}

	void Color::SetFloatA(float a)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.w = a;
#else
		this->a = a;
#endif
	}

	void Color::SetByteR(int r)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x = r / 255.0f;
#else
		this->r = r / 255.0f;
#endif
	}

	void Color::SetByteG(int g)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.y = g / 255.0f;
#else
		this->g = g / 255.0f;
#endif
	}

	void Color::SetByteB(int b)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.z = b / 255.0f;
#else
		this->b = b / 255.0f;
#endif
	}

	void Color::SetByteA(int a)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.w = a / 255.0f;
#else
		this->a = a / 255.0f;
#endif
	}

	void Color::Invert()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x = 1.0f - color.x;
		color.y = 1.0f - color.y;
		color.z = 1.0f - color.z;
#else
		r = 1.0f - r;
		g = 1.0f - g;
		b = 1.0f - b;
#endif
	}

	Color Color::Invert(const Color& color)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(color);
		temp.Invert();
		return temp;
	}

	void Color::Combine(const Color& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x = (color.x + other.GetFloatR()) / 2.0f;
		color.y = (color.y + other.GetFloatG()) / 2.0f;
		color.z = (color.z + other.GetFloatB()) / 2.0f;
#else
		r = (r + other.GetFloatR()) / 2.0f;
		g = (g + other.GetFloatG()) / 2.0f;
		b = (b + other.GetFloatB()) / 2.0f;
#endif
	}

	Color Color::Combine(const Color& c1, const Color& c2)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c1);
		temp.Combine(c2);
		return temp;
	}

	void Color::Saturate()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x = MathExtensions::Clamp(color.x, 0, 1);
		color.y = MathExtensions::Clamp(color.y, 0, 1);
		color.z = MathExtensions::Clamp(color.z, 0, 1);
		color.w = MathExtensions::Clamp(color.w, 0, 1);
#else
		r = MathExtensions::Clamp(r, 0, 1);
		g = MathExtensions::Clamp(g, 0, 1);
		b = MathExtensions::Clamp(b, 0, 1);
		a = MathExtensions::Clamp(a, 0, 1);
#endif
	}

	Color Color::Saturate(const Color& color)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(color);
		temp.Saturate();
		return temp;
	}

	void Color::Add(const Color& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x += other.GetFloatR();
		color.y += other.GetFloatG();
		color.z += other.GetFloatB();
#else
		r += other.GetFloatR();
		g += other.GetFloatG();
		b += other.GetFloatB();
#endif
		//Saturate();
	}

	Color Color::Add(const Color& c1, const Color& c2)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c2);
		temp.Add(c1);
		return temp;
	}

	void Color::Add(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x += scalar;
		color.y += scalar;
		color.z += scalar;
#else
		r += scalar;
		g += scalar;
		b += scalar;
#endif
		//Saturate();
	}

	Color Color::Add(const Color& c, float scalar)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c);
		temp.Add(scalar);
		return temp;
	}

	void Color::Subtract(const Color& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x -= other.GetFloatR();
		color.y -= other.GetFloatG();
		color.z -= other.GetFloatB();
#else
		r -= other.GetFloatR();
		g -= other.GetFloatG();
		b -= other.GetFloatB();
#endif
		//Saturate();
	}

	Color Color::Subtract(const Color& c1, const Color& c2)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c1);
		temp.Subtract(c2);
		return temp;
	}

	void Color::Subtract(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x -= scalar;
		color.y -= scalar;
		color.z -= scalar;
#else
		r -= scalar;
		g -= scalar;
		b -= scalar;
#endif
		//Saturate();
	}

	Color Color::Subtract(const Color& c, float scalar)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c);
		temp.Subtract(scalar);
		return temp;
	}

	void Color::Multiply(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x *= scalar;
		color.y *= scalar;
		color.z *= scalar;
#else
		r *= scalar;
		g *= scalar;
		b *= scalar;
#endif
		//Saturate();
	}

	Color Color::Multiply(const Color& c, float scalar)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c);
		temp.Multiply(scalar);
		return temp;
	}

	void Color::Multiply(const Color& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x *= other.GetFloatR();
		color.y *= other.GetFloatG();
		color.z *= other.GetFloatB();
#else
		r *= other.GetFloatR();
		g *= other.GetFloatG();
		b *= other.GetFloatB();
#endif
		//Saturate();
	}

	Color Color::Multiply(const Color& c1, const Color& c2)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c1);
		temp.Multiply(c2);
		return temp;
	}

	void Color::Divide(float scalar)
		ULTIMA_NOEXCEPT
	{
		scalar = 1.0f / scalar;
#if DIRECTX
		color.x *= scalar;
		color.y *= scalar;
		color.z *= scalar;
#else
		r *= scalar;
		g *= scalar;
		b *= scalar;
#endif
		//Saturate();
	}

	Color Color::Divide(const Color& q, float scalar)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(q);
		temp.Multiply(1.0f / scalar);
		return temp;
	}

	Color Color::Lerp(const Color& c1, const Color& c2, float val)
		ULTIMA_NOEXCEPT
	{
		Color ret = c1 * (1.0f - val) + c2 * val;
		return ret;
	}

	void Color::Divide(const Color& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		color.x /= other.GetFloatR();
		color.y /= other.GetFloatG();
		color.z /= other.GetFloatB();
#else
		r /= other.GetFloatR();
		g /= other.GetFloatG();
		b /= other.GetFloatB();
#endif
		//Saturate();
	}

	Color Color::Divide(const Color& c1, const Color& c2)
		ULTIMA_NOEXCEPT
	{
		Color temp = std::move(c1);
		temp.Divide(c2);
		return temp;
	}

	Color& Color::operator+=(const Color& other)
		ULTIMA_NOEXCEPT
	{
		this->Add(other);
		return *this;
	}

	Color Color::operator+(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Add(other);
		return c;
	}

	Color& Color::operator+=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Add(scalar);
		return *this;
	}

	Color Color::operator+(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Add(scalar);
		return c;
	}

	Color& Color::operator-=(const Color& other)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(other);
		return *this;
	}

	Color Color::operator-(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Subtract(other);
		return c;
	}

	Color& Color::operator-=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(scalar);
		return *this;
	}

	Color Color::operator-(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Subtract(scalar);
		return c;
	}

	Color Color::operator-() 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Invert();
		return c;
	}

	Color& Color::operator*=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(scalar);
		return *this;
	}

	Color Color::operator*(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Multiply(scalar);
		return c;
	}

	Color& Color::operator*=(const Color& other)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(other);
		return *this;
	}

	Color Color::operator*(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Multiply(other);
		return c;
	}

	Color& Color::operator/=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(1.0f / scalar);
		return *this;
	}

	Color Color::operator/(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Multiply(1.0f / scalar);
		return c;
	}

	Color& Color::operator/=(const Color& other)
		ULTIMA_NOEXCEPT
	{
		this->Divide(other);
		return *this;
	}

	Color Color::operator/(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
		Color c = std::move(*this);
		c.Divide(other);
		return c;
	}

	Color& Color::operator=(const Color& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			this->color = std::move(other.ToXMFLOAT4());
#else
			this->r = other.GetFloatR();
			this->g = other.GetFloatG();
			this->b = other.GetFloatB();
			this->a = other.GetFloatA();
#endif
		}
		return *this;
	}

	Color& Color::operator=(Color&& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			color = std::move(other.ToXMFLOAT4());
			other.SetXMFLOAT4(XMFLOAT4());
#else
			r = other.GetFloatR();
			g = other.GetFloatG();
			b = other.GetFloatB();
			a = other.GetFloatA();
			other.SetFloatR(0);
			other.SetFloatG(0);
			other.SetFloatB(0);
			other.SetFloatA(0);
#endif
		}
		return *this;
	}

	bool Color::operator==(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (color.x != other.GetFloatR())
			return false;
		else if (color.y != other.GetFloatG())
			return false;
		else if (color.z != other.GetFloatB())
			return false;
		else if (color.w != other.GetFloatA())
			return false;
#else
		if (r != other.GetFloatR())
			return false;
		else if (g != other.GetFloatG())
			return false;
		else if (b != other.GetFloatB())
			return false;
		else if (a != other.GetFloatA())
			return false;
#endif
		else
			return true;
	}

	bool Color::operator!=(const Color& other) 
		const ULTIMA_NOEXCEPT
	{
		return !(this == &other);
	}

	const Color Color::White = Color(255, 255, 255);

	const Color Color::Black = Color(0, 0, 0);

	const Color Color::Transparent = Color(193, 13, 10, 0);

	const Color Color::SpectralRed = Color(193, 13, 10);

	const Color Color::WindowsBlue = Color(0, 204, 255);

	const Color Color::XboxGreen = Color(16, 124, 16);

	const Color Color::WindowsPhoneViolet = Color(104, 33, 122);

	const Color Color::AtomicTangerine = Color(255, 164, 116);

	const Color Color::BurntSienna = Color(234, 126, 93);

	const Color Color::Cerulean = Color(29, 172, 214);

	const Color Color::DesertSand = Color(239, 205, 184);

	const Color Color::ElectricLime = Color(206, 255, 29);

	const Color Color::FieryOrange = Color(254, 179, 0);

#if DIRECTX
	void Color::SetXMFLOAT4(const XMFLOAT4& xm)
		ULTIMA_NOEXCEPT
	{
		color = std::move(xm);
	}

	XMFLOAT4 Color::ToXMFLOAT4() 
		const ULTIMA_NOEXCEPT
	{
		return color;
	}

	XMVECTOR Color::ToXMVECTOR() 
		const ULTIMA_NOEXCEPT
	{
		return XMLoadFloat4(&color);
	}

	Color::Color(const XMFLOAT4& f)
		ULTIMA_NOEXCEPT
	{
		color = f;
	}

	Color::Color(const XMVECTOR& v)
		ULTIMA_NOEXCEPT
	{
		XMStoreFloat4(&color, v);
	}
#endif
}