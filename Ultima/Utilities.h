#pragma once

#include "Macros.h"

#include "Color.h"

namespace Ultima
{
	/*!
	Class with utility standalone functions.
	*/
	class Utilities
	{
	public:
		/*!
		Splits an std::string by a char and puts the result in the elements argument.
		*/
		ULTIMA_API static void Split(
			const std::string &str, 
			char delim,
			std::vector<std::string>& elements) 
			ULTIMA_NOEXCEPT;

		/*!
		Splits an std::string by a char and returns an std::vector<std::string> containing the result.
		*/
		ULTIMA_API static std::vector<std::string> Split(
			const std::string& str,
			char delimitator) 
			ULTIMA_NOEXCEPT;

		/*!
		Replaces every occurence of a given char with another char in a std::string.
		*/
		ULTIMA_API static std::string Replace(
			const std::string& str,
			char replaced, char replacing) 
			ULTIMA_NOEXCEPT;

		/*!
		Removes every occurence of a given char in a std::string.
		*/
		ULTIMA_API static std::string Remove(
			const std::string& str, 
			char removed) 
			ULTIMA_NOEXCEPT;

		/*!
		Checks if the given string is an unsigned integer or not.
		*/
		ULTIMA_API static bool IsUnsignedInt(const std::string& str) 
			ULTIMA_NOEXCEPT;

		/*!
		Checks if the given file name exists.
		*/
		ULTIMA_API static bool FileExists(const std::string& str) 
			ULTIMA_NOEXCEPT;

		/*!
		Returns the number of required mip levels for a full mip chain.
		*/
		ULTIMA_API static unsigned int GetNumberOfMipLevels(
			unsigned int width,
			unsigned int height)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Releases a COM object.
		*/
		ULTIMA_API static void Release(IUnknown* object) 
			ULTIMA_NOEXCEPT;

		/*!
		Clears an ID3D11RenderTargetView*.
		*/
		ULTIMA_API static void Clear(
			ID3D11DeviceContext* const context,
			ID3D11RenderTargetView* const rtv,
			const Color& color = Color::Black)
			ULTIMA_NOEXCEPT;

		/*!
		Clears an ID3D11DepthStencilView* of depth.
		*/
		ULTIMA_API static void Clear(
			ID3D11DeviceContext* const context,
			ID3D11DepthStencilView* const dsv,
			float depth = 1.0f)
			ULTIMA_NOEXCEPT;

		/*!
		Creates an ID3D11Buffer*.
		*/
		ULTIMA_API static ID3D11Buffer* CreateBuffer(
			ID3D11Device* const device, 
			unsigned int size,
			D3D11_BIND_FLAG bufferType = D3D11_BIND_CONSTANT_BUFFER,
			D3D11_USAGE = D3D11_USAGE_DEFAULT) 
			ULTIMA_NOEXCEPT;

		/*!
		Creates a color RenderTargetView* and ShaderResourceView*.
		The format will be R8G8B8A8.
		*/
		ULTIMA_API static void CreateColorTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11RenderTargetView** outRTV,
			ID3D11ShaderResourceView** outSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a PCF DepthStencilView* and ShaderResourceView*. 
		*/
		ULTIMA_API static void CreatePcfTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11DepthStencilView** outDSV,
			ID3D11ShaderResourceView** outSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a VSM RenderTargetView* and ShaderResourceView*.
		Can be used by the VSM, EVSM and EVSM4 methods.
		*/
		ULTIMA_API static void CreateVsmTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11RenderTargetView** outRTV,
			ID3D11ShaderResourceView** outSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Creates an HDR RenderTargetView* and ShaderResourceView*.
		Can be also used to write positions, normals, and other data that fits into an R32G32B32A32 format.
		*/
		ULTIMA_API static void CreateHdrTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11RenderTargetView** outRTV,
			ID3D11ShaderResourceView** outSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a luminance RenderTargetView* and ShaderResourceView*.
		*/
		ULTIMA_API static void CreateLuminanceTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11RenderTargetView** outRTV,
			ID3D11ShaderResourceView** ourSRV,
			unsigned int& outLastMipLevel)
			ULTIMA_NOEXCEPT;

		/*!
		Creates an ambient occlusion RenderTargetView* and ShaderResourceView*.
		*/
		ULTIMA_API static void CreateAmbientOcclusionTexture(
			ID3D11Device* device,
			unsigned int width,
			unsigned int height,
			ID3D11RenderTargetView** outRTV,
			ID3D11ShaderResourceView** outSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Asserts a HRESULT object. Returns true if everything is ok, false otherwise.
		*/
		ULTIMA_API static bool Assert(HRESULT hr)
			ULTIMA_NOEXCEPT;
#endif

		/*!
		Catches an error thrown by a component, and it gives info on what went wrong.
		*/
		ULTIMA_API static void CatchError(const std::string& errorMessage)
			ULTIMA_NOEXCEPT;
	};
}