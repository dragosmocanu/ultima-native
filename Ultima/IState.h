#pragma once

#include "Macros.h"

//#include "StateManager.h"

namespace Ultima
{
	class StateManager;

	/*!
	Defines an interface for a state in the application.
	*/
	class IState
	{
	public:
		/*!
		Updates the logic of the state.
		*/
		ULTIMA_API virtual void Update(float deltaTime, StateManager& stateManager) 
			ULTIMA_NOEXCEPT = 0;
	};
}