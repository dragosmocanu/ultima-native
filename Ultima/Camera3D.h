#pragma once

#include "Macros.h"

#include "Frustum.h"
#include "IManipulable3D.h"
#include "IViewable.h"
#include "Matrix.h"
#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines an abstract class for a 3D camera.
	*/
	class Camera3D : public IManipulable3D, public IViewable
	{
	public:

		/*!
		Gets the position of the camera.
		*/
		ULTIMA_API virtual const Vector3& GetPosition()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the X coordinate of the camera.
		*/
		ULTIMA_API virtual float GetX()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Y coordinate of the camera.
		*/
		ULTIMA_API virtual float GetY()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Z coordinate of the camera.
		*/
		ULTIMA_API virtual float GetZ() 
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the camera.
		*/
		ULTIMA_API virtual void SetPosition(const Vector3& position)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the rotation of the camera.
		*/
		ULTIMA_API virtual void SetRotation(const Vector3& rotation)
			ULTIMA_NOEXCEPT = 0;

		/*!
		Moves the camera by a given Vector3.
		*/
		ULTIMA_API virtual void Move(const Vector3& moveVector)
			ULTIMA_NOEXCEPT = 0;

		/*!
		Rotates the camera by a given Vector3.
		*/
		ULTIMA_API virtual void Rotate(const Vector3& rotateVector) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Gets the view matrix of the camera.
		*/
		ULTIMA_API virtual const Matrix& GetView() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the projection matrix of the camera.
		*/
		ULTIMA_API virtual const Matrix& GetProjection() 
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the view-projection matrix of the camera.
		*/
		ULTIMA_API virtual const Matrix& GetViewProjection() 
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Sets the X coordinate of the camera.
		*/
		ULTIMA_API virtual void SetX(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate of the camera.
		*/
		ULTIMA_API virtual void SetY(float y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Z coordinate of the camera.
		*/
		ULTIMA_API virtual void SetZ(float z)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the bounding frustum of the camera.
		*/
		ULTIMA_API virtual const Frustum& GetFrustum() 
			const ULTIMA_NOEXCEPT;

		/*!
		Reinitializes the camera with a different aspect ratio and view distance.
		*/
		ULTIMA_API virtual void Reinitialize(
			float aspectRatio, 
			float viewDistance = 10000.0f)
			ULTIMA_NOEXCEPT;

	protected:
		Vector3 position;

		Matrix view;
		Matrix projection;

		Frustum frustum;
	};
}