#pragma once

#include "Macros.h"

#include "Vector3.h"

namespace Ultima
{
	/*!
	Defines an interface for standardized object manipulation in a 3D environment.
	*/
	class IManipulable3D
	{
	public:

		/*!
		Moves the object by a given Vector3.
		*/
		ULTIMA_API virtual void Move(const Vector3& moveVector) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Rotates the object by a given Vector3.
		Values represent radians, and the rotation is clock-wise.
		*/
		ULTIMA_API virtual void Rotate(const Vector3& rotateVector) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Sets the object's position.
		*/
		ULTIMA_API virtual void SetPosition(const Vector3& position) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Sets the object's rotation.
		Values represent radians, and the rotation is clock-wise.
		*/
		ULTIMA_API virtual void SetRotation(const Vector3& rotation) 
			ULTIMA_NOEXCEPT = 0;

	};
}