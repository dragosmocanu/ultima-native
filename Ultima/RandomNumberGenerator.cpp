#include "stdafx.h"

#include "RandomNumberGenerator.h"

#include <random>
#include <time.h>

namespace Ultima
{

	RandomNumberGenerator& RandomNumberGenerator::GetInstance()
		ULTIMA_NOEXCEPT
	{
		static RandomNumberGenerator instance;
		return instance;
	}

	RandomNumberGenerator::RandomNumberGenerator()
		ULTIMA_NOEXCEPT
	{
		std::random_device rd;
		generator = std::mt19937(rd());
	}

	int RandomNumberGenerator::Generate(int max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_int_distribution<int> dist 
			= std::uniform_int_distribution<int>(0, max - 1);
		return dist(generator);
	}

	int RandomNumberGenerator::GenerateInclusive(int max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_int_distribution<int> dist 
			= std::uniform_int_distribution<int>(0, max);
		return dist(generator);
	}

	int RandomNumberGenerator::Generate(int min, int max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_int_distribution<int> dist 
			= std::uniform_int_distribution<int>(min, max - 1);
		return dist(generator);
	}

	int RandomNumberGenerator::GenerateInclusive(int min, int max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_int_distribution<int> dist 
			= std::uniform_int_distribution<int>(min, max);
		return dist(generator);
	}

	float RandomNumberGenerator::Generate(float min, float max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_real_distribution<float> dist 
			= std::uniform_real_distribution<float>(min, max - FLT_EPSILON);
		return dist(generator);
	}

	float RandomNumberGenerator::GenerateInclusive(float min, float max)
		ULTIMA_NOEXCEPT
	{
		std::uniform_real_distribution<float> dist 
			= std::uniform_real_distribution<float>(min, max);
		return dist(generator);
	}

	double RandomNumberGenerator::Generate()
		ULTIMA_NOEXCEPT
	{
		std::uniform_real_distribution<double> dist 
			= std::uniform_real_distribution<double>(0, 1 - DBL_EPSILON);
		return dist(generator);
	}
	
	double RandomNumberGenerator::GenerateInclusive()
		ULTIMA_NOEXCEPT
	{
		std::uniform_real_distribution<double> dist 
			= std::uniform_real_distribution<double>(0, 1);
		return dist(generator);
	}
}