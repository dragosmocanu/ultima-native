#include "stdafx.h"

#include "Timer.h"

#include <float.h>

#if DIRECTX
#include<Windows.h>
#endif

namespace Ultima
{
	Timer::Timer()
		ULTIMA_NOEXCEPT
	{
		totalTime = 0;
		deltaTime = 0;

#if DIRECTX
		unsigned long long countsPerSecond;
		QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSecond);
		secondsPerCount = 1.0f / static_cast<float>(countsPerSecond);
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
#endif
	}

	Timer::~Timer() 
		ULTIMA_NOEXCEPT
	{ }

	float Timer::GetDeltaTime() 
		const ULTIMA_NOEXCEPT
	{
		return deltaTime;
	}

	unsigned long long Timer::GetTotalMilliseconds() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return static_cast<unsigned long long>(totalTime 
			* static_cast<float>(secondsPerCount)
			* 1000.0f);
#else

#endif
	}

	unsigned long long Timer::GetTotalMicroseconds()
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return static_cast<unsigned long long>(totalTime
		* static_cast<float>(secondsPerCount)
		* 1000000.0f);
#else

#endif
	}

	void Timer::Update()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		unsigned long long time;
		QueryPerformanceCounter((LARGE_INTEGER*)&time);
		time -= startTime;
		deltaTime = (time - totalTime) * static_cast<float>(secondsPerCount);
		deltaTime *= 1000.0f;
		totalTime = time;
#else

#endif

		if (deltaTime <= 0)
			deltaTime = FLT_EPSILON;
	}

	void Timer::Restart()
		ULTIMA_NOEXCEPT
	{
		totalTime = 0;
		deltaTime = 0;

#if DIRECTX
		unsigned long long countsPerSecond;
		QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSecond);
		secondsPerCount = 1.0f / static_cast<float>(countsPerSecond);
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);
#else

#endif
	}

}