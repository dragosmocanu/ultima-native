#include "stdafx.h"

#include "Sphere.h"

#include "Box.h"
#include "Frustum.h"

namespace Ultima
{
	Sphere::Sphere() 
		ULTIMA_NOEXCEPT
		: sphere(Vector3::Zero.ToXMFLOAT3(), 0.0f)
	{ }

	Sphere::Sphere(
		const Vector3& center,
		float radius,
		const Vector3& position) 
		ULTIMA_NOEXCEPT
		: sphere((position + center).ToXMFLOAT3(), radius),
		center(center)
	{ }

	Sphere::~Sphere()
		ULTIMA_NOEXCEPT
	{ }

	bool Sphere::IsColliding(const ICollisionable& other)
		const ULTIMA_NOEXCEPT
	{
		const Box* const b = dynamic_cast<const Box* const>(&other);
		if (b != nullptr)
			return sphere.Intersects(b->GetInnerBox());

		const Sphere* const s = dynamic_cast<const Sphere* const>(&other);
		if (s != nullptr)
			return sphere.Intersects(s->GetInnerSphere());

		const Frustum* const f = dynamic_cast<const Frustum* const>(&other);
		if (f != nullptr)
			return sphere.Intersects(f->GetInnerFrustum());

		return false;
	}

	void Sphere::SetPosition(const Vector3& position) 
		ULTIMA_NOEXCEPT
	{
		sphere.Center = (position + center).ToXMFLOAT3();
	}

	float Sphere::GetRadius()
		const ULTIMA_NOEXCEPT
	{
		return sphere.Radius;
	}

	void Sphere::SetRadius(float radius)
		ULTIMA_NOEXCEPT
	{
		sphere = DirectX::BoundingSphere((Vector3(sphere.Center) + center).ToXMFLOAT3(), radius);
	}

#if DIRECTX
	const DirectX::BoundingSphere& Sphere::GetInnerSphere() 
		const ULTIMA_NOEXCEPT
	{
		return sphere;
	}
#endif
}