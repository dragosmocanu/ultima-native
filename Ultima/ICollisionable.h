#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines an interface for collisionable volumes.
	*/
	class ICollisionable
	{
	public:
		/*!
		Returns true if the collisionable volume is colliding with another.
		*/
		ULTIMA_API virtual bool IsColliding(const ICollisionable& other)
			const ULTIMA_NOEXCEPT = 0;
	};
}