#include "stdafx.h"

#include "RenderableStateManager.h"

#include "IRenderableState.h"

namespace Ultima
{
	RenderableStateManager::RenderableStateManager()
		ULTIMA_NOEXCEPT
	{
		stateIndex = 0;
	}

	RenderableStateManager::~RenderableStateManager()
		ULTIMA_NOEXCEPT
	{ }

	void RenderableStateManager::Render(ID3D11DeviceContext* const context)
		ULTIMA_NOEXCEPT
	{
		((IRenderableState*)states[stateIndex])->Render(context, *this);
	}
}