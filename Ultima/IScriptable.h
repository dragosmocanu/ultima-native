#pragma once

#include "stdafx.h"

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines an interface for standardized interaction with external script files.
	*/
	class IScriptable
	{
	public:

		/*!
		Gets a global string variable.
		*/
		ULTIMA_API virtual std::string GetGlobalString(const std::string& variableName)
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Sets a global string variable.
		*/
		ULTIMA_API virtual void SetGlobalString(
			const std::string& variableName,
			const std::string& value) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Gets a global number variable.
		*/
		ULTIMA_API virtual double GetGlobalNumber(const std::string& variableName)
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Sets a global number variable
		*/
		ULTIMA_API virtual void SetGlobalNumber(
			const std::string& variableName,
			double value) 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Gets a global bool variable.
		*/
		ULTIMA_API virtual bool GetGlobalBool(const std::string& variableName)
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Sets a global bool value.
		*/
		ULTIMA_API virtual void SetGlobalBool(
			const std::string& variableName,
			bool value)
			ULTIMA_NOEXCEPT = 0;

		/*!
		Executes a function with no arguments.
		*/
		ULTIMA_API virtual void ExecuteFunction(const std::string& functionName)
			ULTIMA_NOEXCEPT = 0;

		/*!
		Parses and executes a given string.
		*/
		ULTIMA_API virtual void Parse(const std::string& text)
			ULTIMA_NOEXCEPT = 0;
	};
}