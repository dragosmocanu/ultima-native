#pragma once

#include "Macros.h"

#include <random>

namespace Ultima
{
	/*!
	[THREADSAFE] Fast and precise random number generator.
	*/
	class RandomNumberGenerator
	{
	public:
		/*!
		Gets the single instance of RandomNumberGenerator.
		*/
		ULTIMA_API static RandomNumberGenerator& GetInstance()
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [0, max) interval.
		*/
		ULTIMA_API int Generate(int max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [0, max] interval.
		*/
		ULTIMA_API int GenerateInclusive(int max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [min, max) interval.
		*/
		ULTIMA_API int Generate(int min, int max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [min, max] interval.
		*/
		ULTIMA_API int GenerateInclusive(int min, int max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [min, max) interval.
		*/
		ULTIMA_API float Generate(float min, float max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [min, max] interval.
		*/
		ULTIMA_API float GenerateInclusive(float min, float max)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [0, 1) interval.
		*/
		ULTIMA_API double Generate()
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random number in the [0, 1] interval.
		*/
		ULTIMA_API double GenerateInclusive()
			ULTIMA_NOEXCEPT;

	protected:
		RandomNumberGenerator()
			ULTIMA_NOEXCEPT;

		std::mt19937 generator;
	};
}