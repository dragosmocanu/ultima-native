#include "stdafx.h"

#include "Camera3D.h"

namespace Ultima
{

	const Vector3& Camera3D::GetPosition() 
		const ULTIMA_NOEXCEPT
	{
		return position;
	}

	float Camera3D::GetX() 
		const ULTIMA_NOEXCEPT
	{
		return position.GetX();
	}

	float Camera3D::GetY() 
		const ULTIMA_NOEXCEPT
	{
		return position.GetY();
	}

	float Camera3D::GetZ() 
		const ULTIMA_NOEXCEPT
	{
		return position.GetZ();
	}

	const Matrix& Camera3D::GetView() 
		const ULTIMA_NOEXCEPT
	{
		return view;
	}

	const Matrix& Camera3D::GetProjection() 
		const ULTIMA_NOEXCEPT
	{
		return projection;
	}

	void Camera3D::SetPosition(const Vector3& position) 
		ULTIMA_NOEXCEPT
	{
		this->position = position;
	}

	void Camera3D::SetX(float x) 
		ULTIMA_NOEXCEPT
	{
		position.SetX(x);
	}

	void Camera3D::SetY(float y) 
		ULTIMA_NOEXCEPT
	{
		position.SetY(y);
	}

	void Camera3D::SetZ(float z) 
		ULTIMA_NOEXCEPT
	{
		position.SetZ(z);
	}

	void Camera3D::Reinitialize(float aspectRatio, float viewDistance) 
		ULTIMA_NOEXCEPT
	{
		projection = Matrix(0.78539f, aspectRatio,
			1.0f, viewDistance);

		frustum = Frustum(projection);
	}

	const Frustum& Camera3D::GetFrustum() 
		const ULTIMA_NOEXCEPT
	{
		return frustum;
	}
}