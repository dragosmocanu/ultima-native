#pragma once

#include "stdafx.h"

#include "Macros.h"

namespace Ultima
{

	/*!
	Defines a four component color using red, green, blue and alpha channels.
	*/
	class Color
	{
	public:
		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color()
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of Color.
		*/
		ULTIMA_API Color(const Color& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Color using a move.
		*/
		ULTIMA_API Color(Color&& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(float r, float g, float b, float a = 1.0f)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(int r, int g, int b, int a = 255)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(const float* rgba)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(const int* rgba)
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~Color()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the red component as a float.
		*/
		ULTIMA_API virtual float GetFloatR() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the green component as a float.
		*/
		ULTIMA_API virtual float GetFloatG() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the blue component as a float.
		*/
		ULTIMA_API virtual float GetFloatB() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the alpha component as a float.
		*/
		ULTIMA_API virtual float GetFloatA() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the red component as a char.
		*/
		ULTIMA_API virtual char GetByteR() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the green component as a char.
		*/
		ULTIMA_API virtual char GetByteG() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the blue component as a char.
		*/
		ULTIMA_API virtual char GetByteB() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the alpha component as a char.
		*/
		ULTIMA_API virtual char GetByteA() 
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the red component as a float.
		*/
		ULTIMA_API virtual void SetFloatR(float r)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the green component as a float.
		*/
		ULTIMA_API virtual void SetFloatG(float g)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the blue component as a float.
		*/
		ULTIMA_API virtual void SetFloatB(float b)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the alpha component as a float.
		*/
		ULTIMA_API virtual void SetFloatA(float a)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the red component as a char.
		*/
		ULTIMA_API virtual void SetByteR(int r)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the green component as a char.
		*/
		ULTIMA_API virtual void SetByteG(int g)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the blue component as a char.
		*/
		ULTIMA_API virtual void SetByteB(int b)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the alpha component as a char.
		*/
		ULTIMA_API virtual void SetByteA(int a)
			ULTIMA_NOEXCEPT;

		/*!
		Inverts the color.
		*/
		ULTIMA_API virtual void Invert()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the inverse of a color.
		*/
		ULTIMA_API static Color Invert(const Color& color)
			ULTIMA_NOEXCEPT;

		/*!
		Combines the color with another.
		*/
		ULTIMA_API virtual void Combine(const Color& otherColor)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a combined color computed from two given ones.
		*/
		ULTIMA_API static Color Combine(const Color& c1, const Color& c2)
			ULTIMA_NOEXCEPT;

		/*!
		Saturates the color. (clamps its channel to [0, 1] range).
		*/
		ULTIMA_API virtual void Saturate()
			ULTIMA_NOEXCEPT;

		/*!
		Returns a saturated copy.
		*/
		ULTIMA_API static Color Saturate(const Color& color)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two colors together.
		*/
		ULTIMA_API virtual void Add(const Color& other)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two colors together.
		*/
		ULTIMA_API static Color Add(const Color& c1, const Color& c2)
			ULTIMA_NOEXCEPT;

		/*!
		Adds a scalar to the color.
		*/
		ULTIMA_API virtual void Add(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Adds a scalar to a color.
		*/
		ULTIMA_API static Color Add(const Color& c, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a color from another.
		*/
		ULTIMA_API virtual void Subtract(const Color& other)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a color from another.
		*/
		ULTIMA_API static Color Subtract(const Color& c1, const Color& c2)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a scalar from the color.
		*/
		ULTIMA_API virtual void Subtract(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a scalar from a color.
		*/
		ULTIMA_API static Color Subtract(const Color& c, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a color by a scalar.
		*/
		ULTIMA_API virtual void Multiply(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a color by a scalar.
		*/
		ULTIMA_API static Color Multiply(const Color& c, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a color by another color.
		*/
		ULTIMA_API virtual void Multiply(const Color& other)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a color by another color.
		*/
		ULTIMA_API static Color Multiply(const Color& c1, const Color& c2)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a color by a scalar.
		*/
		ULTIMA_API virtual void Divide(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a color by a scalar.
		*/
		ULTIMA_API static Color Divide(const Color& c, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a color by another color.
		*/
		ULTIMA_API virtual void Divide(const Color& other)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a color by another color.
		*/
		ULTIMA_API static Color Divide(const Color& c1, const Color& c2)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two vectors.
		*/
		ULTIMA_API static Color Lerp(const Color& c1, const Color& c2, float val)
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual Color& operator+=(const Color& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator+(const Color& other) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator+=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator+(float scalar) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator-=(const Color& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator-(const Color& other) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator-=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator-(float scalar) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator-() 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator*=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator*(float scalar) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator*=(const Color& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator*(const Color& other) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator/=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator/(float scalar) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator/=(const Color& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color operator/(const Color& other) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator=(const Color& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual Color& operator=(Color&& other)
			ULTIMA_NOEXCEPT;
		ULTIMA_API virtual bool operator==(const Color& other) 
			const ULTIMA_NOEXCEPT;
		ULTIMA_API virtual bool operator!=(const Color& other) 
			const ULTIMA_NOEXCEPT;

		/*!
		The color white.
		*/
		ULTIMA_API static const Color White;

		/*!
		The color black.
		*/
		ULTIMA_API static const Color Black;

		/*!
		Transparent color.
		*/
		ULTIMA_API static const Color Transparent;

		/*!
		The shade of red used in the Spectral Creations logo.
		*/
		ULTIMA_API static const Color SpectralRed;

		/*!
		The shade of blue used in the Windows logo.
		*/
		ULTIMA_API static const Color WindowsBlue;

		/*!
		The shade of green used in the Xbox logo.
		*/
		ULTIMA_API static const Color XboxGreen;

		/*!
		The shade of violet used in the Windows Phone logo.
		*/
		ULTIMA_API static const Color WindowsPhoneViolet;

		/*!
		The Atomic Tangerine color.
		*/
		ULTIMA_API static const Color AtomicTangerine;

		/*!
		The Burnt Sienna color.
		*/
		ULTIMA_API static const Color BurntSienna;

		/*!
		The Cerulean color.
		*/
		ULTIMA_API static const Color Cerulean;

		/*!
		The Desert Sand color.
		*/
		ULTIMA_API static const Color DesertSand;

		/*!
		The Electric Lime color.
		*/
		ULTIMA_API static const Color ElectricLime;

		/*!
		The Fiery Orange color.
		*/
		ULTIMA_API static const Color FieryOrange;

#if DIRECTX
		/*!
		Loads color data from a given XMFLOAT4.
		*/
		ULTIMA_API void SetXMFLOAT4(const XMFLOAT4& xm)
			ULTIMA_NOEXCEPT;

		/*!
		Returns an XMFLOAT4 holding the color data.
		*/
		ULTIMA_API virtual XMFLOAT4 ToXMFLOAT4() 
			const ULTIMA_NOEXCEPT;
		
		/*!
		Returns an XMVECTOR holding the color data.
		*/
		ULTIMA_API virtual XMVECTOR ToXMVECTOR() 
			const ULTIMA_NOEXCEPT;
		
		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(const XMFLOAT4& f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Creates a new instance of Color.
		*/
		ULTIMA_API Color(const XMVECTOR& v)
			ULTIMA_NOEXCEPT;
		
#endif

	protected:
#if DIRECTX
		XMFLOAT4 color;
#else
		float r;
		float g;
		float b;
		float a;
#endif
	};
}