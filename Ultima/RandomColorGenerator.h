#pragma once

#include "Macros.h"

#include "RandomNumberGenerator.h"
#include "Color.h"

namespace Ultima
{
	/*!
	Randomly generates a color between a specified Color interval.
	*/
	class RandomColorGenerator
	{
	public:
		/*!
		Creates a new instance of RandomColorGenerator.
		*/
		ULTIMA_API RandomColorGenerator(
			const Color& min = Color::Black, 
			const Color& max = Color::White,
			const Color& color = Color::SpectralRed, 
			float speed = 0.01f)
			ULTIMA_NOEXCEPT;
		
		ULTIMA_API virtual ~RandomColorGenerator()
			ULTIMA_NOEXCEPT;

		/*!
		Sets the speed of the generator.
		*/
		ULTIMA_API virtual void SetSpeed(float speed)
			ULTIMA_NOEXCEPT;

		/*!
		Generates a new random color.
		*/
		ULTIMA_API virtual Color Generate(float deltaTime)
			ULTIMA_NOEXCEPT;

	protected:
		Color min;
		Color max;
		Color color;
		float speed;

		float time;
	};
}