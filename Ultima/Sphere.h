#pragma once

#include "Macros.h"

#include "ICollisionable.h"
#include "Vector3.h"

namespace Ultima
{
	class Box;
	class Frustum;

	/*!
	Defines a bounding sphere which can be checked for collision with other bounding volumes.
	*/
	class Sphere : public ICollisionable
	{
	public:
		/*!
		Creates a new instance of Sphere.
		*/
		ULTIMA_API Sphere() 
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Sphere.
		*/
		ULTIMA_API Sphere(
			const Vector3& center, ///Local space, relative to the model.
			float radius, ///The radius of the sphere.
			const Vector3& position = Vector3::Zero ///The position of the sphere.
			) 
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~Sphere()
			ULTIMA_NOEXCEPT;

		/*!
		Returns true if the frustum is colliding with another collisionable volume.
		*/
		ULTIMA_API virtual bool IsColliding(const ICollisionable& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the position of the sphere in the world.
		*/
		ULTIMA_API virtual void SetPosition(const Vector3& position) 
			ULTIMA_NOEXCEPT;
		
		/*!
		Gets the radius of the sphere.
		*/
		ULTIMA_API virtual float GetRadius()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the radius of the sphere.
		*/
		ULTIMA_API virtual void SetRadius(float radius)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Gets the inner bounding sphere object.
		*/
		ULTIMA_API const DirectX::BoundingSphere& GetInnerSphere() 
			const ULTIMA_NOEXCEPT;
#endif

	protected:
		Vector3 center;
		DirectX::BoundingSphere sphere;
	};
}