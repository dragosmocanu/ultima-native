#include "stdafx.h"

#include "RandomColorGenerator.h"

#include "MathExtensions.h"

namespace Ultima
{
	
	RandomColorGenerator::RandomColorGenerator(
		const Color& min, 
		const Color& max, 
		const Color& color, 
		float speed)
		ULTIMA_NOEXCEPT
	{
		this->min = min;
		this->max = max;
		this->color = color;
		this->speed = speed;

		time = 0.0f;
	}

	RandomColorGenerator::~RandomColorGenerator()
		ULTIMA_NOEXCEPT
	{ }

	void RandomColorGenerator::SetSpeed(float speed)
		ULTIMA_NOEXCEPT
	{
		this->speed = speed;
	}

	Color RandomColorGenerator::Generate(float deltaTime)
		ULTIMA_NOEXCEPT
	{
		time += deltaTime;

		while (time > 16.666f)
		{
			color.SetFloatR(color.GetFloatR()
				+ RandomNumberGenerator::GetInstance().Generate(-speed, +speed));
			color.SetFloatG(color.GetFloatG()
				+ RandomNumberGenerator::GetInstance().Generate(-speed, +speed));
			color.SetFloatB(color.GetFloatB()
				+ RandomNumberGenerator::GetInstance().Generate(-speed, +speed));
		
			time -= 16.666f;
		}

		color.SetFloatR(MathExtensions::Clamp(
			color.GetFloatR(), min.GetFloatR(), max.GetFloatR()));
		color.SetFloatG(MathExtensions::Clamp(
			color.GetFloatG(), min.GetFloatG(), max.GetFloatG()));
		color.SetFloatB(MathExtensions::Clamp(
			color.GetFloatB(), min.GetFloatB(), max.GetFloatB()));
		
		return color;
	}
}