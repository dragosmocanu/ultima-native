#include "stdafx.h"

#include "VertexPTN.h"

namespace Ultima
{
		VertexPTN::VertexPTN()
			ULTIMA_NOEXCEPT
			: VertexPT()
		{
			Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		}

		VertexPTN::VertexPTN(
			const Vector3& position, 
			const Vector2& texture,
			const Vector3& normal)
			ULTIMA_NOEXCEPT
			: VertexPT(position, texture)
		{
			Normal = normal.ToXMFLOAT3();
		}

		VertexPTN::VertexPTN(
			float positionX, float positionY, float positionZ,
			float textureU, float textureV,
			float normalX, float normalY, float normalZ)
			ULTIMA_NOEXCEPT
			: VertexPT(positionX, positionY, positionZ,
			textureU, textureV)
		{
			Normal = XMFLOAT3(normalX, normalY, normalZ);
		}

		bool VertexPTN::operator==(const VertexPTN& other)
			const ULTIMA_NOEXCEPT
		{
			bool result = true;

			if (Position.x != other.Position.x)
				result = false;
			else if (Position.y != other.Position.y)
				result = false;
			else if (Position.z != other.Position.z)
				result = false;
			else if (Texture.x != other.Texture.x)
				result = false;
			else if (Texture.y != other.Texture.y)
				result = false;
			else if (Normal.x != other.Normal.x)
				result = false;
			else if (Normal.y != other.Normal.y)
				result = false;
			else if (Normal.z != other.Normal.z)
				result = false;
			
			return result;
		}
}