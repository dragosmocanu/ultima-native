#pragma once

#include "stdafx.h"

#include "Macros.h"

#include "Sphere.h"
#include "Vertex.h"

namespace Ultima
{
	/*!
	Defines a structure for mesh data.
	*/
	struct Mesh
	{
	public:
		std::vector<Vertex> Vertices;
		std::vector<unsigned short int> Indices;

		Sphere Sphere;
	};
}