#include "stdafx.h"

#include "Box.h"

#include "Frustum.h"
#include "Sphere.h"
#include "Quaternion.h"
#include "Vector3.h"

namespace Ultima
{
	Box::Box() 
		ULTIMA_NOEXCEPT
		: box()
	{ }

	Box::Box(
		const Vector3& position,
		const Vector3& size,
		const Matrix& rotation) 
		ULTIMA_NOEXCEPT
		: Box(position, size, Quaternion::CreateFromRotationMatrix(rotation))
	{ }

	Box::Box(
		const Vector3& position,
		const Vector3& size,
		const Quaternion& orientation)
		ULTIMA_NOEXCEPT
		: box(
		position.ToXMFLOAT3(),
		size.ToXMFLOAT3(),
		orientation.ToXMFLOAT4()),
		center(position)
	{ }

	Box::~Box()
	{ }

	bool Box::IsColliding(const ICollisionable& other)
		const ULTIMA_NOEXCEPT
	{
		const Box* const b = dynamic_cast<const Box* const>(&other);
		if (b != nullptr)
			return box.Intersects(b->GetInnerBox());

		const Sphere* const s = dynamic_cast<const Sphere* const>(&other);
		if (s != nullptr)
			return box.Intersects(s->GetInnerSphere());

		const Frustum* const f = dynamic_cast<const Frustum* const>(&other);
		if (f != nullptr)
			return box.Intersects(f->GetInnerFrustum());

		return false;
	}

	void Box::Transform(
		const Vector3& position,
		const Matrix& rotation)
		ULTIMA_NOEXCEPT
	{
		box = DirectX::BoundingOrientedBox(
			(center + position).ToXMFLOAT3(),
			box.Extents,
			Quaternion::CreateFromRotationMatrix(rotation).ToXMFLOAT4());
	}

	void Box::SetPosition(
		const Vector3& position)
		ULTIMA_NOEXCEPT
	{
		box = DirectX::BoundingOrientedBox(
			(center + position).ToXMFLOAT3(),
			box.Extents,
			box.Orientation);
	}

	void Box::SetRotation(
		const Matrix& rotation)
		ULTIMA_NOEXCEPT
	{
		SetRotation(Quaternion::CreateFromRotationMatrix(rotation));
	}

	void Box::SetRotation(
		const Quaternion& orientation)
		ULTIMA_NOEXCEPT
	{
		box = DirectX::BoundingOrientedBox(
			box.Center,
			box.Extents,
			orientation.ToXMFLOAT4());
	}

#if DIRECTX
	const DirectX::BoundingOrientedBox& Box::GetInnerBox() 
		const ULTIMA_NOEXCEPT
	{
		return box;
	}
#endif
}