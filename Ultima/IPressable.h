#pragma once

#include "stdafx.h"
#include "Macros.h"

#include "IControllable.h"

namespace Ultima
{
	class IPressable : public IControllable
	{
	public:
		/*!
		Returns true if the control has been pressed once, not continously.
		*/
		ULTIMA_API virtual bool HasPressed(unsigned int control) 
			const ULTIMA_NOEXCEPT = 0;
		
		/*!
		Returns true if the control has been released.
		*/
		ULTIMA_API virtual bool HasReleased(unsigned int control) 
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Returns true if the control is being pressed.
		*/
		ULTIMA_API virtual bool IsPressing(unsigned int control) 
			const ULTIMA_NOEXCEPT = 0;
	};
}