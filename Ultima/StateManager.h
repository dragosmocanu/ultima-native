#pragma once

#include "Macros.h"

#include "IState.h"

namespace Ultima
{
	//class IState;

	/*!
	Defines a state mananger that can handle different states.
	*/
	class StateManager
	{
	public:
		/*!
		Creates a new instance of StateManager.
		*/
		ULTIMA_API StateManager()
			ULTIMA_NOEXCEPT;

		ULTIMA_API virtual ~StateManager()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the index of the current state.
		*/
		ULTIMA_API virtual unsigned int GetStateIndex() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the list of states.
		*/
		ULTIMA_API virtual const std::vector<IState*>& GetStates() 
			const ULTIMA_NOEXCEPT;

		/*!
		Updates the StateManager logic.
		*/
		ULTIMA_API virtual void Update(float deltaTime)
			ULTIMA_NOEXCEPT;

		/*!
		Changes the state.
		*/
		ULTIMA_API virtual void ChangeStateTo(unsigned int futureStateIndex)
			ULTIMA_NOEXCEPT;

		/*!
		Adds a new state to the list of states.
		*/
		ULTIMA_API virtual void AddState(IState* const newState)
			ULTIMA_NOEXCEPT;

		/*!
		Removes a state from the list of states.
		*/
		ULTIMA_API virtual void RemoveState(unsigned int stateIndex)
			ULTIMA_NOEXCEPT;

		/*!
		Removes a state from the list of states.
		*/
		ULTIMA_API virtual void RemoveState(const IState* const state)
			ULTIMA_NOEXCEPT;

	protected:
		unsigned int stateIndex;
		std::vector<IState*> states;
	};
}