#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines a timer to be used in the application.
	*/
	class Timer
	{
	public:
		/*!
		Creates a new instance of Timer and starts ticking.
		*/
		ULTIMA_API Timer()
			ULTIMA_NOEXCEPT;

		ULTIMA_API ~Timer()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the time that has passed between the last two Update() calls.
		*/
		ULTIMA_API virtual float GetDeltaTime() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the total number of elapsed milliseconds since the timer was created.
		*/
		ULTIMA_API virtual unsigned long long GetTotalMilliseconds() 
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the total number of elapsed microseconds since the timer was created.
		*/
		ULTIMA_API virtual unsigned long long GetTotalMicroseconds()
			const ULTIMA_NOEXCEPT;

		/*!
		Updates the timer. Should be called every frame or whenever time values are needed.
		*/
		ULTIMA_API virtual void Update()
			ULTIMA_NOEXCEPT;

		/*!
		Resets the timer.
		*/
		ULTIMA_API virtual void Restart()
			ULTIMA_NOEXCEPT;

	protected:
		unsigned long long totalTime;
		float deltaTime;

#if DIRECTX
		float secondsPerCount;
		unsigned long long startTime;
#endif

	};
}