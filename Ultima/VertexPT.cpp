#include "stdafx.h"

#include "VertexPT.h"

namespace Ultima
{
	VertexPT::VertexPT()
		ULTIMA_NOEXCEPT
	{
		Position = Vector3().ToXMFLOAT3();
		Texture = Vector2().ToXMFLOAT2();
	}

	VertexPT::VertexPT(
		const Vector3& position,
		const Vector2& texture)
		ULTIMA_NOEXCEPT
	{
		Position = position.ToXMFLOAT3();
		Texture = texture.ToXMFLOAT2();
	}

	VertexPT::VertexPT(
		float positionX, float positionY, float positionZ,
		float textureU, float textureV)
		ULTIMA_NOEXCEPT
	{
		Position = XMFLOAT3(positionX, positionY, positionZ);
		Texture = XMFLOAT2(textureU, textureV);
	}

	bool VertexPT::operator==(const VertexPT& other)
		const ULTIMA_NOEXCEPT
	{
		bool result = true;

		if (Position.x != other.Position.x)
			result = false;
		else if (Position.y != other.Position.y)
			result = false;
		else if (Position.z != other.Position.z)
			result = false;
		else if (Texture.x != other.Texture.x)
			result = false;
		else if (Texture.y != other.Texture.y)
			result = false;

		return result;
	}
}