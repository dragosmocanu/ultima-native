#include "stdafx.h"

#include "Frustum.h"

#include "Box.h"
#include "Sphere.h"

namespace Ultima
{
	Frustum::Frustum() 
		ULTIMA_NOEXCEPT
		: frustum()
	{ }

	Frustum::Frustum(const Matrix& projection) 
		ULTIMA_NOEXCEPT
		: frustum(projection.ToXMMATRIX())
	{ }

	Frustum::~Frustum()
		ULTIMA_NOEXCEPT
	{ }

	bool Frustum::IsColliding(const ICollisionable& other)
		const ULTIMA_NOEXCEPT
	{
		const Box* const b = dynamic_cast<const Box* const>(&other);
		if (b != nullptr)
			return frustum.Intersects(b->GetInnerBox());

		const Sphere* const s = dynamic_cast<const Sphere* const>(&other);
		if (s != nullptr)
			return frustum.Intersects(s->GetInnerSphere());

		const Frustum* const f = dynamic_cast<const Frustum* const>(&other);
		if (f != nullptr)
			return frustum.Intersects(f->GetInnerFrustum());

		return false;
	}

	void Frustum::Transform(
		const Matrix& projection,
		const Vector3& position,
		const Matrix& rotation)
		ULTIMA_NOEXCEPT
	{
		frustum = DirectX::BoundingFrustum(
			projection.ToXMMATRIX());
		frustum.Transform(frustum, 
			Matrix::Transpose(rotation).ToXMMATRIX());
		frustum.Origin = position.ToXMFLOAT3();
	}

#if DIRECTX
	const DirectX::BoundingFrustum& Frustum::GetInnerFrustum() 
		const ULTIMA_NOEXCEPT
	{
		return frustum;
	}
#endif
}