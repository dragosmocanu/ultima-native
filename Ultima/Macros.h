#ifdef ULTIMA_EXPORTS
#define ULTIMA_API __declspec(dllexport)
#else
#define ULTIMA_API __declspec(dllimport)
#endif

#if _MSC_VER > 1800
#define ULTIMA_NOEXCEPT noexcept
#else
#define ULTIMA_NOEXCEPT throw()
#endif