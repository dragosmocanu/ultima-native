#pragma once

#include "Macros.h"

#include "IControllable.h"
#include "Vector2.h"

namespace Ultima
{
	/*!
	Defines an interface for controllable input devices.
	*/
	class IMovable : public IControllable
	{
	public:
		/*!
		Returns the X coordinate of the movement of the device.
		*/
		ULTIMA_API virtual float GetDeltaX() 
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Returns the Y coordinate of the movement of the device.
		*/
		ULTIMA_API virtual float GetDeltaY() 
			const ULTIMA_NOEXCEPT = 0;

		/*!
		Returns the movement vector of the device.
		*/
		ULTIMA_API virtual Vector2 GetDeltaPosition() 
			const ULTIMA_NOEXCEPT = 0;
	};
}