#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines an interface for controllable input devices.
	*/
	class IControllable
	{
	public:
		/*!
		Updates the input device.
		*/
		ULTIMA_API virtual void Update() 
			ULTIMA_NOEXCEPT = 0;

		/*!
		Flushes/cleans the state of the input device.
		*/
		ULTIMA_API virtual void Flush() 
			ULTIMA_NOEXCEPT = 0;
	};
}