#pragma once

#include "Macros.h"

#include <fbxsdk.h>

#include "Mesh.h"
#include "Vertex.h"

namespace Ultima
{
	/*!
	Defines the SCM that will be used.
	*/
	enum class SmartContentManagementType
	{
		Automatic, //When an asset is not used anymore, it will be automatically discarded.
		Manual //Unused assets will only be discarded when UnloadUnused() is called.
	};

	/*!
	Represents a mesh that has been loaded onto the VRAM.
	*/
	struct LoadedMesh
	{
		ID3D11Buffer* VertexBuffer;
		ID3D11Buffer* IndexBuffer;
		unsigned int NumberOfIndices;
		Sphere Sphere;
		unsigned int UseCount;
	};

	/*
	Represents a loaded sound.
	*/
	struct LoadedSound3D
	{
		IDirectSoundBuffer8* SoundBuffer;
		IDirectSound3DBuffer* SoundBuffer3D;
		unsigned int UseCount;
	};

	/*!
	Defines the header of a WAV file.
	*/
	struct WavHeader
	{
		char ChunkID[4];
		unsigned long ChunkSize;
		char Format[4];
		char SubChunkID[4];
		unsigned long SubChunkSize;
		unsigned short AudioFormat;
		unsigned short NumberOfChannels;
		unsigned long SampleRate;
		unsigned long BytesPerSecond;
		unsigned short BlockAlign;
		unsigned short BitsBerSample;
		char DataChunkID[4];
		unsigned long DataSize;
	};

	/*!
	[THREADSAFE] Singleton smart content management class that loads assets.
	*/
	class ContentManager
	{
	public:
		/*!
		Gets the single instance of ContentManager.
		*/
		ULTIMACONTENT_API static ContentManager& GetInstance()
			ULTIMA_NOEXCEPT;

		ULTIMACONTENT_API virtual ~ContentManager()
			ULTIMA_NOEXCEPT;

#if DIRECTX
        /*!
        Fills up two ID3D11Buffers with data loaded from an FBX file.
        */
        ULTIMACONTENT_API virtual void LoadMesh(
            ID3D11Device* const device,
            const std::string& filePath,
            ID3D11Buffer*& outVertexBuffer,
            ID3D11Buffer*& outIndexBuffer,
            unsigned int& outNumberOfIndices,
            Sphere& outSphere)
            ULTIMA_NOEXCEPT;

		/*!
		Returns a pointer to an IDirectSoundBuffer8 object with data loaded from a specified file.
		WAV file must have 2 channels.
		*/
		ULTIMACONTENT_API virtual IDirectSoundBuffer8* LoadSoundWAV(
			IDirectSound8* const directSound,
			const std::string& filePath)
			ULTIMA_NOEXCEPT;

		/*!
		Loads the data from a specified WAV file into the two sound buffers sent as arguments.
		WAV file must have 1 channel.
		*/
		ULTIMACONTENT_API virtual void LoadSound3DWAV(
			IDirectSound8* const directSound,
			const std::string& filePath,
			IDirectSoundBuffer8*& soundBuffer,
			IDirectSound3DBuffer*& soundBuffer3D)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used mesh.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11Buffer* const vertexBuffer)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used texture.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11ShaderResourceView* const textureSRV)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used vertex shader.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11VertexShader* const vertexShader)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used pixel shader.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11PixelShader* const pixelShader)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used hull shader.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11HullShader* const hullShader)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used domain shader.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11DomainShader* const domainShader)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used compute shader.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			ID3D11ComputeShader* const computeShader)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used sound.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			IDirectSoundBuffer8* const sound)
			ULTIMA_NOEXCEPT;

		/*!
		Unregisters a used 3D sound.
		*/
		ULTIMACONTENT_API virtual void Unregister(
			IDirectSound3DBuffer* const sound)
			ULTIMA_NOEXCEPT;

		/*!
		Unloads any unnecessary assets from the memory.
		Called automatically by the Application's destructor.
		*/
		ULTIMACONTENT_API virtual void UnloadUnused()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the SmartContentManagementType currently used.
		*/
		ULTIMACONTENT_API virtual SmartContentManagementType GetSmartContentManagementType()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the SmartContentManagementType.
		*/
		ULTIMACONTENT_API virtual void SetSmartContentManagementType(
			SmartContentManagementType scmType)
			ULTIMA_NOEXCEPT;

#endif
	protected:
		ULTIMACONTENT_API ContentManager()
			ULTIMA_NOEXCEPT;

		SmartContentManagementType scmType;

        mutable std::map<std::string, LoadedMesh> meshes;

		std::map<std::string, std::pair<IDirectSoundBuffer8*, unsigned int>> sounds;
		std::map<std::string, LoadedSound3D> sounds3D;

		FbxManager* fbxManager;
	};
}