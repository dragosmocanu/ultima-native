#include "stdafx.h"

#include "ContentManager.h"

#include "Utilities.h"

namespace Ultima
{
	ContentManager::ContentManager()
		ULTIMA_NOEXCEPT
	{
		scmType = SmartContentManagementType::Automatic;

		fbxManager = FbxManager::Create();

		FbxIOSettings* settings = FbxIOSettings::Create(fbxManager, IOSROOT);

		fbxManager->SetIOSettings(settings);
	}

	ContentManager& ContentManager::GetInstance()
		ULTIMA_NOEXCEPT
	{
		static ContentManager instance;

		return instance;
	}

	ContentManager::~ContentManager()
		ULTIMA_NOEXCEPT
	{
		fbxManager->Destroy();
	}

	SmartContentManagementType ContentManager::GetSmartContentManagementType()
		const ULTIMA_NOEXCEPT
	{
		return scmType;
	}

	void ContentManager::SetSmartContentManagementType(SmartContentManagementType scmType)
		ULTIMA_NOEXCEPT
	{
		this->scmType = scmType;
	}

#if DIRECTX
	//ID3D11VertexShader* ContentManager::LoadVertexShader(
	//	const std::string& filePath,
	//	ID3D11Device* const device)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11VertexShader* vertexShader = nullptr;

	//	for (auto& x : vertexShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			vertexShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (vertexShader == nullptr)
	//	{
	//		char* data;
	//		unsigned int size;

	//		vertexShader = ContentManager::LoadVertexShader(filePath, device, data, size);

	//		delete[] data;
	//	}

	//	return vertexShader;
	//}

	//ID3D11VertexShader* ContentManager::LoadVertexShader(
	//	const std::string& filePath,
	//	ID3D11Device* const device,
	//	char*& data,
	//	unsigned int& size)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11VertexShader* vertexShader = nullptr;

	//	for (auto& x : vertexShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			vertexShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);
	//	size = 0;

	//	if (ifs.good())
	//	{
	//		ifs.seekg(0, std::ifstream::end);
	//		size = static_cast<unsigned int>(ifs.tellg());
	//		data = new char[size];

	//		ifs.seekg(0, std::ifstream::beg);
	//		ifs.read(&data[0], size);
	//		ifs.close();

	//		if (vertexShader == nullptr)
	//		{
	//			Utilities::Assert(
	//				device->CreateVertexShader(data, size, 0, &vertexShader));

	//			auto p = std::pair<ID3D11VertexShader*, unsigned int>(vertexShader, 2);

	//			auto p2 = std::pair<
	//				std::string,
	//				std::pair<ID3D11VertexShader*, unsigned int>>
	//				(filePath, p);

	//			vertexShaders.insert(p2);
	//		}
	//	}

	//	return vertexShader;
	//}

	//ID3D11PixelShader* ContentManager::LoadPixelShader(
	//	const std::string& filePath,
	//	ID3D11Device* device)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11PixelShader* pixelShader = nullptr;

	//	for (auto& x : pixelShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			pixelShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (pixelShader == nullptr)
	//	{
	//		std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

	//		if (ifs.good())
	//		{
	//			unsigned int size = 0;
	//			char* data = nullptr;

	//			ifs.seekg(0, std::ifstream::end);
	//			size = static_cast<unsigned int>(ifs.tellg());
	//			data = new char[size];

	//			ifs.seekg(0, std::ifstream::beg);
	//			ifs.read(&data[0], size);
	//			ifs.close();

	//			Utilities::Assert(
	//				device->CreatePixelShader(data, size, 0, &pixelShader));

	//			auto p = std::pair<ID3D11PixelShader*, unsigned int>(pixelShader, 2);

	//			auto p2 = std::pair<
	//				std::string,
	//				std::pair<ID3D11PixelShader*, unsigned int>>
	//				(filePath, p);

	//			pixelShaders.insert(p2);

	//			delete[] data;
	//		}
	//	}

	//	return pixelShader;
	//}

	//ID3D11HullShader* ContentManager::LoadHullShader(
	//	const std::string& filePath,
	//	ID3D11Device* device)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11HullShader* hullShader = nullptr;

	//	for (auto& x : hullShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			hullShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (hullShader == nullptr)
	//	{
	//		std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

	//		if (ifs.good())
	//		{
	//			unsigned int size = 0;
	//			char* data = nullptr;

	//			ifs.seekg(0, std::ifstream::end);
	//			size = static_cast<unsigned int>(ifs.tellg());
	//			data = new char[size];

	//			ifs.seekg(0, std::ifstream::beg);
	//			ifs.read(&data[0], size);
	//			ifs.close();

	//			Utilities::Assert(
	//				device->CreateHullShader(data, size, 0, &hullShader));

	//			auto p = std::pair<ID3D11HullShader*, unsigned int>(hullShader, 2);

	//			auto p2 = std::pair<
	//				std::string,
	//				std::pair<ID3D11HullShader*, unsigned int>>
	//				(filePath, p);

	//			hullShaders.insert(p2);

	//			delete[] data;
	//		}
	//	}

	//	return hullShader;
	//}

	//ID3D11DomainShader* ContentManager::LoadDomainShader(
	//	const std::string& filePath,
	//	ID3D11Device* device)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11DomainShader* domainShader = nullptr;

	//	for (auto& x : domainShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			domainShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (domainShader == nullptr)
	//	{
	//		std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

	//		if (ifs.good())
	//		{
	//			unsigned int size = 0;
	//			char* data = nullptr;

	//			ifs.seekg(0, std::ifstream::end);
	//			size = static_cast<unsigned int>(ifs.tellg());
	//			data = new char[size];

	//			ifs.seekg(0, std::ifstream::beg);
	//			ifs.read(&data[0], size);
	//			ifs.close();

	//			Utilities::Assert(
	//				device->CreateDomainShader(data, size, 0, &domainShader));

	//			auto p = std::pair<ID3D11DomainShader*, unsigned int>(domainShader, 2);

	//			auto p2 = std::pair<
	//				std::string,
	//				std::pair<ID3D11DomainShader*, unsigned int>>
	//				(filePath, p);

	//			domainShaders.insert(p2);

	//			delete[] data;
	//		}
	//	}

	//	return domainShader;
	//}

	//ID3D11ComputeShader* ContentManager::LoadComputeShader(
	//	const std::string& filePath,
	//	ID3D11Device* device)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11ComputeShader* computeShader = nullptr;

	//	for (auto& x : computeShaders)
	//	{
	//		if (x.first == filePath)
	//		{
	//			computeShader = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (computeShader == nullptr)
	//	{
	//		std::ifstream ifs(filePath, std::ifstream::in | std::ifstream::binary);

	//		if (ifs.good())
	//		{
	//			unsigned int size = 0;
	//			char* data = nullptr;

	//			ifs.seekg(0, std::ifstream::end);
	//			size = static_cast<unsigned int>(ifs.tellg());
	//			data = new char[size];

	//			ifs.seekg(0, std::ifstream::beg);
	//			ifs.read(&data[0], size);
	//			ifs.close();

	//			Utilities::Assert(
	//				device->CreateComputeShader(data, size, 0, &computeShader));

	//			auto p = std::pair<ID3D11ComputeShader*, unsigned int>(computeShader, 2);

	//			auto p2 = std::pair<
	//				std::string,
	//				std::pair<ID3D11ComputeShader*, unsigned int>>
	//				(filePath, p);

	//			computeShaders.insert(p2);

	//			delete[] data;
	//		}
	//	}

	//	return computeShader;
	//}

	void ContentManager::LoadMesh(
		ID3D11Device* const device,
		const std::string& filePath,
		ID3D11Buffer*& outVertexBuffer,
		ID3D11Buffer*& outIndexBuffer,
		unsigned int& outNumberOfIndices,
		Sphere& outSphere)
		ULTIMA_NOEXCEPT
	{
		for (auto& x : meshes)
		{
			if (x.first == filePath)
			{
				outVertexBuffer = x.second.VertexBuffer;
				outIndexBuffer = x.second.IndexBuffer;
				outNumberOfIndices = x.second.NumberOfIndices;
				outSphere = x.second.Sphere;
				x.second.UseCount++;
				return;
			}
		}

		if (!Utilities::FileExists(filePath))
		{
			return;
		}

		Vertex v;
		Mesh m;

		FbxImporter* fbxImporter = FbxImporter::Create(fbxManager, "");

		if (fbxImporter->Initialize(filePath.c_str(), -1, fbxManager->GetIOSettings()))
		{
			FbxScene* fbxScene = FbxScene::Create(fbxManager, "");

			if (fbxImporter->Import(fbxScene))
			{
				FbxNode* root = fbxScene->GetRootNode();

				if (root)
				{
					for (int i = 0; i < root->GetChildCount(); i++)
					{
						FbxNode* child = root->GetChild(i);

						if (child->GetNodeAttribute() == nullptr)
							continue;

						FbxNodeAttribute::EType attributeType
							= child->GetNodeAttribute()->GetAttributeType();

						if (attributeType != FbxNodeAttribute::eMesh)
							continue;

						FbxMesh* fMesh = (FbxMesh*)child->GetNodeAttribute();

						FbxVector4* fVertices = fMesh->GetControlPoints();

						int counter = 0;

						for (int j = 0; j < fMesh->GetPolygonCount(); j++)
						{
							unsigned int numVer = fMesh->GetPolygonSize(j);

							for (unsigned int k = 0; k < numVer; k++)
							{
								int index = fMesh->GetPolygonVertex(j, k);

								v.Position.x = static_cast<float>(fVertices[index].mData[0]);
								v.Position.y = static_cast<float>(fVertices[index].mData[2]);
								v.Position.z = static_cast<float>(fVertices[index].mData[1]);

								if (fMesh->GetElementUVCount() > 0)
								{
									FbxGeometryElementUV* vertexUV = fMesh->GetElementUV();
									switch (vertexUV->GetMappingMode())
									{
									case FbxGeometryElement::eByPolygonVertex:
										switch (vertexUV->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Texture.x = static_cast<float>(vertexUV->GetDirectArray().GetAt(counter).mData[0]);
											v.Texture.y = static_cast<float>(vertexUV->GetDirectArray().GetAt(counter).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexUV->GetIndexArray().GetAt(counter);
											v.Texture.x = static_cast<float>(vertexUV->GetDirectArray().GetAt(index2).mData[0]);
											v.Texture.y = static_cast<float>(vertexUV->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;

									case FbxGeometryElement::eByControlPoint:
										switch (vertexUV->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Texture.x = static_cast<float>(vertexUV->GetDirectArray().GetAt(index).mData[0]);
											v.Texture.y = static_cast<float>(vertexUV->GetDirectArray().GetAt(index).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexUV->GetIndexArray().GetAt(index);
											v.Texture.x = static_cast<float>(vertexUV->GetDirectArray().GetAt(index2).mData[0]);
											v.Texture.y = static_cast<float>(vertexUV->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;
									}

									v.Texture.y = 1.0f - v.Texture.y;
								}

								if (fMesh->GetElementNormalCount() > 0)
								{
									FbxGeometryElementNormal* vertexNormal = fMesh->GetElementNormal(0);
									switch (vertexNormal->GetMappingMode())
									{
									case FbxGeometryElement::eByPolygonVertex:
										switch (vertexNormal->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Normal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(counter).mData[0]);
											v.Normal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(counter).mData[2]);
											v.Normal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(counter).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexNormal->GetIndexArray().GetAt(counter);
											v.Normal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[0]);
											v.Normal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[2]);
											v.Normal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;

									case FbxGeometryElement::eByControlPoint:
										switch (vertexNormal->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Normal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[0]);
											v.Normal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[2]);
											v.Normal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexNormal->GetIndexArray().GetAt(index);
											v.Normal.x = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[0]);
											v.Normal.y = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[2]);
											v.Normal.z = static_cast<float>(vertexNormal->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;
									}
								}

								if (fMesh->GetElementTangentCount() > 0)
								{
									FbxGeometryElementTangent* vertexTangent = fMesh->GetElementTangent(0);
									switch (vertexTangent->GetMappingMode())
									{
									case FbxGeometryElement::eByPolygonVertex:
										switch (vertexTangent->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Tangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(counter).mData[0]);
											v.Tangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(counter).mData[2]);
											v.Tangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(counter).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexTangent->GetIndexArray().GetAt(counter);
											v.Tangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[0]);
											v.Tangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[2]);
											v.Tangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;

									case FbxGeometryElement::eByControlPoint:
										switch (vertexTangent->GetReferenceMode())
										{
										case FbxGeometryElement::eDirect:
										{
											v.Tangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[0]);
											v.Tangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[2]);
											v.Tangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index).mData[1]);
										}
										break;

										case FbxGeometryElement::eIndexToDirect:
										{
											int index2 = vertexTangent->GetIndexArray().GetAt(index);
											v.Tangent.x = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[0]);
											v.Tangent.y = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[2]);
											v.Tangent.z = static_cast<float>(vertexTangent->GetDirectArray().GetAt(index2).mData[1]);
										}
										break;
										}
										break;
									}
								}

								m.Vertices.push_back(v);
								counter++;
							}
						}

						fMesh->Destroy(true);

						child->Destroy(true);
					}
				}

				root->Destroy(true);
			}

			fbxScene->Destroy(true);
		}

		fbxImporter->Destroy(true);

		m.Indices = std::vector<unsigned short int>(m.Vertices.size());

		for (unsigned short int i = 0; i < m.Indices.size(); i += 3)
		{
			m.Indices[i] = i;
			m.Indices[i + 1] = i + 2;
			m.Indices[i + 2] = i + 1;
		}

		XMFLOAT3 minV(FLT_MAX, FLT_MAX, FLT_MAX);
		XMFLOAT3 maxV(FLT_MIN, FLT_MIN, FLT_MIN);

		for (unsigned int i = 0; i < m.Vertices.size(); i++)
		{
			if (m.Vertices[i].Position.x < minV.x)
				minV.x = m.Vertices[i].Position.x;
			if (m.Vertices[i].Position.y < minV.y)
				minV.y = m.Vertices[i].Position.y;
			if (m.Vertices[i].Position.z < minV.z)
				minV.z = m.Vertices[i].Position.z;

			if (m.Vertices[i].Position.x > maxV.x)
				maxV.x = m.Vertices[i].Position.x;
			if (m.Vertices[i].Position.y > maxV.y)
				maxV.y = m.Vertices[i].Position.y;
			if (m.Vertices[i].Position.z > maxV.z)
				maxV.z = m.Vertices[i].Position.z;
		}

		Vector3 center;
		float vertLen = static_cast<float>(m.Vertices.size());
		for (unsigned int i = 0; i < m.Vertices.size(); i++)
			center += Vector3(m.Vertices[i].Position) / vertLen;

		//center /= static_cast<float>(m.Vertices.size());

		Vector3 v3;
		for (unsigned int i = 0; i < m.Vertices.size(); i++)
		{
			v3 = m.Vertices[i].Position;
			v3 -= center;
			m.Vertices[i].Position = v3.ToXMFLOAT3();
		}
		center -= center;

		float max = FLT_MIN;
		float temp;

		for (unsigned int i = 0; i < m.Vertices.size(); i++)
		{
			temp = center.DistanceTo(m.Vertices[i].Position);

			if (max < temp)
				max = temp;
		}

		m.Sphere = Sphere(center, max);

		if (m.Vertices.size() > 0)
		{
			D3D11_BUFFER_DESC vertexDesc;
			SecureZeroMemory(&vertexDesc, sizeof(vertexDesc));
			vertexDesc.Usage = D3D11_USAGE_DEFAULT;
			vertexDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexDesc.ByteWidth = sizeof(Vertex) * m.Vertices.size();

			D3D11_SUBRESOURCE_DATA resourceData;
			SecureZeroMemory(&resourceData, sizeof(resourceData));
			resourceData.pSysMem = &m.Vertices[0];

			Utilities::Assert(
				device->CreateBuffer(&vertexDesc, &resourceData, &outVertexBuffer));

			D3D11_BUFFER_DESC indexDesc;
			SecureZeroMemory(&indexDesc, sizeof(indexDesc));
			indexDesc.Usage = D3D11_USAGE_DEFAULT;
			indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexDesc.ByteWidth = sizeof(unsigned short int) * m.Indices.size();
			indexDesc.CPUAccessFlags = 0;

			resourceData.pSysMem = &m.Indices[0];

			Utilities::Assert(
				device->CreateBuffer(&indexDesc, &resourceData, &outIndexBuffer));

			outNumberOfIndices = m.Indices.size();

			outSphere = m.Sphere;

			LoadedMesh lm;
			lm.IndexBuffer = outIndexBuffer;
			lm.NumberOfIndices = outNumberOfIndices;
			lm.Sphere = outSphere;
			lm.VertexBuffer = outVertexBuffer;
			lm.UseCount = 2;

			auto pair = std::make_pair(filePath, lm);

			meshes.insert(pair);
		}
	}

	//ID3D11ShaderResourceView* ContentManager::LoadTexture(
	//	ID3D11Device* const device,
	//	const std::string& filePath)
	//	ULTIMA_NOEXCEPT
	//{
	//}

	//ID3D11ShaderResourceView* ContentManager::LoadSkyboxTexture(
	//	ID3D11Device* const device,
	//	const std::string& filePath)
	//	ULTIMA_NOEXCEPT
	//{
	//	ID3D11ShaderResourceView* resultSRV = nullptr;

	//	for (auto& x : skyboxTextures)
	//	{
	//		if (x.first == filePath)
	//		{
	//			resultSRV = x.second.first;
	//			x.second.second++;
	//			break;
	//		}
	//	}

	//	if (resultSRV == nullptr
	//		&& Utilities::FileExists(filePath))
	//	{
	//		D3DX11_IMAGE_LOAD_INFO info;
	//		info.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	//		ID3D11Texture2D* texture = nullptr;
	//		Utilities::Assert(
	//			D3DX11CreateTextureFromFile(device, filePath.c_str(),
	//				&info, 0, (ID3D11Resource**)&texture, 0));

	//		D3D11_TEXTURE2D_DESC textureDesc;
	//		texture->GetDesc(&textureDesc);

	//		D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
	//		viewDesc.Format = textureDesc.Format;
	//		viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	//		viewDesc.TextureCube.MipLevels = textureDesc.MipLevels;
	//		viewDesc.TextureCube.MostDetailedMip = 0;

	//		Utilities::Assert(
	//			device->CreateShaderResourceView(
	//				texture, &viewDesc, &resultSRV));


	//		auto p = std::pair<ID3D11ShaderResourceView*, unsigned int>(resultSRV, 2);

	//		auto p2 = std::pair<
	//			std::string,
	//			std::pair<ID3D11ShaderResourceView*, unsigned int>>
	//			(filePath, p);

	//		skyboxTextures.insert(p2);
	//	}

	//	return resultSRV;
	//}

	IDirectSoundBuffer8* ContentManager::LoadSoundWAV(
		IDirectSound8* const directSound,
		const std::string& filePath)
		ULTIMA_NOEXCEPT
	{
		IDirectSoundBuffer8* sound = nullptr;

		for (auto& x : sounds)
		{
			if (x.first == filePath)
			{
				sound = x.second.first;
				x.second.second++;
				break;
			}
		}

		if (sound == nullptr)
		{
			//http://rastertek.com/dx11tut14.html
			FILE* file;
			DSBUFFERDESC bufferDesc;
			WavHeader wavHeader;
			WAVEFORMATEX format;
			unsigned long bufferSize;

			int error = fopen_s(&file, filePath.c_str(), "rb");

			if (error == 0)
			{
				unsigned int count = fread(&wavHeader, sizeof(wavHeader), 1, file);

				if (count == 1)
				{
					if (wavHeader.ChunkID[0] == 'R' && wavHeader.ChunkID[1] == 'I'
						&& wavHeader.ChunkID[2] == 'F' && wavHeader.ChunkID[3] == 'F'
						&& wavHeader.Format[0] == 'W' && wavHeader.Format[1] == 'A'
						&& wavHeader.Format[2] == 'V' && wavHeader.Format[3] == 'E'
						&& wavHeader.SubChunkID[0] == 'f'
						&& wavHeader.SubChunkID[1] == 'm'
						&& wavHeader.SubChunkID[2] == 't'
						&& wavHeader.AudioFormat == WAVE_FORMAT_PCM
						&& wavHeader.NumberOfChannels == 2
						&& wavHeader.SampleRate == 44100
						&& wavHeader.BitsBerSample == 16
						&& wavHeader.DataChunkID[0] == 'd'
						&& wavHeader.DataChunkID[1] == 'a'
						&& wavHeader.DataChunkID[2] == 't'
						&& wavHeader.DataChunkID[3] == 'a')
					{
						SecureZeroMemory(&format, sizeof(format));
						format.wFormatTag = WAVE_FORMAT_PCM;
						format.nSamplesPerSec = 44100;
						format.wBitsPerSample = 16;
						format.nChannels = 2;
						format.nBlockAlign = (format.wBitsPerSample / 8)
							* format.nChannels;
						format.nAvgBytesPerSec = format.nSamplesPerSec
							* format.nBlockAlign;

						SecureZeroMemory(&bufferDesc, sizeof(bufferDesc));
						bufferDesc.dwSize = sizeof(DSBUFFERDESC);
						bufferDesc.dwFlags = DSBCAPS_CTRLVOLUME;
						bufferDesc.dwBufferBytes = wavHeader.DataSize;
						bufferDesc.lpwfxFormat = &format;
						bufferDesc.guid3DAlgorithm = GUID_NULL;

						IDirectSoundBuffer* tempBuffer;

						if (Utilities::Assert(
							directSound->CreateSoundBuffer(
							&bufferDesc, &tempBuffer, nullptr)))
						{
							if (Utilities::Assert(
								tempBuffer->QueryInterface(
									IID_IDirectSoundBuffer8, (void**)&sound)))
							{
								fseek(file, sizeof(WavHeader), SEEK_SET);

								unsigned char* wavData = new unsigned
									char[wavHeader.DataSize];

								if (wavData)
								{
									count = fread(wavData, 1,
										wavHeader.DataSize, file);

									error = fclose(file);

									if (error == 0)
									{
										unsigned char* bufferPtr;

										Utilities::Assert(sound->Lock(0,
											wavHeader.DataSize,
											(void**)&bufferPtr,
											(DWORD*)&bufferSize,
											nullptr, 0, 0));

										memcpy(bufferPtr, wavData,
											wavHeader.DataSize);

										Utilities::Assert(sound->Unlock(
											(void*)bufferPtr, bufferSize,
											nullptr, 0));

										auto p = std::make_pair(sound, 2);

										auto p2 = std::make_pair(filePath, p);

										sounds.insert(p2);
									}
								}

								delete[] wavData;
							}
						}

						Utilities::Release(tempBuffer);
					}
				}
			}

			fclose(file);
		}

		return sound;
	}

	void ContentManager::LoadSound3DWAV(
		IDirectSound8* const directSound,
		const std::string& filePath,
		IDirectSoundBuffer8*& outSoundBuffer,
		IDirectSound3DBuffer*& outSoundBuffer3D)
		ULTIMA_NOEXCEPT
	{
		outSoundBuffer = nullptr;
		outSoundBuffer3D = nullptr;

		for (auto& x : sounds3D)
		{
			if (x.first == filePath)
			{
				outSoundBuffer = x.second.SoundBuffer;
				outSoundBuffer3D = x.second.SoundBuffer3D;
				x.second.UseCount++;
				break;
			}
		}

		if (outSoundBuffer == nullptr)
		{
			//http://rastertek.com/dx11tut31.html
			FILE* file;
			DSBUFFERDESC bufferDesc;
			WavHeader wavHeader;
			WAVEFORMATEX format;
			unsigned long bufferSize;

			int error = fopen_s(&file, filePath.c_str(), "rb");

			if (error == 0)
			{
				unsigned int count = fread(&wavHeader, sizeof(wavHeader), 1, file);

				if (count == 1)
				{
					if (wavHeader.ChunkID[0] == 'R' && wavHeader.ChunkID[1] == 'I'
						&& wavHeader.ChunkID[2] == 'F' && wavHeader.ChunkID[3] == 'F'
						&& wavHeader.Format[0] == 'W' && wavHeader.Format[1] == 'A'
						&& wavHeader.Format[2] == 'V' && wavHeader.Format[3] == 'E'
						&& wavHeader.SubChunkID[0] == 'f'
						&& wavHeader.SubChunkID[1] == 'm'
						&& wavHeader.SubChunkID[2] == 't'
						&& wavHeader.AudioFormat == WAVE_FORMAT_PCM
						&& wavHeader.NumberOfChannels == 1
						&& wavHeader.SampleRate == 44100
						&& wavHeader.BitsBerSample == 16
						&& wavHeader.DataChunkID[0] == 'd'
						&& wavHeader.DataChunkID[1] == 'a'
						&& wavHeader.DataChunkID[2] == 't'
						&& wavHeader.DataChunkID[3] == 'a')
					{
						SecureZeroMemory(&format, sizeof(format));
						format.wFormatTag = WAVE_FORMAT_PCM;
						format.nSamplesPerSec = 44100;
						format.wBitsPerSample = 16;
						format.nChannels = 1;
						format.nBlockAlign = (format.wBitsPerSample / 8)
							* format.nChannels;
						format.nAvgBytesPerSec = format.nSamplesPerSec
							* format.nBlockAlign;

						SecureZeroMemory(&bufferDesc, sizeof(bufferDesc));
						bufferDesc.dwSize = sizeof(DSBUFFERDESC);
						bufferDesc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_CTRL3D;
						bufferDesc.dwBufferBytes = wavHeader.DataSize;
						bufferDesc.lpwfxFormat = &format;
						bufferDesc.guid3DAlgorithm = GUID_NULL;

						IDirectSoundBuffer* tempBuffer;

						if (Utilities::Assert(
							directSound->CreateSoundBuffer(
							&bufferDesc, &tempBuffer, nullptr)))
						{
							if (Utilities::Assert(
								tempBuffer->QueryInterface(
								IID_IDirectSoundBuffer8, (void**)&outSoundBuffer)))
							{
								fseek(file, sizeof(WavHeader), SEEK_SET);

								unsigned char* wavData = new unsigned
									char[wavHeader.DataSize];

								if (wavData)
								{
									count = fread(wavData, 1,
										wavHeader.DataSize, file);

									error = fclose(file);

									if (error == 0)
									{
										unsigned char* bufferPtr;

										Utilities::Assert(outSoundBuffer->Lock(0,
											wavHeader.DataSize,
											(void**)&bufferPtr,
											(DWORD*)&bufferSize,
											nullptr, 0, 0));

										memcpy(bufferPtr, wavData,
											wavHeader.DataSize);

										Utilities::Assert(outSoundBuffer->Unlock(
											(void*)bufferPtr, bufferSize,
											nullptr, 0));

										Utilities::Assert(outSoundBuffer->QueryInterface(
											IID_IDirectSound3DBuffer,
											(void**)&outSoundBuffer3D));

										LoadedSound3D ls;
										ls.SoundBuffer = outSoundBuffer;
										ls.SoundBuffer3D = outSoundBuffer3D;
										ls.UseCount = 2;

										auto p = std::make_pair(filePath, ls);

										sounds3D.insert(p);
									}
								}

								delete[] wavData;
							}
						}

						Utilities::Release(tempBuffer);
					}
				}
			}

			fclose(file);
		}
	}

	void ContentManager::Unregister(
		ID3D11Buffer* const vertexBuffer)
		ULTIMA_NOEXCEPT
	{
		//if (vertexBuffer == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : meshes)
		//	{
		//		if (x.second.VertexBuffer == vertexBuffer)
		//		{
		//			x.second.UseCount--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (meshes.size() > 0)
		//	{
		//		auto iterator1 = meshes.end();

		//		iterator1--;

		//		for (unsigned int i = meshes.size() - 1;; i--)
		//		{
		//			if (iterator1->second.VertexBuffer == vertexBuffer)
		//			{
		//				iterator1->second.UseCount--;

		//				if (iterator1->second.UseCount < 2)
		//				{
		//					Utilities::Release(iterator1->second.VertexBuffer);
		//					Utilities::Release(iterator1->second.IndexBuffer);
		//					meshes.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11ShaderResourceView* const srv)
		ULTIMA_NOEXCEPT
	{
		//if (srv == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : textures)
		//	{
		//		if (x.second.first == srv)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}

		//	for (auto& x : skyboxTextures)
		//	{
		//		if (x.second.first == srv)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (textures.size() > 0)
		//	{
		//		auto iterator1 = textures.end();

		//		iterator1--;

		//		for (unsigned int i = textures.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == srv)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					textures.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}

		//	if (skyboxTextures.size() > 0)
		//	{
		//		auto iterator1 = skyboxTextures.end();

		//		iterator1--;

		//		for (unsigned int i = skyboxTextures.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == srv)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					skyboxTextures.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11VertexShader* const shader)
		ULTIMA_NOEXCEPT
	{
		//if (shader == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : vertexShaders)
		//	{
		//		if (x.second.first == shader)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (vertexShaders.size() > 0)
		//	{
		//		auto iterator1 = vertexShaders.end();

		//		iterator1--;

		//		for (unsigned int i = vertexShaders.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == shader)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					vertexShaders.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11PixelShader* const shader)
		ULTIMA_NOEXCEPT
	{
		//if (shader == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : pixelShaders)
		//	{
		//		if (x.second.first == shader)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (pixelShaders.size() > 0)
		//	{
		//		auto iterator1 = pixelShaders.end();

		//		iterator1--;

		//		for (unsigned int i = pixelShaders.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == shader)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					pixelShaders.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11HullShader* const shader)
		ULTIMA_NOEXCEPT
	{
		//if (shader == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : hullShaders)
		//	{
		//		if (x.second.first == shader)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (hullShaders.size() > 0)
		//	{
		//		auto iterator1 = hullShaders.end();

		//		iterator1--;

		//		for (unsigned int i = hullShaders.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == shader)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					hullShaders.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11DomainShader* const shader)
		ULTIMA_NOEXCEPT
	{
		//if (shader == nullptr)
		//	return;

		//if (scmType == SmartContentManagementType::Manual)
		//{
		//	for (auto& x : domainShaders)
		//	{
		//		if (x.second.first == shader)
		//		{
		//			x.second.second--;
		//			break;
		//		}
		//	}
		//}
		//else
		//{
		//	if (domainShaders.size() > 0)
		//	{
		//		auto iterator1 = domainShaders.end();

		//		iterator1--;

		//		for (unsigned int i = domainShaders.size() - 1;; i--)
		//		{
		//			if (iterator1->second.first == shader)
		//			{
		//				iterator1->second.second--;

		//				if (iterator1->second.second < 2)
		//				{
		//					Utilities::Release(iterator1->second.first);
		//					domainShaders.erase(iterator1--);
		//				}

		//				break;
		//			}
		//			else
		//				iterator1--;

		//			if (i == 0)
		//				break;
		//		}
		//	}
		//}
	}

	void ContentManager::Unregister(
		ID3D11ComputeShader* const shader)
		ULTIMA_NOEXCEPT
	{
		/*if (shader == nullptr)
			return;

		if (scmType == SmartContentManagementType::Manual)
		{
			for (auto& x : computeShaders)
			{
				if (x.second.first == shader)
				{
					x.second.second--;
					break;
				}
			}
		}
		else
		{
			if (computeShaders.size() > 0)
			{
				auto iterator1 = computeShaders.end();

				iterator1--;

				for (unsigned int i = computeShaders.size() - 1;; i--)
				{
					if (iterator1->second.first == shader)
					{
						iterator1->second.second--;

						if (iterator1->second.second < 2)
						{
							Utilities::Release(iterator1->second.first);
							computeShaders.erase(iterator1--);
						}

						break;
					}
					else
						iterator1--;

					if (i == 0)
						break;
				}
			}
		}*/
	}

	void ContentManager::Unregister(
		IDirectSoundBuffer8* const sound)
		ULTIMA_NOEXCEPT
	{
		if (sound == nullptr)
			return;

		if (scmType == SmartContentManagementType::Manual)
		{
			for (auto& x : sounds)
			{
				if (x.second.first == sound)
				{
					x.second.second--;
					break;
				}
			}
		}
		else
		{
			if (sounds.size() > 0)
			{
				auto iterator1 = sounds.end();

				iterator1--;

				for (unsigned int i = sounds.size() - 1;; i--)
				{
					if (iterator1->second.first == sound)
					{
						iterator1->second.second--;

						if (iterator1->second.second < 2)
						{
							Utilities::Release(iterator1->second.first);
							sounds.erase(iterator1--);
						}

						break;
					}
					else
						iterator1--;

					if (i == 0)
						break;
				}
			}
		}
	}

	void ContentManager::Unregister(
		IDirectSound3DBuffer* const sound)
		ULTIMA_NOEXCEPT
	{
		if (sound == nullptr)
			return;

		if (scmType == SmartContentManagementType::Manual)
		{
			for (auto& x : sounds3D)
			{
				if (x.second.SoundBuffer3D == sound)
				{
					x.second.UseCount--;
					break;
				}
			}
		}
		else
		{
			if (sounds3D.size() > 0)
			{
				auto iterator1 = sounds3D.end();

				iterator1--;

				for (unsigned int i = sounds3D.size() - 1;; i--)
				{
					if (iterator1->second.SoundBuffer3D == sound)
					{
						iterator1->second.UseCount--;

						if (iterator1->second.UseCount < 2)
						{
							Utilities::Release(iterator1->second.SoundBuffer);
							Utilities::Release(iterator1->second.SoundBuffer3D);
							sounds3D.erase(iterator1--);
						}

						break;
					}
					else
						iterator1--;

					if (i == 0)
						break;
				}
			}
		}
	}

	void ContentManager::UnloadUnused()
		ULTIMA_NOEXCEPT
	{
		if (scmType == SmartContentManagementType::Automatic)
			return;

		unsigned int i;

		////meshes
		//if (meshes.size() > 0)
		//{
		//	auto iterator1 = meshes.end();

		//	iterator1--;

		//	for (i = meshes.size() - 1;; i--)
		//	{
		//		if (iterator1->second.UseCount < 2)
		//		{
		//			Utilities::Release(iterator1->second.VertexBuffer);
		//			Utilities::Release(iterator1->second.IndexBuffer);
		//			meshes.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////meshes

		////textures
		//if (textures.size() > 0)
		//{
		//	auto iterator1 = textures.end();

		//	iterator1--;

		//	for (i = textures.size() - 1; ; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			textures.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////textures

		////skybox textures
		//if (skyboxTextures.size() > 0)
		//{
		//	auto iterator1 = skyboxTextures.end();

		//	iterator1--;

		//	for (i = skyboxTextures.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			skyboxTextures.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////skybox textures

		////VS
		//if (vertexShaders.size() > 0)
		//{
		//	auto iterator1 = vertexShaders.end();

		//	iterator1--;

		//	for (i = vertexShaders.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			vertexShaders.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////VS

		////PS
		//if (pixelShaders.size() > 0)
		//{
		//	auto iterator1 = pixelShaders.end();

		//	iterator1--;

		//	for (i = pixelShaders.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			pixelShaders.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////PS

		////HS
		//if (hullShaders.size() > 0)
		//{
		//	auto iterator1 = hullShaders.end();

		//	iterator1--;

		//	for (i = hullShaders.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			hullShaders.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////HS

		////DS
		//if (domainShaders.size() > 0)
		//{
		//	auto iterator1 = domainShaders.end();

		//	iterator1--;

		//	for (i = domainShaders.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			domainShaders.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////DS

		////CS
		//if (computeShaders.size() > 0)
		//{
		//	auto iterator1 = computeShaders.end();

		//	iterator1--;

		//	for (i = computeShaders.size() - 1;; i--)
		//	{
		//		if (iterator1->second.second < 2)
		//		{
		//			Utilities::Release(iterator1->second.first);
		//			computeShaders.erase(iterator1--);
		//		}
		//		else
		//			iterator1--;

		//		if (i == 0)
		//			break;
		//	}
		//}
		////CS

		//sound
		if (sounds.size() > 0)
		{
			auto iterator1 = sounds.end();

			iterator1--;

			for (i = sounds.size() - 1;; i--)
			{
				if (iterator1->second.second < 2)
				{
					Utilities::Release(iterator1->second.first);
					sounds.erase(iterator1--);
				}
				else
					iterator1--;

				if (i == 0)
					break;
			}
		}
		//sound

		//sound 3D
		if (sounds3D.size() > 0)
		{
			auto iterator1 = sounds3D.end();

			iterator1--;

			for (i = sounds3D.size() - 1;; i--)
			{
				if (iterator1->second.UseCount < 2)
				{
					Utilities::Release(iterator1->second.SoundBuffer);
					Utilities::Release(iterator1->second.SoundBuffer3D);
					sounds3D.erase(iterator1--);
				}
				else
					iterator1--;

				if (i == 0)
					break;
			}
		}
		//sound 3D
	}

#endif

}