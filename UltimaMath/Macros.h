#ifdef ULTIMAMATH_EXPORTS
#define ULTIMAMATH_API __declspec(dllexport)
#else
#define ULTIMAMATH_API __declspec(dllimport)
#endif

#if _MSC_VER > 1800
#define ULTIMA_NOEXCEPT noexcept
#else
#define ULTIMA_NOEXCEPT throw()
#endif