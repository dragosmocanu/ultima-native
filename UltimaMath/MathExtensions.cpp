#include "stdafx.h"

#include "MathExtensions.h"

namespace Ultima
{
	float MathExtensions::Clamp(float value, float min, float max) 
		ULTIMA_NOEXCEPT
	{
		if (value < min)
			return min;
		else if (value > max)
			return max;
		else 
			return value;
	}

	float MathExtensions::Saturate(float value)
		ULTIMA_NOEXCEPT
	{
		if (value < 0.0f)
			return 0.0f;
		else if (value > 1.0f)
			return 1.0f;
		else
			return value;
	}

	double MathExtensions::MapIntervalTo01(
		double number, double intervalStart, double intervalEnd)
		ULTIMA_NOEXCEPT
	{
		return (number - intervalStart) / (intervalEnd - intervalStart);
	}

	double MathExtensions::MapIntervalFrom01(
		double number, double intervalStart, double intervalEnd)
		ULTIMA_NOEXCEPT
	{
		return number * (intervalEnd - intervalStart) + intervalStart;
	}

	double MathExtensions::MapInterval(double number,
		double firstIntervalStart, double firstIntervalEnd, 
		double secondIntervalStart, double secondIntervalEnd)
		ULTIMA_NOEXCEPT
	{
		return MapIntervalFrom01(MapIntervalTo01(number, 
			firstIntervalStart, firstIntervalEnd), 
			secondIntervalStart, secondIntervalEnd);
	}

	double MathExtensions::ReprojectLerp(
		double lastLerpResult, double intervalEnd, double lastLerpFactorUsed)
		ULTIMA_NOEXCEPT
	{
		if (abs(lastLerpFactorUsed - 1.0f) <= FLT_EPSILON)
			return (lastLerpResult - intervalEnd);
		else
			return (lastLerpResult - intervalEnd  * lastLerpFactorUsed)
			/ (1.0f - lastLerpFactorUsed);
	}

	//Source: https://github.com/id-Software/Quake-III-Arena
	float MathExtensions::InverseSquareRootFast(float x) 
		ULTIMA_NOEXCEPT
	{
		if (x == 0)
			return std::numeric_limits<float>::infinity();

		float xhalf = 0.5f * x;
		int i = *(int*)&x;
		i = 0x5f3759df - (i >> 1);
		x = *(float*)&i;
		x = x * (1.5f - (xhalf * x * x));
		return x;
	}

	//Source: https://github.com/id-Software/Quake-III-Arena
	float MathExtensions::InverseSquareRoot(float x) 
		ULTIMA_NOEXCEPT
	{
		if (x == 0)
			return std::numeric_limits<float>::infinity();

		float xhalf = 0.5f * x;
		int i = *(int*)&x;
		i = 0x5f3759df - (i >> 1);
		x = *(float*)&i;
		x = x * (1.5f - (xhalf * x * x));
		x = x * (1.5f - (xhalf * x * x));
		return x;
	}
	
	//Source: http://h14s.p5r.org/2012/09/0x5f3759df.html
	float MathExtensions::SquareRootFast(float x) 
		ULTIMA_NOEXCEPT
	{
		if (x == 0)
			return 0;

		float xhalf = 0.5f * x;
		int i = *(int*)&x;
		i = 0x1fbd1df5 + (i >> 1);
		x = *(float*)&i;
		x = x * (0.5f + xhalf * (1 / (x * x)));
		return x;
	}

	//Source: http://h14s.p5r.org/2012/09/0x5f3759df.html
	float MathExtensions::SquareRoot(float x) 
		ULTIMA_NOEXCEPT
	{
		if (x == 0) 
			return 0;

		float xhalf = 0.5f * x;
		int i = *(int*)&x;
		i = 0x1fbd1df5 + (i >> 1);
		x = *(float*)&i;
		x = x * (0.5f + xhalf * (1 / (x * x)));
		x = x * (0.5f + xhalf * (1 / (x * x)));
		return x;
	}

	int MathExtensions::IndexFrom2DTo1D(
		int matrixWidth, int rowIndex, int columnIndex)
		ULTIMA_NOEXCEPT
	{
		return (rowIndex * matrixWidth + columnIndex);
	}

	void MathExtensions::IndexFrom1DTo2D(int matrixWidth, int index,
		int& outRowIndex, int& outColumnIndex) 
		ULTIMA_NOEXCEPT
	{
		outRowIndex = static_cast<int>(index / matrixWidth);
		outColumnIndex = index % matrixWidth;
	}

	float MathExtensions::ToRadians(float degrees) 
		ULTIMA_NOEXCEPT
	{
		return degrees * Pi / 180.0f;
	}

	float MathExtensions::ToDegrees(float radians) 
		ULTIMA_NOEXCEPT
	{
		return radians * 180.0f / Pi;
	}

	const float MathExtensions::Pi = 3.14159274f;
	const float MathExtensions::TwoPi = 6.28318548f;
}