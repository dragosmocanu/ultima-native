#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Defines a 2 dimensional row vector.
	*/
	class Vector2
	{
	public:
		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2()
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(const Vector2& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2 by using move.
		*/
		ULTIMAMATH_API Vector2(Vector2&& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(float x, float y)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(int x, int y)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(float value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(XMFLOAT2 xm)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(int value)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual ~Vector2()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the X coordinate.
		*/
		ULTIMAMATH_API virtual float GetX()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Y coordinate.
		*/
		ULTIMAMATH_API virtual float GetY()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the length of the vector.
		*/
		ULTIMAMATH_API virtual float GetLength()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(int x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(float y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(int y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the length.
		*/
		ULTIMAMATH_API virtual void SetLength(float l)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two vectors together.
		*/
		ULTIMAMATH_API virtual void Add(const Vector2& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Adds two vectors together.
		*/
		ULTIMAMATH_API static Vector2 Add(const Vector2& v1, const Vector2& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a vector from another.
		*/
		ULTIMAMATH_API virtual void Subtract(const Vector2& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Subtracts a vector from another.
		*/
		ULTIMAMATH_API static Vector2 Subtract(const Vector2& v1, const Vector2& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a vector by a scalar.
		*/
		ULTIMAMATH_API virtual void Multiply(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Multiplies a vector by a scalar.
		*/
		ULTIMAMATH_API static Vector2 Multiply(const Vector2& v, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a vector by a scalar.
		*/
		ULTIMAMATH_API virtual void Divide(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Divides a vector by a scalar.
		*/
		ULTIMAMATH_API static Vector2 Divide(const Vector2& v, float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Normalizes the vector.
		*/
		ULTIMAMATH_API virtual void Normalize()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the distance to another vector.
		*/
		ULTIMAMATH_API virtual float DistanceTo(const Vector2& other)
			ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two vectors.
		*/
		ULTIMAMATH_API virtual float Dot(const Vector2& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two vectors.
		*/
		ULTIMAMATH_API static float Dot(const Vector2& v1, const Vector2& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two vectors.
		*/
		ULTIMAMATH_API virtual void Lerp(const Vector2& other, float amount)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two vectors.
		*/
		ULTIMAMATH_API static Vector2 Lerp(
			const Vector2& v1, 
			const Vector2& v2, 
			float amount)
			ULTIMA_NOEXCEPT;
		
		ULTIMAMATH_API virtual Vector2& operator+=(const Vector2& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2 operator+(const Vector2& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2& operator-=(const Vector2& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2 operator-(const Vector2& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2 operator-()
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2& operator*=(const float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2 operator*(const float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2& operator/=(const float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2 operator/(const float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2& operator=(const Vector2& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector2& operator=(Vector2&& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator==(const Vector2& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator!=(const Vector2& other)
			const ULTIMA_NOEXCEPT;
		
		/*!
		The Vector2 object with all of its components set to zero.
		*/
		ULTIMAMATH_API static const Vector2 Zero;

		/*!
		The Vector2 object with all of its components set to one.
		*/
		ULTIMAMATH_API static const Vector2 One;

		/*!
		The unit vector designating the right direction.
		*/
		ULTIMAMATH_API static const Vector2 Right;

		/*!
		The unit vector designating the left direction.
		*/
		ULTIMAMATH_API static const Vector2 Left;

		/*!
		The unit vector designating the up direction.
		*/
		ULTIMAMATH_API static const Vector2 Up;

		/*!
		The unit vector designating the down direction.
		*/
		ULTIMAMATH_API static const Vector2 Down;

#if DIRECTX
		/*!
		Loads vector data from a given XMFLOAT2.
		*/
		ULTIMAMATH_API void SetXMFLOAT2(const XMFLOAT2& xm)
			ULTIMA_NOEXCEPT;

		/*!
		Returns an XMFLOAT2 holding the vector data.
		*/
		ULTIMAMATH_API virtual XMFLOAT2 ToXMFLOAT2()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns an XMVECTOR holding the vector data.
		*/
		ULTIMAMATH_API virtual XMVECTOR ToXMVECTOR()
			const ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector2.
		*/
		ULTIMAMATH_API Vector2(XMVECTOR v)
			ULTIMA_NOEXCEPT;

#endif

	protected:
#if DIRECTX
		XMFLOAT2 vector;
#else
		float x;
		float y;
#endif
	};
}