#pragma once

#include "Macros.h"

namespace Ultima
{
	class Matrix;
	class Quaternion;

	/*!
	Defines a 3 dimensional row vector.
	*/
	class Vector3
	{
	public:
		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3()
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(const Vector3& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3 using a move.
		*/
		ULTIMAMATH_API Vector3(Vector3&& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(float x, float y, float z)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(int x, int y, int z)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(float value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(int value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a forward vector from a given quaternion.
		*/
		ULTIMAMATH_API Vector3(const Quaternion& q)
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Creates a vector from a given XMFLOAT3 object.
		*/
		ULTIMAMATH_API Vector3(const XMFLOAT3& xm)
			ULTIMA_NOEXCEPT;
#endif

		/*!
		Calculates the normal between 3 vectors.
		*/
		ULTIMAMATH_API static Vector3 CreateNormal(const Vector3& v1,
			const Vector3& v2, const Vector3& v3)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual ~Vector3()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the X coordinate.
		*/
		ULTIMAMATH_API virtual float GetX()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Y coordinate.
		*/
		ULTIMAMATH_API virtual float GetY()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Z coordinate.
		*/
		ULTIMAMATH_API virtual float GetZ()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the length of the vector.
		*/
		ULTIMAMATH_API virtual float GetLength()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(int x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(float y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(int y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Z coordinate.
		*/
		ULTIMAMATH_API virtual void SetZ(float z)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Z coordinate.
		*/
		ULTIMAMATH_API virtual void SetZ(int z)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the length.
		*/
		ULTIMAMATH_API virtual void SetLength(float l)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two vectors together.
		*/
		ULTIMAMATH_API virtual void Add(const Vector3& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Adds two vectors together.
		*/
		ULTIMAMATH_API static Vector3 Add(const Vector3& v1, const Vector3& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a vector from another.
		*/
		ULTIMAMATH_API virtual void Subtract(const Vector3& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Subtracts a vector from another.
		*/
		ULTIMAMATH_API static Vector3 Subtract(const Vector3& v1, const Vector3& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a vector by a scalar.
		*/
		ULTIMAMATH_API virtual void Multiply(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Multiplies a vector by a scalar.
		*/
		ULTIMAMATH_API static Vector3 Multiply(const Vector3& v, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a vector by a scalar.
		*/
		ULTIMAMATH_API virtual void Divide(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Divides a vector by a scalar.
		*/
		ULTIMAMATH_API static Vector3 Divide(const Vector3& v, float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Normalizes the vector.
		*/
		ULTIMAMATH_API virtual void Normalize()
			ULTIMA_NOEXCEPT;

		/*!
		Returns a normalized vector.
		*/
		ULTIMAMATH_API static Vector3 Normalize(const Vector3& v)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the distance to another vector.
		*/
		ULTIMAMATH_API virtual float DistanceTo(const Vector3& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two vectors.
		*/
		ULTIMAMATH_API virtual float Dot(const Vector3& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two vectors.
		*/
		ULTIMAMATH_API static float Dot(const Vector3& v1, const Vector3& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Calculates the cross product between two vectors.
		*/
		ULTIMAMATH_API virtual Vector3 Cross(const Vector3& other)
			const ULTIMA_NOEXCEPT;
		
		/*!
		Calculates the cross product between two vectors.
		*/
		ULTIMAMATH_API static Vector3 Cross(const Vector3& v1, const Vector3& v2)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two vectors.
		*/
		ULTIMAMATH_API virtual void Lerp(const Vector3& other, float amount)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two vectors.
		*/
		ULTIMAMATH_API static Vector3 Lerp(
			const Vector3& v1, 
			const Vector3& v2, 
			float amount)
			ULTIMA_NOEXCEPT;
		
		/*!
		Transforms a vector by a matrix.
		*/
		ULTIMAMATH_API virtual void Transform(const Matrix& m)
			ULTIMA_NOEXCEPT;

		/*!
		Transforms a vector by a matrix.
		*/
		ULTIMAMATH_API static Vector3 Transform(const Vector3& v, const Matrix& m)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual Vector3& operator+=(const Vector3& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3 operator+(const Vector3& other)
			const
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3& operator-=(const Vector3& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3 operator-(const Vector3& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3 operator-()
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3& operator*=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3 operator*(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3& operator/=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3 operator/(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3& operator=(const Vector3& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Vector3& operator=(Vector3&& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator==(const Vector3& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator!=(const Vector3& other)
			const ULTIMA_NOEXCEPT;
		
		/*!
		The Vector3 object with all of its components set to zero.
		*/
		ULTIMAMATH_API static const Vector3 Zero;

		/*!
		The Vector3 object with all of its components set to one.
		*/
		ULTIMAMATH_API static const Vector3 One;

		/*!
		The unit vector designating the forward direction.
		*/
		ULTIMAMATH_API static const Vector3 Forward;

		/*!
		Returns a unit vector designating the backward direction.
		*/
		ULTIMAMATH_API static const Vector3 Backward;

		/*!
		The unit vector designating the right direction.
		*/
		ULTIMAMATH_API static const Vector3 Right;

		/*!
		The unit vector designating the left direction.
		*/
		ULTIMAMATH_API static const Vector3 Left;

		/*!
		The unit vector designating the up direction.
		*/
		ULTIMAMATH_API static const Vector3 Up;

		/*!
		The unit vector designating the down direction.
		*/
		ULTIMAMATH_API static const Vector3 Down;

#if DIRECTX
		/*!
		Loads vector data from a given XMFLOAT2.
		*/
		ULTIMAMATH_API void SetXMFLOAT3(const XMFLOAT3& xm)
			ULTIMA_NOEXCEPT;

		/*!
		Returns an XMFLOAT3 holding the vector data.
		*/
		ULTIMAMATH_API virtual XMFLOAT3 ToXMFLOAT3()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns an XMVECTOR holding the vector data.
		*/
		ULTIMAMATH_API virtual XMVECTOR ToXMVECTOR()
			const ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Vector3.
		*/
		ULTIMAMATH_API Vector3(XMVECTOR v)
			ULTIMA_NOEXCEPT;
#endif

	protected:
#if DIRECTX
		XMFLOAT3 vector;
#else
		float x;
		float y;
		float z;
#endif
	};
}