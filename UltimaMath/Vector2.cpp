#include "stdafx.h"

#include "Vector2.h"

#include "MathExtensions.h"

namespace Ultima
{
	Vector2::Vector2()
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT2(1, 1);
#else
		x = 1;
		y = 1;
#endif
	}

	Vector2::Vector2(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
		*this = other;
	}

	Vector2::Vector2(Vector2&& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Vector2::Vector2(float x, float y) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector = XMFLOAT2(x, y);
#else
		this->x = x;
		this->y = y;
#endif
	}
	
	Vector2::Vector2(int x, int y) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector = XMFLOAT2(static_cast<float>(x), static_cast<float>(y));
#else
		this->x = static_cast<float>(x);
		this->y = static_cast<float>(y);
#endif
	}

	Vector2::Vector2(float value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT2(value, value);
#else
		x = value;
		y = value;
#endif
	}
	
	Vector2::Vector2(int value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT2(static_cast<float>(value), static_cast<float>(value));
#else
		x = static_cast<float>(value);
		y = static_cast<float>(value);
#endif
	}

	Vector2::Vector2(XMFLOAT2 xm)
		ULTIMA_NOEXCEPT
	{
		vector = xm;
	}

	Vector2::~Vector2()
		ULTIMA_NOEXCEPT
	{ }

	float Vector2::GetX() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return vector.x;
#else
		return x;
#endif
	}

	float Vector2::GetY() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return vector.y;
#else
		return y;
#endif
	}

	float Vector2::GetLength() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return MathExtensions::SquareRootFast(pow(vector.x, 2) + pow(vector.y, 2));
#else
		return MathExtensions::SquareRootFast(pow(x, 2) + pow(y, 2));
#endif
	}
	
	void Vector2::SetX(float x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x = x;
#else
		this->x = x;
#endif
	}
	
	void Vector2::SetX(int x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x = static_cast<float>(x);
#else
		this->x = static_cast<float>(x);
#endif
	}

	void Vector2::SetY(float y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.y = y;
#else
		this->y = y;
#endif
	}
	
	void Vector2::SetY(int y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.y = static_cast<float>(y);
#else
		this->y = static_cast<float>(y);
#endif
	}

	void Vector2::SetLength(float l)
		ULTIMA_NOEXCEPT
	{
		Normalize();
		Multiply(l);
	}

	void Vector2::Add(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x += other.GetX();
		vector.y += other.GetY();
#else
		x += other.GetX();
		y += other.GetY();
#endif
	}

	Vector2 Vector2::Add(const Vector2& v1, const Vector2& v2)
		ULTIMA_NOEXCEPT
	{
		Vector2 temp = std::move(v1);
		temp.Add(v2);
		return temp;
	}
	
	void Vector2::Subtract(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x -= other.GetX();
		vector.y -= other.GetY();
#else
		x -= other.GetX();
		y -= other.GetY();
#endif
	}
	
	Vector2 Vector2::Subtract(const Vector2& v1, const Vector2& v2)
		ULTIMA_NOEXCEPT
	{
		Vector2 temp = std::move(v1);
		temp.Subtract(v2);
		return temp;
	}
	
	void Vector2::Multiply(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x *= scalar;
		vector.y *= scalar;
#else
		x *= scalar;
		y *= scalar;
#endif
	}
	
	Vector2 Vector2::Multiply(const Vector2& v, float scalar)
		ULTIMA_NOEXCEPT
	{
		Vector2 temp = std::move(v);
		temp.Multiply(scalar);
		return temp;
	}
	
	void Vector2::Divide(float scalar)
		ULTIMA_NOEXCEPT
	{
		scalar = 1.0f / scalar;
#if DIRECTX
		vector.x *= scalar;
		vector.y *= scalar;
#else
		x *= scalar;
		y *= scalar;
#endif
	}
	
	Vector2 Vector2::Divide(const Vector2& v, float scalar)
		ULTIMA_NOEXCEPT
	{
		Vector2 temp = std::move(v);
		temp.Divide(scalar);
		return temp;
	}
	
	void Vector2::Normalize()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		float inverseLength = MathExtensions::InverseSquareRootFast(
			pow(vector.x, 2) + pow(vector.y, 2));
		vector.x *= inverseLength;
		vector.y *= inverseLength;
#else
		float inverseLength = MathExtensions::InverseSquareRootFast(
			pow(x, 2) + pow(y, 2));
		x *= inverseLength;
		y *= inverseLength;
#endif
	}

	float Vector2::DistanceTo(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return MathExtensions::SquareRootFast(
			(vector.x - other.GetX()) * (vector.x - other.GetX()) +
			(vector.y - other.GetY()) * (vector.y - other.GetY()));
#else
		return MathExtensions::SquareRootFast(
			(this->x - other.GetX()) * (this->x - other.GetX()) +
			(this->y - other.GetY()) * (this->y - other.GetY()));
#endif
	}
	
	float Vector2::Dot(const Vector2& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return vector.x * other.GetX() + vector.y * other.GetY();
#else
		return this->x * other.GetX() + this->y * other.GetY();
#endif
	}

	float Vector2::Dot(const Vector2& v1, const Vector2& v2)
		ULTIMA_NOEXCEPT
	{
		return v1.Dot(v2);
	}
	
	void Vector2::Lerp(const Vector2& other, float amount)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat2(&vector, (XMVectorLerp(this->ToXMVECTOR(), 
			other.ToXMVECTOR(), amount)));
#else
		Add(Vector2::Multiply((other - *this), amount));
#endif
	}
	
	Vector2 Vector2::Lerp(const Vector2& v1, const Vector2& v2, float amount)
		ULTIMA_NOEXCEPT
	{
		Vector2 temp = std::move(v1);
		temp.Lerp(v2, amount);
		return temp;
	}
	
	Vector2& Vector2::operator+=(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
		this->Add(other);
		return *this;
	}

	Vector2 Vector2::operator+(const Vector2& other) 
		const ULTIMA_NOEXCEPT
	{
		Vector2 v = std::move(*this);
		v.Add(other);
		return v;
	}

	Vector2& Vector2::operator-=(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(other);
		return *this;
	}

	Vector2 Vector2::operator-(const Vector2& other) 
		const ULTIMA_NOEXCEPT
	{
		Vector2 v = std::move(*this);
		v.Subtract(other);
		return v;
	}

	Vector2 Vector2::operator-() 
		const ULTIMA_NOEXCEPT
	{
		Vector2 v = std::move(*this);
		v.SetX(-v.GetX());
		v.SetY(-v.GetY());
		return v;
	}

	Vector2& Vector2::operator*=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(scalar);
		return *this;
	}

	Vector2 Vector2::operator*(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Vector2 v = std::move(*this);
		v.Multiply(scalar);
		return v;
	}
	
	Vector2& Vector2::operator/=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Divide(scalar);
		return *this;
	}

	Vector2 Vector2::operator/(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Vector2 v = std::move(*this);
		v.Divide(scalar);
		return v;
	}

	Vector2& Vector2::operator=(const Vector2& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			this->vector = other.ToXMFLOAT2();
#else
			this->x = other.GetX();
			this->y = other.GetY();
#endif
		}
		return *this;
	}

	Vector2& Vector2::operator=(Vector2&& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			vector = other.ToXMFLOAT2();
			other.SetXMFLOAT2(XMFLOAT2());
#else
			x = other.GetX();
			y = other.GetY();
			other.SetX(0);
			other.SetY(0);
#endif
		}
		return *this;
	}

	bool Vector2::operator==(const Vector2& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (vector.x != other.GetX())
			return false;
		else if (vector.y != other.GetY())
			return false;
#else
		if (x != other.GetX())
			return false;
		else if (y != other.GetY())
			return false;
#endif
		else
			return true;
	}
	
	bool Vector2::operator!=(const Vector2& other) 
		const ULTIMA_NOEXCEPT
	{
		return !(this == &other);
	}

	const Vector2 Vector2::Zero = Vector2(0.0f, 0.0f);

	const Vector2 Vector2::One = Vector2(1.0f, 1.0f);

	const Vector2 Vector2::Right = Vector2(1.0f, 0.0f);

	const Vector2 Vector2::Left = Vector2(-1.0f, 0.0f);

	const Vector2 Vector2::Up = Vector2(0.0f, -1.0f);

	const Vector2 Vector2::Down = Vector2(0.0f, 1.0f);

#if DIRECTX
	void Vector2::SetXMFLOAT2(const XMFLOAT2& xm)
		ULTIMA_NOEXCEPT
	{
		vector = std::move(xm);
	}

	XMFLOAT2 Vector2::ToXMFLOAT2() 
		const ULTIMA_NOEXCEPT
	{
		return vector;
	}

	XMVECTOR Vector2::ToXMVECTOR() 
		const ULTIMA_NOEXCEPT
	{
		return XMLoadFloat2(&vector);
	}

	Vector2::Vector2(XMVECTOR v)
		ULTIMA_NOEXCEPT
	{
		XMStoreFloat2(&vector, v);
	}
#endif
}