#include "stdafx.h"

#include "Matrix.h"

#include "Math.h"
#include "Quaternion.h"
#include "Vector3.h"

namespace Ultima
{
	Matrix::Matrix()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4x4(&matrix, XMMatrixIdentity());
#else
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (i == j)
					matrix[i][j] = 1;
				else
					matrix[i][j] = 0;
			}
		}
#endif
	}

	Matrix::Matrix(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
		*this = other;
	}

	Matrix::Matrix(Matrix&& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Matrix::Matrix(float value)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 = value;
		matrix._12 = value;
		matrix._13 = value;
		matrix._14 = value;
		matrix._21 = value;
		matrix._22 = value;
		matrix._23 = value;
		matrix._24 = value;
		matrix._31 = value;
		matrix._32 = value;
		matrix._33 = value;
		matrix._34 = value;
		matrix._41 = value;
		matrix._42 = value;
		matrix._43 = value;
		matrix._44 = value;
#else
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = value;
#endif
	}

	Matrix::Matrix(float diagonalValue, float otherValue)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 = diagonalValue;
		matrix._12 = otherValue;
		matrix._13 = otherValue;
		matrix._14 = otherValue;
		matrix._21 = otherValue;
		matrix._22 = diagonalValue;
		matrix._23 = otherValue;
		matrix._24 = otherValue;
		matrix._31 = otherValue;
		matrix._32 = otherValue;
		matrix._33 = diagonalValue;
		matrix._34 = otherValue;
		matrix._41 = otherValue;
		matrix._42 = otherValue;
		matrix._43 = otherValue;
		matrix._44 = diagonalValue;
#else
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (i == j)
					matrix[i][j] = diagonalValue;
				else
					matrix[i][j] = otherValue;
			}
		}
#endif
	}

	Matrix::Matrix(int value)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 = static_cast<float>(value);
		matrix._12 = static_cast<float>(value);
		matrix._13 = static_cast<float>(value);
		matrix._14 = static_cast<float>(value);
		matrix._21 = static_cast<float>(value);
		matrix._22 = static_cast<float>(value);
		matrix._23 = static_cast<float>(value);
		matrix._24 = static_cast<float>(value);
		matrix._31 = static_cast<float>(value);
		matrix._32 = static_cast<float>(value);
		matrix._33 = static_cast<float>(value);
		matrix._34 = static_cast<float>(value);
		matrix._41 = static_cast<float>(value);
		matrix._42 = static_cast<float>(value);
		matrix._43 = static_cast<float>(value);
		matrix._44 = static_cast<float>(value);
#else
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = static_cast<float>(value);
#endif
	}

	Matrix::Matrix(int diagonalValue, int otherValue)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 = static_cast<float>(diagonalValue);
		matrix._12 = static_cast<float>(otherValue);
		matrix._13 = static_cast<float>(otherValue);
		matrix._14 = static_cast<float>(otherValue);
		matrix._21 = static_cast<float>(otherValue);
		matrix._22 = static_cast<float>(diagonalValue);
		matrix._23 = static_cast<float>(otherValue);
		matrix._24 = static_cast<float>(otherValue);
		matrix._31 = static_cast<float>(otherValue);
		matrix._32 = static_cast<float>(otherValue);
		matrix._33 = static_cast<float>(diagonalValue);
		matrix._34 = static_cast<float>(otherValue);
		matrix._41 = static_cast<float>(otherValue);
		matrix._42 = static_cast<float>(otherValue);
		matrix._43 = static_cast<float>(otherValue);
		matrix._44 = static_cast<float>(diagonalValue);
#else
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (i == j)
					matrix[i][j] = static_cast<float>(diagonalValue);
				else
					matrix[i][j] = static_cast<float>(otherValue);
			}
		}
#endif
	}

	Matrix::Matrix(const Quaternion& q)
		ULTIMA_NOEXCEPT
	{
		float xx = q.GetX() * q.GetX();
		float xy = q.GetX() * q.GetY();
		float xz = q.GetX() * q.GetZ();
		float xw = q.GetX() * q.GetW();

		float yy = q.GetY() * q.GetY();
		float yz = q.GetY() * q.GetZ();
		float yw = q.GetY() * q.GetW();

		float zz = q.GetZ() * q.GetZ();
		float zw = q.GetZ() * q.GetW();
#if DIRECTX
		matrix._11 = 1 - 2 * (yy + zz);
		matrix._12 = 2 * (xy - zw);
		matrix._13 = 2 * (xz + yw);

		matrix._21 = 2 * (xy + zw);
		matrix._22 = 1 - 2 * (xx + zz);
		matrix._23 = 2 * (yz - xw);

		matrix._31 = 2 * (xz - yw);
		matrix._32 = 2 * (yz + xw);
		matrix._33 = 1 - 2 * (xx + yy);

		matrix._14 = matrix._24 = matrix._34 = matrix._41 = matrix._42 
			= matrix._43 = 0;
		matrix._44 = 1;
#else
		matrix[0][0] = 1 - 2 * (yy + zz);
		matrix[0][1] = 2 * (xy - zw);
		matrix[0][2] = 2 * (xz + yw);

		matrix[1][0] = 2 * (xy + zw);
		matrix[1][1] = 1 - 2 * (xx + zz);
		matrix[1][2] = 2 * (yz - xw);

		matrix[2][0] = 2 * (xz - yw);
		matrix[2][1] = 2 * (yz + xw);
		matrix[2][2] = 1 - 2 * (xx + yy);

		matrix[0][3] = matrix[1][3] = matrix[2][3] = matrix[3][0] = matrix[3][1] 
			= matrix[3][2] = 0;
		matrix[3][3] = 1;
#endif
	}

	Matrix::Matrix(float fieldOfView, float aspectRatio, float zNear, float zFar)
		ULTIMA_NOEXCEPT
	{
		float y = 1 / std::tan(fieldOfView / 2);
		float x = y / aspectRatio;
#if DIRECTX
		matrix._11 = x;
		matrix._22 = y;
		matrix._33 = zFar / (zFar - zNear);
		matrix._34 = 1;
		matrix._43 = -zNear * matrix._33;
		matrix._12 = matrix._13 = matrix._14 = matrix._21 = matrix._23 
			= matrix._24 = matrix._31 = matrix._32 = matrix._41 = matrix._42 
			= matrix._44 = 0;
#else
		matrix.[0][0] = x;
		matrix.[1][1] = y;
		matrix.[2][2] = zFar / (zFar - zNear);
		matrix.[2][3] = 1;
		matrix.[3][2] = -zNear * matrix[2][2];
		matrix.[0][1] = matrix[0][2] = matrix[0][3] = matrix[1][0] = matrix[1][2] 
			= matrix[1][3] = matrix[2][0] = matrix[2][1] = matrix[3][0] 
			= matrix[3][1] = matrix[3][3] = 0;
#endif
	}

	Matrix Matrix::CreateScale(const Vector3& v)
		ULTIMA_NOEXCEPT
	{
		Matrix temp(0);

		temp.SetM00(v.GetX());
		temp.SetM11(v.GetY());
		temp.SetM22(v.GetZ());
		temp.SetM33(1.0f);

		return temp;
	}

	Matrix Matrix::CreateRotationX(float radians)
		ULTIMA_NOEXCEPT
	{
		float cos = std::cos(radians);
		float sin = std::sin(radians);

		Matrix temp = std::move(Matrix::Zero);
		temp.SetM00(1);
		temp.SetM11(cos);
		temp.SetM12(-sin);
		temp.SetM21(sin);
		temp.SetM22(cos);
		temp.SetM33(1);
		return temp;
	}

	Matrix Matrix::CreateRotationY(float radians)
		ULTIMA_NOEXCEPT
	{
		float cos = std::cos(radians);
		float sin = std::sin(radians);

		Matrix temp = std::move(Matrix::Zero);
		temp.SetM00(cos);
		temp.SetM02(sin);
		temp.SetM11(1);
		temp.SetM20(-sin);
		temp.SetM22(cos);
		temp.SetM33(1);
		return temp;
	}

	Matrix Matrix::CreateRotationZ(float radians)
		ULTIMA_NOEXCEPT
	{
		float cos = std::cos(radians);
		float sin = std::sin(radians);

		Matrix temp = std::move(Matrix::Zero);
		temp.SetM00(cos);
		temp.SetM01(-sin);
		temp.SetM10(sin);
		temp.SetM11(cos);
		temp.SetM22(1);
		temp.SetM33(1);
		return temp;
	}

	Matrix Matrix::CreateTranslation(const Vector3& v)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(Matrix::Identity);
		temp.SetM30(v.GetX());
		temp.SetM31(v.GetY());
		temp.SetM32(v.GetZ());
		return temp;
	}

	Matrix Matrix::CreateTranslation(float x, float y, float z)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(Matrix::Identity);
		temp.SetM30(x);
		temp.SetM31(y);
		temp.SetM32(z);
		return temp;
	}

	Matrix Matrix::CreateFromPitchYawRoll(float pitch, float yaw, float roll)
		ULTIMA_NOEXCEPT
	{
		return CreateRotationZ(roll) 
			* CreateRotationX(pitch) 
			* CreateRotationY(yaw);
	}

	Matrix Matrix::CreateLookAt(const Vector3& position,
		const Vector3& target, const Vector3& up)
		ULTIMA_NOEXCEPT
	{
		Matrix temp;

		Vector3 zAxis = Vector3::Normalize(target - position);
		Vector3 xAxis = Vector3::Normalize(Vector3::Cross(up, zAxis));
		Vector3 yAxis = Vector3::Cross(zAxis, xAxis);

		temp.SetM00(xAxis.GetX());
		temp.SetM01(yAxis.GetX());
		temp.SetM02(zAxis.GetX());
		temp.SetM03(0);

		temp.SetM10(xAxis.GetY());
		temp.SetM11(yAxis.GetY());
		temp.SetM12(zAxis.GetY());
		temp.SetM13(0);

		temp.SetM20(xAxis.GetZ());
		temp.SetM21(yAxis.GetZ());
		temp.SetM22(zAxis.GetZ());
		temp.SetM23(0);

		temp.SetM30(-Vector3::Dot(xAxis, position));
		temp.SetM31(-Vector3::Dot(yAxis, position));
		temp.SetM32(-Vector3::Dot(zAxis, position));
		temp.SetM33(1);

		return temp;
	}

	Matrix Matrix::CreateOthographicProjection(float width, float height,
		float zNear, float zFar)
		ULTIMA_NOEXCEPT
	{
		Matrix temp(0.0f);

		float z = zFar - zNear;

		temp.SetM00(2.0f / width);
		temp.SetM11(2.0f / height);
		temp.SetM22(1.0f / z);
		temp.SetM32(-zNear / z);
		temp.SetM33(1.0f);

		return temp;
	}

	Matrix Matrix::CreateProjection(float fieldOfView,
		float aspectRatio, float zNear, float zFar)
		ULTIMA_NOEXCEPT
	{
		return Matrix(fieldOfView, aspectRatio, zNear, zFar);
	}

	Matrix::~Matrix() 
		ULTIMA_NOEXCEPT
	{ }

	float Matrix::GetM00() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._11;
#else
		return matrix[0][0];
#endif
	}
	
	float Matrix::GetM01() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._12;
#else
		return matrix[0][1];
#endif
	}

	float Matrix::GetM02() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._13;
#else
		return matrix[0][2];
#endif
	}

	float Matrix::GetM03() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._14;
#else
		return matrix[0][3];
#endif
	}

	float Matrix::GetM10() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._21;
#else
		return matrix[1][0];
#endif
	}

	float Matrix::GetM11()
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._22;
#else
		return matrix[1][1];
#endif
	}

	float Matrix::GetM12() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._23;
#else
		return matrix[1][2];
#endif
	}

	float Matrix::GetM13() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._24;
#else
		return matrix[1][3];
#endif
	}

	float Matrix::GetM20() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._31;
#else
		return matrix[2][0];
#endif
	}

	float Matrix::GetM21() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._32;
#else
		return matrix[2][1];
#endif
	}

	float Matrix::GetM22() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._33;
#else
		return matrix[2][2];
#endif
	}

	float Matrix::GetM23() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._34;
#else
		return matrix[2][3];
#endif
	}

	float Matrix::GetM30() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._41;
#else
		return matrix[3][0];
#endif
	}

	float Matrix::GetM31() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._42;
#else
		return matrix[3][1];
#endif
	}

	float Matrix::GetM32() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._43;
#else
		return matrix[3][2];
#endif
	}

	float Matrix::GetM33() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._44;
#else
		return matrix[3][3];
#endif
	}
	
	float Matrix::GetDeterminant() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		float f;
		XMStoreFloat(&f, XMMatrixDeterminant(ToXMMATRIX()));
		return f;
#else
		float f;
		f = matrix[0][0] * matrix[1][1] * matrix[2][2] * matrix[3][3] 
			+ matrix[0][1] * matrix[1][2] * matrix[2][3] * matrix[3][0]
			+ matrix[0][2] * matrix[1][3] * matrix[2][0] * matrix[3][1]
			+ matrix[0][3] * matrix[1][0] * matrix[2][1] * matrix[3][2]
			- matrix[0][3] * matrix[1][2] * matrix[2][1] * matrix[3][0]
			- matrix[0][2] * matrix[1][1] * matrix[2][0] * matrix[3][3]
			- matrix[0][1] * matrix[1][0] * matrix[2][3] * matrix[3][2]
			- matrix[0][0] * matrix[1][3] * matrix[2][2] * matrix[3][1];
		return f;
#endif
	}

	float Matrix::GetTrace() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return matrix._11 + matrix._22 + matrix._33 + matrix._44;
#else
		return matrix[0][0] + matrix[1][1] + matrix [2][2] + matrix[3][3];
#endif
	}

	void Matrix::SetM00(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 = f;
#else
		matrix[0][0] = f;
#endif
	}
	
	void Matrix::SetM01(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._12 = f;
#else
		matrix[0][1] = f;
#endif
	}

	void Matrix::SetM02(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._13 = f;
#else
		matrix[0][2] = f;
#endif
	}

	void Matrix::SetM03(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._14 = f;
#else
		matrix[0][3] = f;
#endif
	}
	
	void Matrix::SetM10(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._21 = f;
#else
		matrix[1][0] = f;
#endif
	}
	
	void Matrix::SetM11(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._22 = f;
#else
		matrix[1][1] = f;
#endif
	}

	void Matrix::SetM12(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._23 = f;
#else
		matrix[1][2] = f;
#endif
	}

	void Matrix::SetM13(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._24 = f;
#else
		matrix[1][3] = f;
#endif
	}

	void Matrix::SetM20(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._31 = f;
#else
		matrix[2][0] = f;
#endif
	}
	
	void Matrix::SetM21(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._32 = f;
#else
		matrix[2][1] = f;
#endif
	}

	void Matrix::SetM22(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._33 = f;
#else
		matrix[2][2] = f;
#endif
	}

	void Matrix::SetM23(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._34 = f;
#else
		matrix[2][3] = f;
#endif
	}

	void Matrix::SetM30(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._41 = f;
#else
		matrix[3][0] = f;
#endif
	}
	
	void Matrix::SetM31(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._42 = f;
#else
		matrix[3][1] = f;
#endif
	}

	void Matrix::SetM32(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._43 = f;
#else
		matrix[3][2] = f;
#endif
	}

	void Matrix::SetM33(float f)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._44 = f;
#else
		matrix[3][3] = f;
#endif
	}

	void Matrix::Add(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 += other.GetM00();
		matrix._12 += other.GetM01();
		matrix._13 += other.GetM02();
		matrix._14 += other.GetM03();
		matrix._21 += other.GetM10();
		matrix._22 += other.GetM11();
		matrix._23 += other.GetM12();
		matrix._24 += other.GetM13();
		matrix._31 += other.GetM20();
		matrix._32 += other.GetM21();
		matrix._33 += other.GetM22();
		matrix._34 += other.GetM23();
		matrix._41 += other.GetM30();
		matrix._42 += other.GetM31();
		matrix._43 += other.GetM32();
		matrix._44 += other.GetM33();
#else
		matrix[0][0] += other.GetM00();
		matrix[0][1] += other.GetM01();
		matrix[0][2] += other.GetM02();
		matrix[0][3] += other.GetM03();
		matrix[1][0] += other.GetM10();
		matrix[1][1] += other.GetM11();
		matrix[1][2] += other.GetM12();
		matrix[1][3] += other.GetM13();
		matrix[2][0] += other.GetM20();
		matrix[2][1] += other.GetM21();
		matrix[2][2] += other.GetM22();
		matrix[2][3] += other.GetM23();
		matrix[3][0] += other.GetM30();
		matrix[3][1] += other.GetM31();
		matrix[3][2] += other.GetM32();
		matrix[3][3] += other.GetM33();
#endif
	}

	Matrix Matrix::Add(const Matrix& m1, const Matrix& m2)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m1);
		temp.Add(m2);
		return temp;
	}

	void Matrix::Subtract(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 -= other.GetM00();
		matrix._12 -= other.GetM01();
		matrix._13 -= other.GetM02();
		matrix._14 -= other.GetM03();
		matrix._21 -= other.GetM10();
		matrix._22 -= other.GetM11();
		matrix._23 -= other.GetM12();
		matrix._24 -= other.GetM13();
		matrix._31 -= other.GetM20();
		matrix._32 -= other.GetM21();
		matrix._33 -= other.GetM22();
		matrix._34 -= other.GetM23();
		matrix._41 -= other.GetM30();
		matrix._42 -= other.GetM31();
		matrix._43 -= other.GetM32();
		matrix._44 -= other.GetM33();
#else
		matrix[0][0] -= other.GetM00();
		matrix[0][1] -= other.GetM01();
		matrix[0][2] -= other.GetM02();
		matrix[0][3] -= other.GetM03();
		matrix[1][0] -= other.GetM10();
		matrix[1][1] -= other.GetM11();
		matrix[1][2] -= other.GetM12();
		matrix[1][3] -= other.GetM13();
		matrix[2][0] -= other.GetM20();
		matrix[2][1] -= other.GetM21();
		matrix[2][2] -= other.GetM22();
		matrix[2][3] -= other.GetM23();
		matrix[3][0] -= other.GetM30();
		matrix[3][1] -= other.GetM31();
		matrix[3][2] -= other.GetM32();
		matrix[3][3] -= other.GetM33();
#endif
	}

	Matrix Matrix::Subtract(const Matrix& m1, const Matrix& m2)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m1);
		temp.Subtract(m2);
		return temp;
	}

	void Matrix::Multiply(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4x4(&matrix, (XMMatrixMultiply(ToXMMATRIX(), 
			other.ToXMMATRIX())));
#else
		float* b = other.To2DArray();
		float c[4][4];
		
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				c[i][j] = 0;

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				for (int k = 0; k < 4; k++)
					c[i][j] = c[i][j] + matrix[i][k] * b[k][j];
		
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = c[i][j];

		delete b;
		delete[][] c;
#endif
	}

	Matrix Matrix::Multiply(const Matrix& m1, const Matrix& m2)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m1);
		temp.Multiply(m2);
		return temp;
	}

	void Matrix::Multiply(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 *= scalar;
		matrix._12 *= scalar;
		matrix._13 *= scalar;
		matrix._14 *= scalar;
		matrix._21 *= scalar;
		matrix._22 *= scalar;
		matrix._23 *= scalar;
		matrix._24 *= scalar;
		matrix._31 *= scalar;
		matrix._32 *= scalar;
		matrix._33 *= scalar;
		matrix._34 *= scalar;
		matrix._41 *= scalar;
		matrix._42 *= scalar;
		matrix._43 *= scalar;
		matrix._44 *= scalar;
#else
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] *= scalar;
#endif
	}

	Matrix Matrix::Multiply(const Matrix& m, float scalar)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m);
		temp.Multiply(scalar);
		return temp;
	}

	void Matrix::Divide(float scalar)
		ULTIMA_NOEXCEPT
	{
		Multiply(1.0f / scalar);
	}

	Matrix Matrix::Divide(const Matrix& m, float scalar)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m);
		temp.Multiply(1.0f / scalar);
		return temp;
	}

	void Matrix::Inverse()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMVECTOR v;
		XMMATRIX mat = XMLoadFloat4x4(&matrix);
		XMStoreFloat4x4(&matrix, XMMatrixInverse(&v, mat));
#else
		float inv[4][4];

		for (int i = 0; i < 4; i++) 
		{
			for (int j = 0; j < 4; j++) 
			{
				if (i == j)
					matrix[i][j] = 2;
				else
					matrix[i][j] = 3;
			}
		}

		float S0 = matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        float S1 = matrix[0][0] * matrix[1][2] - matrix[0][2] * matrix[1][0];
        float S2 = matrix[0][0] * matrix[1][3] - matrix[0][3] * matrix[1][0];
        float S3 = matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1];
        float S4 = matrix[0][1] * matrix[1][3] - matrix[0][3] * matrix[1][1];
        float S5 = matrix[0][2] * matrix[1][3] - matrix[0][3] * matrix[1][2];
 
        float C5 = matrix[2][2] * matrix[3][3] - matrix[2][3] * matrix[3][2];
        float C4 = matrix[2][1] * matrix[3][3] - matrix[2][3] * matrix[3][1];
        float C3 = matrix[2][1] * matrix[3][2] - matrix[2][2] * matrix[3][1];
        float C2 = matrix[2][0] * matrix[3][3] - matrix[2][3] * matrix[3][0];
        float C1 = matrix[2][0] * matrix[3][2] - matrix[2][2] * matrix[3][0];
        float C0 = matrix[2][0] * matrix[3][1] - matrix[2][1] * matrix[3][0];
 
        float det = S0 * C5 - S1 * C4 + S2 * C3 + S3 * C2 - S4 * C1 + S5 * C0;
		
		inv[0][0] = matrix[1][1] * C5  - matrix[1][2] * C4  + matrix[1][3] * C3 ;
		inv[0][1] = -matrix[0][1] * C5 + matrix[0][2] * C4  - matrix[0][3] * C3 ;
		inv[0][2] = matrix[3][1] * S5 - matrix[3][2] * S4 + matrix[3][3] * S3 ;
		inv[0][3] = -matrix[2][1] * S5 + matrix[2][2] * S4 - matrix[2][3] * S3 ;

		inv[1][0] = -matrix[1][0] * C5  + matrix[1][2] * C2  - matrix[1][3] * C1 ;
		inv[1][1] = matrix[0][0] * C5 - matrix[0][2] * C2  + matrix[0][3] * C1 ;
		inv[1][2] = -matrix[3][0] * S5 + matrix[3][2] * S2 - matrix[3][3] * S1 ; 
		inv[1][3] = matrix[2][0] * S5 - matrix[2][2] * S2 + matrix[2][3] * S1,

		inv[2][0] = matrix[1][0] * C4  - matrix[1][1] * C2  + matrix[1][3] * C0 ;
		inv[2][1] = -matrix[0][0] * C4 + matrix[0][1] * C2  - matrix[0][3] * C0,
		inv[2][2] = matrix[3][0] * S4 - matrix[3][1] * S2 + matrix[3][3] * S0 ;
		inv[2][3] = -matrix[2][0] * S4 + matrix[2][1] * S2  - matrix[2][3] * S0,

		inv[3][0] = -matrix[1][0] * C3  + matrix[1][1] * C1  - matrix[1][2] * C0 ;
		inv[3][1] = matrix[0][0] * C3 - matrix[0][1] * C1  + matrix[0][2] * C0,
		inv[3][2] = -matrix[3][0] * S3 + matrix[3][1] * S1 - matrix[3][2] * S0 ;
		inv[3][3] = matrix[2][0] * S3 - matrix[2][1] * S1  + matrix[2][2] * S0 ;

		det = 1 / det;

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				matrix[i][j] *= det;
			}
		}
#endif
	}
	
	Matrix Matrix::Inverse(const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m);
		temp.Inverse();
		return temp;
	}

	void Matrix::Negate()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		matrix._11 *= matrix._11;
		matrix._12 *= matrix._12;
		matrix._13 *= matrix._13;
		matrix._14 *= matrix._14;
		matrix._21 *= matrix._21;
		matrix._22 *= matrix._22;
		matrix._23 *= matrix._23;
		matrix._24 *= matrix._24;
		matrix._31 *= matrix._31;
		matrix._32 *= matrix._32;
		matrix._33 *= matrix._33;
		matrix._34 *= matrix._34;
		matrix._41 *= matrix._41;
		matrix._42 *= matrix._42;
		matrix._43 *= matrix._43;
		matrix._44 *= matrix._44;
#else
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = -matrix[i][j];
#endif
	}

	Matrix Matrix::Negate(const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m);
		temp.Negate();
		return temp;
	}

	void Matrix::Transpose()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		std::swap(matrix._12, matrix._21);
		std::swap(matrix._13, matrix._31);
		std::swap(matrix._14, matrix._41);

		std::swap(matrix._23, matrix._32);
		std::swap(matrix._24, matrix._42);

		std::swap(matrix._34, matrix._43);
#else
		std::swap(matrix[0][1], matrix[1][0]);
		std::swap(matrix[0][2], matrix[2][0]);
		std::swap(matrix[0][3], matrix[3][0]);

		std::swap(matrix[1][2], matrix[2][1]);
		std::swap(matrix[1][3], matrix[3][1]);

		std::swap(matrix[2][3], matrix[3][2]);
#endif
	}

	Matrix Matrix::Transpose(const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = std::move(m);
		temp.Transpose();
		return temp;
	}

	void Matrix::InverseTranspose()
		ULTIMA_NOEXCEPT
	{
		SetM30(0.0f);
		SetM31(0.0f);
		SetM32(0.0f);
		SetM33(1.0f);
		Inverse();
		Transpose();
	}

	Matrix Matrix::InverseTranspose(const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		Matrix temp = m;
		temp.InverseTranspose();
		return temp;
	}

	Matrix& Matrix::operator+=(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
		this->Add(other);
		return *this;
	}

	Matrix Matrix::operator+(const Matrix& other) 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Add(other);
		return m;
	}

	Matrix& Matrix::operator-=(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(other);
		return *this;
	}

	Matrix Matrix::operator-(const Matrix& other) 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Subtract(other);
		return m;
	}

	Matrix Matrix::operator-() 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Negate();
		return m;
	}

	Matrix& Matrix::operator*=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(scalar);
		return *this;
	}

	Matrix Matrix::operator*(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Multiply(scalar);
		return m;
	}
	
	Matrix& Matrix::operator*=(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(other);
		return *this;
	}

	Matrix Matrix::operator*(const Matrix& other) 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Multiply(other);
		return m;
	}
	
	Matrix& Matrix::operator/=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(1.0f / scalar);
		return *this;
	}

	Matrix Matrix::operator/(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Matrix m = std::move(*this);
		m.Multiply(1.0f / scalar);
		return m;
	}
	
	Matrix& Matrix::operator=(const Matrix& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			XMStoreFloat4x4(&matrix, other.ToXMMATRIX());
#else
			matrix[0][0] = other.GetM00();
			matrix[0][1] = other.GetM01();
			matrix[0][2] = other.GetM02();
			matrix[0][3] = other.GetM03();
			matrix[1][0] = other.GetM10();
			matrix[1][1] = other.GetM11();
			matrix[1][2] = other.GetM12();
			matrix[1][3] = other.GetM13();
			matrix[2][0] = other.GetM20();
			matrix[2][1] = other.GetM21();
			matrix[2][2] = other.GetM22();
			matrix[2][3] = other.GetM23();
			matrix[3][0] = other.GetM30();
			matrix[3][1] = other.GetM31();
			matrix[3][2] = other.GetM32();
			matrix[3][3] = other.GetM33();
#endif
		}
		return *this;
	}

	Matrix& Matrix::operator=(Matrix&& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			matrix = other.ToXMFLOAT4X4();
			other.SetXMFLOAT4X4(XMFLOAT4X4());
#else
			matrix[0][0] = other.GetM00();
			matrix[0][1] = other.GetM01();
			matrix[0][2] = other.GetM02();
			matrix[0][3] = other.GetM03();
			matrix[1][0] = other.GetM10();
			matrix[1][1] = other.GetM11();
			matrix[1][2] = other.GetM12();
			matrix[1][3] = other.GetM13();
			matrix[2][0] = other.GetM20();
			matrix[2][1] = other.GetM21();
			matrix[2][2] = other.GetM22();
			matrix[2][3] = other.GetM23();
			matrix[3][0] = other.GetM30();
			matrix[3][1] = other.GetM31();
			matrix[3][2] = other.GetM32();
			matrix[3][3] = other.GetM33();

			other.SetM00(0);
			other.SetM01(0);
			other.SetM02(0);
			other.SetM03(0);
			other.SetM10(0);
			other.SetM11(0);
			other.SetM12(0);
			other.SetM13(0);
			other.SetM20(0);
			other.SetM21(0);
			other.SetM22(0);
			other.SetM23(0);
			other.SetM30(0);
			other.SetM31(0);
			other.SetM32(0);
			other.SetM33(0);
#endif
		}
		return *this;
	}

	bool Matrix::operator==(const Matrix& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (matrix._11 != other.GetM00())
			return false;
		else if (matrix._12 != other.GetM01())
			return false;
		else if (matrix._13 != other.GetM02())
			return false;
		else if (matrix._14 != other.GetM03())
			return false;
		else if (matrix._21 != other.GetM10())
			return false;
		else if (matrix._22 != other.GetM11())
			return false;
		else if (matrix._23 != other.GetM12())
			return false;
		else if (matrix._24 != other.GetM13())
			return false;
		else if (matrix._31 != other.GetM20())
			return false;
		else if (matrix._32 != other.GetM21())
			return false;
		else if (matrix._33 != other.GetM22())
			return false;
		else if (matrix._34 != other.GetM23())
			return false;
		else if (matrix._41 != other.GetM30())
			return false;
		else if (matrix._42 != other.GetM31())
			return false;
		else if (matrix._43 != other.GetM32())
			return false;
		else if (matrix._44 != other.GetM33())
			return false;
#else
		if (matrix[0][0] != other.GetM00())
			return false;
		else if (matrix[0][1] != other.GetM01())
			return false;
		else if (matrix[0][2] != other.GetM02())
			return false;
		else if (matrix[0][3] != other.GetM03())
			return false;
		else if (matrix[1][0] != other.GetM10())
			return false;
		else if (matrix[1][1] != other.GetM11())
			return false;
		else if (matrix[1][2] != other.GetM12())
			return false;
		else if (matrix[1][3] != other.GetM13())
			return false;
		else if (matrix[2][0] != other.GetM20())
			return false;
		else if (matrix[2][1] != other.GetM21())
			return false;
		else if (matrix[2][2] != other.GetM22())
			return false;
		else if (matrix[2][3] != other.GetM23())
			return false;
		else if (matrix[3][0] != other.GetM30())
			return false;
		else if (matrix[3][1] != other.GetM31())
			return false;
		else if (matrix[3][2] != other.GetM32())
			return false;
		else if (matrix[3][3] != other.GetM33())
			return false;
#endif
		else
			return true;
	}

	bool Matrix::operator!=(const Matrix& other) 
		const ULTIMA_NOEXCEPT
	{
		return !(this == &other);
	}
	
	const Matrix Matrix::Zero = Matrix(0.0f);

	const Matrix Matrix::Identity = Matrix(1.0f, 0.0f);

#if DIRECTX
	void Matrix::SetXMFLOAT4X4(const XMFLOAT4X4& xm)
		ULTIMA_NOEXCEPT
	{
		matrix = std::move(xm);
	}

	XMMATRIX Matrix::ToXMMATRIX() 
		const ULTIMA_NOEXCEPT
	{
		return XMLoadFloat4x4(&matrix);
	}

	XMFLOAT4X4 Matrix::ToXMFLOAT4X4() 
		const ULTIMA_NOEXCEPT
	{
		return matrix;
	}

	Matrix::Matrix(const XMFLOAT4X4& m)
		ULTIMA_NOEXCEPT
	{
		matrix = std::move(m);
	}
#else
	//TODO: Find a cleaner solution, this looks weak.
	float* Matrix::To1DArray()
		ULTIMA_NOEXCEPT
	{
		static float f[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				f[i * 4 + j] = matrix[i][j];
	}

	float* Matrix::To2DArray()
		ULTIMA_NOEXCEPT
	{
		static float f[4][4] { {0, 0, 0, 0} , {0, 0, 0, 0} , 
		{0, 0, 0, 0} , {0, 0, 0, 0} };

		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				f[i][j] = matrix[i][j];
	}
	
	void Matrix::From1DArray(float f[16])
		ULTIMA_NOEXCEPT
	{
		for (int i = 0; i < 16; i++)
			matrix[static_cast<int>(i / 4)][i % 4] = f[i];
	}

	void Matrix::From2DArray(float f[][4])
		ULTIMA_NOEXCEPT
	{
		matrix = f;
	}

#endif
}