#include "stdafx.h"

#include "Vector3.h"

#include "MathExtensions.h"
#include "Matrix.h"
#include "Quaternion.h"

namespace Ultima
{
	Vector3::Vector3()
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT3(1, 1, 1);
#else
		x = 1;
		y = 1;
		z = 1;
#endif
	}

	Vector3::Vector3(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
		*this = other;
	}

	Vector3::Vector3(Vector3&& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Vector3::Vector3(float x, float y, float z) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector = XMFLOAT3(x, y, z);
#else
		this->x = x;
		this->y = y;
		this->z = z;
#endif
	}

	Vector3::Vector3(int x, int y, int z) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector = XMFLOAT3(static_cast<float>(x), static_cast<float>(y), 
			static_cast<float>(z));
#else
		this->x = static_cast<float>(x);
		this->y = static_cast<float>(y);
		this->z = static_cast<float>(z);
#endif
	}

	Vector3::Vector3(float value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT3(value, value, value);
#else
		x = value;
		y = value;
		z = value;
#endif
	}

	Vector3::Vector3(int value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		vector = XMFLOAT3(static_cast<float>(value), static_cast<float>(value), 
			static_cast<float>(value));
#else
		x = static_cast<float>(value);
		y = static_cast<float>(value);
		z = static_cast<float>(value);
#endif
	}

	Vector3::Vector3(const Quaternion& q)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector = XMFLOAT3(2 * (q.GetX() * q.GetZ() + q.GetW() * q.GetY()),
			2 * (q.GetY() * q.GetX() - q.GetW() * q.GetX()),
			1 - 2 * (q.GetX() * q.GetX() + q.GetY() * q.GetY()));
#else
		x = 2 * (q.GetX() * q.GetZ() + q.GetW() * q.GetY());
		y = 2 * (q.GetY() * q.GetX() - q.GetW() * q.GetX());
		z = 1 - 2 * (q.GetX() * q.GetX() + q.GetY() * q.GetY());
#endif
	}

#if DIRECTX
	Vector3::Vector3(const XMFLOAT3& xm)
		ULTIMA_NOEXCEPT
	{
		this->SetXMFLOAT3(xm);
	}
#endif

	Vector3 Vector3::CreateNormal(
		const Vector3& v1,
		const Vector3& v2, 
		const Vector3& v3)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp;
		temp = Vector3::Cross(v2 - v1, v3 - v2);
		temp.Normalize();
		return temp;
	}

	Vector3::~Vector3()
		ULTIMA_NOEXCEPT
	{ }

	float Vector3::GetX() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return vector.x;
#else
		return x;
#endif
	}

	float Vector3::GetY() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return vector.y;
#else
		return y;
#endif
	}

	float Vector3::GetZ() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return vector.z;
#else
		return z;
#endif
	}

	float Vector3::GetLength() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return MathExtensions::SquareRootFast(pow(vector.x, 2) + pow(vector.y, 2)
			+ pow(vector.z, 2));
#else
		return Math::SquareRootFast(pow(x, 2) + pow(y, 2) + pow(z, 2));
#endif
	}

	void Vector3::SetX(float x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x = x;
#else
		this->x = x;
#endif
	}

	void Vector3::SetX(int x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x = static_cast<float>(x);
#else
		this->x = static_cast<float>(x);
#endif
	}

	void Vector3::SetY(float y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.y = y;
#else
		this->y = y;
#endif
	}

	void Vector3::SetY(int y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.y = static_cast<float>(y);
#else
		this->y = static_cast<float>(y);
#endif
	}

	void Vector3::SetZ(float z) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.z = z;
#else
		this->z = z;
#endif
	}

	void Vector3::SetZ(int z)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.z = static_cast<float>(z);
#else
		this->z = static_cast<float>(z);
#endif
	}

	void Vector3::SetLength(float l)
		ULTIMA_NOEXCEPT
	{
		Normalize();
		Multiply(l);
	}

	void Vector3::Add(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x += other.GetX();
		vector.y += other.GetY();
		vector.z += other.GetZ();
#else
		x += other.GetX();
		y += other.GetY();
		z += other.GetZ();
#endif
	}
	
	Vector3 Vector3::Add(const Vector3& v1, const Vector3& v2)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v1);
		temp.Add(v2);
		return temp;
	}

	void Vector3::Subtract(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x -= other.GetX();
		vector.y -= other.GetY();
		vector.z -= other.GetZ();
#else
		x -= other.GetX();
		y -= other.GetY();
		z -= other.GetZ();
#endif
	}

	Vector3 Vector3::Subtract(const Vector3& v1, const Vector3& v2)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v1);
		temp.Subtract(v2);
		return temp;
	}

	void Vector3::Multiply(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		vector.x *= scalar;
		vector.y *= scalar;
		vector.z *= scalar;
#else
		x *= scalar;
		y *= scalar;
		z *= scalar;
#endif
	}
	
	Vector3 Vector3::Multiply(const Vector3& v, float scalar)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v);
		temp.Multiply(scalar);
		return temp;
	}

	void Vector3::Divide(float scalar)
		ULTIMA_NOEXCEPT
	{
		scalar = 1.0f / scalar;
#if DIRECTX
		vector.x *= scalar;
		vector.y *= scalar;
		vector.z *= scalar;
#else
		x *= scalar;
		y *= scalar;
		z *= scalar;
#endif
	}

	Vector3 Vector3::Divide(const Vector3& v, float scalar)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v);
		temp.Divide(scalar);
		return temp;
	}
	
	void Vector3::Normalize()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		float inverseLength = MathExtensions::InverseSquareRootFast(
			pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
		vector.x *= inverseLength;
		vector.y *= inverseLength;
		vector.z *= inverseLength;
#else
		float inverseLength = MathExtensions::InverseSquareRootFast(
			pow(x, 2) + pow(y, 2) + pow(z, 2));
		x *= inverseLength;
		y *= inverseLength;
		z *= inverseLength;
#endif
	}

	Vector3 Vector3::Normalize(const Vector3& v)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v);
		temp.Normalize();
		return temp;
	}

	float Vector3::DistanceTo(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return MathExtensions::SquareRootFast(
			(vector.x - other.GetX()) * (vector.x - other.GetX()) +
			(vector.y - other.GetY()) * (vector.y - other.GetY()) +
			(vector.z - other.GetZ()) * (vector.z - other.GetZ()));
#else
		return MathExtensions::SquareRootFast(
			(this->x - other.GetX()) * (this->x - other.GetX()) +
			(this->y - other.GetY()) * (this->y - other.GetY()) +
			(this->z - other.GetZ()) * (this->z - other.GetZ()));
#endif
	}

	float Vector3::Dot(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return vector.x * other.GetX() 
			+ vector.y * other.GetY() 
			+ vector.z * other.GetZ();
#else
		return this->x * other.GetX() 
			+ this->y * other.GetY() 
			+ this->z * other.GetZ();
#endif
	}
	
	float Vector3::Dot(const Vector3& v1, const Vector3& v2)
		ULTIMA_NOEXCEPT
	{
		return v1.Dot(v2);
	}

	Vector3 Vector3::Cross(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMVECTOR temp = XMVector3Cross(this->ToXMVECTOR(), other.ToXMVECTOR());
		return Vector3(temp);
#else
		return Vector3(this->y * other.GetZ() - this->z * other.GetY(),
			this->z * other.GetX() - this->x * other.GetZ(),
			this->x * other.GetY() - this->y * other.GetX());
#endif
	}

	Vector3 Vector3::Cross(const Vector3& v1, const Vector3& v2)
		ULTIMA_NOEXCEPT
	{
		return v1.Cross(v2);
	}

	void Vector3::Lerp(const Vector3& other, float amount)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat3(&vector, (XMVectorLerp(this->ToXMVECTOR(), 
			other.ToXMVECTOR(), amount)));
#else
		Add(Vector3::Multiply((other - *this), amount));
#endif
	}

	Vector3 Vector3::Lerp(
		const Vector3& v1, 
		const Vector3& v2, 
		float amount)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v1);
		temp.Lerp(v2, amount);
		return temp;
	}

	void Vector3::Transform(const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		float tempX, tempY, tempZ, tempW;
#if DIRECTX
		tempX = vector.x * m.GetM00() 
			+ vector.y * m.GetM10() 
			+ vector.z * m.GetM20() 
			+ m.GetM30();
		tempY = vector.x * m.GetM01() 
			+ vector.y * m.GetM11() 
			+ vector.z * m.GetM21() 
			+ m.GetM31();
		tempZ = vector.x * m.GetM02() 
			+ vector.y * m.GetM12() 
			+ vector.z * m.GetM22() 
			+ m.GetM32();
		tempW = vector.x * m.GetM03() 
			+ vector.y * m.GetM13() 
			+ vector.z * m.GetM23() 
			+ m.GetM33();
		tempW = 1 / tempW;
		vector.x = tempX * tempW;
		vector.y = tempY * tempW;
		vector.z = tempZ * tempW;
#else
		tempX = x * m.GetM00() + y * m.GetM10() + z * m.GetM20() + m.GetM30();
		tempY = x * m.GetM01() + y * m.GetM11() + z * m.GetM21() + m.GetM31();
		tempZ = x * m.GetM02() + y * m.GetM12() + z * m.GetM22() + m.GetM32();
		tempW = x * m.GetM03() + y * m.GetM13() + z * m.GetM23() + m.GetM33();
		tempW = 1 / tempW;
		x = tempX * tempW;
		y = tempY * tempW;
		z = tempZ * tempW;
#endif
	}

	Vector3 Vector3::Transform(const Vector3& v, const Matrix& m)
		ULTIMA_NOEXCEPT
	{
		Vector3 temp = std::move(v);
		temp.Transform(m);
		return temp;
	}

	Vector3& Vector3::operator+=(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
		this->Add(other);
		return *this;
	}

	Vector3 Vector3::operator+(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
		Vector3 v = std::move(*this);
		v.Add(other);
		return v;
	}

	Vector3& Vector3::operator-=(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(other);
		return *this;
	}

	Vector3 Vector3::operator-(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
		Vector3 v = std::move(*this);
		v.Subtract(other);
		return v;
	}

	Vector3 Vector3::operator-() 
		const ULTIMA_NOEXCEPT
	{
		Vector3 v = std::move(*this);
		v.SetX(-v.GetX());
		v.SetY(-v.GetY());
		v.SetZ(-v.GetZ());
		return v;
	}

	Vector3& Vector3::operator*=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(scalar);
		return *this;
	}

	Vector3 Vector3::operator*(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Vector3 v = std::move(*this);
		v.Multiply(scalar);
		return v;
	}

	Vector3& Vector3::operator/=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Divide(scalar);
		return *this;
	}

	Vector3 Vector3::operator/(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Vector3 v = std::move(*this);
		v.Divide(scalar);
		return v;
	}

	Vector3& Vector3::operator=(const Vector3& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			this->vector = other.ToXMFLOAT3();
#else
			this->x = other.GetX();
			this->y = other.GetY();
			this->z = other.GetZ();
#endif
		}
		return *this;
	}

	Vector3& Vector3::operator=(Vector3&& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			vector = other.ToXMFLOAT3();
			other.SetXMFLOAT3(XMFLOAT3());
#else
			x = other.GetX();
			y = other.GetY();
			z = other.GetZ();
			other.SetX(0);
			other.SetY(0);
			other.SetZ(0);
#endif
		}
		return *this;
	}

	bool Vector3::operator==(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (vector.x != other.GetX())
			return false;
		else if (vector.y != other.GetY())
			return false;
		else if (vector.z != other.GetZ())
			return false;
#else
		if (x != other.GetX())
			return false;
		else if (y != other.GetY())
			return false;
		else if (z != other.GetZ())
			return false;
#endif
		else
			return true;
	}
	
	bool Vector3::operator!=(const Vector3& other) 
		const ULTIMA_NOEXCEPT
	{
		return !(this == &other);
	}

	const Vector3 Vector3::Zero = Vector3(0.0f, 0.0f, 0.0f);

	const Vector3 Vector3::One = Vector3(1.0f, 1.0f, 1.0f);

	const Vector3 Vector3::Forward = Vector3(0, 0, 1);

	const Vector3 Vector3::Backward = Vector3(0, 0, -1);

	const Vector3 Vector3::Right = Vector3(1, 0, 0);

	const Vector3 Vector3::Left = Vector3(-1, 0, 0);

	const Vector3 Vector3::Up = Vector3(0, 1, 0);

	const Vector3 Vector3::Down = Vector3(0, -1, 0);

#if DIRECTX
	void Vector3::SetXMFLOAT3(const XMFLOAT3& xm)
		ULTIMA_NOEXCEPT
	{
		vector = std::move(xm);
	}

	XMFLOAT3 Vector3::ToXMFLOAT3() 
		const ULTIMA_NOEXCEPT
	{
		return vector;
	}

	XMVECTOR Vector3::ToXMVECTOR() 
		const ULTIMA_NOEXCEPT
	{
		return XMLoadFloat3(&vector);
	}

	Vector3::Vector3(XMVECTOR v)
		ULTIMA_NOEXCEPT
	{
		XMStoreFloat3(&vector, v);
	}
#endif
}