#pragma once

#include "Macros.h"

namespace Ultima
{
	class Matrix;
	class Vector3;

	/*!
	Defines a 4 dimensional vector.
	*/
	class Quaternion
	{
	public:
		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion()
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(const Quaternion& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion using a move.
		*/
		ULTIMAMATH_API Quaternion(Quaternion&& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(float x, float y, float z, float w)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(int x, int y, int z, int w)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(float value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(int value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a quaternion from three vectors.
		*/
		ULTIMAMATH_API static Quaternion CreateFromVectors(
			const Vector3& source,
			const Vector3& destination, 
			const Vector3& up)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a quaternion from an axis and an angle.
		*/
		ULTIMAMATH_API static Quaternion CreateFromAxisAngle(
			const Vector3& axis, 
			float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a quaternion using yaw, pitch and roll values.
		*/
		ULTIMAMATH_API static Quaternion CreateFromPitchYawRoll(
			float pitch, 
			float yaw, 
			float roll)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a quaternion from a rotation matrix.
		*/
		ULTIMAMATH_API static Quaternion CreateFromRotationMatrix(
			const Matrix& rotation)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual ~Quaternion()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the X coordinate.
		*/
		ULTIMAMATH_API virtual float GetX()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Y coordinate.
		*/
		ULTIMAMATH_API virtual float GetY()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the Z coordinate.
		*/
		ULTIMAMATH_API virtual float GetZ()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the W coordinate.
		*/
		ULTIMAMATH_API virtual float GetW()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the length of the vector.
		*/
		ULTIMAMATH_API virtual float GetLength()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the X coordinate.
		*/
		ULTIMAMATH_API virtual void SetX(int x)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(float y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Y coordinate.
		*/
		ULTIMAMATH_API virtual void SetY(int y)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Z coordinate.
		*/
		ULTIMAMATH_API virtual void SetZ(float z)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the Z coordinate.
		*/
		ULTIMAMATH_API virtual void SetZ(int z)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the W coordinate.
		*/
		ULTIMAMATH_API virtual void SetW(float w)
			ULTIMA_NOEXCEPT;

		/*!
		Sets the W coordinate.
		*/
		ULTIMAMATH_API virtual void SetW(int w)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two quaternions together.
		*/
		ULTIMAMATH_API virtual void Add(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Adds two quaternions together.
		*/
		ULTIMAMATH_API static Quaternion Add(
			const Quaternion& q1, 
			const Quaternion& q2)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a quaternion from another.
		*/
		ULTIMAMATH_API virtual void Subtract(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		
		/*!
		Subtracts a quaternion from another.
		*/
		ULTIMAMATH_API static Quaternion Subtract(
			const Quaternion& q1, 
			const Quaternion& q2)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a quaternion by a scalar.
		*/
		ULTIMAMATH_API virtual void Multiply(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Multiplies a quaternion by a scalar.
		*/
		ULTIMAMATH_API static Quaternion Multiply(
			const Quaternion& q, 
			float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a quaternion by another quaternion.
		*/
		ULTIMAMATH_API virtual void Multiply(
			const Quaternion& other)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a quaternion by another quaternion.
		*/
		ULTIMAMATH_API static Quaternion Multiply(
			const Quaternion& q1, 
			const Quaternion& q2)
			ULTIMA_NOEXCEPT;
		
		/*!
		Divides a quaternion by a scalar.
		*/
		ULTIMAMATH_API virtual void Divide(float scalar)
			ULTIMA_NOEXCEPT;
		
		/*!
		Divides a quaternion by a scalar.
		*/
		ULTIMAMATH_API static Quaternion Divide(const Quaternion& q, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Normalizes the quaternion.
		*/
		ULTIMAMATH_API virtual void Normalize()
			ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two quaternions.
		*/
		ULTIMAMATH_API virtual float Dot(const Quaternion& other)
			const ULTIMA_NOEXCEPT;

		/*!
		Calculates the dot product between two quaternions.
		*/
		ULTIMAMATH_API static float Dot(const Quaternion& q1, const Quaternion& q2)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two quaternions.
		*/
		ULTIMAMATH_API virtual void Lerp(const Quaternion& other, float amount)
			ULTIMA_NOEXCEPT;

		/*!
		Performs a linear interpolation between two quaternions.
		*/
		ULTIMAMATH_API static Quaternion Lerp(
			const Quaternion& q1, 
			const Quaternion& q2,
			float amount)
			ULTIMA_NOEXCEPT;
		
		/*!
		Concatenates two quaternions together.
		*/
		ULTIMAMATH_API virtual void Concatenate(const Quaternion& other)
			ULTIMA_NOEXCEPT;

		/*!
		Concatenates two quaternions together.
		*/
		ULTIMAMATH_API static Quaternion Concatenate(
			const Quaternion& q1, 
			const Quaternion& q2)
			ULTIMA_NOEXCEPT;

		/*!
		Conjugates the quaternion.
		*/
		ULTIMAMATH_API virtual void Conjugate()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the conjugate of a quaternion.
		*/
		ULTIMAMATH_API static Quaternion Conjugate(const Quaternion& q)
			ULTIMA_NOEXCEPT;

		/*!
		Inverts the quaternion.
		*/
		ULTIMAMATH_API virtual void Inverse()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the inverse of a quaternion.
		*/
		ULTIMAMATH_API static Quaternion Inverse(const Quaternion& q)
			ULTIMA_NOEXCEPT;

		/*!
		Negates the quaternion.
		*/
		ULTIMAMATH_API virtual void Negate()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the negate of a quaternion.
		*/
		ULTIMAMATH_API static Quaternion Negate(const Quaternion& q)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual Quaternion& operator+=(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator+(const Quaternion& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator-=(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator-(const Quaternion& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator-()
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator*=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator*(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator*=(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator*(const Quaternion& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator/=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion operator/(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator=(const Quaternion& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Quaternion& operator=(Quaternion&& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator==(const Quaternion& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator!=(const Quaternion& other)
			const ULTIMA_NOEXCEPT;
		
		/*!
		The Quaternion object with all of its components set to zero;
		*/
		ULTIMAMATH_API static const Quaternion Zero;

		/*!
		The quaternion representing no rotation.
		*/
		ULTIMAMATH_API static const Quaternion Identity;

#if DIRECTX

		/*!
		Loads quaternion data from a given XMFLOAT4.
		*/
		ULTIMAMATH_API void SetXMFLOAT4(const XMFLOAT4& xm)
			ULTIMA_NOEXCEPT;

		/*!
		Returns an XMFLOAT4 holding the quaternion data.
		*/
		ULTIMAMATH_API virtual XMFLOAT4 ToXMFLOAT4()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns an XMVECTOR holding the quaternion data.
		*/
		ULTIMAMATH_API virtual XMVECTOR ToXMVECTOR()
			const ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Quaternion.
		*/
		ULTIMAMATH_API Quaternion(XMVECTOR v)
			ULTIMA_NOEXCEPT;

#endif

	protected:
#if DIRECTX
		XMFLOAT4 quat;
#else
		float x;
		float y;
		float z;
		float w;
#endif
	};
}