#include "stdafx.h"

#include "Quaternion.h"

#include "MathExtensions.h"
#include "Matrix.h"
#include "Vector3.h"

namespace Ultima
{
	Quaternion::Quaternion()
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		quat = XMFLOAT4(0, 0, 0, 1);
#else
		x = 0;
		y = 0;
		z = 0;
		w = 1;
#endif
	}

	Quaternion::Quaternion(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		*this = other;
	}

	Quaternion::Quaternion(Quaternion&& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	Quaternion::Quaternion(float x, float y, float z, float w) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat = XMFLOAT4(x, y, z, w);
#else
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
#endif
		Normalize();
	}

	Quaternion::Quaternion(int x, int y, int z, int w) 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat = XMFLOAT4(static_cast<float>(x), static_cast<float>(y), 
			static_cast<float>(z), static_cast<float>(w));
#else
		this->x = static_cast<float>(x);
		this->y = static_cast<float>(y);
		this->z = static_cast<float>(z);
		this->w = static_cast<float>(w);
#endif
		Normalize();
	}

	Quaternion::Quaternion(float value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		quat = XMFLOAT4(value, value, value, value);
#else
		x = value;
		y = value;
		z = value;
		w = value;
#endif
		Normalize();
	}

	Quaternion::Quaternion(int value)
		ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		quat = XMFLOAT4(static_cast<float>(value), static_cast<float>(value), 
			static_cast<float>(value), static_cast<float>(value));
#else
		x = static_cast<float>(value);
		y = static_cast<float>(value);
		z = static_cast<float>(value);
		w = static_cast<float>(value);
#endif
		Normalize();
	}

	Quaternion Quaternion::CreateFromVectors(const Vector3& source, 
		const Vector3& destination, const Vector3& up)
		ULTIMA_NOEXCEPT
	{
		float dot = Vector3::Dot(source, destination);

		if (std::abs(dot - (-1.0f)) < 0.000001f)
			return CreateFromAxisAngle(up, 1);
		else if (std::abs(dot - (1.0f)) < 0.000001f)
			return Quaternion::Identity;
		else
		{
			float rotAngle = static_cast<float>(std::abs(dot));

			Vector3 rotAxis = Vector3::Cross(source, destination);
			rotAxis.Normalize();

			return CreateFromAxisAngle(rotAxis, rotAngle);
		}
	}

	Quaternion Quaternion::CreateFromAxisAngle(const Vector3& axis, float radians)
		ULTIMA_NOEXCEPT
	{
		Quaternion q;
		radians = radians / 2.0f;
		double sinA = sin(radians);
		q.SetX(static_cast<float>(axis.GetX() / sinA));
		q.SetY(static_cast<float>(axis.GetY() / sinA));
		q.SetZ(static_cast<float>(axis.GetZ() / sinA));
		q.SetW(static_cast<float>(cos(radians)));

		q.Normalize();
		return q;
	}

	Quaternion Quaternion::CreateFromPitchYawRoll(
		float pitch, 
		float yaw, 
		float roll)
		ULTIMA_NOEXCEPT
	{
		Quaternion q;

		float sinYaw(sin(yaw * 0.5f));
		float cosYaw(cos(yaw * 0.5f));
		float sinPitch(sin(pitch * 0.5f));
		float cosPitch(cos(pitch * 0.5f));
		float sinRoll(sin(roll * 0.5f));
		float cosRoll(cos(roll * 0.5f));
		float cosPitchCosYaw(cosPitch * cosYaw);
		float sinPitchSinYaw(sinPitch * sinYaw);

		q.SetX(cosRoll * sinPitch * cosYaw + sinRoll * cosPitch * sinYaw);
		q.SetY(cosRoll * cosPitch * sinYaw - sinRoll * sinPitch * cosYaw);
		q.SetZ(sinRoll * cosPitchCosYaw - cosRoll * sinPitchSinYaw);
		q.SetW(cosRoll * cosPitchCosYaw + sinRoll * sinPitchSinYaw);

		q.Normalize();
		return q;
	}

	Quaternion Quaternion::CreateFromRotationMatrix(const Matrix& rotation)
		ULTIMA_NOEXCEPT
	{
		Quaternion q;
		float t = rotation.GetTrace();
		if (t > 1)
		{
			float s = 0.5f * MathExtensions::InverseSquareRootFast(t);

			q.SetX((rotation.GetM21() - rotation.GetM12()) * s);
			q.SetY((rotation.GetM02() - rotation.GetM20()) * s);
			q.SetZ((rotation.GetM10() - rotation.GetM01()) * s);
			q.SetW(0.25f * s);
		}
		else
		{
			if (rotation.GetM00() > rotation.GetM11()
				&& rotation.GetM00() > rotation.GetM22())
			{
				float s = MathExtensions::SquareRootFast(
					rotation.GetM33() + rotation.GetM00() 
					- rotation.GetM11() - rotation.GetM22()) * 2;
				s = 1 / s;

				q.SetX(0.5f * s);
				q.SetY((rotation.GetM01() + rotation.GetM10()) * s);
				q.SetZ((rotation.GetM02() + rotation.GetM20()) * s);
				q.SetW((rotation.GetM12() + rotation.GetM21()) * s);
			}
			else if (rotation.GetM11() > rotation.GetM22())
			{
				float s = MathExtensions::SquareRootFast(
					rotation.GetM33() + rotation.GetM11() 
					- rotation.GetM00() - rotation.GetM22()) * 2;
				s = 1 / s;

				q.SetX((rotation.GetM01() + rotation.GetM10()) * s);
				q.SetY(0.5f * s);
				q.SetZ((rotation.GetM12() + rotation.GetM21()) * s);
				q.SetW((rotation.GetM02() + rotation.GetM20()) * s);
			}
			else
			{
				float s = MathExtensions::SquareRootFast(
					rotation.GetM33() + rotation.GetM33() 
					- rotation.GetM00() - rotation.GetM11()) * 2;
				s = 1 / s;

				q.SetX((rotation.GetM02() + rotation.GetM20()) * s);
				q.SetY((rotation.GetM12() + rotation.GetM21()) * s);
				q.SetZ(0.5f * s);
				q.SetW((rotation.GetM01() + rotation.GetM10()) * s);
			}
		}

		return q;
	}

	Quaternion::~Quaternion() 
		ULTIMA_NOEXCEPT
	{ }

	float Quaternion::GetX() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return quat.x;
#else
		return x;
#endif
	}

	float Quaternion::GetY() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return quat.y;
#else
		return y;
#endif
	}

	float Quaternion::GetZ() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return quat.z;
#else
		return z;
#endif
	}
	
	float Quaternion::GetW() 
		const ULTIMA_NOEXCEPT
	{ 
#if DIRECTX
		return quat.w;
#else
		return w;
#endif
	}

	float Quaternion::GetLength() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMVECTOR temp = XMQuaternionLength(ToXMVECTOR());
		float f;
		XMStoreFloat(&f, temp);
		return f;
#else
		return FastSquareRoot(pow(x, 2) + pow(y, 2) + pow(z, 2) + pow(w, 2));
#endif
	}

	void Quaternion::SetX(float x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.x = x;
#else
		this->x = x;
#endif
		Normalize();
	}

	void Quaternion::SetX(int x)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.x = static_cast<float>(x);
#else
		this->x = static_cast<float>(x);
#endif
		Normalize();
	}

	void Quaternion::SetY(float y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.y = y;
#else
		this->y = y;
#endif
		Normalize();
	}

	void Quaternion::SetY(int y)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.y = static_cast<float>(y);
#else
		this->y = static_cast<float>(y);
#endif
		Normalize();
	}

	void Quaternion::SetZ(float z)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.z = z;
#else
		this->z = z;
#endif
		Normalize();
	}

	void Quaternion::SetZ(int z)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.z = static_cast<float>(z);
#else
		this->z = static_cast<float>(z);
#endif
		Normalize();
	}
	
	void Quaternion::SetW(float w)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.w = w;
#else
		this->w = w;
#endif
		Normalize();
	}

	void Quaternion::SetW(int w)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.w = static_cast<float>(w);
#else
		this->w = static_cast<float>(w);
#endif
		Normalize();
	}

	void Quaternion::Add(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMVectorAdd(this->ToXMVECTOR(), 
			other.ToXMVECTOR())));
#else
		x += other.GetX();
		y += other.GetY();
		z += other.GetZ();
		w += other.GetW();
#endif
	}

	Quaternion Quaternion::Add(const Quaternion& q1, const Quaternion& q2)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q2);
		temp.Add(q1);
		return temp;
	}

	void Quaternion::Subtract(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMVectorSubtract(this->ToXMVECTOR(), 
			other.ToXMVECTOR())));
#else
		x -= other.GetX();
		y -= other.GetY();
		z -= other.GetZ();
		w -= other.GetW();
#endif
	}

	Quaternion Quaternion::Subtract(const Quaternion& q1, const Quaternion& q2)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q1);
		temp.Subtract(q2);
		return temp;
	}

	void Quaternion::Multiply(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMVectorScale(ToXMVECTOR(), scalar)));
#else
		x *= scalar;
		y *= scalar;
		z *= scalar;
		w *= scalar;
#endif
	}

	Quaternion Quaternion::Multiply(const Quaternion& q, float scalar)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q);
		temp.Multiply(scalar);
		return temp;
	}

	void Quaternion::Multiply(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		float x2 = other.GetX();
		float y2 = other.GetY();
		float z2 = other.GetZ();
		float w2 = other.GetW();

#if DIRECTX
		float w1 = quat.w;
		float x1 = quat.x;
		float y1 = quat.y;
		float z1 = quat.z;

		quat.x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
		quat.y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2;
		quat.z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2;
		quat.w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
#else
		float w1 = w;
		float x1 = x;
		float y1 = y;
		float z1 = z;

		x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2;
		y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2;
		z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2;
		w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2;
#endif

		Normalize();
	}

	Quaternion Quaternion::Multiply(const Quaternion& q1, const Quaternion& q2)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q1);
		temp.Multiply(q2);
		return temp;
	}
	
	void Quaternion::Divide(float scalar)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMVectorScale(ToXMVECTOR(), 1.0f / scalar)));
#else
		scalar = 1.0f / scalar;
		x *= scalar;
		y *= scalar;
		z *= scalar;
		w *= scalar;
#endif
	}

	Quaternion Quaternion::Divide(const Quaternion& q, float scalar)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q);
		temp.Multiply(1.0f / scalar);
		return temp;
	}

	void Quaternion::Normalize()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		float inverseLength = MathExtensions::InverseSquareRoot(
			pow(quat.x, 2) + pow(quat.y, 2) 
			+ pow(quat.z, 2) + pow(quat.w, 2));
		quat.x *= inverseLength;
		quat.y *= inverseLength;
		quat.z *= inverseLength;
		quat.w *= inverseLength;
#else
		float inverseLength = Math::InverseSquareRootFast(
			pow(x, 2) + pow(y, 2) 
			+ pow(z, 2) + pow(w, 2));
		x *= inverseLength;
		y *= inverseLength;
		z *= inverseLength;
		w *= inverseLength;
#endif
	}

	float Quaternion::Dot(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return quat.x * other.GetX() + quat.y * other.GetY() 
			+ quat.z * other.GetZ() + quat.w * other.GetW();
#else
		return x * other.GetX() + y * other.GetY() 
			+ z * other.GetZ() + w * other.GetW();
#endif
	}

	float Quaternion::Dot(const Quaternion& q1, const Quaternion& q2)
		ULTIMA_NOEXCEPT
	{
		return q1.Dot(q2);
	}

	void Quaternion::Lerp(const Quaternion& other, float amount)
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMVectorLerp(this->ToXMVECTOR(), 
			other.ToXMVECTOR(), amount)));
#else
		Add(Quaternion::Multiply((other - *this), amount));
#endif
	}
	
	Quaternion Quaternion::Lerp(
		const Quaternion& q1, 
		const Quaternion& q2, 
		float amount)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q1);
		temp.Lerp(q2, amount);
		return temp;
	}

	void Quaternion::Concatenate(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		float otherX = other.GetX();
		float otherY = other.GetY();
		float otherZ = other.GetZ();
		float otherW = other.GetW();

#if DIRECTX
		float thisX = quat.x;
		float thisY = quat.y;
		float thisZ = quat.z;
		float thisW = quat.w;

		quat.x = thisW * otherX + thisX * otherW + thisY * otherZ - thisZ * otherY;
		quat.y = thisW * otherY + thisY * otherW + thisX * otherZ - thisZ * otherX;
		quat.z = thisW * otherZ + thisZ * otherW + thisX * otherY - thisY * otherX;
		quat.w = thisW * otherW - thisX * otherX - thisY * otherY - thisZ * otherZ;
#else
		float thisX = x;
		float thisY = y;
		float thisZ = z;
		float thisW = w;

		x = thisW * otherX + thisX * otherW + thisY * otherZ - thisZ * otherY;
		y = thisW * otherY + thisY * otherW + thisX * otherZ - thisZ * otherX;
		z = thisW * otherZ + thisZ * otherW + thisX * otherY - thisY * otherX;
		w = thisW * otherW - thisX * otherX - thisY * otherY - thisZ * otherZ;
#endif
	}

	Quaternion Quaternion::Concatenate(const Quaternion& q1, const Quaternion& q2)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q1);
		temp.Concatenate(q2);
		return temp;
	}

	void Quaternion::Conjugate()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMQuaternionConjugate(ToXMVECTOR())));
#else
		x = -x;
		y = -y;
		z = -z;
#endif
	}

	Quaternion Quaternion::Conjugate(const Quaternion& q)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q);
		temp.Conjugate();
		return temp;
	}

	void Quaternion::Inverse()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		XMStoreFloat4(&quat, (XMQuaternionInverse(ToXMVECTOR())));
#else
		float l = GetLength();
		Normalize();
		Conjugate();
		x *= l;
		y *= l;
		z *= l;
#endif
	}

	Quaternion Quaternion::Inverse(const Quaternion& q)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q);
		temp.Inverse();
		return temp;
	}

	void Quaternion::Negate()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		quat.x = -quat.x;
		quat.y = -quat.y;
		quat.z = -quat.z;
		quat.w = -quat.w;
#else
		x = -x;
		y = -y;
		z = -z;
		w = -w;
#endif
	}

	Quaternion Quaternion::Negate(const Quaternion& q)
		ULTIMA_NOEXCEPT
	{
		Quaternion temp = std::move(q);
		temp.Negate();
		return temp;
	}

	Quaternion& Quaternion::operator+=(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		this->Add(other);
		return *this;
	}

	Quaternion Quaternion::operator+(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
		Quaternion v = std::move(*this);
		v.Add(other);
		return v;
	}

	Quaternion& Quaternion::operator-=(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		this->Subtract(other);
		return *this;
	}

	Quaternion Quaternion::operator-(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
		Quaternion v = std::move(*this);
		v.Subtract(other);
		return v;
	}

	Quaternion Quaternion::operator-() 
		const ULTIMA_NOEXCEPT
	{
		Quaternion q = std::move(*this);
		q.Negate();
		return q;
	}

	Quaternion& Quaternion::operator*=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(scalar);
		return *this;
	}

	Quaternion Quaternion::operator*(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Quaternion v = std::move(*this);
		v.Multiply(scalar);
		return v;
	}
	
	Quaternion& Quaternion::operator*=(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(other);
		return *this;
	}

	Quaternion Quaternion::operator*(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
		Quaternion v = std::move(*this);
		v.Multiply(other);
		return v;
	}
	
	Quaternion& Quaternion::operator/=(float scalar)
		ULTIMA_NOEXCEPT
	{
		this->Multiply(1.0f / scalar);
		return *this;
	}

	Quaternion Quaternion::operator/(float scalar) 
		const ULTIMA_NOEXCEPT
	{
		Quaternion v = std::move(*this);
		v.Multiply(1.0f / scalar);
		return v;
	}
	
	Quaternion& Quaternion::operator=(const Quaternion& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			this->quat = std::move(other.ToXMFLOAT4());
#else
			this->x = other.GetX();
			this->y = other.GetY();
			this->z = other.GetZ();
			this->w = other.GetW();
#endif
		}
		return *this;
	}

	Quaternion& Quaternion::operator=(Quaternion&& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
#if DIRECTX
			quat = std::move(other.ToXMFLOAT4());
			other.SetXMFLOAT4(XMFLOAT4());
#else
			x = other.GetX();
			y = other.GetY();
			z = other.GetZ();
			w = other.GetW();
			other.SetX(0);
			other.SetY(0);
			other.SetZ(0);
			other.SetW(0);
#endif
		}
		return *this;
	}

	bool Quaternion::operator==(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (quat.x != other.GetX())
			return false;
		else if (quat.y != other.GetY())
			return false;
		else if (quat.z != other.GetZ())
			return false;
		else if (quat.w != other.GetW())
			return false;
#else
		if (x != other.GetX())
			return false;
		else if (y != other.GetY())
			return false;
		else if (z != other.GetZ())
			return false;
		else if (w != other.GetW())
			return false;
#endif
		else
			return true;
	}

	bool Quaternion::operator!=(const Quaternion& other) 
		const ULTIMA_NOEXCEPT
	{
		return !(this == &other);
	}

	const Quaternion Quaternion::Zero = Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

	const Quaternion Quaternion::Identity = Quaternion(0.0f, 0.0f, 0.0f, 1.0f);

#if DIRECTX
	void Quaternion::SetXMFLOAT4(const XMFLOAT4& xm)
		ULTIMA_NOEXCEPT
	{
		quat = std::move(xm);
	}

	XMFLOAT4 Quaternion::ToXMFLOAT4() 
		const ULTIMA_NOEXCEPT
	{
		return quat;
	}

	XMVECTOR Quaternion::ToXMVECTOR() 
		const ULTIMA_NOEXCEPT
	{
		return XMLoadFloat4(&quat);
	}

	Quaternion::Quaternion(XMVECTOR v)
		ULTIMA_NOEXCEPT
	{
		XMStoreFloat4(&quat, v);
	}
#endif
}