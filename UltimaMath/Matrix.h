#pragma once

#include "Macros.h"

namespace Ultima
{
	class Vector3;
	class Quaternion;

	/*!
	Defines a 4x4 row-major matrix.
	*/
	class Matrix
	{
	public:

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix()
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(const Matrix& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix using a move.
		*/
		ULTIMAMATH_API Matrix(Matrix&& other)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(float value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(float diagonalValue, float otherValue)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(int value)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(int diagonalValue, int otherValue)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a rotation matrix from a quaternion.
		*/
		ULTIMAMATH_API Matrix(const Quaternion& q)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a left-handed field-of-view matrix.
		*/
		ULTIMAMATH_API Matrix(
			float fieldOfView, 
			float aspectRatio, 
			float zNear, 
			float zFar)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a scaling matrix with data from a given vector.
		*/
		ULTIMAMATH_API static Matrix CreateScale(const Vector3& v)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a matrix that represents a clockwise rotation around the X axis.
		*/
		ULTIMAMATH_API static Matrix CreateRotationX(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a matrix that represents a clockwise rotation around the Y axis.
		*/
		ULTIMAMATH_API static Matrix CreateRotationY(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a matrix that represents a clockwise rotation around the Z axis.
		*/
		ULTIMAMATH_API static Matrix CreateRotationZ(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a matrix that represents a translation by a given vector.
		*/
		ULTIMAMATH_API static Matrix CreateTranslation(const Vector3& v)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a matrix that represents a translation by given X, Y and Z values.
		*/
		ULTIMAMATH_API static Matrix CreateTranslation(float x, float y, float z)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a clockwise rotation matrix from pitch, yaw and roll values.
		*/
		ULTIMAMATH_API static Matrix CreateFromPitchYawRoll(
			float pitch, 
			float yaw, 
			float roll)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a left-handed look-at matrix.
		*/
		ULTIMAMATH_API static Matrix CreateLookAt(
			const Vector3& position,
			const Vector3& target, 
			const Vector3& up)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a left-handed ortographic projection matrix.
		*/
		ULTIMAMATH_API static Matrix CreateOthographicProjection(
			float width, 
			float height,
			float zNear, 
			float zFar)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a left-handed field-of-view projection matrix.
		*/
		ULTIMAMATH_API static Matrix CreateProjection(
			float fieldOfView,
			float aspectRatio, 
			float zNear, 
			float zFar)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual ~Matrix()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 0, column 0.
		*/
		ULTIMAMATH_API virtual float GetM00()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 0, column 1.
		*/
		ULTIMAMATH_API virtual float GetM01()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 0, column 2.
		*/
		ULTIMAMATH_API virtual float GetM02()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 0, column 3.
		*/
		ULTIMAMATH_API virtual float GetM03()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 1, column 0.
		*/
		ULTIMAMATH_API virtual float GetM10()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 1, column 1.
		*/
		ULTIMAMATH_API virtual float GetM11()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 1, column 2.
		*/
		ULTIMAMATH_API virtual float GetM12()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 1, column 3.
		*/
		ULTIMAMATH_API virtual float GetM13()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 2, column 0.
		*/
		ULTIMAMATH_API virtual float GetM20()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 2, column 1.
		*/
		ULTIMAMATH_API virtual float GetM21()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 2, column 2.
		*/
		ULTIMAMATH_API virtual float GetM22()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 2, column 3.
		*/
		ULTIMAMATH_API virtual float GetM23()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 3, column 0.
		*/
		ULTIMAMATH_API virtual float GetM30()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 3, column 1.
		*/
		ULTIMAMATH_API virtual float GetM31()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 3, column 2.
		*/
		ULTIMAMATH_API virtual float GetM32()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the value at row 3, column 3.
		*/
		ULTIMAMATH_API virtual float GetM33()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the determinant of the matrix.
		*/
		ULTIMAMATH_API virtual float GetDeterminant()
			const ULTIMA_NOEXCEPT;
		
		/*!
		Gets the trace of the matrix.
		*/
		ULTIMAMATH_API virtual float GetTrace()
			const ULTIMA_NOEXCEPT;

		/*!
		Sets the value at row 0, column 0.
		*/
		ULTIMAMATH_API virtual void SetM00(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 0, column 1.
		*/
		ULTIMAMATH_API virtual void SetM01(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 0, column 2.
		*/
		ULTIMAMATH_API virtual void SetM02(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 0, column 3.
		*/
		ULTIMAMATH_API virtual void SetM03(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 1, column 0.
		*/
		ULTIMAMATH_API virtual void SetM10(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 1, column 1.
		*/
		ULTIMAMATH_API virtual void SetM11(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 1, column 2.
		*/
		ULTIMAMATH_API virtual void SetM12(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 1, column 3.
		*/
		ULTIMAMATH_API virtual void SetM13(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 2, column 0.
		*/
		ULTIMAMATH_API virtual void SetM20(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 2, column 1.
		*/
		ULTIMAMATH_API virtual void SetM21(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 2, column 2.
		*/
		ULTIMAMATH_API virtual void SetM22(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 2, column 3.
		*/
		ULTIMAMATH_API virtual void SetM23(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 3, column 0.
		*/
		ULTIMAMATH_API virtual void SetM30(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 3, column 1.
		*/
		ULTIMAMATH_API virtual void SetM31(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 3, column 2.
		*/
		ULTIMAMATH_API virtual void SetM32(float f)
			ULTIMA_NOEXCEPT;
		
		/*!
		Sets the value at row 3, column 3.
		*/
		ULTIMAMATH_API virtual void SetM33(float f)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two matrices together.
		*/
		ULTIMAMATH_API virtual void Add(const Matrix& other)
			ULTIMA_NOEXCEPT;

		/*!
		Adds two matrices together.
		*/
		ULTIMAMATH_API static Matrix Add(const Matrix& m1, const Matrix& m2)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a matrix from another.
		*/
		ULTIMAMATH_API virtual void Subtract(const Matrix& other)
			ULTIMA_NOEXCEPT;

		/*!
		Subtracts a matrix from another.
		*/
		ULTIMAMATH_API static Matrix Subtract(const Matrix& m1, const Matrix& m2)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a matrix by another.
		*/
		ULTIMAMATH_API virtual void Multiply(const Matrix& other)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a matrix by another.
		*/
		ULTIMAMATH_API static Matrix Multiply(const Matrix& m1, const Matrix& m2)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a matrix by a scalar.
		*/
		ULTIMAMATH_API virtual void Multiply(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Multiplies a matrix by a scalar.
		*/
		ULTIMAMATH_API static Matrix Multiply(const Matrix& m, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a matrix by a scalar.
		*/
		ULTIMAMATH_API virtual void Divide(float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Divides a matrix by a scalar.
		*/
		ULTIMAMATH_API static Matrix Divide(const Matrix& m, float scalar)
			ULTIMA_NOEXCEPT;

		/*!
		Inverts the matrix.
		*/
		ULTIMAMATH_API virtual void Inverse()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the inverse of a matrix.
		*/
		ULTIMAMATH_API static Matrix Inverse(const Matrix& m)
			ULTIMA_NOEXCEPT;

		/*!
		Negates the matrix.
		*/
		ULTIMAMATH_API virtual void Negate()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the negate of a matrix.
		*/
		ULTIMAMATH_API static Matrix Negate(const Matrix& m)
			ULTIMA_NOEXCEPT;

		/*!
		Tranposes the matrix.
		*/
		ULTIMAMATH_API virtual void Transpose()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the transposed of a given matrix.
		*/
		ULTIMAMATH_API static Matrix Transpose(const Matrix& m)
			ULTIMA_NOEXCEPT;

		/*!
		Inverts and then transposes the matrix.
		*/
		ULTIMAMATH_API virtual void InverseTranspose()
			ULTIMA_NOEXCEPT;

		/*!
		Returns the inverted and tranposed of a given matrix.
		*/
		ULTIMAMATH_API static Matrix InverseTranspose(const Matrix& m)
			ULTIMA_NOEXCEPT;

		ULTIMAMATH_API virtual Matrix& operator+=(const Matrix& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator+(const Matrix& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator-=(const Matrix& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator-(const Matrix& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator-()
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator*=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator*(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator*=(const Matrix& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator*(const Matrix& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator/=(float scalar)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix operator/(float scalar)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator=(const Matrix& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual Matrix& operator=(Matrix&& other)
			ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator==(const Matrix& other)
			const ULTIMA_NOEXCEPT;
		ULTIMAMATH_API virtual bool operator!=(const Matrix& other)
			const ULTIMA_NOEXCEPT;

		/*!
		The zero/null matrix.
		*/
		ULTIMAMATH_API static const Matrix Zero;

		/*!
		The identity matrix.
		*/
		ULTIMAMATH_API static const Matrix Identity;

#if DIRECTX
		
		/*!
		Loads matrix data from a given XMFLOAT4X4.
		*/
		ULTIMAMATH_API void SetXMFLOAT4X4(const XMFLOAT4X4& xm)
			ULTIMA_NOEXCEPT;

		/*!
		Returns an XMMatrix holding the matrix data.
		*/
		ULTIMAMATH_API virtual XMMATRIX ToXMMATRIX()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns and XMFLOAT4X4 holding the matrix data.
		*/
		ULTIMAMATH_API virtual XMFLOAT4X4 ToXMFLOAT4X4()
			const ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Matrix.
		*/
		ULTIMAMATH_API Matrix(const XMFLOAT4X4& m)
			ULTIMA_NOEXCEPT;

#else

		/*!
		Returns a one dimensional array holding the matrix data.
		*/
		ULTIMAMATH_API virtual float* To1DArray()
			ULTIMA_NOEXCEPT;

		/*!
		Returns a two dimensional array holding the matrix data.
		*/
		ULTIMAMATH_API virtual float* To2DArray()
			ULTIMA_NOEXCEPT;

		/*!
		Loads the matrix data from a one dimensional array.
		*/
		ULTIMAMATH_API virtual void From1DArray(float f[4])
			ULTIMA_NOEXCEPT;

		/*!
		Loads the matrix data from a two dimensional array.
		*/
		ULTIMAMATH_API virtual void From2DArray(float f[][4])
			ULTIMA_NOEXCEPT;

#endif

	protected:
#if DIRECTX
		XMFLOAT4X4 matrix;
#else
		float[4][4] matrix;
#endif
	};
}