#pragma once

#include "Macros.h"

namespace Ultima
{
	/*!
	Class with standalone functions and members, for extending math capabilities.
	*/
	class MathExtensions
	{
	public:
		/*!
		Returns a clamped value between a specified interval.
		*/
		ULTIMAMATH_API static float Clamp(float value, float min, float max)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a clamped value in the [0, 1] interval.
		*/
		ULTIMAMATH_API static float Saturate(float value)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a specified number from the [A,B] interval in the [0,1] interval.
		*/
		ULTIMAMATH_API static double MapIntervalTo01(
			double number, double intervalStart, double intervalEnd)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a specified number from the [0,1] interval in the [A,B] interval.
		*/
		ULTIMAMATH_API static double MapIntervalFrom01(
			double number, double intervalStart, double intervalEnd)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a specified number from the [A,B] interval in the [C,D] interval.
		*/
		ULTIMAMATH_API static double MapInterval(
			double number, 
			double firstIntervalStart, double firstIntervalEnd, 
			double secondIntervalStart, double secondIntervalEnd)
			ULTIMA_NOEXCEPT;

		/*!
		Reprojects the starting point of the linear interpolation, based on the result the last lerp and the last lerp factor used.
		*/
		ULTIMAMATH_API static double ReprojectLerp(
			double lastLerpResult, double intervalEnd, double lastLerpFactorUsed)
			ULTIMA_NOEXCEPT;

		/*!
		Quickly calculates the inverse square root of a number.
		*/
		ULTIMAMATH_API static float InverseSquareRootFast(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Calculates the inverse square root of a number.
		*/
		ULTIMAMATH_API static float InverseSquareRoot(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Quickly calculates the square root of a number.
		*/
		ULTIMAMATH_API static float SquareRootFast(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Calculates the square root of a number.
		*/
		ULTIMAMATH_API static float SquareRoot(float x)
			ULTIMA_NOEXCEPT;

		/*!
		Returns a 1D array index from a 2D index.
		*/
		ULTIMAMATH_API static int IndexFrom2DTo1D(
			int matrixWidth, int rowIndex, int columnIndex)
			ULTIMA_NOEXCEPT;

		/*!
		Fills up outRowIndex and outColumnIndex with the coresponding indices calculated from a given 1D array index.
		*/
		ULTIMAMATH_API static void IndexFrom1DTo2D(
			int matrixWidth, int index, 
			int& outRowIndex, int& outColumnIndex)
			ULTIMA_NOEXCEPT;

		/*!
		Converts degrees to radians.
		*/
		ULTIMAMATH_API static float ToRadians(float degrees)
			ULTIMA_NOEXCEPT;

		/*!
		Convers radians to degrees.
		*/
		ULTIMAMATH_API static float ToDegrees(float radians)
			ULTIMA_NOEXCEPT;

		/*!
		The Pi constant.
		*/
		ULTIMAMATH_API static const float Pi;

		/*!
		2 * Pi (aka Tau) constant.
		*/
		ULTIMAMATH_API static const float TwoPi;
	};
}