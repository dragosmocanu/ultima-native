//VOLUNTAS SUPREMUM EST

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#define _USE_MATH_DEFINES
#include <math.h>

//COM
#include <Windows.h>

#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>
#include <utility>

#ifdef UNICODE
typedef LPCWSTR LPCTSTR;
#else
typedef LPCSTR LPCTSTR;
#endif

#if DIRECTX
//DXMath
#include <DirectXMath.h>
using namespace DirectX;

#endif