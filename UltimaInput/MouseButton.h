#pragma once

namespace Ultima
{
	/*!
	Defines the buttons on a mouse.
	*/
	enum class MouseButton 
	{ 
		Left, 
		Right, 
		Middle, 
		Extra 
	};
}