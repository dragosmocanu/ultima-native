#include "stdafx.h"

#include "InputManager.h"

#include "Utilities.h"

namespace Ultima
{
	InputManager::InputManager() 
		ULTIMA_NOEXCEPT
	{ }

	InputManager& InputManager::GetInstance()
		ULTIMA_NOEXCEPT
	{
		static InputManager instance;

		return instance;
	}

#if DIRECTX
	void InputManager::Initialize(
		const HINSTANCE& hInstance, 
		const HWND& hwnd)
		ULTIMA_NOEXCEPT
	{
		keyboard = new Keyboard(hInstance, hwnd);
		mouse = new Mouse(hInstance, hwnd);

		LPDIRECTINPUT8 input;

		Utilities::Assert(DirectInput8Create(hInstance,
			DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&input, 0));

		std::vector<GUID> guids;

		Utilities::Assert(input->EnumDevices(DI8DEVCLASS_GAMECTRL,
			GamepadsCallback, &guids, DIEDFL_ATTACHEDONLY));

		for (unsigned int i = 0; i < guids.size(); i++)
		{
			LPDIRECTINPUTDEVICE8 device;

			if (!Utilities::Assert(input->CreateDevice(guids[i], &device, 0)))
				continue;

			if (!Utilities::Assert(device->SetDataFormat(&c_dfDIJoystick)))
				continue;

			if (!Utilities::Assert(device->SetCooperativeLevel(hwnd,
				DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
				continue;

			//if (!Utilities::Assert(device->Acquire()))
			//	continue;

			device->Acquire();

			Gamepad* g = new Gamepad(device);
			gamepads.push_back(g);
		}

		guids.clear();

		Utilities::Release(input);
	}

	BOOL CALLBACK InputManager::GamepadsCallback(
		const DIDEVICEINSTANCE* dInstance, 
		void* ptr)
		ULTIMA_NOEXCEPT
	{
		std::vector<GUID>* guids = static_cast<std::vector<GUID>*>(ptr);
		GUID guid(GUID(dInstance->guidInstance));
		guids->push_back(guid);
		guids = nullptr;
		return DIENUM_CONTINUE;
	}
#endif

	void InputManager::Update()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		keyboard->Update();
		mouse->Update();

		for (auto& g : gamepads)
			g->Update();
#endif
	}

	void InputManager::Flush()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		keyboard->Flush();
		mouse->Flush();

		for (auto& g : gamepads)
			g->Flush();
#endif
	}

	bool InputManager::HasPressed(unsigned int control)
		const ULTIMA_NOEXCEPT
	{
		for (auto& x : keyBindings)
			if (x.second == control)
				if (keyboard->HasPressed(x.first))
					return true;

		for (auto& x : buttonBindings)
			if (x.second == control)
				for (auto& y : gamepads)
					if (y->HasPressed(x.first))
						return true;

		return false;
	}

	bool InputManager::HasReleased(unsigned int control) 
		const ULTIMA_NOEXCEPT
	{
		for (auto& x : keyBindings)
			if (x.second == control)
				if (keyboard->HasReleased(x.first))
					return true;

		for (auto& x : buttonBindings)
			if (x.second == control)
				for (auto& y : gamepads)
					if (y->HasReleased(x.first))
						return true;

		return false;
	}

	bool InputManager::IsPressing(unsigned int control) 
		const ULTIMA_NOEXCEPT
	{
		for (auto& x : keyBindings)
			if (x.second == control)
				if (keyboard->IsPressing(x.first))
					return true;

		for (auto& x : buttonBindings)
			if (x.second == control)
				for (auto& y : gamepads)
					if (y->IsPressing(x.first))
						return true;

		return false;
	}

	float InputManager::GetDeltaX() 
		const ULTIMA_NOEXCEPT
	{
		return mouse->GetDeltaX();
	}

	float InputManager::GetDeltaY() 
		const ULTIMA_NOEXCEPT
	{
		return mouse->GetDeltaY();
	}

	Vector2 InputManager::GetDeltaPosition() 
		const ULTIMA_NOEXCEPT
	{
		return mouse->GetDeltaPosition();
	}

	void InputManager::AddKeyBinding(Key k, unsigned int control)
		ULTIMA_NOEXCEPT
	{
		keyBindings.insert(std::pair<Key, unsigned int>(k, control));
	}

	void InputManager::AddButtonBinding(GamepadButton b, unsigned int control)
		ULTIMA_NOEXCEPT
	{
		buttonBindings.insert(std::pair<GamepadButton, unsigned int>(b, control));
	}

	void InputManager::ClearKeyBindings()
		ULTIMA_NOEXCEPT
	{
		keyBindings.clear();
	}

	void InputManager::ClearButtonBindings()
		ULTIMA_NOEXCEPT
	{
		buttonBindings.clear();
	}

	const std::map<Key, unsigned int>& InputManager::GetKeyBindings()
		ULTIMA_NOEXCEPT
	{
		return keyBindings;
	}

	const std::map<GamepadButton, unsigned int>& InputManager::GetButtonBindings()
		ULTIMA_NOEXCEPT
	{
		return buttonBindings;
	}

#if DIRECTX
	const Keyboard* InputManager::GetKeyboard()
		ULTIMA_NOEXCEPT
	{
		return keyboard;
	}

	const Mouse* InputManager::GetMouse()
		ULTIMA_NOEXCEPT
	{
		return mouse;
	}

	const std::vector<Gamepad*>& InputManager::GetGamepads()
		ULTIMA_NOEXCEPT
	{
		return gamepads;
	}
#endif

}