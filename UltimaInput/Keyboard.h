#pragma once

#include "Macros.h"

#include "IControllable.h"
#include "IPressable.h"
#include "InputManager.h"
#include "Key.h"

namespace Ultima
{
	/*!
	Adapter class for a keyboard input device.
	*/
	class Keyboard : public IPressable
	{
	public:
#if DIRECTX
		/*!
		Creates a new instance of Keyboard.
		*/
		ULTIMAINPUT_API Keyboard(
			const HINSTANCE& hInstance, 
			const HWND& hwnd)
			ULTIMA_NOEXCEPT;
#endif
		ULTIMAINPUT_API virtual ~Keyboard()
			ULTIMA_NOEXCEPT;

		/*!
		Updates the Keyboard logic.
		*/
		ULTIMAINPUT_API virtual void Update()
			ULTIMA_NOEXCEPT;

		/*!
		Flushes the Keyboard state.
		*/
		ULTIMAINPUT_API virtual void Flush()
			ULTIMA_NOEXCEPT;
		
		/*!
		Returns true if the key has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(Key k)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the key has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(Key k)
			const ULTIMA_NOEXCEPT;
		
		/*!
		Returns true if the key is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(Key k)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(unsigned int i)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if control key has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(unsigned int i)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(unsigned int i)
			const ULTIMA_NOEXCEPT;

	protected:
#if DIRECTX
		LPDIRECTINPUTDEVICE8 device;
		char state[256];
		char previousState[256];
#endif
	};
}