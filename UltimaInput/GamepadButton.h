#pragma once

namespace Ultima
{
	/*!
	Identifies a button on a gamepad.
	*/
	enum class GamepadButton
	{
		A,
		B,
		X,
		Y,
		LeftShoulder,
		RightShoulder,
		LeftTrigger,
		RightTrigger,
		Select,
		Start,
		LeftAnalog,
		RightAnalog
	};
}