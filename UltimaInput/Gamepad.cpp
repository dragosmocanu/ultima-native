#include "stdafx.h"

#include "Gamepad.h"

#include "Utilities.h"

namespace Ultima
{
#if DIRECTX
	Gamepad::Gamepad(const LPDIRECTINPUTDEVICE8& device)
		ULTIMA_NOEXCEPT
	{
		this->device = device;
	}

	Gamepad::Gamepad(
		const HINSTANCE& hInstance, 
		const HWND& hwnd, 
		bool& hasBeenCreated)
		ULTIMA_NOEXCEPT
	{
		hasBeenCreated = false;
		state = DIJOYSTATE();
		previousState = DIJOYSTATE();

		LPDIRECTINPUT8 input;

		if (!Utilities::Assert(DirectInput8Create(hInstance,
			DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&input, 0)))
			return;

		if (!Utilities::Assert(input->CreateDevice(GUID_Joystick, &device, 0)))
			return;
			
		Utilities::Release(input);

		if (!Utilities::Assert(device->SetDataFormat(&c_dfDIJoystick)))
			return;
			
		if (!Utilities::Assert(device->SetCooperativeLevel(hwnd,
			DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
			return;

		if (!Utilities::Assert(device->Acquire()))
			return;
			
		hasBeenCreated = true;
	}
#endif

	Gamepad::~Gamepad()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (device)
		{
			device->Unacquire();
			device->Release();
		}
		device = 0;
#endif
	}

	void Gamepad::Update()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		memcpy(&previousState, &state, sizeof(state));

		if (!device)
			return;

		if (FAILED(device->GetDeviceState(sizeof(state), (LPVOID)&state)))
		{
			if (FAILED(device->Acquire()))
			{
				Flush();
				return;
			}

			device->GetDeviceState(sizeof(state), (LPVOID)&state);
		}
#endif
	}

	void Gamepad::Flush()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		state = DIJOYSTATE();
		previousState = DIJOYSTATE();
#endif
	}

	bool Gamepad::HasPressed(GamepadButton b) 
		const ULTIMA_NOEXCEPT
	{
		return HasPressed(static_cast<unsigned int>(b));
	}

	bool Gamepad::HasReleased(GamepadButton b) 
		const ULTIMA_NOEXCEPT
	{
		return HasReleased(static_cast<unsigned int>(b));
	}

	bool Gamepad::IsPressing(GamepadButton b) 
		const ULTIMA_NOEXCEPT
	{
		return IsPressing(static_cast<unsigned int>(b));
	}

	bool Gamepad::HasPressed(unsigned int b) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state.rgbButtons[b] != 0
			&& previousState.rgbButtons[b] == 0)
			return true;
		else
			return false;
#endif
	}

	bool Gamepad::HasReleased(unsigned int b) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state.rgbButtons[b] == 0
			&& previousState.rgbButtons[b] != 0)
			return true;
		else
			return false;
#endif
	}

	bool Gamepad::IsPressing(unsigned int b) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		for (int i = 0; i < 32; i++)
		{
			if (state.rgbButtons[i] != 0)
			{
				break;
			}
		}

		if (state.rgbButtons[b] != 0)
			return true;
		else
			return false;
#endif
	}

}