#include "stdafx.h"

#include "Mouse.h"

#include "Utilities.h"

namespace Ultima
{

	Mouse::~Mouse() 
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (device)
		{
			device->Unacquire();
			device->Release();
		}
		device = 0;
#endif
	}

#if DIRECTX
	Mouse::Mouse(
		const HINSTANCE& hInstance, 
		const HWND& hwnd)
		ULTIMA_NOEXCEPT
	{
		x = 0;
		y = 0;
		scroll = 0;

		state = DIMOUSESTATE();
		previousState = DIMOUSESTATE();

		LPDIRECTINPUT8 input;

		Utilities::Assert(DirectInput8Create(hInstance,
			DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&input, 0));
		
		Utilities::Assert(input->CreateDevice(GUID_SysMouse, &device, 0));

		Utilities::Release(input);

		Utilities::Assert(device->SetDataFormat(&c_dfDIMouse));

		Utilities::Assert(device->SetCooperativeLevel(hwnd,
			DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));

		//Utilities::Assert(device->Acquire());
		device->Acquire();
	}
#endif

	void Mouse::Update()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		memcpy(&previousState, &state, sizeof(state));

		if (!device)
			return;

		if (FAILED(device->GetDeviceState(sizeof(state), (LPVOID)&state)))
		{
			if (FAILED(device->Acquire()))
			{
				Flush();
				return;
			}

			device->GetDeviceState(sizeof(state), (LPVOID)&state);
		}

		x += static_cast<int>(state.lX);
		y += static_cast<int>(state.lY);
		scroll += static_cast<int>(state.lZ);
#endif
	}

	void Mouse::Flush()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		state = DIMOUSESTATE();
		previousState = DIMOUSESTATE();
#endif
	}

	int Mouse::GetX() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return x;
#endif
	}

	int Mouse::GetY() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return y;
#endif
	}

	int Mouse::GetScroll() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return scroll;
#endif
	}

	bool Mouse::HasPressed(MouseButton mb) 
		const ULTIMA_NOEXCEPT
	{
		return HasPressed(static_cast<unsigned int>(mb));
	}

	bool Mouse::HasReleased(MouseButton mb) 
		const ULTIMA_NOEXCEPT
	{
		return HasReleased(static_cast<unsigned int>(mb));
	}

	bool Mouse::IsPressing(MouseButton mb)
		const ULTIMA_NOEXCEPT
	{
		return IsPressing(static_cast<unsigned int>(mb));
	}
	
	bool Mouse::HasPressed(unsigned int mb)
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state.rgbButtons[mb] != 0
			&& previousState.rgbButtons[mb] == 0)
			return true;
		else
			return false;
#endif
	}

	bool Mouse::HasReleased(unsigned int mb)
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state.rgbButtons[mb] == 0
			&& previousState.rgbButtons[mb] != 0)
			return true;
		else
			return false;
#endif
	}

	bool Mouse::IsPressing(unsigned int mb) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state.rgbButtons[mb] != 0)
			return true;
		else
			return false;
#endif
	}

	float Mouse::GetDeltaX() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return static_cast<float>(state.lX);
#endif
	}

	float Mouse::GetDeltaY() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return static_cast<float>(state.lY);
#endif
	}

	Vector2 Mouse::GetDeltaPosition() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return Vector2(static_cast<float>(state.lX), static_cast<float>(state.lY));
#endif
	}

	float Mouse::GetDeltaScroll() 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		return static_cast<float>(state.lZ);
#endif
	}

}