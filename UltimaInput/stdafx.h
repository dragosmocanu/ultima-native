//VOLUNTAS SUPREMUM EST

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#define DIRECTINPUT_VERSION 0x0800

//COM
#include <Windows.h>

#ifdef UNICODE
typedef LPCWSTR LPCTSTR;
#else
typedef LPCSTR LPCTSTR;
#endif

//C++
#include <fstream>
#include <map>
#include <memory>
#include <mmsystem.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <sys/stat.h>
#include <vector>

#if DIRECTX
//DXMath
#include <DirectXMath.h>
using namespace DirectX;

//DInput
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#include <dinput.h>
#endif