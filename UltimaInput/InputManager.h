#pragma once

#include "Macros.h"

#include "Keyboard.h"
#include "Mouse.h"
#include "Gamepad.h"

namespace Ultima
{
	enum class Key;

	class Keyboard;

	enum class GamepadButton;

	class Gamepad;

	/*!
	[THREADSAFE] Singleton manager class for input devices.
	*/
	class InputManager : public IMovable
	{
	public:
		/*!
		Gets the single instance of InputManager.
		*/
		ULTIMAINPUT_API static InputManager& GetInstance()
			ULTIMA_NOEXCEPT;

		/*!
		Updates the InputManager logic.
		*/
		ULTIMAINPUT_API virtual void Update()
			ULTIMA_NOEXCEPT;

		/*!
		Flushes the state of every input device.
		*/
		ULTIMAINPUT_API virtual void Flush()
			ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(unsigned int control)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(unsigned int control)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(unsigned int control)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the X coordinate of the device's movement.
		*/
		ULTIMAINPUT_API virtual float GetDeltaX()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the Y coordinate of the device's movement.
		*/
		ULTIMAINPUT_API virtual float GetDeltaY()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the movement of the device.
		*/
		ULTIMAINPUT_API virtual Vector2 GetDeltaPosition()
			const ULTIMA_NOEXCEPT;

		/*!
		Adds a Key to the key bindings.
		*/
		ULTIMAINPUT_API virtual void AddKeyBinding(Key k, unsigned int control)
			ULTIMA_NOEXCEPT;

		/*!
		Adds a Button to the button bindings.
		*/
		ULTIMAINPUT_API virtual void AddButtonBinding(GamepadButton b, unsigned int control)
			ULTIMA_NOEXCEPT;

		/*!
		Clears the key bindings.
		*/
		ULTIMAINPUT_API virtual void ClearKeyBindings()
			ULTIMA_NOEXCEPT;

		/*!
		Clears the button bindings.
		*/
		ULTIMAINPUT_API virtual void ClearButtonBindings()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the keyboard bindings.
		*/
		ULTIMAINPUT_API virtual const std::map<Key, unsigned int>& GetKeyBindings()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the button bindings.
		*/
		ULTIMAINPUT_API virtual const std::map<GamepadButton, unsigned int>& GetButtonBindings()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the Keyboard object.
		*/
		ULTIMAINPUT_API const Keyboard* GetKeyboard()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the Mouse object.
		*/
		ULTIMAINPUT_API const Mouse* GetMouse()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the gamepad object.
		*/
		ULTIMAINPUT_API const std::vector<Gamepad*>& GetGamepads()
			ULTIMA_NOEXCEPT;

#if DIRECTX
		/*!
		Initializes the InputManager with the required data.
		*/
		ULTIMAINPUT_API void Initialize(const HINSTANCE& hInstance, const HWND& hwnd)
			ULTIMA_NOEXCEPT;

		/*!
		Callback for creating gamepads.
		*/
		ULTIMAINPUT_API static BOOL CALLBACK GamepadsCallback(
			const DIDEVICEINSTANCE* 
			dInstance,
			void* ptr)
			ULTIMA_NOEXCEPT;
#endif
	protected:
		ULTIMAINPUT_API InputManager()
			ULTIMA_NOEXCEPT;

		Keyboard* keyboard;
		Mouse* mouse;
		std::vector<Gamepad*> gamepads;

		std::map<Key, unsigned int> keyBindings;
		std::map<GamepadButton, unsigned int> buttonBindings;
	};
}