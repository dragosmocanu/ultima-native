#pragma once

#include "Macros.h"

#include "GamepadButton.h"
#include "InputManager.h"
#include "IControllable.h"

namespace Ultima
{
	/*!
	Adapter class for a gamepad input device.
	*/
	class Gamepad : public IPressable
	{
	public:
#if DIRECTX
		/*!
		Creates a new instance of Gamepad. Used by the InputManager.
		*/
		ULTIMAINPUT_API Gamepad(const LPDIRECTINPUTDEVICE8& device)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of Gamepad.
		*/
		ULTIMAINPUT_API Gamepad(
			const HINSTANCE& hInstance, 
			const HWND& hwnd,
			bool& hasBeenCreated) 
			ULTIMA_NOEXCEPT;
#endif

		ULTIMAINPUT_API virtual ~Gamepad()
			ULTIMA_NOEXCEPT;

		/*!
		Updates the Gamepad logic.
		*/
		ULTIMAINPUT_API virtual void Update()
			ULTIMA_NOEXCEPT;

		/*!
		Flushes the Gamepad state.
		*/
		ULTIMAINPUT_API virtual void Flush()
			ULTIMA_NOEXCEPT;

		/*!
		Returns true if the button has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(GamepadButton b)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the button has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(GamepadButton b)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the button is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(GamepadButton b)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(unsigned int b)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(unsigned int b)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(unsigned int b)
			const ULTIMA_NOEXCEPT;

	protected:
#if DIRECTX
		LPDIRECTINPUTDEVICE8 device;
		DIJOYSTATE state;
		DIJOYSTATE previousState;
#endif
	};
}