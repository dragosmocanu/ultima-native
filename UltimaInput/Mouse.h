#pragma once

#include "Macros.h"

#include "IControllable.h"
#include "IMovable.h"
#include "IPressable.h"
#include "MouseButton.h"

namespace Ultima
{
	/*!
	Adapter class for a mouse input device.
	*/
	class Mouse : public IMovable
	{
	public:
#if DIRECTX
		/*!
		Creates a new instance of Mouse.
		*/
		ULTIMAINPUT_API Mouse(
			const HINSTANCE& hInstance, 
			const HWND& hwnd)
			ULTIMA_NOEXCEPT;
#endif

		ULTIMAINPUT_API virtual ~Mouse()
			ULTIMA_NOEXCEPT;

		/*!
		Updates the Mouse logic.
		*/
		ULTIMAINPUT_API virtual void Update()
			ULTIMA_NOEXCEPT;

		/*!
		Flushes the Mouse state.
		*/
		ULTIMAINPUT_API virtual void Flush()
			ULTIMA_NOEXCEPT;

		/*!
		Gets the current X value.
		*/
		ULTIMAINPUT_API virtual int GetX()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the current Y value.
		*/
		ULTIMAINPUT_API virtual int GetY()
			const ULTIMA_NOEXCEPT;

		/*!
		Gets the current Scroll value.
		*/
		ULTIMAINPUT_API virtual int GetScroll()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the mouse button has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(MouseButton mb)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the mouse button has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(MouseButton mb)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the mouse button is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(MouseButton mb)
			const ULTIMA_NOEXCEPT;
		
		/*!
		Returns true if the control has been pressed once, not continously.
		*/
		ULTIMAINPUT_API virtual bool HasPressed(unsigned int i)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if control key has been released.
		*/
		ULTIMAINPUT_API virtual bool HasReleased(unsigned int i)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns true if the control is being pressed.
		*/
		ULTIMAINPUT_API virtual bool IsPressing(unsigned int i)
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the X coordinate of the device's movement.
		*/
		ULTIMAINPUT_API virtual float GetDeltaX()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the Y coordinate of the device's movement.
		*/
		ULTIMAINPUT_API virtual float GetDeltaY()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the movement of the device.
		*/
		ULTIMAINPUT_API virtual Vector2 GetDeltaPosition()
			const ULTIMA_NOEXCEPT;

		/*!
		Returns the delta value of the scroll wheel.
		*/
		ULTIMAINPUT_API virtual float GetDeltaScroll()
			const ULTIMA_NOEXCEPT;

	protected:
#if DIRECTX
		LPDIRECTINPUTDEVICE8 device;
		DIMOUSESTATE state;
        DIMOUSESTATE previousState;
		int x;
		int y;
		int scroll;
#endif
	};
}