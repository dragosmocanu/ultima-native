#include "stdafx.h"

#include "Keyboard.h"

#include "Utilities.h"

namespace Ultima
{
	Keyboard::~Keyboard()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (device)
		{
			device->Unacquire();
			device->Release();
		}
		device = 0;
#endif
	}

#if DIRECTX
	Keyboard::Keyboard(
		const HINSTANCE& hInstance, 
		const HWND& hwnd)
		ULTIMA_NOEXCEPT
	{
		memset(state, 0, 256);
		memset(previousState, 0, 256);

		LPDIRECTINPUT8 input;

		Utilities::Assert(DirectInput8Create(hInstance,
			DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&input, 0));
		
		Utilities::Assert(input->CreateDevice(GUID_SysKeyboard, &device, 0));

		Utilities::Release(input);

		Utilities::Assert(device->SetDataFormat(&c_dfDIKeyboard));

		Utilities::Assert(device->SetCooperativeLevel(hwnd,
			DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));

		//Utilities::Assert(device->Acquire());
		device->Acquire();
	}
#endif

	void Keyboard::Update()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		memcpy(previousState, state, sizeof(state));

		if (!device)
			return;

		if (FAILED(device->GetDeviceState(sizeof(state), (LPVOID)&state)))
		{
			if (FAILED(device->Acquire()))
			{
				Flush();
				return;
			}

			device->GetDeviceState(sizeof(state), (LPVOID)&state);
		}

#endif
	}

	void Keyboard::Flush()
		ULTIMA_NOEXCEPT
	{
#if DIRECTX
		memset(state, 0, 256);
		memset(previousState, 0, 256);
#endif
	}

	bool Keyboard::HasPressed(Key k) 
		const ULTIMA_NOEXCEPT
	{
		return HasPressed(static_cast<unsigned int>(k));
	}

	bool Keyboard::HasReleased(Key k) 
		const ULTIMA_NOEXCEPT
	{
		return HasReleased(static_cast<unsigned int>(k));
	}

	bool Keyboard::IsPressing(Key k) 
		const ULTIMA_NOEXCEPT
	{
		return IsPressing(static_cast<unsigned int>(k));
	}
	
	bool Keyboard::HasPressed(unsigned int k) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state[k] != 0
			&& previousState[k] == 0)
			return true;
		else
			return false;
#endif
	}

	bool Keyboard::HasReleased(unsigned int k) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state[k] == 0
			&& previousState[k] != 0)
			return true;
		else
			return false;
#endif
	}

	bool Keyboard::IsPressing(unsigned int k) 
		const ULTIMA_NOEXCEPT
	{
#if DIRECTX
		if (state[k] != 0)
			return true;
		else
			return false;
#endif
	}

}