#ifdef ULTIMAINPUT_EXPORTS
#define ULTIMAINPUT_API __declspec(dllexport)
#else
#define ULTIMAINPUT_API __declspec(dllimport)
#endif

#if _MSC_VER > 1800
#define ULTIMA_NOEXCEPT noexcept
#else
#define ULTIMA_NOEXCEPT throw()
#endif