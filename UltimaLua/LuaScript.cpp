#include "stdafx.h"

#include "LuaScript.h"

#include "Utilities.h"

namespace Ultima
{
	LuaScript::LuaScript(const std::string& scriptPath)
		ULTIMA_NOEXCEPT
		: scriptPath(scriptPath)
	{
		buildScript();
	}

	LuaScript::LuaScript(const LuaScript& other)
		ULTIMA_NOEXCEPT
	{
		*this = std::move(other);
	}

	LuaScript& LuaScript::operator=(const LuaScript& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			destructScript();

			scriptPath = other.GetScriptPath();

			buildScript();
		}

		return *this;
	}

	void LuaScript::buildScript()
		ULTIMA_NOEXCEPT
	{
		//luaState = lua_open();
		//luaL_openlibs(luaState);

		//if (Utilities::FileExists(scriptPath))
		//	luaL_dofile(luaState, scriptPath.c_str());
	}

	void LuaScript::destructScript()
		ULTIMA_NOEXCEPT
	{
		//if (luaState)
		//	lua_close(luaState);
	}

	LuaScript::~LuaScript()
		ULTIMA_NOEXCEPT
	{
		destructScript();
	}

	std::string LuaScript::GetScriptPath()
		const ULTIMA_NOEXCEPT
	{
		return scriptPath;
	}

	std::string LuaScript::GetGlobalString(const std::string& variableName) 
		const ULTIMA_NOEXCEPT
	{
		std::string ret;

		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	//if (!lua_istable(luaState, -1))
		//	//	return ret;

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState, 
		//			static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_gettable(luaState, -2);

		//	ret = lua_tostring(luaState, -1);
		//	lua_pop(luaState, 2);
		//}
		//else
		//{
		//	lua_getglobal(luaState, variableName.c_str());
		//	ret = lua_tostring(luaState, -1);
		//	lua_pop(luaState, 1);
		//}

		return ret;
	}

	void LuaScript::SetGlobalString(
		const std::string& variableName,
		const std::string& value)
		ULTIMA_NOEXCEPT
	{
		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	//if (!lua_istable(luaState, -1))
		//	//	return;

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState,
		//		static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_pushstring(luaState, value.c_str());

		//	lua_settable(luaState, -3);
		//}
		//else
		//{
		//	lua_pushstring(luaState, value.c_str());
		//	lua_setglobal(luaState, variableName.c_str());
		//}
	}

	double LuaScript::GetGlobalNumber(const std::string& variableName) 
		const ULTIMA_NOEXCEPT
	{
		double ret = -1.0;

		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	//if (!lua_istable(luaState, -1))
		//	//	return ret;

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState,
		//		static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_gettable(luaState, -2);

		//	ret = lua_tonumber(luaState, -1);
		//	lua_pop(luaState, 2);
		//}
		//else
		//{
		//	lua_getglobal(luaState, variableName.c_str());
		//	ret = lua_tonumber(luaState, -1);
		//	lua_pop(luaState, 1);
		//}

		return ret;
	}

	void LuaScript::SetGlobalNumber(
		const std::string& variableName,
		double value)
		ULTIMA_NOEXCEPT
	{
		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	//if (!lua_istable(luaState, -1))
		//	//	return;

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState,
		//		static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_pushnumber(luaState, value);

		//	lua_settable(luaState, -3);
		//}
		//else
		//{
		//	lua_pushnumber(luaState, (lua_Number)value);
		//	lua_setglobal(luaState, variableName.c_str());
		//}
	}

	bool LuaScript::GetGlobalBool(const std::string& variableName) 
		const ULTIMA_NOEXCEPT
	{
		bool ret = false;

		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState,
		//		static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_gettable(luaState, -2);

		//	ret = lua_toboolean(luaState, -1) != 0;
		//	lua_pop(luaState, 2);
		//}
		//else
		//{
		//	lua_getglobal(luaState, variableName.c_str());
		//	ret = lua_toboolean(luaState, -1) != 0;
		//	lua_pop(luaState, 1);
		//}

		return ret;
	}

	void LuaScript::SetGlobalBool(
		const std::string& variableName,
		bool value)
		ULTIMA_NOEXCEPT
	{
		//std::vector<std::string> parts = Utilities::Split(variableName, '[');

		//if (parts.size() > 1)
		//{
		//	parts[1] = Utilities::Remove(parts[1], ']');

		//	lua_getglobal(luaState, parts[0].c_str());

		//	//if (!lua_istable(luaState, -1))
		//	//	return;

		//	if (Utilities::IsUnsignedInt(parts[1]))
		//		lua_pushnumber(luaState,
		//		static_cast<unsigned int>(std::atoi(parts[1].c_str())));
		//	else
		//		lua_pushstring(luaState, parts[1].c_str());

		//	lua_pushboolean(luaState, value);

		//	lua_settable(luaState, -3);
		//}
		//else
		//{
		//	lua_pushboolean(luaState, static_cast<int>(value));
		//	lua_setglobal(luaState, variableName.c_str());
		//}
	}

	void LuaScript::ExecuteFunction(const std::string& functionName)
		ULTIMA_NOEXCEPT
	{
		//lua_getglobal(luaState, functionName.c_str());

		//if (lua_pcall(luaState, 0, 0, 0) != 0)
		//{
		//	std::string error = lua_tostring(luaState, -1);
		//	Utilities::CatchError(error);
		//}
	}

	void LuaScript::Parse(const std::string& text)
		ULTIMA_NOEXCEPT
	{
		//std::vector<std::string> parts = Utilities::Split(text, '=');

		//if (parts.size() > 1) //"a = 5" or "a = b = 5" or "a = b = c"
		//{
		//	for (unsigned int i = 0; i < parts.size(); i++)
		//		parts[i] = Utilities::Remove(parts[i], ' ');

		//	const char* rh = parts[parts.size() - 1].c_str();

		//	if (rh[0] < '0' || rh[0] > '9') //"a = b = c"
		//	{
		//		if (rh[0] != '\"' && rh[0] != '\'')
		//			parts[parts.size() - 1] = GetGlobalString(parts[parts.size() - 1]);
		//		
		//		//rh = parts[parts.size() - 1].c_str();
		//	}

		//	for (int i = parts.size() - 1; i >= 0; i--)
		//	{
		//		//lua_pushstring(luaState, parts[parts.size() - 1].c_str());
		//		//lua_setglobal(luaState, parts[i].c_str());

		//		parts[i] = Utilities::Remove(parts[i], '\'');
		//		parts[i] = Utilities::Remove(parts[i], '\"');
		//		SetGlobalString(parts[i], parts[parts.size() - 1]);
		//	}

		//	//no need to delete rh as it's a const char* created by c_str()
		//}
		//else //it's a function with arguments
		//{
		//	std::vector<std::string> func = Utilities::Split(text, '(');

		//	func[1] = Utilities::Replace(func[1], ';', ' ');
		//	func[1] = Utilities::Replace(func[1], ')', ' ');

		//	func[1] = Utilities::Remove(func[1], ' ');

		//	std::vector<std::string> args = Utilities::Split(func[1], ',');

		//	lua_getglobal(luaState, func[0].c_str());

		//	for (auto& x : args)
		//		lua_pushstring(luaState, x.c_str());

		//	if (lua_pcall(luaState, args.size(), 0, 0) != 0)
		//	{
		//		std::string error = lua_tostring(luaState, -1);
		//		Utilities::CatchError(error);
		//	}
		//}
	}
}