#include "stdafx.h"

#include "LuaAgent.h"

#include "ContentManager.h"

namespace Ultima
{
	LuaAgent::LuaAgent(
		const std::string& scriptFileName,
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount)
		ULTIMA_NOEXCEPT
		: Model(device, meshPath, texturePath, "", "", position, rotation, scale, 
			anisotropicSampleCount),
		LuaScript(scriptFileName)
	{ }

	LuaAgent::LuaAgent(
		const std::string& scriptFileName,
		const Device& device,
		const std::string& meshPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount)
		ULTIMA_NOEXCEPT
		: Model(device, meshPath, position, rotation, scale, anisotropicSampleCount),
		LuaScript(scriptFileName)
	{ }

	LuaAgent::LuaAgent(
		const std::string& scriptFileName,
		const Device& device,
		const std::string& meshPath,
		const std::string& texturePath,
		const std::string& bumpMapPath,
		const std::string& displacementMapPath,
		const Vector3& position,
		const Matrix& rotation,
		float scale,
		unsigned int anisotropicSampleCount)
		ULTIMA_NOEXCEPT
		: Model(device, meshPath, texturePath, bumpMapPath, displacementMapPath, position,
		rotation, scale, anisotropicSampleCount),
		LuaScript(scriptFileName)
	{ }

	LuaAgent::LuaAgent(const LuaAgent& other)
		ULTIMA_NOEXCEPT
		: Model(other),
		LuaScript(other)
	{ }

	LuaAgent& LuaAgent::operator=(const LuaAgent& other)
		ULTIMA_NOEXCEPT
	{
		if (this != &other)
		{
			destructModel();
			destructScript();

			scriptPath = other.GetScriptPath();

			position = other.GetPosition();
			rotation = other.GetRotation();
			scale = other.GetScale();
			material = other.GetMaterial();
			meshPath = other.GetMeshPath();
			texturePath = other.GetTexturePath();
			bumpMapPath = other.GetBumpMapPath();
			displacementMapPath = other.GetDisplacementMapPath();
			device = other.GetDevice();
			anisotropicSampleCount = other.GetAnisotropicSampleCount();

			buildScript();
			buildModel();
		}

		return *this;
	}

	void LuaAgent::destructAgent()
		ULTIMA_NOEXCEPT
	{ }

	LuaAgent::~LuaAgent()
	{
		destructAgent();
	}
}