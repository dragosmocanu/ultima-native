#pragma once

#include "Macros.h"

#include "Device.h"
#include "LuaScript.h"
#include "Model.h"

namespace Ultima
{
	/*!
	Defines an agent that has a 3D representation and can be scripted via Lua.
	*/
	class LuaAgent : public Model, public LuaScript
	{
	public:
		/*!
		Creates a new instance of LuaAgent.
		*/
		ULTIMALUA_API LuaAgent(
			const std::string& scriptFileName,
			const Device& device,
			const std::string& meshPath,
			const std::string& texturePath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of LuaAgent.
		*/
		ULTIMALUA_API LuaAgent(
			const std::string& scriptFileName,
			const Device& device,
			const std::string& meshPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*!
		Creates a new instance of LuaAgent.
		*/
		ULTIMALUA_API LuaAgent(
			const std::string& scriptFileName,
			const Device& device,
			const std::string& meshPath,
			const std::string& texturePath,
			const std::string& bumpMapPath,
			const std::string& displacementMapPath,
			const Vector3& position = Vector3::Zero,
			const Matrix& rotation = Matrix::Identity,
			float scale = 1.0f,
			unsigned int anisotropicSampleCount = 4)
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of LuaAgent.
		*/
		ULTIMALUA_API LuaAgent(const LuaAgent& other)
			ULTIMA_NOEXCEPT;

		ULTIMALUA_API LuaAgent& operator=(const LuaAgent& other)
			ULTIMA_NOEXCEPT;

		ULTIMALUA_API virtual ~LuaAgent()
			ULTIMA_NOEXCEPT;

	protected:
		/*!
		Destroys the agent. Called by the actual destructor and the copy/move constructors.
		*/
		ULTIMALUA_API virtual void destructAgent()
			ULTIMA_NOEXCEPT;
	};
}