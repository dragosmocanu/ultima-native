#pragma once

#include "Macros.h"

#include "IScriptable.h"

////Lua API loading
//#include "lua.hpp"
//#pragma comment(lib, "lua5.1.lib")

namespace Ultima
{
	/*!
	Defines a Lua script that loades code from a .lua file, can execute functions, get and set global variables.
	*/
	class LuaScript : public IScriptable
	{
	public:
		/*!
		Creates a new instance of LuaScript.
		*/
		ULTIMALUA_API LuaScript(const std::string& scriptPath)
			ULTIMA_NOEXCEPT;

		/*!
		Copies an instance of LuaScript.
		*/
		ULTIMALUA_API LuaScript(const LuaScript& other)
			ULTIMA_NOEXCEPT;

		ULTIMALUA_API LuaScript& operator=(const LuaScript& other)
			ULTIMA_NOEXCEPT;

		ULTIMALUA_API virtual ~LuaScript()
			ULTIMA_NOEXCEPT;

		/*!
		Gets a global Lua string variable.
		*/
		ULTIMALUA_API virtual std::string GetGlobalString(const std::string& variableName)
			const ULTIMA_NOEXCEPT;

		/*!
		Sets a global Lua string variable.
		*/
		ULTIMALUA_API virtual void SetGlobalString(
			const std::string& variableName,
			const std::string& value)
			ULTIMA_NOEXCEPT;

		/*!
		Gets a global Lua number variable.
		*/
		ULTIMALUA_API virtual double GetGlobalNumber(const std::string& variableName)
			const ULTIMA_NOEXCEPT;

		/*!
		Sets a global Lua number variable
		*/
		ULTIMALUA_API virtual void SetGlobalNumber(
			const std::string& variableName,
			double value)
			ULTIMA_NOEXCEPT;

		/*!
		Gets a global Lua bool variable.
		*/
		ULTIMALUA_API virtual bool GetGlobalBool(const std::string& variableName)
			const ULTIMA_NOEXCEPT;

		/*!
		Sets a global Lua bool value.
		*/
		ULTIMALUA_API virtual void SetGlobalBool(
			const std::string& variableName,
			bool value)
			ULTIMA_NOEXCEPT;

		/*!
		Executes a Lua function with no arguments.
		*/
		ULTIMALUA_API virtual void ExecuteFunction(const std::string& functionName)
			ULTIMA_NOEXCEPT;

		/*!
		Parses and executes a given string.
		*/
		ULTIMALUA_API virtual void Parse(const std::string& text)
			ULTIMA_NOEXCEPT;

		/*!
		Gets the path of the script file.
		*/
		ULTIMALUA_API virtual std::string GetScriptPath()
			const ULTIMA_NOEXCEPT;

	protected:
		/*!
		Builds the script.
		*/
		ULTIMALUA_API virtual void buildScript()
			ULTIMA_NOEXCEPT;

		/*!
		Destroys the script. Called by the actual destructor and the copy/move constructors.
		*/
		ULTIMALUA_API virtual void destructScript()
			ULTIMA_NOEXCEPT;

		//lua_State* luaState;
		std::string scriptPath;
	};
}